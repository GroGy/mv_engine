//
// Created by Matty on 2021-10-14.
//

#include "../include/render/common/FrameBuffer.h"

namespace MV {
    void FrameBuffer::SetSize(uint32_t width, uint32_t height) {
        m_pWidth = width;
        m_pHeight = height;
    }

    void FrameBuffer::MarkForDeletion() {
        m_pMarkedForDeletion = true;
    }

    bool FrameBuffer::MarkedForDeletion() const {
        return m_pMarkedForDeletion;
    }

    FrameBuffer::FrameBuffer(uint32_t width, uint32_t height) : m_pHeight(height), m_pWidth(width), m_pValid(true) {
    }

}
