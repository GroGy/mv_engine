//
// Created by Matty on 2021-10-03.
//

#ifdef VK_BACKEND_SUPPORT

#include <cassert>
#include "../../include/render/vulkan/VulkanUtil.h"
#include "../../include/common/ExceptionCommon.h"
#include "../../include/common/EngineCommon.h"

namespace MV {
    void VulkanUtil::CheckVkResult(VkResult res) {

        //assert(res == VK_SUCCESS);
        if (res != VK_SUCCESS) {
            throw RuntimeException(resToString(res));
        }
    }

    MV::string VulkanUtil::resToString(VkResult res) {
        switch (res) {
            case VK_SUCCESS : { return "VK_SUCCESS";}
            case VK_NOT_READY : { return "VK_NOT_READY";}
            case VK_TIMEOUT : { return "VK_TIMEOUT";}
            case VK_EVENT_SET : { return "VK_EVENT_SET";}
            case VK_EVENT_RESET : { return "VK_EVENT_RESET";}
            case VK_INCOMPLETE : { return "VK_INCOMPLETE";}
            case VK_ERROR_OUT_OF_HOST_MEMORY : { return "VK_ERROR_OUT_OF_HOST_MEMORY";}
            case VK_ERROR_OUT_OF_DEVICE_MEMORY : { return "VK_ERROR_OUT_OF_DEVICE_MEMORY";}
            case VK_ERROR_INITIALIZATION_FAILED : { return "VK_ERROR_INITIALIZATION_FAILED";}
            case VK_ERROR_DEVICE_LOST : { return "VK_ERROR_DEVICE_LOST";}
            case VK_ERROR_MEMORY_MAP_FAILED : { return "VK_ERROR_MEMORY_MAP_FAILED";}
            case VK_ERROR_LAYER_NOT_PRESENT : { return "VK_ERROR_LAYER_NOT_PRESENT";}
            case VK_ERROR_EXTENSION_NOT_PRESENT : { return "VK_ERROR_EXTENSION_NOT_PRESENT";}
            case VK_ERROR_FEATURE_NOT_PRESENT : { return "VK_ERROR_FEATURE_NOT_PRESENT";}
            case VK_ERROR_INCOMPATIBLE_DRIVER : { return "VK_ERROR_INCOMPATIBLE_DRIVER";}
            case VK_ERROR_TOO_MANY_OBJECTS : { return "VK_ERROR_TOO_MANY_OBJECTS";}
            case VK_ERROR_FORMAT_NOT_SUPPORTED : { return "VK_ERROR_FORMAT_NOT_SUPPORTED";}
            case VK_ERROR_FRAGMENTED_POOL : { return "VK_ERROR_FRAGMENTED_POOL";}
            case VK_ERROR_UNKNOWN : { return "VK_ERROR_UNKNOWN";}
            case VK_ERROR_OUT_OF_POOL_MEMORY : { return "VK_ERROR_OUT_OF_POOL_MEMORY";}
            case VK_ERROR_INVALID_EXTERNAL_HANDLE : { return "VK_ERROR_INVALID_EXTERNAL_HANDLE";}
            case VK_ERROR_FRAGMENTATION : { return "VK_ERROR_FRAGMENTATION";}
            case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS : { return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";}
            case VK_ERROR_SURFACE_LOST_KHR : { return "VK_ERROR_SURFACE_LOST_KHR";}
            case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR : { return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";}
            case VK_SUBOPTIMAL_KHR : { return "VK_SUBOPTIMAL_KHR";}
            case VK_ERROR_OUT_OF_DATE_KHR : { return "VK_ERROR_OUT_OF_DATE_KHR";}
            case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR : { return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";}
            case VK_ERROR_VALIDATION_FAILED_EXT : { return "VK_ERROR_VALIDATION_FAILED_EXT";}
            case VK_ERROR_INVALID_SHADER_NV : { return "VK_ERROR_INVALID_SHADER_NV";}
            case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT : { return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";}
            case VK_ERROR_NOT_PERMITTED_EXT : { return "VK_ERROR_NOT_PERMITTED_EXT";}
            case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT : { return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";}
            case VK_THREAD_IDLE_KHR : { return "VK_THREAD_IDLE_KHR";}
            case VK_THREAD_DONE_KHR : { return "VK_THREAD_DONE_KHR";}
            case VK_OPERATION_DEFERRED_KHR : { return "VK_OPERATION_DEFERRED_KHR";}
            case VK_OPERATION_NOT_DEFERRED_KHR : { return "VK_OPERATION_NOT_DEFERRED_KHR";}
            case VK_PIPELINE_COMPILE_REQUIRED_EXT : { return "VK_PIPELINE_COMPILE_REQUIRED_EXT";}
            case VK_RESULT_MAX_ENUM:
                return "WTF";
        }

        return "WTF";
    }

    void
    VulkanUtil::LoadDataToAllocation(VmaAllocator allocator, VmaAllocation allocation, VmaAllocationInfo allocInfo,const void *data, size_t dataSize) {
        VkMemoryPropertyFlags memFlags;
        vmaGetMemoryTypeProperties(allocator, allocInfo.memoryType, &memFlags);
        if ((memFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0) {
            void *mappedData;
            vmaMapMemory(allocator, allocation, &mappedData);
            memcpy(mappedData, data, dataSize);
            vmaUnmapMemory(allocator, allocation);
        } else {
            GetLogger()->Render_LogError("Tried to upload data to unmappable buffer.");
            return;
            // TODO : Do this from other buffer, its doable but idk about speed, will try
            // Allocation ended up in non-mappable memory.
            // You need to create CPU-side buffer in VMA_MEMORY_USAGE_CPU_ONLY and make a transfer.
        }
    }
}

#endif