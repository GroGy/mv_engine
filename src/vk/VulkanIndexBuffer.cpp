//
// Created by Matty on 2021-10-17.
//

#include "../../include/render/vulkan/VulkanIndexBuffer.h"

namespace MV {

    void VulkanIndexBuffer::Bind(FrameContext &context) {
        vkCmdBindIndexBuffer(context.m_FrameCommandBuffer,m_Buffer,0,VK_INDEX_TYPE_UINT32);
    }

    void VulkanIndexBuffer::Upload(const MV::rc<StagingBuffer> &staging, size_t dataSize) {
        if(auto buf = MV::dynamic_pointer_cast<VulkanBufferImpl>(staging)) {
            copyFrom(*buf, dataSize);
        }
    }

    void VulkanIndexBuffer::Init() {
        VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        bufferInfo.size = m_pSize;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;

        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        allocate(bufferInfo,allocInfo);
    }

    void VulkanIndexBuffer::Cleanup() {
        markForDealloc();
    }

    VulkanIndexBuffer::VulkanIndexBuffer(size_t size) : IndexBuffer(size) {

    }
}