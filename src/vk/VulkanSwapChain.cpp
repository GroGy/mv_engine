//
// Created by Matty on 2021-10-04.
//

#ifdef VK_BACKEND_SUPPORT

#include "../../include/render/vulkan/VulkanSwapChain.h"
#include "../../include/render/vulkan/VulkanSwapChainSupportDetails.h"
#include "../../include/render/vulkan/VulkanQueueFamilies.h"
#include "../../include/render/vulkan/VulkanUtil.h"
#include "../../include/common/EngineCommon.h"
#include "../../include/render/vulkan/VulkanRenderer.h"

namespace MV {
    void VulkanSwapChain::Init(VulkanRendererData &rendererData, const std::unique_ptr<Window> &window) {
        GetLogger()->Render_LogDebug("Creating swapchain...");
        auto swapChainSupport = VulkanSwapChainSupportDetails::GetSwapChainSupport(rendererData.m_PhysicalDevice,
                                                                                   rendererData.m_Surface);

        rendererData.m_SurfaceFormat = swapChainSupport.ChooseSwapSurfaceFormat();
        rendererData.m_SurfacePresentMode = swapChainSupport.ChooseSwapPresentMode();
        rendererData.m_SurfaceExtent = swapChainSupport.ChooseSwapExtent(window);

        uint32_t imageCount = swapChainSupport.m_Capabilities.minImageCount + 2;
        if (swapChainSupport.m_Capabilities.maxImageCount > 0 &&
            imageCount > swapChainSupport.m_Capabilities.maxImageCount) {
            imageCount = swapChainSupport.m_Capabilities.maxImageCount;
        }

        VkSwapchainCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = rendererData.m_Surface;
        createInfo.minImageCount = imageCount;
        createInfo.imageFormat = rendererData.m_SurfaceFormat.format;
        createInfo.imageColorSpace = rendererData.m_SurfaceFormat.colorSpace;
        createInfo.imageExtent = rendererData.m_SurfaceExtent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        QueueFamilies indices = QueueFamilies::FindSupportedQueueFamilies(rendererData.m_PhysicalDevice,
                                                                          rendererData.m_Surface);
        uint32_t queueFamilyIndices[] = {indices.m_GraphicsFamily.value(), indices.m_PresentFamily.value()};

        if (indices.m_GraphicsFamily != indices.m_PresentFamily) {
            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            createInfo.queueFamilyIndexCount = 2;
            createInfo.pQueueFamilyIndices = queueFamilyIndices;
        } else {
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            createInfo.queueFamilyIndexCount = 0; // Optional
            createInfo.pQueueFamilyIndices = nullptr; // Optional
        }

        createInfo.preTransform = swapChainSupport.m_Capabilities.currentTransform;
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = rendererData.m_SurfacePresentMode;
        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = VK_NULL_HANDLE;

        VulkanUtil::CheckVkResult(vkCreateSwapchainKHR(rendererData.m_Device, &createInfo, nullptr, &m_SwapChain));

        m_SwapChainImageFormat = rendererData.m_SurfaceFormat.format;
        m_SwapChainExtent = rendererData.m_SurfaceExtent;

        vkGetSwapchainImagesKHR(rendererData.m_Device, m_SwapChain, &imageCount, nullptr);
        m_SwapChainImages.resize(imageCount);
        vkGetSwapchainImagesKHR(rendererData.m_Device, m_SwapChain, &imageCount, m_SwapChainImages.data());

        initImageViews(rendererData);

        GetLogger()->Render_LogDebug("Successfully created swapchain.");
    }

    void VulkanSwapChain::Cleanup(VulkanRendererData &rendererData) {
        GetLogger()->Render_LogDebug("Destroying swapchain...");
        cleanupImageViews(rendererData);
        vkDestroySwapchainKHR(rendererData.m_Device, m_SwapChain, nullptr);
        GetLogger()->Render_LogDebug("Successfully destroyed swapchain.");
    }

    void VulkanSwapChain::initImageViews(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Creating image views...");
        const auto swapChainImageCount = m_SwapChainImages.size();
        m_ImageViews.resize(swapChainImageCount);

        for (size_t i = 0; i < swapChainImageCount; i++)
            createImage(vulkanRendererData, i);

        GetLogger()->Render_LogDebug("Successfully created image views.");
    }

    void VulkanSwapChain::createImage(VulkanRendererData &vulkanRendererData, size_t i) {
        VkImageViewCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = m_SwapChainImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = m_SwapChainImageFormat;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        VulkanUtil::CheckVkResult(
                vkCreateImageView(vulkanRendererData.m_Device, &createInfo, nullptr, &m_ImageViews[i]));
    }

    void VulkanSwapChain::cleanupImageViews(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Destroying image views...");

        for (auto &m_ImageView: m_ImageViews)
            vkDestroyImageView(vulkanRendererData.m_Device, m_ImageView, nullptr);

        GetLogger()->Render_LogDebug("Successfully destroyed image views.");
    }
}

#endif