//
// Created by Matty on 2021-10-01.
//

#ifdef VK_BACKEND_SUPPORT

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <valarray>
#include "render/vulkan/VulkanRenderer.h"
#include "common/EngineCommon.h"
#include "render/vulkan/VulkanUtil.h"
#include "common/ExceptionCommon.h"
#include "render/vulkan/VulkanVertexBuffer.h"
#include "render/vulkan/VulkanIndexBuffer.h"
#include "render/vulkan/VulkanUniformBuffer.h"
#include "profiler/Profiler.h"
#include <EASTL/sort.h>

#if MV_VULKAN_RENDERER_VERBOSE_STAT
#define MV_PROFILE_RENDERER_FUNCTION(Name) MV_PROFILE_FUNCTION(Name)
#define MV_PROFILE_RENDERER_TAG(Name, Value) MV_PROFILE_TAG(Name, Value)
#else
#define MV_PROFILE_RENDERER_FUNCTION(Name)
#define MV_PROFILE_RENDERER_TAG(Name, Value)
#endif

namespace MV {
    void VulkanRenderer::Init() {
        GetLogger()->Render_LogInfo("Loading VULKAN renderer...");

        initBaseInstance();
        if (m_pConfig.m_DebugValidationLayers)
            initDebuggingModules();

        initSurface();
        initPhysicalDevice();
        initLogicalDevice();
        initAllocator();
        m_pFragileData.Init(m_pData, m_pCommonConfig.m_Window);
        initSyncElements();

        initLimits();

        GetLogger()->Render_LogInfo("Loaded VULKAN renderer");
    }

    FrameContext VulkanRenderer::PrepareRender() {
        MV_PROFILE_FUNCTION("VulkanRenderer::PrepareRender")
        FrameContext res{};

        //res.m_DrawQueue.reserve();

        vkWaitForFences(m_pData.m_Device, 1, &m_pData.m_InFlightFences[m_pCurrentFence], VK_TRUE, UINT64_MAX);

        VkResult result = vkAcquireNextImageKHR(m_pData.m_Device, m_pFragileData.m_SwapChain.m_SwapChain, UINT64_MAX,
                                                m_pData.m_ImageAvailableSemaphores[m_pCurrentFence], VK_NULL_HANDLE,
                                                &m_pCurrentImageIndex);

        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
            rebuildFragileData();
            res.m_Invalid = true;
            return res;
        } else {
            VKCheck(result);
        }

        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = 0; // Optional
        beginInfo.pInheritanceInfo = nullptr; // Optional

        VKCheck(vkBeginCommandBuffer(m_pFragileData.m_CommandBuffers.at(m_pCurrentImageIndex), &beginInfo));

        res.m_FrameCommandBuffer = m_pFragileData.m_CommandBuffers.at(m_pCurrentImageIndex);
        res.m_SwapImageIndex = m_pCurrentImageIndex;
        res.m_SwapChain = &m_pFragileData.m_SwapChain;


        if (m_pCommonConfig.m_Editor) {
            m_pFragileData.m_EditorRenderPass->Bind(res);
            return res;
        }
        m_pFragileData.m_MainRenderPass->Bind(res);

        return res;
    }

    void VulkanRenderer::Render(FrameContext &context) {
        MV_PROFILE_FUNCTION("VulkanRenderer::Render")

        resetBatchBuffers();

        for (auto &&cmd: context.m_DrawQueue) {
            rc <RenderingResource> rr;
            if (GetRenderingResource(cmd.first, rr)) {
                renderCommandFromQueue(context, cmd, rr);
            }
        }
    }

    void VulkanRenderer::renderCommandFromQueue(FrameContext &context,
                                                const pair<const uint64_t, PipelineMaterials> &command,
                                                const rc <RenderingResource> &pipelineResource) {
        if (auto pipeline = eastl::dynamic_pointer_cast<VulkanPipeline>(pipelineResource)) {
            MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline");
            MV_PROFILE_RENDERER_TAG("Pipeline ID", command.first);
            pipeline->ResetBatchCounters();
            if (m_pDrawWireframe) {
                pipeline->BindWireframe(context);
            } else {
                pipeline->Bind(context);
            }
            for (auto &&subCmd: command.second.m_Commands) {
                renderSubcommand(context, pipeline, subCmd);
            }
        }
    }

    void VulkanRenderer::renderSubcommand(FrameContext &context, eastl::shared_ptr<VulkanPipeline> &pipeline,
                                          const pair<const MaterialInput, MaterialDrawData> &subCommand) {
        if (subCommand.second.m_Instanced && subCommand.second.m_InstanceCount == 0) return;

        MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command");

        MV::vector<rc < VertexBuffer>>
        vertexBuffers;
        vertexBuffers.resize(subCommand.second.m_VertexBuffers.size());

        bool allBuffersValid = true;
        uint64_t biggestBuffer = 65536ULL;
        const auto &vertexInputs = pipeline->m_pBlueprint.GetVertexInputDescription();

        {
            MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::SetupVertexBuffers")
            for (uint32_t i = 0; i < subCommand.second.m_VertexBuffers.size(); i++) {
                const auto &dataBuffer = subCommand.second.m_VertexBuffers[i];

                if (dataBuffer.empty()) continue;

                if (dataBuffer.size() > biggestBuffer) biggestBuffer = dataBuffer.size();

                auto vertexBuffer = getBatchVertexBuffer((std::max) (dataBuffer.size(), 65536ULL),
                                                         vertexInputs[i].stride);

                auto validityCheck = eastl::dynamic_pointer_cast<VulkanBufferImpl>(vertexBuffer);

                vertexBuffers[i] = vertexBuffer;

                if (!validityCheck) allBuffersValid = false;
            }
        }

        auto indexBuffer = getBatchIndexBuffer(
                (std::max) (subCommand.second.m_Indices.size() * sizeof(uint32_t), 65536ULL));
        auto vkIndexBuf = eastl::dynamic_pointer_cast<VulkanBufferImpl>(indexBuffer);

        if (vkIndexBuf && allBuffersValid) {
            auto stagingBuffer = CreateStagingBuffer(biggestBuffer);

            {
                MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::UploadVertexData")
                for (uint32_t i = 0; i < subCommand.second.m_VertexBuffers.size(); i++) {
                    const auto &dataBuffer = subCommand.second.m_VertexBuffers[i];
                    auto &vertexBuffer = vertexBuffers[i];
                    if (!vertexBuffer) continue;

                    stagingBuffer->Upload(dataBuffer.data(), dataBuffer.size());
                    vertexBuffer->Upload(stagingBuffer, dataBuffer.size());

                    vertexBuffer->Bind(context, i);
                }
            }


            {
                MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::UploadIndexData")
                stagingBuffer->Upload(subCommand.second.m_Indices.data(),
                                      subCommand.second.m_Indices.size() * sizeof(uint32_t));
                indexBuffer->Upload(stagingBuffer, subCommand.second.m_Indices.size() * sizeof(uint32_t));
            }
            indexBuffer->Bind(context);

            vector <eastl::reference_wrapper<UniformDescription>> uniforms{};

            if (!subCommand.first.m_UniformBufferName.empty()) {
                MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::UploadUniformBuffer")
                auto &descriptor = pipeline->GetBatchUniform(subCommand.first.m_UniformBufferName);
                pipeline->PreBindUniform(context, descriptor, subCommand.first.m_UniformBuffer.data(),
                                         subCommand.first.m_UniformBuffer.size(), true);
                uniforms.emplace_back(descriptor);
            }

            if (!subCommand.first.m_UniformTextureName.empty()) {
                MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::UploadTexture")
                rc <RenderingResource> tex;
                bool texLoaded = GetRenderingResource(subCommand.first.m_UniformTextures[0], tex);

                if (!texLoaded) {
                    GetEngine()->LoadResource(subCommand.first.m_UniformTextures[0]);
                }

                if (texLoaded && tex->IsDataLoaded()) {
                    auto &descriptor = pipeline->GetBatchUniform(subCommand.first.m_UniformTextureName);
                    pipeline->PreBindUniform(context, descriptor,
                                             eastl::dynamic_pointer_cast<Texture>(tex), true);
                    uniforms.emplace_back(descriptor);
                }
            }

            eastl::sort(uniforms.begin(), uniforms.end(), [](const eastl::reference_wrapper<UniformDescription> &a,
                                                             const eastl::reference_wrapper<UniformDescription> &b) {
                return a.get().binding < b.get().binding;
            });

            if (!uniforms.empty()) {
                MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::BindUniforms")
                pipeline->BindUniforms(context, uniforms);
            }

            glm::mat4 mvp = context.m_ProjMatrix * context.m_ViewMatrix * subCommand.second.m_ModelMatrix;

            pipeline->BindPushConstant(context, pipeline->GetMVPPushConstant(), &mvp, sizeof(mvp));

            if (subCommand.second.m_Instanced) {
                MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::Draw")
                vkCmdDrawIndexed(context.m_FrameCommandBuffer, subCommand.second.m_Indices.size(),
                                 subCommand.second.m_InstanceCount, 0, 0, 0);
            } else {
                MV_PROFILE_RENDERER_FUNCTION("VulkanRenderer::Render::Pipeline::Command::Draw")
                vkCmdDrawIndexed(context.m_FrameCommandBuffer, subCommand.second.m_Indices.size(), 1, 0, 0, 0);
            }
        }
    }

    void VulkanRenderer::Present(FrameContext &context) {
        MV_PROFILE_FUNCTION("VulkanRenderer::Present")
        if (context.m_IsRenderPassActive) vkCmdEndRenderPass(context.m_FrameCommandBuffer);
        vkWaitForFences(m_pData.m_Device, 1, &m_pData.m_InFlightFences[m_pCurrentFence], VK_TRUE, UINT64_MAX);
        VKCheck(vkEndCommandBuffer(m_pFragileData.m_CommandBuffers.at(m_pCurrentImageIndex)))

        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (m_pFragileData.m_ImagesInFlight[m_pCurrentImageIndex] != VK_NULL_HANDLE) {
            MV_PROFILE_FUNCTION("VulkanRenderer::Present::WaitForFences")
            vkWaitForFences(m_pData.m_Device, 1, &m_pFragileData.m_ImagesInFlight[m_pCurrentImageIndex], VK_TRUE,
                            100000000);
        }
        // Mark the image as now being in use by this frame
        m_pFragileData.m_ImagesInFlight[m_pCurrentImageIndex] = m_pData.m_InFlightFences[m_pCurrentFence];

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = {m_pData.m_ImageAvailableSemaphores[m_pCurrentFence]};
        VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;

        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &m_pFragileData.m_CommandBuffers[m_pCurrentImageIndex];

        VkSemaphore signalSemaphores[] = {m_pData.m_RenderFinishedSemaphores[m_pCurrentFence]};
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        vkResetFences(m_pData.m_Device, 1, &m_pData.m_InFlightFences[m_pCurrentFence]);

        VKCheck(vkQueueSubmit(m_pData.m_GraphicsQueue, 1, &submitInfo, m_pData.m_InFlightFences[m_pCurrentFence]));

        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;

        VkSwapchainKHR swapChains[] = {m_pFragileData.m_SwapChain.m_SwapChain};
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &m_pCurrentImageIndex;
        presentInfo.pResults = nullptr; // Optional
        VkResult result2 = vkQueuePresentKHR(m_pData.m_PresentQueue, &presentInfo);

        if (result2 == VK_ERROR_OUT_OF_DATE_KHR || result2 == VK_SUBOPTIMAL_KHR) {
            rebuildFragileData();
        } else {
            VKCheck(result2);
        }

        m_pCurrentFence = (m_pCurrentFence + 1) % FRAMES_IN_FLIGHT;
    }

    void VulkanRenderer::Cleanup(const std::function<bool()> &moduleCleanupCallback) {
        GetLogger()->Render_LogInfo("Cleaning Vulkan resources...");
        if (m_pData.m_Device) vkDeviceWaitIdle(m_pData.m_Device);

        for (auto &&sampler: m_pData.m_TextureSamplers) {
            if (sampler.second) vkDestroySampler(m_pData.m_Device, sampler.second, nullptr);
        }

        m_pBufferStorage.ClearEverything();

        if (!moduleCleanupCallback()) { GetLogger()->Render_LogError("Failed to render cleanup all modules."); };

        if (m_pConfig.m_DebugValidationLayers && m_pData.m_DebugMessenger) cleanupDebugMessenger();

        if (m_pImguiDescriptorPool) vkDestroyDescriptorPool(m_pData.m_Device, m_pImguiDescriptorPool, nullptr);

        for (size_t i = 0; i < FRAMES_IN_FLIGHT; i++) {
            vkDestroySemaphore(m_pData.m_Device, m_pData.m_RenderFinishedSemaphores[i], nullptr);
            vkDestroySemaphore(m_pData.m_Device, m_pData.m_ImageAvailableSemaphores[i], nullptr);
            vkDestroyFence(m_pData.m_Device, m_pData.m_InFlightFences[i], nullptr);
        }

        m_pFragileData.Cleanup(m_pData);

        if (m_pData.m_CommandPool) vkDestroyCommandPool(m_pData.m_Device, m_pData.m_CommandPool, nullptr);
        if (m_pData.m_UploadCommandPool) vkDestroyCommandPool(m_pData.m_Device, m_pData.m_UploadCommandPool, nullptr);

        if (m_pData.m_Surface) vkDestroySurfaceKHR(m_pData.m_Instance, m_pData.m_Surface, nullptr);

        if (m_pData.m_Allocator) vmaDestroyAllocator(m_pData.m_Allocator);

        if (m_pData.m_Device) vkDestroyDevice(m_pData.m_Device, nullptr);

        if (m_pData.m_Instance) vkDestroyInstance(m_pData.m_Instance, nullptr);

        GetLogger()->Render_LogInfo("Finished cleaning Vulkan resources");
    }

    VulkanRenderer::VulkanRenderer(RendererConfigCommon commonConfig, VulkanRendererConfig config)
            : MV::Renderer(MV::move(commonConfig)), m_pConfig(config) {

    }

    void VulkanRenderer::initBaseInstance() {
        VkApplicationInfo appInfo{};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pApplicationName = m_pCommonConfig.m_ApplicationName.c_str();
        appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.pEngineName = m_pCommonConfig.m_ApplicationName.c_str();
        appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);

#if VK_VERSION_1_2 == 1
        appInfo.apiVersion = VK_API_VERSION_1_2;
#elif VK_VERSION_1_1 == 1
        appInfo.apiVersion = VK_API_VERSION_1_1;
#else
        appInfo.apiVersion = VK_API_VERSION_1_0;
#endif

        VkInstanceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &appInfo;

        auto extensionsR = getRequiredExtensions();

        createInfo.enabledExtensionCount = static_cast<uint32_t>(extensionsR.size());
        createInfo.ppEnabledExtensionNames = extensionsR.data();


#ifndef NDEBUG
        if (m_pConfig.m_DebugValidationLayers) {
            createInfo.enabledLayerCount = static_cast<uint32_t>(m_pValidationLayers.size());
            createInfo.ppEnabledLayerNames = m_pValidationLayers.data();
        } else {
            createInfo.enabledLayerCount = 0;
        }
#else
        createInfo.enabledLayerCount = 0;
#endif
        GetLogger()->Render_LogDebug("Creating base instance...");
        auto res = vkCreateInstance(&createInfo, nullptr, &m_pData.m_Instance);
        VulkanUtil::CheckVkResult(res);

        uint32_t layerCount;
        vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
        MV::vector<VkLayerProperties> availableLayers(layerCount);
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        //GetLogger()->Render_LogDebug("Available layers:");
        //for (const VkLayerProperties& layerProperties : availableLayers)
        //{
        //    GetLogger()->Render_LogDebug(layerProperties.layerName);
        //}

        GetLogger()->Render_LogDebug("Created base instance");
    }

    MV::vector<const char *> VulkanRenderer::getRequiredExtensions() {
        uint32_t extensionCount = 0;
        const char **glfwExtensions;
        glfwExtensions = glfwGetRequiredInstanceExtensions(&extensionCount);

        MV::vector<const char *> extensions(glfwExtensions, glfwExtensions + extensionCount);
#ifndef NDEBUG
        if (m_pConfig.m_DebugValidationLayers)
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
        return extensions;
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
            VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
            VkDebugUtilsMessageTypeFlagsEXT messageType,
            const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
            void *pUserData) {

        if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
            MV::GetLogger()->Render_LogError("Validation layer :\n{}", pCallbackData->pMessage);
        }

        return VK_FALSE;
    }

    void VulkanRenderer::initDebuggingModules() {
        GetLogger()->Render_LogDebug("Initializing Vulkan debug modules...");
        VkDebugUtilsMessengerCreateInfoEXT createInfoDBG{};
        createInfoDBG.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        createInfoDBG.messageSeverity =
                VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        createInfoDBG.messageType =
                VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        createInfoDBG.pfnUserCallback = debugCallback;
        createInfoDBG.pUserData = nullptr; // Optional

        VulkanUtil::CheckVkResult(initDebugMessenger(&createInfoDBG));

        GetLogger()->Render_LogDebug("Finished Initialization of debug modules");
    }

    VkResult VulkanRenderer::initDebugMessenger(const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo) {
        auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(m_pData.m_Instance,
                                                                               "vkCreateDebugUtilsMessengerEXT");
        if (func != nullptr) {
            return func(m_pData.m_Instance, pCreateInfo, nullptr, &m_pData.m_DebugMessenger);
        } else {
            return VK_ERROR_EXTENSION_NOT_PRESENT;
        }
    }

    void VulkanRenderer::cleanupDebugMessenger() {
        auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(m_pData.m_Instance,
                                                                                "vkDestroyDebugUtilsMessengerEXT");
        if (func != nullptr) {
            func(m_pData.m_Instance, m_pData.m_DebugMessenger, nullptr);
        }
    }

    void VulkanRenderer::initSurface() {
        GetLogger()->Render_LogDebug("Creating Vulkan surface...");
        VulkanUtil::CheckVkResult(
                glfwCreateWindowSurface(m_pData.m_Instance, (GLFWwindow *) m_pCommonConfig.m_Window->GetContext(),
                                        nullptr, &m_pData.m_Surface));
        GetLogger()->Render_LogDebug("Successfully created Vulkan surface.");
    }

    void VulkanRenderer::initPhysicalDevice() {
        GetLogger()->Render_LogDebug("Looking for suitable Vulkan graphics card...");

        uint32_t deviceCount = 0;
        vkEnumeratePhysicalDevices(m_pData.m_Instance, &deviceCount, nullptr);

        MV::vector<VkPhysicalDevice> physicalDevices(deviceCount);
        vkEnumeratePhysicalDevices(m_pData.m_Instance, &deviceCount, physicalDevices.data());

        if (deviceCount == 0) {
            throw MV::RuntimeException("No GPU with Vulkan support found.");
        }

        VkPhysicalDevice selectedGPU;
        float selectedGPUValue = 0.0f;
        MV::string selectedGPUName;

        float offset = 0.0f;

        for (const auto &device: physicalDevices) {
            if (isGPUSuitable(device)) {
                VkPhysicalDeviceProperties deviceProperties{};
                vkGetPhysicalDeviceProperties(device, &deviceProperties);

                float gpuValue = 1.0f + offset; //TODO: Calculate value

                offset += 1.0f;

                if (gpuValue > selectedGPUValue) {
                    selectedGPU = device;
                    selectedGPUValue = gpuValue;
                    selectedGPUName = deviceProperties.deviceName;
                }
            }
        }


        m_pData.m_PhysicalDevice = selectedGPU;
        GetLogger()->Render_LogDebug("Selected GPU : {}", selectedGPUName);

        if (m_pData.m_PhysicalDevice == VK_NULL_HANDLE) {
            throw MV::RuntimeException("Failed to find a suitable GPU!");
        }
    }

    bool VulkanRenderer::isGPUSuitable(VkPhysicalDevice device) {
        QueueFamilies indices = QueueFamilies::FindSupportedQueueFamilies(device, m_pData.m_Surface);

        auto extensionsSupported = hasGPUExtensionSupport(device);

        bool swapChainSupported = false;

        if (extensionsSupported) {
            VulkanSwapChainSupportDetails swapChainSupport = VulkanSwapChainSupportDetails::GetSwapChainSupport(device,
                                                                                                                m_pData.m_Surface);
            swapChainSupported = !swapChainSupport.m_Formats.empty() && !swapChainSupport.m_PresentModes.empty();
        }

        VkPhysicalDeviceFeatures supportedFeatures;
        vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

        return indices.IsComplete() && extensionsSupported && swapChainSupported &&
               supportedFeatures.samplerAnisotropy && supportedFeatures.fillModeNonSolid;
    }

    bool VulkanRenderer::hasGPUExtensionSupport(VkPhysicalDevice device) {
        uint32_t extensionCount;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

        MV::vector<VkExtensionProperties> availableExtensions(extensionCount);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

        MV::unordered_set<MV::string> requiredExtensions(m_pRequiredExtensions.begin(), m_pRequiredExtensions.end());

        for (const auto &extension: availableExtensions) {
            requiredExtensions.erase(extension.extensionName);
        }

        return requiredExtensions.empty();
    }

    void VulkanRenderer::initLogicalDevice() {
        GetLogger()->Render_LogDebug("Initializing logical device...");

        QueueFamilies indices = QueueFamilies::FindSupportedQueueFamilies(m_pData.m_PhysicalDevice, m_pData.m_Surface);

        MV::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
        MV::unordered_set<uint32_t> uniqueQueueFamilies = {
                indices.m_GraphicsFamily.value(), indices.m_PresentFamily.value()
        };

        float queuePriority = 1.0f;

        for (uint32_t queueFamily: uniqueQueueFamilies) {
            VkDeviceQueueCreateInfo queueCreateInfo{};
            queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.queueFamilyIndex = indices.m_GraphicsFamily.value();
            queueCreateInfo.queueCount = 1;
            queueCreateInfo.pQueuePriorities = &queuePriority;
            queueCreateInfos.push_back(queueCreateInfo);
        }

        VkPhysicalDeviceFeatures deviceFeatures{};
        deviceFeatures.samplerAnisotropy = VK_TRUE;
        deviceFeatures.fillModeNonSolid = VK_TRUE;
        deviceFeatures.shaderStorageImageExtendedFormats = VK_TRUE;

        VkDeviceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        createInfo.pQueueCreateInfos = queueCreateInfos.data();
        createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
        createInfo.pEnabledFeatures = &deviceFeatures;
        createInfo.enabledExtensionCount = static_cast<uint32_t>(m_pRequiredExtensions.size());
        createInfo.ppEnabledExtensionNames = m_pRequiredExtensions.data();

#ifndef NDEBUG
        if (m_pConfig.m_DebugValidationLayers) {
            createInfo.enabledLayerCount = static_cast<uint32_t>(m_pValidationLayers.size());
            createInfo.ppEnabledLayerNames = m_pValidationLayers.data();
        } else {
            createInfo.enabledLayerCount = 0;
        }
#else
        createInfo.enabledLayerCount = 0;
#endif

        VulkanUtil::CheckVkResult(vkCreateDevice(m_pData.m_PhysicalDevice, &createInfo, nullptr, &m_pData.m_Device));

        vkGetDeviceQueue(m_pData.m_Device, indices.m_GraphicsFamily.value(), 0, &m_pData.m_GraphicsQueue);
        vkGetDeviceQueue(m_pData.m_Device, indices.m_PresentFamily.value(), 0, &m_pData.m_PresentQueue);

        GetLogger()->Render_LogDebug("Successfully initialized logical device.");
    }

    void VulkanRenderer::initAllocator() {
        GetLogger()->Render_LogDebug("Initializing Vulkan allocator...");
        VmaAllocatorCreateInfo allocatorInfo = {};
        allocatorInfo.vulkanApiVersion = VK_API_VERSION_1_2;
        allocatorInfo.physicalDevice = m_pData.m_PhysicalDevice;
        allocatorInfo.device = m_pData.m_Device;
        allocatorInfo.instance = m_pData.m_Instance;

        VulkanUtil::CheckVkResult(vmaCreateAllocator(&allocatorInfo, &m_pData.m_Allocator));
        GetLogger()->Render_LogDebug("Successfully initialized Vulkan allocator.");
    }

    void VulkanRenderer::initSyncElements() {

        GetLogger()->Render_LogDebug("Creating sync elements...");

        m_pData.m_ImageAvailableSemaphores.resize(FRAMES_IN_FLIGHT);
        m_pData.m_RenderFinishedSemaphores.resize(FRAMES_IN_FLIGHT);
        m_pData.m_InFlightFences.resize(FRAMES_IN_FLIGHT);

        VkSemaphoreCreateInfo semaphoreInfo{};
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        VkFenceCreateInfo fenceInfo{};
        fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        for (size_t i = 0; i < FRAMES_IN_FLIGHT; i++) {
            VKCheck(vkCreateSemaphore(m_pData.m_Device, &semaphoreInfo, nullptr,
                                      &m_pData.m_ImageAvailableSemaphores[i]));
            VKCheck(vkCreateSemaphore(m_pData.m_Device, &semaphoreInfo, nullptr,
                                      &m_pData.m_RenderFinishedSemaphores[i]));
            VKCheck(vkCreateFence(m_pData.m_Device, &fenceInfo, nullptr, &m_pData.m_InFlightFences[i]));
        }

        GetLogger()->Render_LogDebug("Successfully created sync elements.");
    }

    void VulkanRenderer::rebuildFragileData() {
        MV_PROFILE_FUNCTION("VulkanRenderer::rebuildFragileData")
        int width = 0, height = 0;

        auto glfwWindow = static_cast<GLFWwindow *>(m_pCommonConfig.m_Window->GetContext());

        glfwGetFramebufferSize(glfwWindow, &width, &height);
        while (width == 0 || height == 0) {
            glfwGetFramebufferSize(glfwWindow, &width, &height);
            glfwWaitEvents();
        }

        m_pFragileData.Rebuild(m_pData, m_pCommonConfig.m_Window);
        m_pCommonConfig.m_ResizeCallback();
    }

    MV::ImGui_ImplVulkan_InitInfo VulkanRenderer::CreateImguiInfo() {
        auto swapChainSupport = VulkanSwapChainSupportDetails::GetSwapChainSupport(m_pData.m_PhysicalDevice,
                                                                                   m_pData.m_Surface);

        VkDescriptorPoolSize pool_sizes[] =
                {
                        {VK_DESCRIPTOR_TYPE_SAMPLER,                1000},
                        {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000},
                        {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,          1000},
                        {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,          1000},
                        {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,   1000},
                        {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,   1000},
                        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,         1000},
                        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,         1000},
                        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000},
                        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000},
                        {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,       1000}
                };

        VkDescriptorPoolCreateInfo pool_info = {};
        pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
        pool_info.maxSets = 1000;
        pool_info.poolSizeCount = std::size(pool_sizes);
        pool_info.pPoolSizes = &pool_sizes[0];

        VKCheck(vkCreateDescriptorPool(m_pData.m_Device, &pool_info, nullptr, &m_pImguiDescriptorPool));

        MV::ImGui_ImplVulkan_InitInfo init_info = {};
        init_info.Instance = m_pData.m_Instance;
        init_info.PhysicalDevice = m_pData.m_PhysicalDevice;
        init_info.Device = m_pData.m_Device;
        init_info.Queue = m_pData.m_GraphicsQueue;
        init_info.DescriptorPool = m_pImguiDescriptorPool;

        init_info.MinImageCount = swapChainSupport.m_Capabilities.minImageCount;
        init_info.ImageCount = m_pFragileData.m_SwapChain.m_SwapChainImages.size();
        return init_info;
    }

    MV::rc<RenderPass> VulkanRenderer::GetMainRenderPass() {
        return m_pFragileData.m_MainRenderPass;
    }

    void VulkanRenderer::BindMainRenderPassEditor(FrameContext &context) {
        MV_PROFILE_FUNCTION("VulkanRenderer::BindMainRenderPassEditor")
        GetMainRenderPass()->Bind(context);
    }

    MV::rc<RenderPass> VulkanRenderer::GetEditorRenderPass() {
        assert(m_pCommonConfig.m_Editor);
        return m_pFragileData.m_EditorRenderPass;
    }

    void VulkanRenderer::ExecuteSingleCommand(const std::function<void(VkCommandBuffer)> &fn) {
        MV_PROFILE_FUNCTION("VulkanRenderer::ExecuteSingleCommand")
        VkCommandBufferAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandPool = m_pData.m_UploadCommandPool;
        allocInfo.commandBufferCount = 1;

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(m_pData.m_Device, &allocInfo, &commandBuffer);

        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        fn(commandBuffer);

        vkEndCommandBuffer(commandBuffer);

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        vkQueueSubmit(m_pData.m_GraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(m_pData.m_GraphicsQueue);

        vkFreeCommandBuffers(m_pData.m_Device, m_pData.m_UploadCommandPool, 1, &commandBuffer);
    }

    MV::rc<VertexBuffer> VulkanRenderer::CreateVertexBuffer(size_t size, size_t vertexStride) {
        MV_PROFILE_FUNCTION("VulkanRenderer::CreateVertexBuffer")
        auto vertexBuffer = MV::make_rc<VulkanVertexBuffer>(size, vertexStride);

        vertexBuffer->Init();

        m_pBufferStorage.TrackVulkanBuffer(vertexBuffer);

        return vertexBuffer;
    }

    MV::rc<StagingBuffer> VulkanRenderer::CreateStagingBuffer(size_t size) {
        MV_PROFILE_FUNCTION("VulkanRenderer::CreateStagingBuffer")

        bool foundValidBuffer = false;
        for (int32_t i = static_cast<int32_t>(m_pReusableStagingBuffers.size()) - 1; i >= 0; i--) {
            const auto &buf = m_pReusableStagingBuffers[i];

            if (!buf) {
                GetLogger()->Render_LogWarning("ReusableStagingBuffers contained nullptr, that should not happen!");
                m_pReusableStagingBuffers.erase(m_pReusableStagingBuffers.begin() + i);
                continue;
            }

            if (buf->GetSize() < size && foundValidBuffer) {
                return m_pReusableStagingBuffers[i + 1];
            }

            if (buf->GetSize() >= size) {
                foundValidBuffer = true;
            }
        }

        if (foundValidBuffer)
            return m_pReusableStagingBuffers[m_pReusableStagingBuffers.size() - 1];

        auto stagingBuffer = MV::make_rc<VulkanStagingBuffer>(size);

        stagingBuffer->Init();

        m_pBufferStorage.TrackVulkanBuffer(stagingBuffer);

        m_pReusableStagingBuffers.emplace_back(stagingBuffer);

        eastl::sort(m_pReusableStagingBuffers.begin(), m_pReusableStagingBuffers.end(),
                    [](const MV::rc<StagingBuffer> &a, const MV::rc<StagingBuffer> &b) {
                        return a->GetSize() < b->GetSize();
                    });

        return stagingBuffer;
    }

    MV::rc<IndexBuffer> VulkanRenderer::CreateIndexBuffer(size_t size) {
        MV_PROFILE_FUNCTION("VulkanRenderer::CreateIndexBuffer")
        auto indexBuffer = MV::make_rc<VulkanIndexBuffer>(size);

        indexBuffer->Init();

        m_pBufferStorage.TrackVulkanBuffer(indexBuffer);

        return indexBuffer;
    }

    void VulkanRenderer::CollectGarbage() {
        MV_PROFILE_FUNCTION("VulkanRenderer::CollectGarbage")
        GetLogger()->Render_LogDebug("Collecting garbage on GPU...");
        m_pBufferStorage.ClearMarked();
        for (auto it = m_pReusableStagingBuffers.begin(); it != m_pReusableStagingBuffers.end(); ++it) {
            if (!(*it).operator bool()) {
                it = m_pReusableStagingBuffers.erase(it);
            }
        }
        GetLogger()->Render_LogDebug("Collected garbage.");
    }

    RendererLimits MV::VulkanRenderer::GetLimits() const {
        return m_pLimits;
    }

    void MV::VulkanRenderer::initLimits() {
        VkPhysicalDeviceProperties properties{};
        vkGetPhysicalDeviceProperties(m_pData.m_PhysicalDevice, &properties);

        VkPhysicalDeviceFeatures supportedFeatures;
        vkGetPhysicalDeviceFeatures(m_pData.m_PhysicalDevice, &supportedFeatures);

        m_pLimits.m_MaxAnisotropy = properties.limits.maxSamplerAnisotropy;
        m_pLimits.m_AnisotropyEnabled = supportedFeatures.samplerAnisotropy == VK_TRUE;
    }

    VkSampler VulkanRenderer::getOrCreateSampler(VulkanSamplerInfo info) {
        const auto &found = m_pData.m_TextureSamplers.find(info);

        if (found != m_pData.m_TextureSamplers.end()) {
            return m_pData.m_TextureSamplers[info];
        }
        MV_PROFILE_FUNCTION("VulkanRenderer::getOrCreateSampler Create sampler")

        VkSamplerCreateInfo samplerInfo{};
        samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        samplerInfo.magFilter = info.m_MagFilter == TextureFilter::NEAREST ? VK_FILTER_NEAREST : VK_FILTER_LINEAR;
        samplerInfo.minFilter = info.m_MinFilter == TextureFilter::NEAREST ? VK_FILTER_NEAREST : VK_FILTER_LINEAR;
        samplerInfo.addressModeU = static_cast<VkSamplerAddressMode>(info.m_AddressingMode);
        samplerInfo.addressModeV = static_cast<VkSamplerAddressMode>(info.m_AddressingMode);
        samplerInfo.addressModeW = static_cast<VkSamplerAddressMode>(info.m_AddressingMode);
        samplerInfo.anisotropyEnable = info.m_EnableAnisotropy ? VK_TRUE : VK_FALSE;
        samplerInfo.maxAnisotropy = info.m_MaxAnisotropy;
        samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
        samplerInfo.unnormalizedCoordinates = VK_FALSE;

        samplerInfo.compareEnable = VK_FALSE;
        samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

        samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        samplerInfo.mipLodBias = 0.0f;
        samplerInfo.minLod = 0.0f;
        samplerInfo.maxLod = 0.25f;

        VkSampler sampler = VK_NULL_HANDLE;

        VKCheck(vkCreateSampler(m_pData.m_Device, &samplerInfo, nullptr, &sampler));

        m_pData.m_TextureSamplers[info] = sampler;

        return m_pData.m_TextureSamplers[info];
    }

    MV::rc<UniformBuffer> VulkanRenderer::CreateUniformBuffer(size_t size) {
        MV_PROFILE_FUNCTION("VulkanRenderer::CreateUniformBuffer")
        auto uniformBuffer = MV::make_rc<VulkanUniformBuffer>(size);

        uniformBuffer->Init();

        m_pBufferStorage.TrackVulkanBuffer(uniformBuffer);

        return uniformBuffer;
    }

    MV::rc<Texture> MV::VulkanRenderer::CreateTexture(TextureBlueprint &&bp) {
        MV_PROFILE_FUNCTION("VulkanRenderer::CreateTexture")
        auto texture = MV::make_rc<VulkanTexture>(MV::move(bp));

        texture->Init();

        m_pBufferStorage.TrackVulkanTexture(texture);

        return texture;
    }

    MV::rc<ShaderPipeline> MV::VulkanRenderer::CreateShaderPipeline(PipelineBlueprint &&bp) {
        auto result = m_pFragileData.m_Pipelines.emplace_back(MV::make_rc<VulkanPipeline>(MV::move(bp)));

        result->Init();

        return result;
    }


    MV::rc<Shader> VulkanRenderer::CreateShader(MV::string path, ShaderType type) {
        auto result = MV::make_rc<VulkanShader>(MV::move(path), type);
        return result;
    }


    void VulkanRenderer::resetBatchBuffers() {
        m_pBatchingVertexBuffersUsed = 0;
        m_pBatchingIndexBuffersUsed = 0;
        m_pBatchUniformBuffersUsed = 0;

        m_pFreeBatchingVertexBuffers = m_pBatchingVertexBuffers;
        m_pFreeBatchingIndexBuffers = m_pBatchingIndexBuffers;
    }

    MV::rc<VertexBuffer> VulkanRenderer::getBatchVertexBuffer(size_t size, size_t vertexStride) {
        MV::rc<VertexBuffer> candidate;
        int32_t candidateIndex = -1;

        for (int32_t i = 0; i < m_pFreeBatchingVertexBuffers.size(); i++) {
            const auto &buf = m_pFreeBatchingVertexBuffers[i];

            if (!buf)
                continue;

            if (buf->GetSize() < size)
                continue;

            if (buf->GetStride() != vertexStride)
                continue;

            if (!candidate) {
                candidate = buf;
                candidateIndex = i;
                continue;
            }

            if (candidate->GetSize() > buf->GetSize()) {
                candidate = buf;
                candidateIndex = i;
            }
        }

        if (candidate) {
            m_pFreeBatchingVertexBuffers[candidateIndex].reset();

            return candidate;
        }


        m_pBatchingVertexBuffers.push_back(MV::move(CreateVertexBuffer(size, vertexStride)));

        return m_pBatchingVertexBuffers.back();
    }

    MV::rc<IndexBuffer> VulkanRenderer::getBatchIndexBuffer(size_t size) {
        MV::rc<IndexBuffer> candidate;
        int32_t candidateIndex = -1;

        for (uint32_t i = 0; i < m_pFreeBatchingIndexBuffers.size(); i++) {
            const auto &buf = m_pFreeBatchingIndexBuffers[i];

            if (!buf)
                continue;

            if (buf->GetSize() < size)
                continue;

            if (!candidate) {
                candidate = buf;
                candidateIndex = i;
                continue;
            }

            if (candidate->GetSize() > buf->GetSize()) {
                candidate = buf;
                candidateIndex = i;
            }
        }

        if (candidate) {
            m_pFreeBatchingIndexBuffers[candidateIndex].reset();

            return candidate;
        }

        m_pBatchingIndexBuffers.push_back(MV::move(CreateIndexBuffer(size)));

        return m_pBatchingIndexBuffers.back();
    }

    MV::rc<UniformBuffer> VulkanRenderer::getBatchUniformBuffer(size_t size) {
        const auto bufIndex = m_pBatchUniformBuffersUsed++;

        if (bufIndex < m_pBatchingUniformBuffers.size()) {
            return m_pBatchingUniformBuffers[bufIndex];
        }

        m_pBatchingUniformBuffers.push_back(MV::move(CreateUniformBuffer(size)));

        return m_pBatchingUniformBuffers.back();
    }

    void VulkanRenderer::ReloadShaders() {
        m_pFragileData.ForceUnloadShaders(m_pData);
    }
}

#endif