//
// Created by Matty on 2021-10-15.
//

#include "../../include/render/vulkan/VulkanActiveBufferStorage.h"

namespace MV {
    VulkanActiveBufferStorage::VulkanActiveBufferStorage() {
        m_pTrackedBuffers.reserve(1024);
    }

    void VulkanActiveBufferStorage::TrackVulkanBuffer(const MV::rc<VulkanBufferImpl> &buffer) {
        m_pTrackedBuffers.emplace_back(buffer);
    }

    void VulkanActiveBufferStorage::ClearMarked() {
        clearMarkedBuffers();
        clearMarkedTextures();
    }

    void VulkanActiveBufferStorage::ClearEverything() {
        for (auto &i: m_pTrackedBuffers) {
            i->deallocate();
        }

        for (auto &i: m_pTrackedTextures) {
            i->deallocate();
        }
    }

    void VulkanActiveBufferStorage::TrackVulkanTexture(const MV::rc<VulkanTextureImpl> &texture) {
        m_pTrackedTextures.emplace_back(texture);
    }

    void VulkanActiveBufferStorage::clearMarkedBuffers() {
        size_t remainingCount = 0;

        for (auto &i: m_pTrackedBuffers) {
            if (i->shouldDealloc()) {
                i->deallocate();
                continue;
            }

            remainingCount++;
        }

        size_t diff = m_pTrackedBuffers.size() - remainingCount;

        if (diff == 0)
            return;

        MV::vector<MV::rc<VulkanBufferImpl>> newTracked{remainingCount};

        size_t offset = 0;

        for (auto &i: m_pTrackedBuffers) {
            if (!i->shouldDealloc())
                newTracked[offset++] = i;
        }

        m_pTrackedBuffers = MV::move(newTracked);
    }

    void VulkanActiveBufferStorage::clearMarkedTextures() {

        size_t remainingCount = 0;

        for (auto &i: m_pTrackedTextures) {
            if (i->shouldDealloc()) {
                i->deallocate();
                continue;
            }

            remainingCount++;
        }

        size_t diff = m_pTrackedTextures.size() - remainingCount;

        if (diff == 0)
            return;

        MV::vector<MV::rc<VulkanTextureImpl>> newTracked{remainingCount};

        size_t offset = 0;

        for (auto &i: m_pTrackedTextures) {
            if (!i->shouldDealloc())
                newTracked[offset++] = i;
        }

        m_pTrackedTextures = MV::move(newTracked);
    }
}
