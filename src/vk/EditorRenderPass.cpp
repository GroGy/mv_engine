//
// Created by Matty on 2021-10-19.
//

#include "../../include/render/vulkan/renderpasses/EditorRenderPass.h"
#include "../../include/render/vulkan/VulkanRenderer.h"
#include "../../include/common/EngineCommon.h"

MV::EditorRenderPass::EditorRenderPass(MV::VulkanSwapChain &swapChain) : VulkanRenderPass({0.2f,0.2f,0.2f,1.0f}) {
    auto colorAttachment = VulkanAttachmentBlueprint(swapChain.m_SwapChainImageFormat);
    colorAttachment.m_SampleCount = 1;
    colorAttachment.m_Layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    colorAttachment.m_Format = static_cast<VkFormat>(TextureFormat::R8G8B8A8_SRGB);
    colorAttachment.m_FinalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    auto mainSubpass = VulkanSubPassBlueprint(colorAttachment);

    //auto depthAttachment = AttachmentBlueprint(m_pDepthTexture.GetFormat());
    //depthAttachment.m_SampleCount = 1;
    //depthAttachment.m_FinalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    //depthAttachment.m_StoreOP = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    //depthAttachment.m_Layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    //mainSubpass.SetDepthAttachment(depthAttachment);

    m_pSwapImageCount = swapChain.m_SwapChainImages.size();

    auto blueprint = VulkanRenderPassBlueprint(m_pSwapImageCount,swapChain.m_SwapChainExtent.width,swapChain.m_SwapChainExtent.height,{});
    blueprint.AddSubPass(mainSubpass);

    {
        VkSubpassDependency dep;
        dep.srcSubpass = VK_SUBPASS_EXTERNAL;
        dep.dstSubpass = 0;
        dep.srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        dep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dep.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
        dep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dep.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        blueprint.AddSubPassDep(dep);
    }

    {
        VkSubpassDependency dep;
        dep.srcSubpass = 0;
        dep.dstSubpass = VK_SUBPASS_EXTERNAL;
        dep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dep.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        dep.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dep.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        dep.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        blueprint.AddSubPassDep(dep);
    }

    SetBlueprint(MV::move(blueprint));
}

void MV::EditorRenderPass::OnPreInit() {
    m_Textures.reserve(m_pSwapImageCount);
    auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());

    MV::vector<VkImageView> imageViews{m_pSwapImageCount};

    m_pBlueprint.SetSize(EDITOR_VIEWPORT_WIDTH, EDITOR_VIEWPORT_HEIGHT);

    for(uint32_t i = 0 ; i < m_pSwapImageCount; i++) {
        TextureBlueprint bp{};
        bp.m_Height = EDITOR_VIEWPORT_HEIGHT;
        bp.m_Width = EDITOR_VIEWPORT_WIDTH;
        bp.m_TextureChannels = 4;
        bp.m_Format = TextureFormat::R8G8B8A8_SRGB;
        bp.m_Filter = TextureFilter::NEAREST;
        bp.m_Usage = TextureUsage::ATTACHMENT;

        auto & texture = m_Textures.emplace_back(MV::static_pointer_cast<VulkanTexture>(renderer->CreateTexture(MV::move(bp))));
        texture->TransitionToLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        imageViews[i] = m_Textures[i]->m_TextureView;
    }

    m_pBlueprint.SetResultImageViews(MV::move(imageViews));
}

void MV::EditorRenderPass::OnCleanup() {
    for(auto & m_Texture : m_Textures)
        m_Texture->Cleanup();

    m_Textures.clear();
}
