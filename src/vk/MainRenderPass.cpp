//
// Created by Matty on 2021-10-05.
//

#include "../../include/render/vulkan/renderpasses/MainRenderPass.h"

namespace MV {
    MainRenderPass::MainRenderPass(MV::VulkanSwapChain &swapChain) : VulkanRenderPass({0.15f, 0.15f, 0.19f, 1.0f}) {
        auto colorAttachment = VulkanAttachmentBlueprint(swapChain.m_SwapChainImageFormat);
        colorAttachment.m_SampleCount = 1;
        colorAttachment.m_Layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        auto mainSubpass = VulkanSubPassBlueprint(colorAttachment);

        //auto depthAttachment = AttachmentBlueprint(m_pDepthTexture.GetFormat());
        //depthAttachment.m_SampleCount = 1;
        //depthAttachment.m_FinalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        //depthAttachment.m_StoreOP = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        //depthAttachment.m_Layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        //mainSubpass.SetDepthAttachment(depthAttachment);
        auto blueprint = VulkanRenderPassBlueprint(swapChain.m_ImageViews.size(), swapChain.m_SwapChainExtent.width,
                                                   swapChain.m_SwapChainExtent.height, swapChain.m_ImageViews);
        blueprint.AddSubPass(mainSubpass);

        VkSubpassDependency dep;

        dep.srcSubpass = VK_SUBPASS_EXTERNAL;
        dep.dstSubpass = 0;
        dep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dep.srcAccessMask = 0;
        dep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dep.dependencyFlags = 0;

        blueprint.AddSubPassDep(dep);

        SetBlueprint(MV::move(blueprint));
    }
}