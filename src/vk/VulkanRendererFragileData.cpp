//
// Created by Matty on 2021-10-03.
//

#include "../../include/render/vulkan/VulkanRendererFragileData.h"
#include "../../include/render/vulkan/VulkanUtil.h"
#include "../../include/common/EngineCommon.h"
#include "../../include/render/vulkan/VulkanQueueFamilies.h"

namespace MV {
    static bool firstInit = true;

    void
    VulkanRendererFragileData::Init(VulkanRendererData &vulkanRendererData, const std::unique_ptr<Window> &window) {
        initSwapChain(vulkanRendererData, window);

        if (firstInit) {
            m_MainRenderPass = MV::make_rc<MainRenderPass>(m_SwapChain);
            m_RenderPasses.emplace_back(m_MainRenderPass);

            m_EditorRenderPass = MV::make_rc<EditorRenderPass>(m_SwapChain);
            m_RenderPasses.emplace_back(m_EditorRenderPass);

            initCommandPool(vulkanRendererData);
        }

        auto & blueprint = eastl::static_pointer_cast<VulkanRenderPass>(m_MainRenderPass)->m_pBlueprint;
        blueprint.m_pRenderImageViews = m_SwapChain.m_ImageViews;
        blueprint.m_pWidth = m_SwapChain.m_SwapChainExtent.width;
        blueprint.m_pHeight = m_SwapChain.m_SwapChainExtent.height;

        initRenderPasses(vulkanRendererData);

        initPipelines();

        initCommandBuffers(vulkanRendererData);

        initFenceSlots(vulkanRendererData);

        firstInit = false;
    }

    void
    VulkanRendererFragileData::Rebuild(VulkanRendererData &vulkanRendererData, const std::unique_ptr<Window> &window) {
        Cleanup(vulkanRendererData);
        Init(vulkanRendererData, window);
    }

    void VulkanRendererFragileData::Cleanup(VulkanRendererData &vulkanRendererData) {
        vkDeviceWaitIdle(vulkanRendererData.m_Device);
        cleanupCommandBuffers(vulkanRendererData);
        cleanupPipelines();
        cleanupRenderPasses(vulkanRendererData);
        cleanupSwapChain(vulkanRendererData);
    }

    void
    VulkanRendererFragileData::initSwapChain(VulkanRendererData &rendererData, const std::unique_ptr<Window> &window) {
        m_SwapChain.Init(rendererData, window);
    }

    void VulkanRendererFragileData::cleanupSwapChain(VulkanRendererData &vulkanRendererData) {
        m_SwapChain.Cleanup(vulkanRendererData);
    }

    void VulkanRendererFragileData::initRenderPasses(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Initializing render passes...");

        for (auto &&renderPass: m_RenderPasses)
            renderPass->Init();

        GetLogger()->Render_LogDebug("Successfully initialized render passes.");
    }

    void VulkanRendererFragileData::cleanupRenderPasses(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Destroying render passes...");

        for (auto &&renderPass: m_RenderPasses)
            renderPass->Cleanup();

        GetLogger()->Render_LogDebug("Successfully destroyed render passes.");
    }

    void VulkanRendererFragileData::initPipelines() {
        GetLogger()->Render_LogDebug("Initializing pipelines...");

        for (auto &&pipeline: m_Pipelines)
            pipeline->Init();

        GetLogger()->Render_LogDebug("Successfully initialized pipelines.");
    }

    void VulkanRendererFragileData::cleanupPipelines() {
        GetLogger()->Render_LogDebug("Destroying pipelines...");

        for (auto &&pipeline: m_Pipelines)
            pipeline->Cleanup();

        GetLogger()->Render_LogDebug("Successfully destroyed pipelines.");
    }

    void VulkanRendererFragileData::initCommandBuffers(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Creating command buffers...");

        m_CommandBuffers.resize(m_SwapChain.m_SwapChainImages.size());

        VkCommandBufferAllocateInfo allocInfo{};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.commandPool = vulkanRendererData.m_CommandPool;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandBufferCount = (uint32_t) m_CommandBuffers.size();

        VKCheck(vkAllocateCommandBuffers(vulkanRendererData.m_Device, &allocInfo, m_CommandBuffers.data()));

        GetLogger()->Render_LogDebug("Successfully created command buffers.");
    }

    void VulkanRendererFragileData::cleanupCommandBuffers(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Destroying command buffers...");
        vkFreeCommandBuffers(vulkanRendererData.m_Device, vulkanRendererData.m_CommandPool,
                             static_cast<uint32_t>(m_CommandBuffers.size()),
                             m_CommandBuffers.data());
        GetLogger()->Render_LogDebug("Successfully destroyed command buffers.");
    }

    void VulkanRendererFragileData::initFenceSlots(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Creating fence slots...");

        auto swapImageCount = m_SwapChain.m_SwapChainImages.size();
        m_ImagesInFlight.resize(swapImageCount, VK_NULL_HANDLE);

        GetLogger()->Render_LogDebug("Successfully created fence slots.");
    }

    void VulkanRendererFragileData::initCommandPool(VulkanRendererData &vulkanRendererdata) {
        GetLogger()->Render_LogDebug("Creating command pools...");
        auto indices = QueueFamilies::FindSupportedQueueFamilies(vulkanRendererdata.m_PhysicalDevice,
                                                                 vulkanRendererdata.m_Surface);

        {
            VkCommandPoolCreateInfo poolInfo{};
            poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            poolInfo.queueFamilyIndex = indices.m_GraphicsFamily.value();
            poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

            VKCheck(vkCreateCommandPool(vulkanRendererdata.m_Device, &poolInfo, nullptr,
                                        &vulkanRendererdata.m_CommandPool));
        }

        {
            VkCommandPoolCreateInfo poolInfo{};
            poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            poolInfo.queueFamilyIndex = indices.m_GraphicsFamily.value();
            poolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT ;

            VKCheck(vkCreateCommandPool(vulkanRendererdata.m_Device, &poolInfo, nullptr,
                                        &vulkanRendererdata.m_UploadCommandPool));
        }

        GetLogger()->Render_LogDebug("Successfully created command pools.");
    }

    void VulkanRendererFragileData::ForceUnloadShaders(VulkanRendererData &vulkanRendererData) {
        GetLogger()->Render_LogDebug("Destroying pipelines...");

        for (auto &&pipeline: m_Pipelines)
            pipeline->Cleanup();

        m_Pipelines.clear();

        GetLogger()->Render_LogDebug("Successfully destroyed pipelines.");
    }
}
