//
// Created by Matty on 2021-10-04.
//

#ifdef VK_BACKEND_SUPPORT

#include "../../include/render/vulkan/VulkanQueueFamilies.h"
#include <EngineMinimal.h>

namespace MV {
    QueueFamilies QueueFamilies::FindSupportedQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface) {
        QueueFamilies indices;

        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

        MV::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

        int i = 0;
        for (const auto &queueFamily: queueFamilies) {
            if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                indices.m_GraphicsFamily = i;
            }
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

            if (presentSupport) {
                indices.m_PresentFamily = i;
            }

            if (indices.IsComplete()) {
                break;
            }

            i++;
        }

        return indices;
    }
}

#endif

