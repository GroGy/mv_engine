//
// Created by Matty on 2021-10-15.
//

#include "../../include/render/vulkan/VulkanVertexBuffer.h"

namespace MV {
    void VulkanVertexBuffer::Init() {
        VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        bufferInfo.size = m_pSize;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        allocate(bufferInfo,allocInfo);
    }

    void VulkanVertexBuffer::Cleanup() {
        markForDealloc();
    }

    VulkanVertexBuffer::VulkanVertexBuffer(size_t size,size_t vertexStride) : VertexBuffer(size,vertexStride) {
    }

    void VulkanVertexBuffer::Bind(FrameContext &context, uint32_t binding) {
        VkBuffer vertexBuffers[] = {m_Buffer };
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(context.m_FrameCommandBuffer, binding, 1, vertexBuffers, offsets);
    }

    size_t VulkanVertexBuffer::GetVertexCount() const {
        return m_pFilled / m_pSingleVertSize;
    }

    void VulkanVertexBuffer::Upload(const MV::rc<StagingBuffer> &staging, size_t dataSize) {
        if(auto buf = MV::dynamic_pointer_cast<VulkanBufferImpl>(staging)) {
            copyFrom(*buf, dataSize);
            m_pFilled = dataSize == 0 ? staging->GetSize() : dataSize;
        }
    }
}
