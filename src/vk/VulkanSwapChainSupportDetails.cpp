//
// Created by Matty on 2021-10-03.
//

#include <GLFW/glfw3.h>
#include "render/vulkan/VulkanSwapChainSupportDetails.h"

namespace MV {
    VkSurfaceFormatKHR VulkanSwapChainSupportDetails::ChooseSwapSurfaceFormat() {
        for (const auto& availableFormat : m_Formats) {
            if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return availableFormat;
            }
        }

        return m_Formats.at(0);
    }

    VkPresentModeKHR VulkanSwapChainSupportDetails::ChooseSwapPresentMode() {
        for (const auto& availablePresentMode : m_PresentModes) {
            if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return availablePresentMode;
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D VulkanSwapChainSupportDetails::ChooseSwapExtent(const std::unique_ptr<MV::Window> & window) {
        if (m_Capabilities.currentExtent.width != UINT32_MAX) {
            return m_Capabilities.currentExtent;
        } else {
            int width, height;
            glfwGetFramebufferSize((GLFWwindow*)window->GetContext(), &width, &height);

            VkExtent2D actualExtent = {
                    static_cast<uint32_t>(width),
                    static_cast<uint32_t>(height)
            };

            actualExtent.width = (std::max)(m_Capabilities.minImageExtent.width, (std::min)(m_Capabilities.maxImageExtent.width, actualExtent.width));
            actualExtent.height = (std::max)(m_Capabilities.minImageExtent.height, (std::min)(m_Capabilities.maxImageExtent.height, actualExtent.height));

            return actualExtent;
        }
    }

    VulkanSwapChainSupportDetails VulkanSwapChainSupportDetails::GetSwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface) {
        VulkanSwapChainSupportDetails details;

        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.m_Capabilities);

        uint32_t formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

        if (formatCount != 0) {
            details.m_Formats.resize(formatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.m_Formats.data());
        }

        uint32_t presentModeCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

        if (presentModeCount != 0) {
            details.m_PresentModes.resize(presentModeCount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount,
                                                      details.m_PresentModes.data());
        }

        return details;
    }
}