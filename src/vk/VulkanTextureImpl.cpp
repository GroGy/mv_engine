//
// Created by Matty on 2021-10-17.
//

#include "../../include/render/vulkan/VulkanTextureImpl.h"
#include "../../include/common/EngineCommon.h"
#include "../../include/render/vulkan/VulkanRenderer.h"
#include "../../include/render/vulkan/VulkanUtil.h"
#include "../../include/common/ExceptionCommon.h"

namespace MV {
    void VulkanTextureImpl::deallocate() {
        GetLogger()->Render_LogDebug("Destroying vulkan texture...");
        auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetRenderer())->m_pData;
        vkDestroyImageView(rendererData.m_Device,m_TextureView,nullptr);
        vmaDestroyImage(rendererData.m_Allocator, m_Texture, m_pAllocation);
        GetLogger()->Render_LogDebug("Successfully destroyed vulkan texture.");
    }

    void VulkanTextureImpl::allocate(VkImageCreateInfo imageInfo, VmaAllocationCreateInfo allocInfo,VulkanSamplerInfo samplerInfo) {
        assert(!m_pMarkedForDealloc);
        if(m_Texture && m_TextureView) return;
        GetLogger()->Render_LogDebug("Allocating vulkan texture...");
        m_pSamplerInfo = samplerInfo;
        m_pFormat = imageInfo.format;

        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;

        auto res = vmaCreateImage(rendererData.m_Allocator, &imageInfo, &allocInfo, &m_Texture, &m_pAllocation,
                                  &m_pAllocInfo);
        VKCheck(res)

        VkImageViewCreateInfo viewInfo{};
        viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewInfo.image = m_Texture;
        viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        viewInfo.format = m_pFormat;
        viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        viewInfo.subresourceRange.baseMipLevel = imageInfo.mipLevels - 1;
        viewInfo.subresourceRange.levelCount = 1;
        viewInfo.subresourceRange.baseArrayLayer = 0;
        viewInfo.subresourceRange.layerCount = 1;

        VKCheck(vkCreateImageView(rendererData.m_Device, &viewInfo, nullptr, &m_TextureView));

        renderer->getOrCreateSampler(m_pSamplerInfo);

        GetLogger()->Render_LogDebug("Successfully allocated vulkan texture.");
    }

    void VulkanTextureImpl::loadData(const void *data, size_t size) {
        assert(!m_pMarkedForDealloc);

        auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetRenderer())->m_pData;

        assert(false); // You cant do that with textures, use staging buffer
        //VulkanUtil::LoadDataToAllocation(rendererData.m_Allocator,m_pAllocation,m_pAllocInfo,data,size);
    }

    void VulkanTextureImpl::copyFrom(VulkanBufferImpl &other) {
        transitionImageLayout(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;

        renderer->ExecuteSingleCommand([&](VkCommandBuffer buffer) {
            VkBufferImageCopy region{};
            region.bufferOffset = 0;
            region.bufferRowLength = 0;
            region.bufferImageHeight = 0;

            region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            region.imageSubresource.mipLevel = 0;
            region.imageSubresource.baseArrayLayer = 0;
            region.imageSubresource.layerCount = 1;

            region.imageOffset = {0, 0, 0};
            region.imageExtent = {
                    m_TextureWidth,
                    m_TextureHeight,
                    1
            };

            vkCmdCopyBufferToImage(
                    buffer,
                    other.m_Buffer,
                    m_Texture,
                    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                    1,
                    &region
            );
        });

        transitionImageLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        m_pHasData = true;
    }

    void VulkanTextureImpl::transitionImageLayout(VkImageLayout oldLayout, VkImageLayout newLayout) {
        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;

        if(oldLayout == newLayout)
            return;

        renderer->ExecuteSingleCommand([&](VkCommandBuffer buffer) {
            VkImageMemoryBarrier barrier{};
            barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            barrier.oldLayout = oldLayout;
            barrier.newLayout = newLayout;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

            barrier.image = m_Texture;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = 1;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;

            VkPipelineStageFlags sourceStage;
            VkPipelineStageFlags destinationStage;

            if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

                sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
                barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

                sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
                destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            } else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

                sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            } else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR) {
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask = 0;

                sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
                destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            } else {
                throw MV::RuntimeException("unsupported layout transition!");
            }

            vkCmdPipelineBarrier(
                    buffer,
                    sourceStage, destinationStage,
                    0,
                    0, nullptr,
                    0, nullptr,
                    1, &barrier
            );

            m_TextureLayout = newLayout;
        });
    }

    VkSampler VulkanTextureImpl::GetSampler() {
        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        return renderer->getOrCreateSampler(m_pSamplerInfo);
    }

    VulkanTextureImpl::VulkanTextureImpl(uint32_t width, uint32_t height ) : m_TextureHeight(height), m_TextureWidth(width) {

    }
}