//
// Created by Matty on 2021-10-18.
//

#include "../../include/render/vulkan/VulkanUniformBuffer.h"

namespace MV {
    void VulkanUniformBuffer::Init() {
        VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        bufferInfo.size = m_pSize;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

        allocate(bufferInfo,allocInfo);
    }

    void VulkanUniformBuffer::Cleanup() {
        markForDealloc();
    }

    void VulkanUniformBuffer::Bind(FrameContext &context) {

    }

    void VulkanUniformBuffer::Upload(const void *data, size_t size) {
        loadData(data, size);
    }

    void VulkanUniformBuffer::Refresh() {
        Cleanup();
        Init();
    }

    VulkanUniformBuffer::VulkanUniformBuffer(size_t size) : UniformBuffer(size) {}
}
