//
// Created by Matty on 2021-10-14.
//

#include "../../include/render/vulkan/pipelines/MainPipeline.h"
#include "../../include/render/common/Vertex.h"

namespace MV {
    PipelineBlueprint MainPipeline::GetBlueprint() {
        PipelineBlueprint blueprint{};

        blueprint.SetVertexInputDescription(Vertex::GetBindingDescription(),Vertex::GetAttributeDescriptions());

        blueprint.EnableAlphaBlending(true);
        blueprint.AddShader(MV::make_rc<VulkanShader>(".shadercache/base_vertex.shadercache",ShaderType::VERTEX));
        blueprint.AddShader(MV::make_rc<VulkanShader>(".shadercache/base_fragment.shadercache",ShaderType::FRAGMENT));

        return blueprint;
    }
}
