//
// Created by Matty on 2021-10-05.
//

#include "../../include/render/vulkan/VulkanRenderPass.h"

#include <utility>
#include "../../include/common/ExceptionCommon.h"
#include "../../include/render/vulkan/VulkanUtil.h"
#include "../../include/render/vulkan/VulkanRenderer.h"
#include "../../include/common/EngineCommon.h"

namespace MV {
    VulkanAttachmentBlueprint::VulkanAttachmentBlueprint(VkFormat format) : m_Format(format) , m_Valid(true) {

    }

    VkSubpassDescription VulkanSubPassBlueprint::buildSubPass() {
        VkSubpassDescription res{};

        return res;
    }

    void VulkanRenderPass::Init() {
        auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pData;
        auto &fragileData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pFragileData;

        MV::GetLogger()->Render_LogDebug("Initializing render pass...");

        if(m_pValid) {
            OnPreInit();
            auto res = m_pBlueprint.buildRenderPass(rendererData);
            m_RenderPass = res.m_RenderPass;
            m_FrameBuffers = MV::move(res.m_FrameBuffers);
        } else {
            MV::GetLogger()->Render_LogError("RENDER PASS NOT VALID");
        }
        MV::GetLogger()->Render_LogDebug("Initialized render pass.");
    }

    void VulkanRenderPass::Cleanup() {auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pData;

        if (m_pValid) {
            OnCleanup();

            for (auto &&fb: m_FrameBuffers) {
                fb.Cleanup();
            }

            vkDestroyRenderPass(rendererData.m_Device, m_RenderPass, nullptr);
        }
    }

    VulkanRenderPass::VulkanRenderPass(const glm::vec4 & clearColor,VulkanRenderPassBlueprint blueprint) : RenderPass(clearColor),m_pBlueprint(MV::move(blueprint)), m_pValid(true) {

    }

    void VulkanRenderPass::SetBlueprint(VulkanRenderPassBlueprint blueprint) {
        m_pBlueprint = MV::move(blueprint);
        m_pValid = true;
    }

    void VulkanRenderPass::Bind(FrameContext &context) {
        if(context.m_ActiveRenderPassID == GetID()) return;

        if(context.m_IsRenderPassActive) {
           vkCmdEndRenderPass(context.m_FrameCommandBuffer);
        }

        VkRenderPassBeginInfo renderPassInfo{};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = m_RenderPass;
        renderPassInfo.framebuffer = m_FrameBuffers.at(context.m_SwapImageIndex).m_FrameBuffer;
        renderPassInfo.renderArea.offset = {0, 0};
        renderPassInfo.renderArea.extent = {m_pBlueprint.m_pWidth,m_pBlueprint.m_pHeight};

        std::array<VkClearValue, 1> clearValues{};

        clearValues.at(0).color = {m_pClearColor.r, m_pClearColor.g, m_pClearColor.b, m_pClearColor.a};

        renderPassInfo.clearValueCount = 1;
        renderPassInfo.pClearValues = clearValues.data();
        vkCmdBeginRenderPass(context.m_FrameCommandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        context.m_IsRenderPassActive = true;
        context.m_ActiveRenderPassID = GetID();
    }

    VulkanSubPassBlueprint::VulkanSubPassBlueprint(VulkanAttachmentBlueprint attachment) {
        m_pAttachments.reserve(64);
        m_pAttachments.emplace_back(attachment);
    }

    void VulkanSubPassBlueprint::SetDepthAttachment(VulkanAttachmentBlueprint depthAttachment) {
        m_pDepthStencilAttachment = depthAttachment;
    }

    void VulkanSubPassBlueprint::AddAttachment(VulkanAttachmentBlueprint attachment) {
        m_pAttachments.emplace_back(attachment);
    }

    RenderPassBlueprintResult VulkanRenderPassBlueprint::buildRenderPass(VulkanRendererData &rendererData) {
        RenderPassBlueprintResult res{};

        uint32_t depIndexOffset = 0;
        uint32_t depCount = 0;

        for (auto &&pass : m_pSubPasses) {
            depCount += pass.m_pAttachments.size();
            if (pass.m_pDepthStencilAttachment.m_Valid) depCount++;
        }

        MV::vector<VkAttachmentDescription> renderPassAttachments{depCount};
        MV::vector<VkSubpassDescription> subpasses{m_pSubPasses.size()};
        MV::vector<VkSubpassDependency> subpassDeps = m_pSubpassDeps;

        MV::vector<MV::vector<VkAttachmentDescription>> sp_attachments{m_pSubPasses.size()};
        MV::vector<MV::vector<VkAttachmentReference>> sp_attachmentReferences{m_pSubPasses.size()};

        MV::vector<VkAttachmentDescription> sp_depthStencilAttachment{m_pSubPasses.size()};
        MV::vector<VkAttachmentReference> sp_depthStencilReference{m_pSubPasses.size()};

        for (uint32_t pass = 0; pass < m_pSubPasses.size(); pass++) {
            auto & attachments = sp_attachments.at(pass);
            attachments.resize(m_pSubPasses.at(pass).m_pAttachments.size());
            auto & attachmentReferences = sp_attachmentReferences.at(pass);
            attachmentReferences.resize(m_pSubPasses.at(pass).m_pAttachments.size());

            auto & depthStencilAttachment = sp_depthStencilAttachment.at(pass);
            auto & depthStencilReference = sp_depthStencilReference.at(pass);

            for (uint32_t i = 0; i < m_pSubPasses.at(pass).m_pAttachments.size(); i++) {
                auto &attachmentBP = m_pSubPasses.at(pass).m_pAttachments.at(i);
                auto &attachment = attachments.at(i);
                auto &reference = attachmentReferences.at(i);

                uint32_t sampleCount = attachmentBP.m_SampleCount;
                attachment.format = attachmentBP.m_Format;
                attachment.samples = GetSamples(sampleCount);
                attachment.loadOp = attachmentBP.m_LoadOP;
                attachment.storeOp = attachmentBP.m_StoreOP;
                attachment.stencilLoadOp = attachmentBP.m_StencilLoadOP;
                attachment.stencilStoreOp = attachmentBP.m_StencilStoreOP;
                attachment.initialLayout = attachmentBP.m_InitialLayout;
                attachment.finalLayout = attachmentBP.m_FinalLayout;

                renderPassAttachments.at(depIndexOffset) = attachment;
                reference.attachment = depIndexOffset++;
                reference.layout = attachmentBP.m_Layout;
            }

            if (m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Valid) {
                uint32_t sampleCount = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_SampleCount;
                depthStencilAttachment.format = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Format;
                depthStencilAttachment.samples = GetSamples(sampleCount);
                depthStencilAttachment.loadOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_LoadOP;
                depthStencilAttachment.storeOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_StoreOP;
                depthStencilAttachment.stencilLoadOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_StencilLoadOP;
                depthStencilAttachment.stencilStoreOp = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_StencilStoreOP;
                depthStencilAttachment.initialLayout = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_InitialLayout;
                depthStencilAttachment.finalLayout = m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_FinalLayout;

                depthStencilReference.attachment = 1;
                depthStencilReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

                renderPassAttachments.at(depIndexOffset++) = depthStencilAttachment;
            }


            subpasses.at(pass).pipelineBindPoint = m_pSubPasses.at(pass).m_BindPoint;
            subpasses.at(pass).colorAttachmentCount = static_cast<uint32_t>(attachmentReferences.size());
            subpasses.at(pass).pColorAttachments = attachmentReferences.data();
            if (m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Valid) {
                subpasses.at(pass).pDepthStencilAttachment = &depthStencilReference;
            }

            if (m_pSubPasses.at(pass).m_pDepthStencilAttachment.m_Valid) {
                subpassDeps.at(pass).srcStageMask =
                        subpassDeps.at(pass).srcStageMask | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
                subpassDeps.at(pass).dstStageMask =
                        subpassDeps.at(pass).srcStageMask | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
                subpassDeps.at(pass).dstAccessMask =
                        subpassDeps.at(pass).srcStageMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            }
        }

        VkRenderPassCreateInfo renderPassInfo{};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassInfo.attachmentCount = static_cast<uint32_t>(renderPassAttachments.size());
        renderPassInfo.pAttachments = renderPassAttachments.data();
        renderPassInfo.subpassCount = static_cast<uint32_t>(subpasses.size());
        renderPassInfo.pSubpasses = subpasses.data();
        renderPassInfo.dependencyCount = static_cast<uint32_t>(subpassDeps.size());
        renderPassInfo.pDependencies = subpassDeps.data();

        VulkanUtil::CheckVkResult(vkCreateRenderPass(rendererData.m_Device, &renderPassInfo, nullptr, &res.m_RenderPass));

        initFrameBuffers(rendererData,res);

        return res;
    }

    void VulkanRenderPassBlueprint::AddSubPass(VulkanSubPassBlueprint pass) {
        m_pSubPasses.emplace_back(MV::move(pass));
    }

    VkSampleCountFlagBits VulkanRenderPassBlueprint::GetSamples(uint32_t sampleCount) {
        if (sampleCount == 1) {
            return VK_SAMPLE_COUNT_1_BIT;
        }
        if (sampleCount == 2) {
            return VK_SAMPLE_COUNT_2_BIT;
        }
        if (sampleCount == 4) {
            return VK_SAMPLE_COUNT_4_BIT;
        }
        if (sampleCount == 8) {
            return VK_SAMPLE_COUNT_8_BIT;
        }
        if (sampleCount == 16) {
            return VK_SAMPLE_COUNT_16_BIT;
        }
        if (sampleCount == 32) {
            return VK_SAMPLE_COUNT_32_BIT;
        }
        if (sampleCount == 64) {
            return VK_SAMPLE_COUNT_64_BIT;
        }

        throw MV::RuntimeException("Invalid sample count");
        return VK_SAMPLE_COUNT_64_BIT;
    }

    void VulkanRenderPassBlueprint::initFrameBuffers(VulkanRendererData &vulkanRendererData,RenderPassBlueprintResult & res) {
        res.m_FrameBuffers.resize(m_pSwapImageCount);

        for (size_t i = 0; i < m_pSwapImageCount; i++) {
            VulkanColorAttachment colorAttachment{0, m_pRenderImageViews[i]};

            VulkanFrameBufferBlueprint bp{res.m_RenderPass, m_pWidth,m_pHeight,{colorAttachment}};
            VulkanFrameBuffer fb{MV::move(bp)};

            fb.Init();

            res.m_FrameBuffers[i] = MV::move(fb);
        }
    }

    VulkanRenderPassBlueprint::VulkanRenderPassBlueprint(uint32_t swapImageCount, uint32_t width, uint32_t height,
                                                         MV::vector<VkImageView> renderImageViews) : m_pValid(true), m_pSwapImageCount(swapImageCount), m_pHeight(height), m_pWidth(width), m_pRenderImageViews(MV::move(renderImageViews)) {
    }

    void VulkanRenderPassBlueprint::SetResultImageViews(MV::vector<VkImageView> imageViews) {
        m_pRenderImageViews = MV::move(imageViews);
    }

    void VulkanRenderPassBlueprint::AddSubPassDep(VkSubpassDependency deps) {
        m_pSubpassDeps.emplace_back(deps);
    }
}
