//
// Created by Matty on 2021-10-14.
//

#ifdef VK_BACKEND_SUPPORT

#include "../../include/render/vulkan/VulkanFrameBuffer.h"
#include "../../include/render/vulkan/VulkanRenderer.h"
#include "../../include/common/EngineCommon.h"
#include "../../include/render/vulkan/VulkanUtil.h"
#include <cassert>

namespace MV {
    bool VulkanFrameBufferBlueprint::IsValid() const {
        return m_pValid;
    }

    VulkanFrameBufferBlueprint::VulkanFrameBufferBlueprint(const VkRenderPass &renderPass, uint32_t width,
                                                           uint32_t height,
                                                           MV::vector<VulkanColorAttachment> attachments,
                                                           uint32_t layers) : m_pValid(true), m_pWidth(width),
                                                                              m_pHeight(height), m_pLayers(layers),
                                                                              m_pRenderPass(renderPass) {
        m_pAttachments.resize(attachments.size());
        for (uint32_t i = 0; i < attachments.size(); i++) m_pAttachments[i] = attachments[i].m_ImageView;
    }

    VulkanFrameBufferBlueprintResult VulkanFrameBufferBlueprint::buildFrameBuffer() {
        assert(m_pValid);
        VulkanFrameBufferBlueprintResult res{};
        auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pData;

        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = m_pRenderPass;
        framebufferInfo.attachmentCount = static_cast<uint32_t>(m_pAttachments.size());
        framebufferInfo.pAttachments = m_pAttachments.data();
        framebufferInfo.width = m_pWidth;
        framebufferInfo.height = m_pHeight;
        framebufferInfo.layers = 1;

        VKCheck(vkCreateFramebuffer(rendererData.m_Device, &framebufferInfo, nullptr, &res.m_FrameBuffer));

        return res;
    }

    VulkanColorAttachment::VulkanColorAttachment(uint32_t id, VkImageView imageView) : ColorAttachment(id),
                                                                                       m_ImageView(imageView) {

    }

    void VulkanFrameBuffer::Init() {
        if (!m_pValid) {
            GetLogger()->Render_LogError("Tried to create invalid framebuffer.");
            return;
        }

        GetLogger()->Render_LogDebug("Creating frame buffer...");
        m_FrameBuffer = m_pBlueprint.buildFrameBuffer().m_FrameBuffer;
        GetLogger()->Render_LogDebug("Created frame buffer.");
    }

    void VulkanFrameBuffer::Cleanup() {
        if (!m_pValid) {
            GetLogger()->Render_LogError("Tried to destroy invalid framebuffer.");
            return;
        }

        GetLogger()->Render_LogDebug("Destroying frame buffer...");

        if (m_FrameBuffer) {
            auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pData;
            vkDestroyFramebuffer(rendererData.m_Device, m_FrameBuffer, nullptr);
        }
        GetLogger()->Render_LogDebug("Destroyed frame buffer.");
    }

    VulkanFrameBuffer::VulkanFrameBuffer(VulkanFrameBufferBlueprint &&blueprint) : m_pBlueprint(MV::move(blueprint)),
                                                                                   FrameBuffer(blueprint.m_pWidth,
                                                                                               blueprint.m_pHeight) {
    }
}

#endif