//
// Created by Matty on 2021-10-05.
//

#include "../../include/render/vulkan/VulkanPipeline.h"
#include "../../include/common/EngineCommon.h"
#include "../../include/render/vulkan/VulkanRenderer.h"
#include "../../include/render/vulkan/VulkanUtil.h"
#include "../../include/render/vulkan/VulkanUniformBuffer.h"
#include "EASTL/sort.h"

namespace MV {
    void VulkanPipeline::Init() {
        if (!m_pValid) {
            GetLogger()->Render_LogError("Attempted to init invalid pipeline");
            return;
        }
        if (!m_pLoaded) {
            GetLogger()->Render_LogDebug("Creating pipeline..");

            try { ;
                buildShaders();

                buildPipeline();
            } catch(std::exception &e) {
                m_pLoaded = false;
                m_pDataLoaded = false;

                GetLogger()->Render_LogError("Failed to load shader pipeline with error: {}", e.what());
                return;
            }
            m_pLoaded = true;
            m_pDataLoaded = true;
            GetLogger()->Render_LogDebug("Successfully created pipeline.");
        }
    }

    void VulkanPipeline::Cleanup() {
        if (!m_pValid) {
            GetLogger()->Render_LogError("Attempted to cleanup invalid pipeline");
            return;
        }
        if (m_pLoaded) {
            GetLogger()->Render_LogDebug("Cleaning up pipeline...");
            m_pDescriptorSets.clear();

            auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pData;

            if (m_pHasUniforms) {
                for (auto &&lay: m_pUniformDescriptorSetLayouts) {
                    vkDestroyDescriptorSetLayout(rendererData.m_Device, lay.second, nullptr);
                }
                vkDestroyDescriptorPool(rendererData.m_Device, m_pDescriptorPool, nullptr);
            }
            vkDestroyPipelineLayout(rendererData.m_Device, m_pLayout, nullptr);
            vkDestroyPipeline(rendererData.m_Device, m_pPipeline, nullptr);
            vkDestroyPipeline(rendererData.m_Device, m_pWireframePipeline, nullptr);
            m_pLoaded = false;
            m_pDataLoaded = false;
            m_pVersion++;
            GetLogger()->Render_LogDebug("Cleaned up pipeline.");
        }
    }

    void VulkanPipeline::Bind(FrameContext &context) {
        vkCmdBindPipeline(context.m_FrameCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_pPipeline);
    }

    void VulkanPipeline::BindWireframe(FrameContext &context) {
        vkCmdBindPipeline(context.m_FrameCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_pWireframePipeline);
    }

    void
    VulkanPipeline::BindPushConstant(FrameContext &context, PushConstantDescription description, const void *data,
                                     size_t size) {
        assert(description.m_Size == size);
        vkCmdPushConstants(context.m_FrameCommandBuffer, m_pLayout, description.m_ShaderStage,
                           description.m_Offset, description.m_Size, data);
    }


    void VulkanPipeline::updateDescriptor(MV::FrameContext &context, MV::UniformDescription &description) {
        assert(description.pipelineID == m_pID);
        if (description.version != m_pVersion) {
            description.descriptorSets.clear();
            for (auto &&ubo: description.uniformBuffers) {
                ubo->Cleanup();
            }
            description.uniformBuffers.clear();
            description.layout = VK_NULL_HANDLE;
        }

        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;
        auto swapImageCount = renderer->m_pFragileData.m_SwapChain.m_SwapChainImages.size();

        if(description.layout == VK_NULL_HANDLE) {
            const auto res = m_pUniformDescriptorSetLayouts.find(description.name);
            Assert(res != m_pUniformDescriptorSetLayouts.end(), "Descritor layout not found in pipeline.");

            description.layout = res->second;
            MV::GetLogger()->Render_LogDebug("Assigned layout to descriptor, layout: {:x}, descriptorCount: {}, name: {}", (uint64_t)description.layout, description.descriptorCount, description.name);
        }

        if (description.descriptorSets.empty()) {
            const auto descSetsRes = m_pDescriptorSets.find(description.layout);

            auto & sets = m_pDescriptorSets[description.layout];

            if(descSetsRes == m_pDescriptorSets.end()) {
                sets.first = 0;
            }

            if (sets.first < sets.second.size()) {
                const uint32_t usableSets = sets.second.size() - sets.first;
                description.descriptorSets.reserve(swapImageCount);

                for (uint32_t i = 0; i < usableSets; i++) {
                    description.descriptorSets.emplace_back(sets.second[sets.first++]);
                }

            }
            if (description.descriptorSets.size() < swapImageCount) {
                const uint32_t setsToCreate = swapImageCount - description.descriptorSets.size();

                MV::vector<VkDescriptorSet> descriptorSets{setsToCreate};

                MV::vector<VkDescriptorSetLayout> layouts(setsToCreate, description.layout);
                VkDescriptorSetAllocateInfo allocInfo{};
                allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
                allocInfo.descriptorPool = m_pDescriptorPool;
                allocInfo.descriptorSetCount = static_cast<uint32_t>(setsToCreate);
                allocInfo.pSetLayouts = layouts.data();

                GetLogger()->Render_LogDebug("Allocating {} sets from pool {:0x}",setsToCreate, (uint64_t)m_pDescriptorPool);
                VKCheck(vkAllocateDescriptorSets(rendererData.m_Device, &allocInfo, descriptorSets.data()));

                description.descriptorSets.reserve(swapImageCount);
                sets.second.reserve(sets.first + setsToCreate);

                for (auto &&desc: descriptorSets) {
                    description.descriptorSets.emplace_back(desc);
                    sets.second.emplace_back(desc);
                }

                sets.first += setsToCreate;

            }
            description.version = m_pVersion;
        }
    }

    void VulkanPipeline::PreBindUniform(FrameContext &context, UniformDescription &description, const void *data,
                                        size_t size, bool update) {
        assert(size == description.size);
        assert(description.type == UniformDescriptorType::BUFFER);

        updateDescriptor(context, description);

        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;
        auto swapImageCount = renderer->m_pFragileData.m_SwapChain.m_SwapChainImages.size();

        if (description.uniformBuffers.empty()) {
            for (uint32_t i = 0; i < swapImageCount; i++) {
                description.uniformBuffers.emplace_back(renderer->CreateUniformBuffer(size));
            }
        }

        if (update) {
            if (description.type == UniformDescriptorType::BUFFER) {
                auto vulkanBufferWrapper = MV::dynamic_pointer_cast<VulkanUniformBuffer>(
                        description.uniformBuffers[context.m_SwapImageIndex]);

                if (vulkanBufferWrapper) {
                    VkDescriptorBufferInfo bufferInfo{};

                    vulkanBufferWrapper->Upload(data, size);

                    bufferInfo.buffer = vulkanBufferWrapper->m_Buffer;
                    bufferInfo.offset = 0;
                    bufferInfo.range = vulkanBufferWrapper->GetSize();

                    VkWriteDescriptorSet descriptorWrite{};
                    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                    descriptorWrite.dstSet = description.descriptorSets[context.m_SwapImageIndex];
                    descriptorWrite.dstBinding = description.binding;
                    descriptorWrite.dstArrayElement = 0;
                    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                    descriptorWrite.descriptorCount = description.descriptorCount;
                    descriptorWrite.pBufferInfo = &bufferInfo;

                    vkUpdateDescriptorSets(rendererData.m_Device, 1, &descriptorWrite, 0, nullptr);
                } else {
                    GetLogger()->Render_LogError("Passed nonvulkan uniform buffer to vulkan pipeline.");
                }
            }
        }
    }


    void VulkanPipeline::PreBindUniform(FrameContext &context, UniformDescription &description,
                                        const MV::rc<Texture> &texture, bool update) {
        assert(description.type == UniformDescriptorType::TEXTURE);

        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;

        updateDescriptor(context, description);

        if (update) {
            if (auto textureWrapper = MV::dynamic_pointer_cast<VulkanTexture>(texture)) {
                VkDescriptorImageInfo imageInfo{};
                imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                imageInfo.imageView = textureWrapper->IsLoaded() ? textureWrapper->m_TextureView : VK_NULL_HANDLE;
                imageInfo.sampler = textureWrapper->IsLoaded() ? textureWrapper->GetSampler() : VK_NULL_HANDLE;

                VkWriteDescriptorSet descriptorWrite{};
                descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
                descriptorWrite.dstSet = description.descriptorSets[context.m_SwapImageIndex];
                descriptorWrite.dstBinding = description.binding;
                descriptorWrite.dstArrayElement = 0;
                descriptorWrite.descriptorType =
                        description.type == UniformDescriptorType::BUFFER ? VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
                                                                          : VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                descriptorWrite.descriptorCount = description.descriptorCount;
                descriptorWrite.pBufferInfo = nullptr;
                descriptorWrite.pImageInfo = &imageInfo;
                descriptorWrite.pTexelBufferView = nullptr; // Optional
                vkUpdateDescriptorSets(rendererData.m_Device, 1, &descriptorWrite, 0, nullptr);
            } else {
                GetLogger()->Render_LogError("Passed nonvulkan texture to vulkan pipeline.");
            }
        }
    }

    VulkanPipeline::VulkanPipeline(PipelineBlueprint &&blueprint) : ShaderPipeline(MV::move(blueprint)) {
        m_pValid = true;
    }

    void VulkanPipeline::buildPipeline() {
        const auto &shaders = getShaders();
        MV::vector<VkPipelineShaderStageCreateInfo> stages(shaders.size());

        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer);
        auto &rendererData = renderer->m_pData;
        auto &rendererFragileData = renderer->m_pFragileData;
        VulkanRenderPass *renderpass = renderer->m_pCommonConfig.m_Editor
                                       ? (VulkanRenderPass *) (rendererFragileData.m_EditorRenderPass.get())
                                       : (VulkanRenderPass *) (rendererFragileData.m_MainRenderPass.get());
        bool createDescriptors = false;

        for (uint32_t i = 0; i < shaders.size(); i++) {
            auto &shader = shaders[i];
            stages.at(i).sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            switch (shader->GetType()) {
                case ShaderType::VERTEX:
                    stages.at(i).stage = VK_SHADER_STAGE_VERTEX_BIT;
                    break;
                case ShaderType::FRAGMENT:
                    stages.at(i).stage = VK_SHADER_STAGE_FRAGMENT_BIT;
                    break;
                case ShaderType::GEOMETRY:
                    stages.at(i).stage = VK_SHADER_STAGE_GEOMETRY_BIT;
                    break;
                case ShaderType::TESSELATION:
                    stages.at(i).stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
                    break;
                case ShaderType::COMPUTE:
                    assert(false); //CANT USE COMPUTE YET
                    break;
                case ShaderType::SIZE:
                    assert(false); //impossible, but has to be because of warning
                    break;
            }
            stages.at(i).module = eastl::static_pointer_cast<VulkanShader>(shader)->m_Module;
            stages.at(i).pName = "main";
        }

        const auto &vertexAttributeDescriptions = getVertexAttributeDescriptions();
        const auto &vertexBindingDescriptions = getVertexBindingDescriptors();

        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(vertexBindingDescriptions.size());

        MV::vector<VkVertexInputBindingDescription> rawBindingData{vertexBindingDescriptions.size()};

        Assert(!vertexBindingDescriptions.empty());
        Assert(!vertexAttributeDescriptions.empty());

        for (uint32_t i = 0; i < rawBindingData.size(); i++) {
            auto &binding = rawBindingData[i];
            binding.binding = vertexBindingDescriptions[i].binding;
            binding.stride = vertexBindingDescriptions[i].stride;
            binding.inputRate = static_cast<VkVertexInputRate>(vertexBindingDescriptions[i].inputRate);
        }

        vertexInputInfo.pVertexBindingDescriptions = rawBindingData.empty() ? nullptr : rawBindingData.data();

        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexAttributeDescriptions.size());

        MV::vector<VkVertexInputAttributeDescription> rawAttributeData{vertexAttributeDescriptions.size()};
        for (uint32_t i = 0; i < rawAttributeData.size(); i++) {
            auto &att = rawAttributeData[i];
            att.location = vertexAttributeDescriptions[i].location;
            att.binding = vertexAttributeDescriptions[i].binding;
            att.format = static_cast<VkFormat>(vertexAttributeDescriptions[i].format);
            att.offset = vertexAttributeDescriptions[i].offset;
        }


        vertexInputInfo.pVertexAttributeDescriptions = rawAttributeData.empty() ? nullptr : rawAttributeData.data();

        VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
        inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssembly.primitiveRestartEnable = VK_FALSE;

        auto swapChainExtent = rendererFragileData.m_SwapChain.m_SwapChainExtent;

        VkViewport viewport{};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        auto renderPassSize = renderpass->GetSize();
        viewport.width = (float) renderPassSize.width;
        viewport.height = (float) renderPassSize.height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor{};
        scissor.offset = {0, 0};
        scissor.extent = renderPassSize;

        VkPipelineViewportStateCreateInfo viewportState{};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.pViewports = &viewport;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor;

        VkPipelineRasterizationStateCreateInfo rasterizer{};
        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE;
        rasterizer.rasterizerDiscardEnable = VK_FALSE;
        rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizer.lineWidth = 1.0f;
        rasterizer.cullMode = VK_CULL_MODE_NONE;
        rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;


        rasterizer.depthBiasEnable = VK_FALSE;
        rasterizer.depthBiasConstantFactor = 0.0f; // Optional
        rasterizer.depthBiasClamp = 0.0f; // Optional
        rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

        VkPipelineMultisampleStateCreateInfo multisampling{};
        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.sampleShadingEnable = VK_FALSE;
        multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisampling.minSampleShading = 1.0f; // Optional
        multisampling.pSampleMask = nullptr; // Optional
        multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
        multisampling.alphaToOneEnable = VK_FALSE; // Optional

        VkPipelineColorBlendAttachmentState colorBlendAttachment{};
        colorBlendAttachment.colorWriteMask =
                VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
                VK_COLOR_COMPONENT_A_BIT;

        colorBlendAttachment.blendEnable = isAlphaBlendingEnabled() ? VK_TRUE : VK_FALSE;
        colorBlendAttachment.srcColorBlendFactor = isAlphaBlendingEnabled() ? VK_BLEND_FACTOR_SRC_ALPHA
                                                                            : VK_BLEND_FACTOR_ONE; // Optional
        colorBlendAttachment.dstColorBlendFactor = isAlphaBlendingEnabled() ? VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
                                                                            : VK_BLEND_FACTOR_ZERO; // Optional
        colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
        colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA; // Optional
        colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA; // Optional
        colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

        VkPipelineColorBlendStateCreateInfo colorBlending{};
        colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlending.logicOpEnable = VK_FALSE;
        colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
        colorBlending.attachmentCount = 1;
        colorBlending.pAttachments = &colorBlendAttachment;
        colorBlending.blendConstants[0] = 0.0f; // Optional
        colorBlending.blendConstants[1] = 0.0f; // Optional
        colorBlending.blendConstants[2] = 0.0f; // Optional
        colorBlending.blendConstants[3] = 0.0f; // Optional

        VkDynamicState dynamicStates[] = {
                VK_DYNAMIC_STATE_VIEWPORT,
                VK_DYNAMIC_STATE_LINE_WIDTH
        };

        VkPipelineDynamicStateCreateInfo dynamicState{};
        dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicState.dynamicStateCount = 2;
        dynamicState.pDynamicStates = dynamicStates;

        const auto &uniforms = getUniformBlueprints();

        MV::vector<eastl::pair<MV::string,UniformBlueprint>> uniformVec{};
        uniformVec.reserve(uniforms.size());

        for(auto && v : uniforms) {
            uniformVec.emplace_back(v);
        }

        eastl::sort(uniformVec.begin(), uniformVec.end(), [](const eastl::pair<MV::string,UniformBlueprint> & a, const eastl::pair<MV::string,UniformBlueprint> & b){
            return a.second.binding > b.second.binding;
        });

        for (auto &&uniform: uniformVec) {
            VkDescriptorSetLayoutBinding uboBindings;

            uboBindings.binding = uniform.second.binding;
            uboBindings.descriptorType =
                    uniform.second.type == UniformDescriptorType::BUFFER
                    ? VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER : VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            uboBindings.descriptorCount = uniform.second.descriptorCount;
            uboBindings.stageFlags = static_cast<VkShaderStageFlags>(uniform.second.shaderStage);
            uboBindings.pImmutableSamplers = nullptr; // TODO


            VkDescriptorSetLayoutCreateInfo layoutInfo{};
            layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            layoutInfo.bindingCount = 1;
            layoutInfo.pBindings = &uboBindings;

            auto &uniformLayout = m_pUniformDescriptorSetLayouts[uniform.first];

            VKCheck(vkCreateDescriptorSetLayout(rendererData.m_Device, &layoutInfo, nullptr,
                                                &uniformLayout));

            createDescriptors = true;
        }

        const auto &pshConstants = getPushConstants();

        MV::vector<VkPushConstantRange> pushConstantInfos(pshConstants.size());

        for (uint32_t i = 0; i < pshConstants.size(); i++) {
            pushConstantInfos.at(i).stageFlags = pshConstants.at(i).second.shaderStage;
            pushConstantInfos.at(i).offset = pshConstants.at(i).second.offset;
            pushConstantInfos.at(i).size = pshConstants.at(i).second.size;
        }

        MV::vector<VkDescriptorSetLayout> uniformLayouts{m_pUniformDescriptorSetLayouts.size()};

        auto it = m_pUniformDescriptorSetLayouts.cbegin();
        for (uint32_t i = 0; i < m_pUniformDescriptorSetLayouts.size(); i++) {
            uniformLayouts[i] = it->second;
            it++;
        }

        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = uniformLayouts.size();
        pipelineLayoutInfo.pSetLayouts = uniformLayouts.data();
        pipelineLayoutInfo.pushConstantRangeCount = pushConstantInfos.size();
        pipelineLayoutInfo.pPushConstantRanges = pshConstants.empty() ? nullptr : pushConstantInfos.data();

        VKCheck(vkCreatePipelineLayout(rendererData.m_Device, &pipelineLayoutInfo, nullptr, &m_pLayout))

        VkGraphicsPipelineCreateInfo pipelineInfo{};
        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = static_cast<uint32_t>(stages.size());
        pipelineInfo.pStages = stages.data();
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &inputAssembly;
        pipelineInfo.pViewportState = &viewportState;
        pipelineInfo.pRasterizationState = &rasterizer;
        pipelineInfo.pMultisampleState = &multisampling;
        pipelineInfo.pColorBlendState = &colorBlending;
        pipelineInfo.pDynamicState = nullptr; // Optional
        pipelineInfo.layout = m_pLayout;
        //pipelineInfo.pDepthStencilState = m_pHasDepthStencilState ? &m_pDepthStencilState : nullptr; FIXME
        pipelineInfo.pDepthStencilState = nullptr;
        pipelineInfo.renderPass = renderpass->m_RenderPass;
        pipelineInfo.subpass = 0;

        pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
        pipelineInfo.basePipelineIndex = -1; // Optional

        VKCheck(vkCreateGraphicsPipelines(rendererData.m_Device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr,
                                          &m_pPipeline))

        auto wireframePipeline = pipelineInfo;

        auto wireframeRasterizer = rasterizer;

        wireframeRasterizer.polygonMode = VK_POLYGON_MODE_LINE;
        wireframePipeline.pRasterizationState = &wireframeRasterizer;

        VKCheck(vkCreateGraphicsPipelines(rendererData.m_Device, VK_NULL_HANDLE, 1, &wireframePipeline, nullptr,
                                          &m_pWireframePipeline))

        if (createDescriptors) {
            uint32_t swapImageCount = rendererFragileData.m_SwapChain.m_SwapChainImages.size();

            MV::vector<VkDescriptorPoolSize> poolSizes{uniforms.size()};
            for (uint32_t i = 0; i < uniforms.size(); i++) {
                poolSizes.at(i).type = uniforms.at(i).second.type == UniformDescriptorType::BUFFER
                                       ? VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER : VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                poolSizes.at(i).descriptorCount = getCacheSize() * swapImageCount;
            }

            VkDescriptorPoolCreateInfo poolInfo{};
            poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
            poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
            poolInfo.pPoolSizes = poolSizes.data();
            poolInfo.maxSets = static_cast<uint32_t>(poolSizes.size() * getCacheSize() * swapImageCount);

            VKCheck(vkCreateDescriptorPool(rendererData.m_Device, &poolInfo, nullptr, &m_pDescriptorPool))

            m_pHasUniforms = true;
        }

        for (auto &&shader: shaders) {
            shader->Cleanup();
        }
    }

    void VulkanPipeline::buildShaders() {
        for (auto &&v: getShaders()) {
            if (!v->IsLoaded())
                v->Init();
        }
    }

    void VulkanPipeline::ResetBatchCounters() {
        for (auto &&val: m_pBatchUniforms) {
            val.second.first = 0;
        }
    }

    UniformDescription &VulkanPipeline::GetBatchUniform(MV::string_view name) {
        if (m_pBatchUniforms.find(name.data()) == m_pBatchUniforms.end()) {
            m_pBatchUniforms[name.data()]; // Creates new uniform group
        }

        eastl::pair<uint32_t, MV::vector<UniformDescription>> &batch = m_pBatchUniforms[name.data()];

        uint32_t &currentBatchIndex = batch.first;

        // Is batch fully used?
        if (currentBatchIndex == batch.second.size()) {
            batch.second.push_back(MV::move(CreateUniform(name)));
        }

        return batch.second[currentBatchIndex++];
    }

    PushConstantDescription VulkanPipeline::GetMVPPushConstant() {
        Assert(m_pBlueprint.m_pPushConstants.count("MVP") > 0, "No MVP push constant in pipeline");
        const auto &val = m_pBlueprint.m_pPushConstants["MVP"];
        return {val.shaderStage, val.offset, val.size};
    }

    void VulkanPipeline::BindUniforms(FrameContext &context,
                                      const MV::vector<eastl::reference_wrapper<UniformDescription>> &uniforms) {
        MV::vector<VkDescriptorSet> sets{uniforms.size()};

        for (uint32_t i = 0; i < sets.size(); i++) {
            const auto set = uniforms[i].get().descriptorSets[context.m_SwapImageIndex];
            sets[i] = set;
        }

        vkCmdBindDescriptorSets(context.m_FrameCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                                m_pLayout, 0, sets.size(), sets.data(),
                                0, nullptr);

    }

}