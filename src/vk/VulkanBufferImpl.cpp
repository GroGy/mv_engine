//
// Created by Matty on 2021-10-15.
//

#include "../../include/render/vulkan/VulkanBufferImpl.h"
#include "../../include/common/EngineCommon.h"
#include "../../include/render/vulkan/VulkanRenderer.h"
#include "../../include/render/vulkan/VulkanUtil.h"

namespace MV {
    void VulkanBufferImpl::allocate(VkBufferCreateInfo bufferInfo, VmaAllocationCreateInfo allocInfo) {
        assert(!m_pMarkedForDealloc);
        if(m_Buffer) return;
        GetLogger()->Render_LogDebug("Allocating vulkan buffer...");
        auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetRenderer())->m_pData;

        auto res = vmaCreateBuffer(rendererData.m_Allocator, &bufferInfo, &allocInfo, &m_Buffer, &m_pAllocation,
                                   &m_pAllocInfo);
        VKCheck(res);

        GetLogger()->Render_LogDebug("Successfully allocated vulkan buffer.");
    }

    void VulkanBufferImpl::deallocate() {
        GetLogger()->Render_LogDebug("Destroying vulkan buffer...");
        auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetRenderer())->m_pData;
        vmaDestroyBuffer(rendererData.m_Allocator, m_Buffer, m_pAllocation);
        GetLogger()->Render_LogDebug("Successfully destroyed vulkan buffer.");
    }

    void VulkanBufferImpl::loadData(const void *data, size_t size) {
        assert(!m_pMarkedForDealloc);

        auto &rendererData = MV::static_pointer_cast<VulkanRenderer>(GetRenderer())->m_pData;

        VulkanUtil::LoadDataToAllocation(rendererData.m_Allocator,m_pAllocation,m_pAllocInfo,data,size);
    }

    void VulkanBufferImpl::copyTo(VulkanBufferImpl &other) {
        assert(!m_pMarkedForDealloc);

        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;

        renderer->ExecuteSingleCommand([&](VkCommandBuffer buffer) {
            VkBufferCopy copyRegion{};
            copyRegion.size = m_pDataSize;
            vkCmdCopyBuffer(buffer, m_Buffer, other.m_Buffer, 1, &copyRegion);
        });
    }

    void VulkanBufferImpl::copyFrom(VulkanBufferImpl &other, size_t size) {
        assert(!m_pMarkedForDealloc);

        auto renderer = MV::static_pointer_cast<VulkanRenderer>(GetRenderer());
        auto &rendererData = renderer->m_pData;

        renderer->ExecuteSingleCommand([&](VkCommandBuffer buffer) {
            VkBufferCopy copyRegion{};
            copyRegion.size = size == 0 ? other.m_pDataSize : size;
            vkCmdCopyBuffer(buffer, other.m_Buffer, m_Buffer, 1, &copyRegion);
        });
    }
}
