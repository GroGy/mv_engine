//
// Created by Matty on 2021-10-17.
//

#include "../../include/render/vulkan/VulkanTexture.h"
#include "../../include/common/EngineCommon.h"

namespace MV {
    void VulkanTexture::Upload(const MV::rc<StagingBuffer> &staging) {
        if (auto buf = MV::dynamic_pointer_cast<VulkanBufferImpl>(staging)) {
            copyFrom(*buf);
            m_pLoaded = true;
            m_pDataLoaded = true;
        }
    }

    void VulkanTexture::Init() {
        VkImageCreateInfo imageInfo{};
        imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageInfo.imageType = VK_IMAGE_TYPE_2D;
        imageInfo.extent.width = m_pBlueprint.m_Width;
        imageInfo.extent.height = m_pBlueprint.m_Height;
        imageInfo.extent.depth = 1;
        imageInfo.mipLevels = m_pBlueprint.m_Mipmaps;
        imageInfo.arrayLayers = 1;
        imageInfo.format = static_cast<VkFormat>(m_pBlueprint.m_Format);
        imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        switch (m_pBlueprint.m_Usage) {
            case TextureUsage::TEXTURE: {
                imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
                break;
            }
            case TextureUsage::ATTACHMENT: {
                imageInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
                break;
            }
        }

        imageInfo.samples = static_cast<VkSampleCountFlagBits>(m_pBlueprint.m_Samples);

        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        VulkanSamplerInfo samplerInfo{};

        samplerInfo.m_MaxAnisotropy = m_pBlueprint.m_MaxAnisotropy;
        samplerInfo.m_EnableAnisotropy = m_pBlueprint.m_EnableAnisotropy;
        samplerInfo.m_AddressingMode = m_pBlueprint.m_AddressingMode;
        samplerInfo.m_MagFilter = m_pBlueprint.m_Filter;
        samplerInfo.m_MinFilter = m_pBlueprint.m_Filter;

        allocate(imageInfo, allocInfo, samplerInfo);
    }

    void VulkanTexture::Cleanup() {
        markForDealloc();
    }

    VulkanTexture::VulkanTexture(TextureBlueprint &&blueprint) : Texture(MV::move(blueprint)),
                                                                 VulkanTextureImpl(blueprint.m_Width,
                                                                                   blueprint.m_Height) {

    }

    void VulkanTexture::TransitionToLayout(VkImageLayout layout) {
        transitionImageLayout(m_TextureLayout, layout);
    }
}