//
// Created by Matty on 2021-10-05.
//

#include "../../include/render/vulkan/VulkanShader.h"
#include "../../include/common/EngineCommon.h"
#include "../../include/render/vulkan/VulkanRenderer.h"
#include "../../include/common/ExceptionCommon.h"
#include "../../include/util/FileUtil.h"
#include "../../include/render/vulkan/VulkanUtil.h"

namespace MV {
    void VulkanShader::Init() {
        GetLogger()->Render_LogDebug("Loading shader ", m_pPath);
        auto & rendererData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pData;

        if(!m_pValid) {
            throw MV::RuntimeException("Attempted to load invalid shader");
        }

        if (!MV::FileUtil::FileExists(m_pPath)) {
            throw MV::RuntimeException(MV::string("Shader file not found : " + m_pPath));
        }

        std::ifstream file(m_pPath.c_str(), std::ios::ate | std::ios::binary);

        if (!file.is_open()) {
            throw MV::RuntimeException(MV::string("Failed to open shader file : "  + m_pPath));
        }

        size_t fileSize = (size_t) file.tellg();
        MV::vector<char> buffer(fileSize);
        file.seekg(0);
        file.read(buffer.data(), fileSize);
        file.close();

        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = buffer.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(buffer.data());

        VulkanUtil::CheckVkResult(vkCreateShaderModule(rendererData.m_Device, &createInfo, nullptr, &m_Module));
        m_pLoaded = true;
        GetLogger()->Render_LogDebug("Successfully loaded shader {}", m_pPath);
    }

    void VulkanShader::Cleanup() {
        auto & rendererData = MV::static_pointer_cast<VulkanRenderer>(GetEngine()->m_Renderer)->m_pData;
        m_pLoaded = false;
        vkDestroyShaderModule(rendererData.m_Device,m_Module,nullptr);
    }

    VulkanShader::VulkanShader(MV::string path, ShaderType type) : Shader(MV::move(path), type){
    }

}