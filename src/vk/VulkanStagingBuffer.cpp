//
// Created by Matty on 2021-10-15.
//

#include "../../include/render/vulkan/VulkanStagingBuffer.h"

namespace MV {
    void VulkanStagingBuffer::Init() {

        VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
        bufferInfo.size = m_pSize;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

        VmaAllocationCreateInfo allocInfo = {};
        allocInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
        allocate(bufferInfo,allocInfo);
    }

    void VulkanStagingBuffer::Cleanup() {
        markForDealloc();
    }

    void VulkanStagingBuffer::Upload(const void *data, size_t size) {
        loadData(data,size);
        m_pDataSize = size;
    }

    VulkanStagingBuffer::VulkanStagingBuffer(size_t size) : StagingBuffer(size) {
        m_pDataSize = 0;
    }

    VkBuffer VulkanStagingBuffer::GetBuffer() {
        return m_Buffer;
    }
}
