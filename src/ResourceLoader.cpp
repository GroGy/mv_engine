//
// Created by Matty on 2021-10-15.
//

#include <cassert>
#include "../include/resources/common/ResourceLoader.h"
#include "../include/common/EngineCommon.h"
#include "../include/profiler/Profiler.h"

namespace MV {
    ResourceLoader::ResourceLoader(ResourceQueue resourceQueue,
                                   ResourceQueue finishedQueue)
            : m_pFinishedResourcesQueue(MV::move(finishedQueue)), m_pResourceQueue(MV::move(resourceQueue)),
              m_pValid(true) {

    }

    void ResourceLoader::Cleanup() {
        if (!m_pRun) return;
        m_pRun = false;

        for (uint32_t i = 0; i < 5; i++) {
            m_pResourceQueue->wake_cv();
            std::this_thread::sleep_for(std::chrono::milliseconds(40));
        }
        m_pResourceQueue->wake_cv();
        m_pWorkerThread.join();
    }

    void ResourceLoader::Init() {
        assert(m_pValid);
        m_pWorkerThread = std::thread{
                [&]() {
                    MV_PROFILE_THREAD("Worker thread");
                    processLoop();
                }
        };
    }

    void ResourceLoader::processLoop() {
        while (m_pRun) {
            try {
                auto resource = MV::move(m_pResourceQueue->pop());
                MV_PROFILE_FUNCTION("ResourceLoader process")
                try {
                    auto result = resource->Process();
                    if (result != ResourceLoadResult::SUCCESS) {
                        resource->m_pSuccess = false;
                        MV::GetLogger()->LogError("Failed to load resource {}.\n\tError: {}", resource->m_pPath,
                                                  resource->Error());
                    }
                    m_pFinishedResourcesQueue->push(MV::move(resource));
                }
                catch (std::exception &e) {
                    MV::GetLogger()->LogError("Failed to load resource {}.\n\tError: {}", resource->m_pPath,
                                              e.what());
                }
            } catch (std::exception &e) {
                (void) e;
            }
        }
    }
}
