//
// Created by Matty on 2022-02-11.
//

#include "../include/scripting/ScriptingModule.h"

namespace MV {

    size_t ScriptingModule::GetPropertyIndex(MonoObject *worldObject, MonoObject *boxCollider, World &world,
                                             ScriptManager &scriptManager) {
        size_t index;
        uint64_t woID;
        ScriptClassField monoField;

        FindObjectField(worldObject, boxCollider, world, scriptManager, woID, monoField);

        const auto &woFieldProperties = world.m_WorldObjectsFields[woID];

        bool foundIndex = false;

        for (auto &&v: woFieldProperties) {
            if (v.first == monoField.m_Field) {
                index = v.second;
                foundIndex = true;
                break;
            }
        }

        ENSURE_TRUE(foundIndex, 0);

        return index;
    }

    void ScriptingModule::FindObjectField(MonoObject *worldObject, MonoObject *boxCollider, World &world,
                                          ScriptManager &scriptManager, uint64_t &woID, ScriptClassField &monoField) {
        ScriptClass woClass = scriptManager.GetClass("WorldObject");
        ScriptObjectInstance obj = ScriptObjectInstance::CreateFromMonoObject(worldObject);

        const ScriptClassField *field = woClass.FindField("_id");

        ENSURE_VALID(field);

        woClass.GetFieldValue(&woID, obj, *field);
        bool found = false;

        auto cachedField = world.m_ObjectFieldCache.find(boxCollider);

        if (cachedField == world.m_ObjectFieldCache.end()) {
            const auto &woFields = world.m_WorldObjectsFields.at(woID);
            Script script{mono_class_get_name(mono_object_get_class(worldObject))};
            script.Init();

            script.SetInstance(move(obj));

            for (auto &&f: script.GetFields()) {
                if (f.second.m_Type != ScriptClassField::Type::PROPERTY) continue;
                Script fieldScript = script.GetFieldScript(f.second);
                const auto *fieldObj = fieldScript.GetObjectInstance().GetMonoObject();

                if (fieldObj == boxCollider) {
                    monoField = f.second;
                    world.m_ObjectFieldCache[boxCollider] = f.second;
                    found = true;
                    break;
                }
            }
        } else {
            monoField = cachedField->second;
            found = true;
        }

        ENSURE_TRUE(found);
    }

    MV::Property *ScriptingModule::GetPropertyRaw(MonoObject *worldObject, MonoObject *boxCollider) {
        const auto engine = GetEngine();
        auto & world = engine->GetWorld();
        auto & sm = engine->m_ScriptManager;
        ENSURE_TRUE(world.IsValid(), nullptr);

        size_t index;
        uint64_t woID;
        ScriptClassField monoField;

        FindObjectField(worldObject, boxCollider, world, sm, woID, monoField);

        bool foundIndex = false;
        const auto &woFieldProperties = world.m_WorldObjectsFields[woID];

        for (auto &&v: woFieldProperties) {
            if (v.first == monoField.m_Field) {
                index = v.second;
                foundIndex = true;
                break;
            }
        }

        ENSURE_TRUE(foundIndex, 0);


        auto * storage = world.m_PropertyManager.GetIStorageFromToken(monoField.m_Class);
        auto * res = storage->get_property(index);
        return res;
    }
}