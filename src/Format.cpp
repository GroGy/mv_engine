//
// Created by Matty on 2022-03-08.
//

#include "../include/common/Format.h"

namespace MV {
    void from_json(const json &j, string &p) {
        p = j.get<std::string>().c_str();
    }

    void to_json(json &j, const string &p) {
        j = p.c_str();
    }
}

int Vsnprintf8(char *pDestination, size_t n, const char *pFormat, va_list arguments) {
    return std::vsnprintf(pDestination, n, pFormat, arguments);
}

int Vsnprintf16(char16_t *pDestination, size_t n, const char16_t *pFormat, va_list arguments) {
    return 0;
}

int Vsnprintf32(char32_t *pDestination, size_t n, const char32_t *pFormat, va_list arguments) {
    return 0;
}

int VsnprintfW(wchar_t *pDestination, size_t n, const wchar_t *pFormat, va_list arguments) {
    return 0;
}

namespace glm {
    void to_json(json &j, const vec2 &p) {
        j = json::array({p.x, p.y});
    }

    void from_json(const json &j, vec2 &p) {
        p.x = j.at(0).get<float>();
        p.y = j.at(1).get<float>();
    }
}