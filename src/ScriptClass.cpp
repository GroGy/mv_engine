//
// Created by Matty on 2022-02-06.
//

#include "../include/scripting/ScriptClass.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    static void createMonoObject(MonoObject *& result, uint32_t & handle,MonoDomain * domain, MonoClass * klass) {
        result = mono_object_new(domain, klass);
        handle = mono_gchandle_new(result, false);
        Assert(result != nullptr, "mono_object_new failed!");
        mono_runtime_object_init(result);
    }

    ScriptObjectInstance ScriptClass::CreateObject() {
        auto & sm = GetEngine()->m_ScriptManager;
        ScriptObjectInstance res{};
        createMonoObject(res.m_pObject,res.m_pGCHandle,sm.m_pMonoAppDomain,m_pClass);
        sm.registerGCHandle(res.m_pGCHandle);
        for(auto && f : m_pFields) {
            if(f.second.m_Type == ScriptClassField::Type::PROPERTY) {
                MonoObject * tmp = nullptr;
                uint32_t handle;
                auto klass = mono_type_get_class(mono_field_get_type(f.second.m_Field));

                bool hasPropertyAsParent = false;

                MV::string parentName;
                MonoClass * parent = klass;

                do {
                    parent = mono_class_get_parent(parent);
                    parentName = mono_class_get_name(parent);
                    if(parentName == "Property") {
                        hasPropertyAsParent = true;
                        break;
                    }
                } while (parentName != "Object");

                ENSURE_TRUE(hasPropertyAsParent,{}); // All properties must have MV.Property as their parent

                createMonoObject(tmp,handle, sm.m_pMonoAppDomain, klass);

                MonoClassField * ownerField = mono_class_get_field_from_name(klass,"_owner");

                ENSURE_VALID(ownerField, {});

                mono_field_set_value(tmp, ownerField, res.m_pObject);

                res.m_pPropertyGCHandles.emplace_back(handle);
                sm.registerGCHandle(handle);
                mono_field_set_value(res.m_pObject,f.second.m_Field, tmp);
            }
        }
        return res;
    }

    void ScriptClass::init() {
        initFunctions();
        initFields();
    }

    void ScriptClass::initFunctions() {
        m_pFunctions.clear();

        if (m_pClassVersion != GetEngine()->m_ScriptManager.m_pDomainVersion && !m_pClassName.empty()) {
            m_pClass = GetEngine()->m_ScriptManager.GetClass(m_pClassName).m_pClass;
        }
        MonoClass *parent = m_pClass;

        if(m_pClassName.empty())
            m_pClassName = mono_class_get_name(parent);

        MV::string parentName;

        do {
            initClassFunctions(parent);
            parent = mono_class_get_parent(parent);
            parentName = mono_class_get_name(parent);
        } while (parentName != "Object");
    }

    void ScriptClass::CallFunction_Void(const ScriptObjectInstance &instance, const MV::string_view name) {
        checkVersion();
        const auto &foundFN = m_pFunctions.find(MV::string(name.data()));

        if (foundFN != m_pFunctions.end()) {
            [[maybe_unused]]
            MonoObject *result = mono_runtime_invoke(foundFN->second, instance.m_pObject, nullptr,
                                                     nullptr);
        }
    }

    void
    ScriptClass::CallFunction_Void_Float(const ScriptObjectInstance &instance, const MV::string_view name, float val) {
        checkVersion();
        void *args[1];
        args[0] = &val;
        const auto &foundFN = m_pFunctions.find(name.data());

        if (foundFN != m_pFunctions.end()) {
            [[maybe_unused]]
            MonoObject *result = mono_runtime_invoke(foundFN->second, instance.m_pObject, args,
                                                     nullptr);
        }
    }

    void ScriptClass::initFields() {
        m_pFields.clear();

        MonoClass *parent = m_pClass;
        MV::string parentName;

        do {
            initClassFields(parent);
            parent = mono_class_get_parent(parent);
            parentName = mono_class_get_name(parent);
        } while (parentName != "Object");

        m_pClassVersion = GetEngine()->m_ScriptManager.m_pDomainVersion;
    }

    void ScriptClass::initClassFunctions(MonoClass *klass) {
        void *iter = nullptr;
        MonoMethod *method = nullptr;
        while ((method = mono_class_get_methods(klass, &iter)) != nullptr) {
            MV::string name = mono_method_get_name(method);

            const auto &foundFN = m_pFunctions.find(name.data());

            if (foundFN == m_pFunctions.end())
                m_pFunctions[name] = method;
        }
    }

    void ScriptClass::initClassFields(MonoClass *klass) {
        void *iter = nullptr;
        MonoClassField *field = nullptr;
        while ((field = mono_class_get_fields(klass, &iter)) != nullptr) {
            MV::string name = mono_field_get_name(field);
            const auto &found = m_pFields.find(name);
            if (found == m_pFields.end()) {
                ScriptClassField f;
                f.m_Field = field;
                f.m_FieldName = name;
                f.m_ShowInEditor = false;
                const auto monoType = mono_field_get_type(field);

                auto attrs = mono_custom_attrs_from_field(klass, field);

                if (attrs != nullptr) {
                    for (size_t i = 0; i < attrs->num_attrs; ++i) {
                        MonoCustomAttrEntry *centry = &attrs->attrs[i];
                        if (centry->ctor == NULL)
                            continue;
                        MonoClass *attClass = mono_method_get_class(centry->ctor);

                        if (attClass) {
                            MV::string attClassName = mono_class_get_name(attClass);
                            MV::string cmpVal = "ShowInEditor";

                            if (attClassName == cmpVal) {
                                f.m_ShowInEditor = true;
                            }
                        }
                    }
                    mono_custom_attrs_free(attrs);
                }

                auto typeID = mono_type_get_type(monoType);

                auto e = static_cast<MonoTypeEnum>(typeID);

                switch (typeID) {
                    case MONO_TYPE_BOOLEAN: {
                        f.m_Type = ScriptClassField::Type::BOOL;
                        break;
                    }
                    case MONO_TYPE_CHAR: {
                        f.m_Type = ScriptClassField::Type::CHAR;
                        break;
                    }
                    case MONO_TYPE_U1: {
                        f.m_Type = ScriptClassField::Type::U8;
                        break;
                    }
                    case MONO_TYPE_U2: {
                        f.m_Type = ScriptClassField::Type::U16;
                        break;
                    }
                    case MONO_TYPE_U4: {
                        f.m_Type = ScriptClassField::Type::U32;
                        break;
                    }
                    case MONO_TYPE_U8: {
                        f.m_Type = ScriptClassField::Type::U64;
                        break;
                    }
                    case MONO_TYPE_I1: {
                        f.m_Type = ScriptClassField::Type::I8;
                        break;
                    }
                    case MONO_TYPE_I2: {
                        f.m_Type = ScriptClassField::Type::I16;
                        break;
                    }
                    case MONO_TYPE_I4: {
                        f.m_Type = ScriptClassField::Type::I32;
                        break;
                    }
                    case MONO_TYPE_I8: {
                        f.m_Type = ScriptClassField::Type::I64;
                        break;
                    }
                    case MONO_TYPE_R4: {
                        f.m_Type = ScriptClassField::Type::F32;
                        break;
                    }
                    case MONO_TYPE_R8: {
                        f.m_Type = ScriptClassField::Type::F64;
                        break;
                    }
                    case MONO_TYPE_STRING: {
                        f.m_Type = ScriptClassField::Type::STRING;
                        break;
                    }
                    case MONO_TYPE_ARRAY: {
                        f.m_Type = ScriptClassField::Type::ARRAY;
                        break;
                    }
                    case MONO_TYPE_CLASS: {
                        //TODO: Check if its supported, else just dont show or something
                        f.m_Type = ScriptClassField::Type::CLASS;
                        break;
                    }
                    case MONO_TYPE_VALUETYPE: {
                        f.m_Type = ScriptClassField::Type::CLASS;
                        break;
                    }
                    default: {
                        f.m_Type = ScriptClassField::Type::UNKNOWN;
                        break;
                    }
                }

                m_pFields[name] = f;
            }
        }
/*
        iter = nullptr;
        MonoProperty* property = nullptr;
        while((property = mono_class_get_properties(klass, &iter)) != nullptr) {
            MV::string propertyName = mono_property_get_name(property);
            MV::GetLogger()->LogDebug("Property found: ", propertyName);
        }*/
    }

    const MV::unordered_map<MV::string, ScriptClassField> &ScriptClass::GetFields() const {
        return m_pFields;
    }

    void ScriptClass::initFieldOfClassType() {
        for (auto &&field: m_pFields) {
            auto &data = field.second;
            if (data.m_Type != ScriptClassField::Type::CLASS)
                continue;

            auto monoType = mono_field_get_type(data.m_Field);
            const auto klass = mono_type_get_class(monoType);

            if (!klass) {
                data.m_Type = ScriptClassField::Type::UNKNOWN;
                continue;
            }

            const auto typeToken = mono_class_get_type_token(klass);

            //Is registred?
            if (!ScriptManager::s_GlobalInstance->IsClassTokenValid(typeToken)) {
                data.m_Type = ScriptClassField::Type::UNKNOWN;
                MV::GetLogger()->Script_LogError("{} is not valid property!", mono_class_get_name(klass));
                continue;
            }

            if (typeToken == ScriptManager::s_GlobalInstance->GetClassToken("Vector2")) {
                data.m_Type = ScriptClassField::Type::VEC2;
                continue;
            }

            data.m_Type = ScriptClassField::Type::PROPERTY;
            field.second.m_Class = typeToken;
        }
    }

    void ScriptClass::SetFieldValue(const ScriptObjectInstance &instance, MV::string_view fieldName, void *val) {
        mono_field_set_value(instance.m_pObject, m_pFields[MV::string(fieldName)].m_Field, val);
    }

    void ScriptClass::checkVersion() {
        if (m_pClassVersion != GetEngine()->m_ScriptManager.m_pDomainVersion) {
            initFunctions();
            initFields();
            initFieldOfClassType();
        }
    }

    ScriptObjectInstance ScriptClass::GetFieldClassObject(const ScriptObjectInstance &instance, const ScriptClassField &field) {
        ScriptObjectInstance res;
        res.m_pObject = mono_field_get_value_object(GetEngine()->m_ScriptManager.m_pMonoAppDomain, field.m_Field, instance.m_pObject);
        res.m_pMonoCreatedObject = true;
        return res;
    }

    MonoString *ScriptClass::convertToMonoString(MV::string_view string) {
        return mono_string_new(MV::GetEngine()->m_ScriptManager.m_pMonoDomain,string.data());
    }

    const ScriptClassField *ScriptClass::FindField(MV::string_view name) const {
        for(const auto & field : m_pFields) {
            if(field.first == name) {
                return &field.second;
            }
        }

        return nullptr;
    }

    bool ScriptClass::IsChildOfClass(const ScriptClass &other) {
        MonoClass *parent = m_pClass;
        MV::string parentName;

        if(mono_class_get_type_token(m_pClass) == mono_class_get_type_token(other.m_pClass))
            return false;

        do {
            parent = mono_class_get_parent(parent);
            parentName = mono_class_get_name(parent);

            if(mono_class_get_type_token(parent) == mono_class_get_type_token(other.m_pClass)) {
                return true;
            }
        } while (parentName != "Object");

        return false;
    }
}