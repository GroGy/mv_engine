//
// Created by Matty on 2022-09-23.
//

#include "2d/ColliderProperty.h"


namespace MV {
    void ColliderProperty::ApplyForceToCenter(const glm::vec2 &force) {
        ENSURE_VALID(m_pFixture);

        m_pFixture->GetBody()->ApplyForceToCenter({force.x * sc_ForceMultiplier, force.y * sc_ForceMultiplier},true);
    }

    void ColliderProperty::ApplyForce(const glm::vec2 &force,const glm::vec2 &point) {
        ENSURE_VALID(m_pFixture);

        m_pFixture->GetBody()->ApplyForce({force.x * sc_ForceMultiplier, force.y * sc_ForceMultiplier},{point.x, point.y},true);
    }

    void ColliderProperty::AllowSleep(bool value) {
        m_pFixture->GetBody()->SetSleepingAllowed(value);
    }
}
