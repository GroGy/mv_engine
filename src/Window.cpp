//
// Created by Matty on 2021-10-01.
//

#include "../include/render/common/Window.h"
#include "../include/common/EngineCommon.h"
#include "../include/common/ExceptionCommon.h"

#include <GLFW/glfw3.h>

namespace MV {
    MV::string Window::GetWindowName() const {
        return m_pWindowName;
    }

    void Window::SetWindowName(const MV::string & name) {
        m_pWindowName = name;
    }

    uint32_t Window::m_pWindowHeight;
    uint32_t Window::m_pWindowWidth;

    WindowBuilder::WindowBuilder(uint32_t width, uint32_t height) : m_pHeight(height), m_pWidth(width) {

    }

    MV::rc<WindowBuilder> WindowBuilder::setWindowName(const MV::string &name) {
        m_pWindowName = name;
        m_pHasName = true;
        return shared_from_this();
    }

    MV::rc<WindowBuilder> WindowBuilder::setBackgroundColor(const glm::vec4 &bgColor) {
        m_pBackgroundColor = bgColor;
        m_pHasColor = true;
        return shared_from_this();
    }

    Window *WindowBuilder::finish() {
        glfwInit();

        if (!m_pHasColor) {
            throw RuntimeException("Background color is not set!");
        }

        if (!m_pHasName) {
            throw RuntimeException("Window name is not set!");
        }

        auto finished = new MV::Window();
        finished->m_pBackgroundColor = m_pBackgroundColor;
        //RenderData::GetInstance()->m_ClearColor = finished->m_pBackgroundColor;

        MV::Window::m_pWindowWidth = m_pWidth;
        MV::Window::m_pWindowHeight = m_pHeight;

        finished->m_pWindowName = m_pWindowName;
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, m_pEnableResize ? GLFW_TRUE : GLFW_FALSE);
        glfwWindowHint(GLFW_MAXIMIZED, m_pMaximized ? GLFW_TRUE : GLFW_FALSE);
        GLFWwindow* window = glfwCreateWindow((int32_t) MV::Window::m_pWindowWidth,
                                              (int32_t) MV::Window::m_pWindowHeight, finished->m_pWindowName.c_str(), nullptr,
                                              nullptr);
        if (window==nullptr) {
            glfwTerminate();
            throw RuntimeException("Failed to initialize GLFW Window");
        }
        glfwMakeContextCurrent(window);
        glfwSwapInterval( 1 );
        glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, int32_t width, int32_t height) {
            auto& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
            MV::Window::m_pWindowWidth = width;
            MV::Window::m_pWindowHeight = height;
        });

        finished->m_pWindowImpl = window;

        return finished;
    }

    MV::rc<WindowBuilder> WindowBuilder::setResizable(bool resizable) {
        m_pEnableResize = resizable;
        return shared_from_this();
    }

    MV::rc<WindowBuilder> WindowBuilder::setMaximized(bool maximized) {
        m_pMaximized = maximized;
        return shared_from_this();
    }

    void *Window::GetContext() const {
        return m_pWindowImpl;
    }

    void Window::cleanup() {
        glfwDestroyWindow((GLFWwindow*)m_pWindowImpl);
    }
}