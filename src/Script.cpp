//
// Created by Matty on 2022-02-06.
//

#include "../include/scripting/Script.h"
#include "../include/common/EngineCommon.h"


namespace MV {
    void Script::CreateInstance() {
        auto & manager = GetEngine()->m_ScriptManager;

        //Destroys old instance
        m_pInstance.Destroy();

        if(manager.m_pDomainVersion != m_pClassVersion) {
            Init();
        }

        m_pInstance = m_pClass.CreateObject();
    }

    Script::Script(MV::string_view className,ScriptType type) : m_pType(type), m_pName(MV::move(className)) {

    }

    void Script::Init() {
        auto & manager = GetEngine()->m_ScriptManager;

        if(manager.m_pDomainVersion != m_pClassVersion) {
            m_pClass = manager.GetClass(m_pName);
            m_pClassVersion = manager.m_pDomainVersion;
        }
    }

    void Script::CallFunction_Void(const MV::string_view name) {
        Assert(IsInstanceValid(), "Instance has to be valid to be able to call functions!");
        m_pClass.CallFunction_Void(m_pInstance, name);
    }

    void Script::CallFunction_Void_Float(const MV::string_view name, float val) {
        Assert(IsInstanceValid(), "Instance has to be valid to be able to call functions!");
        m_pClass.CallFunction_Void_Float(m_pInstance, name,val);
    }

    const MV::unordered_map<MV::string, ScriptClassField> &Script::GetFields() const {
        return m_pClass.GetFields();
    }

    void Script::Destroy() {
        m_pInstance.Destroy();
    }

    void Script::SetFieldRaw(MV::string_view fieldName, void *val) {
        m_pClass.SetFieldValue(m_pInstance,fieldName,val);
    }

    bool Script::IsInstanceValid() const {
        return m_pInstance.IsValid() && m_pClassVersion == GetEngine()->m_ScriptManager.m_pDomainVersion;
    }

    const ScriptObjectInstance & Script::GetObjectInstance() const {
        return m_pInstance;
    }

    Script Script::GetFieldScript(const ScriptClassField &field) {
        ScriptObjectInstance instance = m_pClass.GetFieldClassObject(m_pInstance,field);

        Script result{instance.GetClassNamePotentionallyEmpty()};

        result.m_pInstance = MV::move(instance);
        result.m_pClassVersion = MV::GetEngine()->m_ScriptManager.m_pDomainVersion;

        if(instance.IsValid()) {
            result.m_pClass = MV::GetEngine()->m_ScriptManager.GetClass(result.m_pName);
        }

        return result;
    }

    void Script::SetInstance(ScriptObjectInstance &&instance) {
        m_pInstance = MV::move(instance);
    }
}