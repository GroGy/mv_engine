//
// Created by Matty on 2022-09-25.
//

#include "common/Gradient.h"
#include "common.hpp"
#include "util/Math.h"
#include "ensure/Ensure.h"
#include "EASTL/sort.h"

namespace MV {
    Gradient::Gradient(const glm::vec4 &color) : Gradient(color, color){
    }

    void Gradient::AddColor(float value, const glm::vec4 &color) {
        ENSURE_TRUE(value <= 1.0f && value >= 0.0f)
        m_pValues.push_back(MV::move(eastl::make_pair(value, color)));

        eastl::quick_sort(m_pValues.begin(), m_pValues.end(), [](const MV::pair<float, glm::vec4> & a, const MV::pair<float, glm::vec4> & b){
            return a.first < b.first;
        });
    }

    glm::vec4 Gradient::GetColor(float value) const {
        ENSURE_TRUE(value <= 1.0f && value >= 0.0f,{})
        float realValue = (glm::clamp)(value, 0.0f, 1.0f);

        MV::pair<float, glm::vec4> under;
        MV::pair<float, glm::vec4> above;

        for(uint32_t i = 0; i < m_pValues.size(); i++) {
            if(realValue >= m_pValues[i].first) {
                under = m_pValues[i];
                above = m_pValues[i + 1 > m_pValues.size() - 1 ? m_pValues.size() - 1 : i + 1];
                break;
            }
        }

        return glm::mix(under.second, above.second,MV::Math::Map(realValue,under.first, above.first,0.0f,1.0f));
    }

    Gradient::Gradient(const glm::vec4 &startColor, const glm::vec4 &endColor) {
        m_pValues.emplace_back(eastl::make_pair(0.0f, startColor));
        m_pValues.emplace_back(eastl::make_pair(1.0f, endColor));
    }
}
