//
// Created by Matty on 2022-11-12.
//

#include "precompiler/PropertyRegistrator.h"

namespace MV {
    PropertyIDType RegisteredProperties::s_NextComponentTypeID = 0;

    RegisteredProperties &RegisteredProperties::GetInstance() {
        static RegisteredProperties singleton;
        return singleton;
    }

    void RegisteredProperties::RegisterProperty(MV::string_view name, uint32_t id,eastl::function<void*()> && ctor) {
        PropertyData val;
        val.m_ID = id;
        val.m_ClassName = name;
        val.m_Ctor = MV::move(ctor);
        val.m_HasRegisteredScriptToken = false;
        auto foundRes = eastl::find(m_Properties.begin(), m_Properties.end(),val, [](const PropertyData & a, const PropertyData & lf ){
            return a.m_ID == lf.m_ID;
        });

        if(foundRes == m_Properties.end()) {
            std::cout << "Registered property: " << val.m_ClassName.c_str() << std::endl;
            m_Properties.push_back(MV::move(val));
        }
    }
}
