//
// Created by Matty on 2022-09-22.
//

#include "scripting/module/ScriptingCollidersModule.h"
#include "mono/metadata/object-forward.h"
#include "common/EngineCommon.h"
#include "2d/BoxColliderProperty.h"

namespace MV {
    static void collider_SetScale(MonoObject * worldObject, MonoObject * boxCollider, glm::vec2 scale) {
        ENSURE_VALID(worldObject)

        auto * property = ScriptingModule::GetProperty<MV::BoxColliderProperty>(worldObject, boxCollider);

        property->m_Scale = scale;
    }

    static void collider_SetOffset(MonoObject * worldObject, MonoObject * boxCollider, glm::vec2 offset) {
        ENSURE_VALID(worldObject)

        auto * property = ScriptingModule::GetProperty<MV::BoxColliderProperty>(worldObject, boxCollider);

        property->m_Offset = offset;
    }

    static void collider_SetRotation(MonoObject * worldObject, MonoObject * boxCollider, float rotation) {
        ENSURE_VALID(worldObject)

        auto * property = ScriptingModule::GetProperty<MV::BoxColliderProperty>(worldObject, boxCollider);

        property->m_Rotation = rotation;
    }

    static void collider_ApplyForceCenter(MonoObject * worldObject, MonoObject * boxCollider, glm::vec2 force) {
        ENSURE_VALID(worldObject)

        auto * property = dynamic_cast<MV::ColliderProperty*>(ScriptingModule::GetPropertyRaw(worldObject, boxCollider));

        ENSURE_VALID(property);

        property->ApplyForceToCenter(force);
    }

    static void collider_ApplyForce(MonoObject * worldObject, MonoObject * boxCollider, glm::vec2 force, glm::vec2 point) {
        ENSURE_VALID(worldObject)

        auto * property = dynamic_cast<MV::ColliderProperty*>(ScriptingModule::GetPropertyRaw(worldObject, boxCollider));

        ENSURE_VALID(property);

        property->ApplyForce(force, point);
    }

    void ScriptingCollidersModule::InitFunctionCallbacks() {
        mono_add_internal_call ("MV.Collider::SetScale_Native", (const void *) collider_SetScale);
        mono_add_internal_call ("MV.Collider::SetOffset_Native", (const void *) collider_SetOffset);
        mono_add_internal_call ("MV.Collider::SetRotation_Native", (const void *) collider_SetRotation);

        mono_add_internal_call ("MV.Collider::ApplyForceToCenter_Native", (const void *) collider_ApplyForceCenter);
        mono_add_internal_call ("MV.Collider::ApplyForce_Native", (const void *) collider_ApplyForce);
    }

    void ScriptingCollidersModule::CheckClassesExistence(MonoImage *engineAssembly) {

    }

    void ScriptingCollidersModule::CheckClassFunctionsExistence(MonoImage *engineAssembly) {

    }
}