//
// Created by Matty on 2021-10-15.
//

#include <filesystem>
#include "../include/resources/common/Resource.h"
#include "../include/common/EngineCommon.h"

MV::ResourceLoadResult MV::Resource::Process() {
    m_pFinished = false;
    auto res = process();
    m_pFinished = true;
    return res;
}

MV::ResourceHeader MV::Resource::ReadAssetHeader(std::ifstream &input) {
    MV::ResourceHeader res{};

    input.read((char*)&res.m_Type, sizeof(uint32_t));
    input.read((char*)&res.m_Version, sizeof(uint32_t));

    return res;
}


MV::string MV::Resource::PathProjectRelativeToAbsolute(MV::string_view pathRel) {
    return (std::filesystem::path(MV::GetEngine()->GetProject().m_Path.c_str()) /= pathRel.data()).string().c_str();
}