//
// Created by Matty on 2021-10-01.
//

#include "../include/log/Logger.h"
#include "../include/common/Types.h"
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace MV {
    static MV::rc<spdlog::logger> s_CoreLogger;
    static MV::rc<spdlog::logger> s_RenderLogger;
    static MV::rc<spdlog::logger> s_ScriptLogger;

    void Logger::init() {
        auto core_console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        core_console_sink->set_level(spdlog::level::debug);

        auto render_console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        render_console_sink->set_level(spdlog::level::debug);

        auto script_console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        script_console_sink->set_level(spdlog::level::debug);

        s_CoreLogger = MV::make_rc<spdlog::logger>("core", core_console_sink);
        s_CoreLogger->set_level(spdlog::level::debug);
        s_CoreLogger->set_pattern("[CORE] [%^%l%$] %v");

        s_RenderLogger = MV::make_rc<spdlog::logger>("render", render_console_sink);
        s_RenderLogger->set_level(spdlog::level::debug);
        s_RenderLogger->set_pattern("[RENDER] [%^%l%$] %v");

        s_ScriptLogger = MV::make_rc<spdlog::logger>("script", script_console_sink);
        s_ScriptLogger->set_level(spdlog::level::debug);
        s_ScriptLogger->set_pattern("[SCRIPT] [%^%l%$] %v");
    }

    void Logger::logInfo(const eastl::string_view msg) {
        s_CoreLogger->info(msg.data());
    }

    void Logger::logDebug(const eastl::string_view msg) {
        s_CoreLogger->debug(msg.data());
    }

    void Logger::logWarning(const eastl::string_view msg) {
        s_CoreLogger->warn(msg.data());
    }

    void Logger::logError(const eastl::string_view msg) {
        s_CoreLogger->error(msg.data());
    }

    void Logger::render_logInfo(const eastl::string_view msg) {
        s_RenderLogger->info(msg.data());
    }

    void Logger::render_logDebug(const eastl::string_view msg) {
        if(m_pRendererDebuggingEnabled) s_RenderLogger->debug(msg.data());
    }

    void Logger::render_logWarning(const eastl::string_view msg) {
        s_RenderLogger->warn(msg.data());
    }

    void Logger::render_logError(const eastl::string_view msg) {
        s_RenderLogger->error(msg.data());
    }

    void Logger::script_logInfo(const eastl::string_view msg) {
        s_ScriptLogger->info(msg.data());
    }

    void Logger::script_logDebug(const eastl::string_view msg) {
        if(m_pScriptDebuggingEnabled) s_ScriptLogger->debug(msg.data());
    }

    void Logger::script_logWarning(const eastl::string_view msg) {
        s_ScriptLogger->warn(msg.data());
    }

    void Logger::script_logError(const eastl::string_view msg) {
        s_ScriptLogger->error(msg.data());
    }
}