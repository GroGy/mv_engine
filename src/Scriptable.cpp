//
// Created by Matty on 2021-11-03.
//

#include "../include/common/Scriptable.h"

namespace MV {
    Scriptable::Scriptable(ScriptableInitData &&init) : WorldObject(init.m_pScriptName), m_pInitData(MV::move(init)) {

    }

    void Scriptable::OnStart() {
        WorldObject::OnStart();
    }

    void Scriptable::OnEnd() {
        WorldObject::OnEnd();
    }

    void Scriptable::Update(float delta) {
        WorldObject::Update(delta);
    }

    void Scriptable::FixedUpdate(float delta) {
        WorldObject::FixedUpdate(delta);
    }

    void Scriptable::Render(FrameContext &context) {
        WorldObject::Render(context);

    }
}
