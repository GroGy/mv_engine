//
// Created by Matty on 2021-10-15.
//

#include "../include/common/World.h"
#include "../include/common/EngineCommon.h"
#include "../include/profiler/Profiler.h"

namespace MV {
    void World::Render(FrameContext &context) {
        MV_PROFILE_FUNCTION("World::Render");
        auto &tilemapPipeline = GetBuildins()->m_TilemapPipeline;

        m_PropertyManager.ForEachStorage([&](IPropertyStorage *storage) {
            storage->for_each([&](Property *property) {
                if (property->IsValid()) {
                    if(auto renderProperty = dynamic_cast<IRenderedProperty*>(property))
                        renderProperty->Render(context);
                }
            });
        });

        if (m_ShowColliders)
            renderPhysicsOverlay(context);
    }

    void World::Update(float delta) {
        MV_PROFILE_FUNCTION("World::Update");

        auto *engine = GetEngine();

        if (engine->IsGameRunning()) {
            m_pPhysicsWorld->Step(delta, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

            std::for_each(m_WorldObjects.begin(), m_WorldObjects.end(), [delta](auto &v) {
                if(!GetEngine()->IsForceStopped()) {
                    v.second->Update(delta);
                }
            });

            if(GetEngine()->IsForceStopped()) {
                return;
            }

            m_PropertyManager.ForEachStorage([&](IPropertyStorage *storage) {
                storage->for_each([&](Property *property) {
                    if (property->IsValid())
                        if(!GetEngine()->IsForceStopped()) {
                            property->Update(delta);
                        }
                });
            });

            if(GetEngine()->IsForceStopped()) {
                return;
            }

            while(!m_PendingWorldObjects.empty()) {
                auto worldObject = m_PendingWorldObjects.back();
                m_PendingWorldObjects.pop();

                m_WorldObjects[worldObject->m_WorldID] = worldObject;

                worldObject->OnStart();
            }
        }
    }

    void World::Init() {
        m_pPhysicsWorld = new b2World{{0.0f, -10.0f}};
        m_pPhysicsWorld->SetDebugDraw(&m_pDebugDrawer);
        m_Camera = make_rc<Camera>();
        m_pDebugDrawer.Init();
    }

    void World::Cleanup() {
        for (auto &&wo: m_WorldObjects) {
            wo.second->Cleanup();
        }
        m_pDebugDrawer.Cleanup();
        delete m_pPhysicsWorld;
        m_PropertyManager.Cleanup();
        m_WorldObjectsFields.clear();
    }

    b2World *World::GetPhysicsWorld() {
        return m_pPhysicsWorld;
    }

    void World::renderPhysicsOverlay(FrameContext &context) {
        MV_PROFILE_FUNCTION("World::renderPhysicsOverlay");
        auto pipeline = GetBuildins()->m_PhysicsDebugPipeline;
        pipeline.m_Pipeline->Bind(context);

        struct MVPPushConstant {
            glm::mat4 m_MVP;
        };

        MVPPushConstant data;

        data.m_MVP = context.m_ProjMatrix*context.m_ViewMatrix;

        pipeline.m_Pipeline->BindPushConstant(context,pipeline.m_MVPmatrixDescription,&data, sizeof(data));

        m_pPhysicsWorld->DebugDraw();
        m_pDebugDrawer.Render(context);
    }

    World::World(WorldData &&resDataArg) : m_pValid(true) {
        if (GetEngine()->IsEditor()) {
            m_WorldData = MV::move(resDataArg);
        }
        const auto &resData = GetEngine()->IsEditor() ? m_WorldData : resDataArg;

        loadFromData(resData);
    }

    void World::OnStart() {
        auto *engine = GetEngine();
        MV_PROFILE_FUNCTION("World::OnStart");

        if (engine->IsGameRunning()) {
            MV_PROFILE_FUNCTION("World::OnStart game running");
            std::for_each(m_WorldObjects.begin(), m_WorldObjects.end(), [&](auto &v) {
                v.second->Init(); //Recreate wo
                if (m_WorldData.m_WorldObjects.find(v.first) != m_WorldData.m_WorldObjects.end()) {
                    const auto &woData = m_WorldData.m_WorldObjects[v.first];
                    v.second->SetPosition(woData.m_Position);
                    v.second->SetRotation(woData.m_Rotation);
                    v.second->SetScale(woData.m_Scale);
                    v.second->SyncFromData(woData.m_Data);
                    syncWorldObjectProperties(woData.m_Data, v.second);
                }
                if(!GetEngine()->IsForceStopped()) {
                    v.second->OnStart();
                }
            });

            if(GetEngine()->IsForceStopped()) {
                return;
            }

            m_PropertyManager.ForEachStorage([](IPropertyStorage* storage) {
               storage->for_each([](Property * property){
                   property->Init();
               });
            });
        }
    }

    void World::OnEnd() {
        auto *engine = GetEngine();
        MV_PROFILE_FUNCTION("World::OnEnd");

        if (engine->IsGameRunning()) {
            MV_PROFILE_FUNCTION("World::OnEnd game running");
            std::for_each(m_WorldObjects.begin(), m_WorldObjects.end(), [](auto &v) {
                if(!GetEngine()->IsForceStopped()) {
                    v.second->OnEnd();
                }
            });

            if(GetEngine()->IsForceStopped()) {
                return;
            }

            m_PropertyManager.ForEachStorage([](IPropertyStorage* storage) {
                storage->for_each([](Property * property){
                    property->Cleanup();
                });
            });
        }

        m_WorldObjects.clear();
        m_ObjectFieldCache.clear();
    }

    void World::FixedUpdate(float delta) {
        auto *engine = GetEngine();
        MV_PROFILE_FUNCTION("World::FixedUpdate");

        if (engine->IsGameRunning()) {
            MV_PROFILE_FUNCTION("World::FixedUpdate game running");
            std::for_each(m_WorldObjects.begin(), m_WorldObjects.end(), [delta](auto &v) {
                v.second->FixedUpdate(delta);
            });
        }
    }

    MV::vector<rc<WorldObject>> World::GetWorldObjectsByTag(MV::string_view tag) {
        MV_PROFILE_FUNCTION("World::GetWorldObjectsByTag")

        MV::vector<rc<WorldObject>> res{};

        if(tag.empty())
            return res;

        res.reserve(64);

        for(auto &&wo: m_WorldObjects) {
            if(wo.second && wo.second->m_Tag == tag) {
                res.emplace_back(wo.second);
            }
        }

        return res;
    }

    void World::Reload() {
        Cleanup();

        Init();
        loadFromData();
    }

    void World::loadFromData(const WorldData &resData) {
        MV_PROFILE_FUNCTION("World::loadFromData");
        m_pSize = {resData.m_Width, resData.m_Height};
        m_PropertyManager.Init();

        uint32_t max = 0;

        for (const auto &wo: resData.m_WorldObjects) {
            const auto &woData = wo.second;

            MV::string scriptName = woData.m_Script;
            auto newWO = MV::make_rc<Scriptable>(ScriptableInitData{MV::move(scriptName)});
            newWO->m_Name = woData.m_Name;
            newWO->m_WorldID = woData.m_WorldID;
            newWO->ForceSetPosition(woData.m_Position);
            newWO->ForceSetRotation(woData.m_Rotation);
            newWO->ForceSetScale(woData.m_Scale);
            newWO->m_IsStatic = woData.m_Static;

            newWO->Init();

            const auto & fields = newWO->GetScript().GetFields();

            for (auto &&f: fields) {
                if (f.second.m_Type == ScriptClassField::Type::PROPERTY) {
                    auto index = m_PropertyManager.CreatePropertyFromToken(f.second.m_Class, newWO);
                    GetLogger()->LogDebug("Created property in storage with index {}, (token: {})", index, f.second.m_Class);
                    m_WorldObjectsFields[newWO->m_WorldID].emplace_back(eastl::make_pair(f.second.m_Field, index));
                }
            }


            m_WorldObjects[woData.m_WorldID] = MV::move(newWO);

            if (max < woData.m_WorldID) max = woData.m_WorldID;
        }

        m_WoIDCounter = max + 1;
        SyncAllWorldObjects();
    }

    void World::loadFromData() {
        loadFromData(m_WorldData);
    }

    void World::MarkWOForSync(uint64_t id) {
        m_pWorldObjectsToSync.emplace(id);
    }

    void World::SyncDirtyWorldObjects() {
        for (auto &&v: m_pWorldObjectsToSync) {
            if(m_WorldData.m_WorldObjects.find(v) != m_WorldData.m_WorldObjects.end()) {
                const auto &woData = m_WorldData.m_WorldObjects[v];
                auto &wo = m_WorldObjects[v];
                wo->SetPosition(woData.m_Position);
                wo->SetRotation(woData.m_Rotation);
                wo->SetScale(woData.m_Scale);
                wo->SyncFromData(woData.m_Data);
                syncWorldObjectProperties(woData.m_Data, wo);
            }
        }

        m_pWorldObjectsToSync.clear();
    }

    void World::SyncAllWorldObjects() {
        for (auto &&v: m_WorldObjects) {
            if(m_WorldData.m_WorldObjects.find(v.first) != m_WorldData.m_WorldObjects.end()) {
                const auto &woData = m_WorldData.m_WorldObjects[v.first];
                v.second->SetPosition(woData.m_Position);
                v.second->SetRotation(woData.m_Rotation);
                v.second->SetScale(woData.m_Scale);

                v.second->SyncFromData(woData.m_Data);
                syncWorldObjectProperties(woData.m_Data, v.second);
            }
        }
        m_pWorldObjectsToSync.clear();
    }

    void World::syncWorldObjectProperties(const nlohmann::json &data, const rc <WorldObject> &wo) {
        auto &woScript = wo->GetScript();

        uint32_t counter = 0;

        for (auto &&field: woScript.GetFields()) {
            auto type = field.second.m_Type;
            if(type != ScriptClassField::Type::PROPERTY) continue;

            const auto foundRes = data.find(field.first.c_str());

            if (foundRes != data.end()) {

                const auto& val = foundRes.value();

                auto *storage = m_PropertyManager.GetIStorageFromToken(field.second.m_Class);
                auto & objectFields = m_WorldObjectsFields.at(wo->m_WorldID);
                if(objectFields.size() > counter) {
                    auto * property = storage->get_property(objectFields[counter++].second);
                    property->Deserialize(val);
                }

                storage->sync_property_fields(field.second, woScript, val);
            }
        }
    }

    rc<WorldObject> World::SpawnWorldObject(const WorldObjectSpawnParams& spawnParams) {
        ENSURE_TRUE(spawnParams.m_SpawnType == WorldObjectSpawnType::Script, nullptr);

        MV::string scriptName = spawnParams.m_Name;
        auto newWO = MV::make_rc<Scriptable>(ScriptableInitData{MV::move(scriptName)});

        auto id = m_WoIDCounter++;

        newWO->m_Name = spawnParams.m_Name.empty() ? fmt::format("wo_{}", id).c_str() : spawnParams.m_Name;;
        newWO->m_WorldID = id;
        newWO->ForceSetPosition(spawnParams.m_SpawnLocation);
        newWO->ForceSetRotation(spawnParams.m_SpawnRotation);
        newWO->ForceSetScale(spawnParams.m_SpawnScale);

        newWO->Init();

        const auto & fields = newWO->GetScript().GetFields();

        for (auto &&f: fields) {
            if (f.second.m_Type == ScriptClassField::Type::PROPERTY) {
                auto index = m_PropertyManager.CreatePropertyFromToken(f.second.m_Class, newWO);
                m_WorldObjectsFields[newWO->m_WorldID].emplace_back(eastl::make_pair(f.second.m_Field, index));
            }
        }

        GetLogger()->LogInfo("Creating world object of id {}. Type: {}", id, spawnParams.m_Name);

        m_PendingWorldObjects.emplace(newWO);

        return newWO;
    }

    RayHitResult World::CastRay(const glm::vec2 &start, const glm::vec2 &dir, float len) {
        MV_PROFILE_FUNCTION("World::CastRay");
        RayHitResult res;

        const auto end = start + glm::normalize(dir) * len;

        class RayCallback : public b2RayCastCallback {
        public:
            RayCallback(RayHitResult & result ) : m_Res(result) {};

            RayHitResult & m_Res;

            float
            ReportFixture(b2Fixture *fixture, const b2Vec2 &point, const b2Vec2 &normal, float fraction) override {
                m_Res.m_Hit = fixture;
                m_Res.m_Normal = {normal.x, normal.y};
                m_Res.m_Position = {point.x, point.y};
                m_Res.m_WorldObject =(WorldObject*)fixture->GetUserData().pointer;
                return 0;
            }
        };
        RayCallback inst{res};

        m_pPhysicsWorld->RayCast(&inst, {start.x,start.y}, {end.x,end.y});

        return res;
    }

}