//
// Created by Matty on 10.12.2022.
//

#include "scripting/module/ScriptingInputModule.h"

namespace MV {
    static bool engine_IsKeyPressed(int32_t key) {
        return GetEngine()->m_Input.IsKeyPressed(static_cast<Key>(key));
    };

    static bool engine_IsKeyReleased(int32_t key) {
        return GetEngine()->m_Input.IsKeyReleased(static_cast<Key>(key));
    };

    static bool engine_IsKeyDown(int32_t key) {
        return GetEngine()->m_Input.IsKeyDown(static_cast<Key>(key));
    };

    static bool engine_IsKeyUp(int32_t key) {
        return GetEngine()->m_Input.IsKeyUp(static_cast<Key>(key));
    };

    static bool engine_IsGamepadKeyPressed(int32_t controller, int32_t key) {
        return GetEngine()->m_Input.IsGamepadKeyPressed(controller,static_cast<GamepadKey>(key));
    };

    static bool engine_IsGamepadKeyReleased(int32_t controller, int32_t key) {
        return GetEngine()->m_Input.IsGamepadKeyReleased(controller,static_cast<GamepadKey>(key));
    };

    static bool engine_IsGamepadKeyDown(int32_t controller, int32_t key) {
        return GetEngine()->m_Input.IsGamepadKeyDown(controller,static_cast<GamepadKey>(key));
    };

    static bool engine_IsGamepadKeyUp(int32_t controller, int32_t key) {
        return GetEngine()->m_Input.IsGamepadKeyUp(controller,static_cast<GamepadKey>(key));
    };

    static float engine_GetGamepadTrigger(int32_t controller, int32_t trigger) {
        return GetEngine()->m_Input.GetGamepadTrigger(controller,static_cast<GamepadTrigger>(trigger));
    };

    static float engine_GetGamepadJoystickAxis(int32_t controller, int32_t joystick, bool yInsteadOfX) {
        auto v = GetEngine()->m_Input.GetGamepadJoystick(controller,static_cast<GamepadJoystick>(joystick));
        return yInsteadOfX ? v.y : v.x;
    };

    static bool engine_IsGamepadConnected(int32_t controller) {
        return GetEngine()->m_Input.IsGamepadConnected(controller);
    };

    static bool engine_IsGamepadJoystick(int32_t controller) {
        return GetEngine()->m_Input.IsGamepadJoystick(controller);
    };

    static MonoString* engine_GetGamepadName(int32_t controller) {
        return GetEngine()->m_ScriptManager.CreateMonoString(GetEngine()->m_Input.GetGamepadName(controller));
    }

    void ScriptingInputModule::InitFunctionCallbacks() {
        mono_add_internal_call("MV.Input::IsKeyPressed_Native", (const void *) engine_IsKeyPressed);
        mono_add_internal_call("MV.Input::IsKeyReleased_Native", (const void *) engine_IsKeyReleased);
        mono_add_internal_call("MV.Input::IsKeyDown_Native", (const void *) engine_IsKeyDown);
        mono_add_internal_call("MV.Input::IsKeyUp_Native", (const void *) engine_IsKeyUp);

        mono_add_internal_call("MV.Input::IsGamepadKeyPressed_Native", (const void *) engine_IsGamepadKeyPressed);
        mono_add_internal_call("MV.Input::IsGamepadKeyReleased_Native", (const void *) engine_IsGamepadKeyReleased);
        mono_add_internal_call("MV.Input::IsGamepadKeyDown_Native", (const void *) engine_IsGamepadKeyDown);
        mono_add_internal_call("MV.Input::IsGamepadKeyUp_Native", (const void *) engine_IsGamepadKeyUp);
        mono_add_internal_call("MV.Input::GetGamepadTrigger_Native", (const void *) engine_GetGamepadTrigger);
        mono_add_internal_call("MV.Input::GetGamepadJoystickAxis_Native", (const void *) engine_GetGamepadJoystickAxis);
        mono_add_internal_call("MV.Input::IsGamepadConnected_Native", (const void *) engine_IsGamepadConnected);
        mono_add_internal_call("MV.Input::IsGamepadJoystick_Native", (const void *) engine_IsGamepadJoystick);
        mono_add_internal_call("MV.Input::GetGamepadName_Native", (const void *) engine_GetGamepadName);
    }

    void ScriptingInputModule::CheckClassesExistence(MonoImage *engineAssembly) {
        ENSURE_VALID(mono_class_from_name(engineAssembly, "MV", "Input"));
    }

    void ScriptingInputModule::CheckClassFunctionsExistence(MonoImage *engineAssembly) {

    }
}