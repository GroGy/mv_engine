//
// Created by Matty on 2021-10-24.
//

#include "../include/2d/PhysicsDebugPipeline.h"
#include "../include/2d/PhysicsDebugVertex.h"

namespace MV {
    PipelineBlueprint PhysicsDebugPipeline::GetBlueprint() {
        PipelineBlueprint blueprint{};

        blueprint.SetVertexInputDescription(PhysicsDebugVertex::GetBindingDescription(),
                                            PhysicsDebugVertex::GetAttributeDescriptions());

        blueprint.EnableAlphaBlending(true);

        m_MVPmatrixDescription = blueprint.AddPushConstant("MVP",VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4));

        blueprint.AddShader(
                MV::make_rc<VulkanShader>(".shadercache/debug_2d_physics_vertex_0.shadercache", ShaderType::VERTEX));
        blueprint.AddShader(
                MV::make_rc<VulkanShader>(".shadercache/debug_2d_physics_fragment_0.shadercache", ShaderType::FRAGMENT));

        return blueprint;
    }
}
