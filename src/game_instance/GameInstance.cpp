//
// Created by Matty on 07.12.2022.
//

#include "scripting/module/game_instance/GameInstance.h"
#include "common/EngineCommon.h"

namespace MV {
    void GameInstance::Init() {
        if(m_pCustomClassName.empty()) return;

        m_pGameInstance.CreateInstance();
    }

    void GameInstance::Cleanup() {
        if(m_pCustomClassName.empty()) return;

        m_pGameInstance.Destroy();
    }

    void GameInstance::OnStart() {
        if(m_pCustomClassName.empty()) return;

        ENSURE_TRUE(m_pGameInstance.IsInstanceValid())

        m_pGameInstance.CallFunction_Void("OnStart");
    }

    void GameInstance::OnEnd() {
        if(m_pCustomClassName.empty()) return;

        ENSURE_TRUE(m_pGameInstance.IsInstanceValid())

        m_pGameInstance.CallFunction_Void("OnEnd");
    }

    void GameInstance::SetClass(MV::string_view className) {
        ENSURE_TRUE(!m_pGameInstance.IsInstanceValid())
        m_pCustomClassName = className;
        auto klass = MV::GetEngine()->m_ScriptManager.GetClass(className);
        m_pGameInstance = MV::Script{klass.GetClassName()};
    }

    GameInstance::GameInstance() : m_pGameInstance("GameInstance") {
    }
}