//
// Created by Matty on 2021-10-05.
//

#include "../include/render/common/Shader.h"

uint64_t MV::Shader::getID() {
    static uint64_t id = 0;
    return id++;
}

void MV::Shader::Refresh() {
    if(m_pLoaded) Cleanup();
    Init();
}
