//
// Created by Matty on 2021-11-03.
//

#include <ext/matrix_clip_space.hpp>
#include <ext/matrix_transform.hpp>
#include "../include/common/camera/Camera.h"

namespace MV {
    void Camera::SetPosition(glm::vec2 pos) {
        m_pCameraData.m_Position = {pos};
    }

    static const float LERP_BASE = 0.25f;

    void Camera::Update(float deltaTime) {
        m_pCameraData.m_Position = glm::mix(m_pCameraData.m_Position, m_pCameraData.m_TargetPosition, LERP_BASE * deltaTime);
    }

    void Camera::ForcePosition() {
        m_pCameraData.m_Position = m_pCameraData.m_TargetPosition;
    }

    glm::mat4 Camera::GetView() const {
        return glm::lookAt(glm::vec3(m_pCameraData.m_Position.x,m_pCameraData.m_Position.y,0.0f), glm::vec3(m_pCameraData.m_Position.x,m_pCameraData.m_Position.y,0.0f) + glm::vec3{0.f, 0.f, 1.f},
                           glm::vec3(0.0f, 1.0f, 0.0f));
    }

    glm::mat4 Camera::CalcProj() {
        if(m_pProjDirty) {
            auto width = 1.0f * m_pViewWidth * m_pZoom;
            auto height = m_pRatio * m_pViewWidth * m_pZoom;
            m_pCameraData.m_ProjMatrix = glm::ortho(width / 2, -width / 2, height/2,
                                       -height/2, -10.f, 10.f);
            m_pProjDirty = false;
        }

        return m_pCameraData.m_ProjMatrix;
    }

    void Camera::SetZoom(float zoom) {
        m_pProjDirty = true;
        m_pZoom = zoom;
    }

    void Camera::SetViewWidth(float val) {
        m_pViewWidth = val;
        m_pProjDirty = true;
    }

    void Camera::SetRatio(float ratio) {
        m_pRatio = ratio;
        m_pProjDirty = true;
    }

    glm::mat4 Camera::GetView(const glm::vec2 & pos) {
        return glm::lookAt(glm::vec3(pos.x,pos.y,0.0f), glm::vec3(pos.x,pos.y,0.0f) + glm::vec3{0.f, 0.f, 1.f},
                           glm::vec3(0.0f, 1.0f, 0.0f));
    }

    glm::mat4 Camera::CalcYInverseProj() {
        auto width = 1.0f * m_pViewWidth * m_pZoom;
        auto height = m_pRatio * m_pViewWidth* m_pZoom;
        return glm::ortho(width / 2, -width / 2, -height/2,
                                                height/2, -10.f, 10.f);
    }
}