//
// Created by Matty on 2021-11-03.
//

#include <cassert>
#include "../include/resources/TilesetResource.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    TilesetResource::TilesetResource(MV::string path, uint64_t id) : Resource(MV::move(path)), m_pID(id) {

    }

    ResourceLoadResult TilesetResource::process() {
        m_pPath = PathProjectRelativeToAbsolute(m_pPath);
        std::ifstream input{m_pPath.c_str()};
        if (input.is_open()) {
            input >> m_Result.m_RawData;

            m_Result.m_Atlas = m_Result.m_RawData.at(Config::sc_AtlasJsonFieldName.data()).get<uint64_t>();

            nlohmann::json tiles = m_Result.m_RawData.at(Config::sc_TilesJsonFieldName.data());

            m_Result.m_Tiles.resize(tiles.size());

            for(uint32_t i = 0; i < tiles.size(); i++) {
                auto & tile = m_Result.m_Tiles[i];
                tile.m_SpriteIndex = tiles.at(i)[Config::sc_SpriteJsonFieldName.data()].get<uint32_t>();

                nlohmann::json collisionData = tiles.at(i).at(Config::sc_TileCollisionJsonFieldName.data());

                tile.m_Collision.resize(collisionData.size());

                for(uint32_t j = 0; j < tile.m_Collision.size(); j++) {
                    tile.m_Collision[j] = collisionData.at(j).get<glm::vec2>();
                }
            }

            input.close();
        } else {
            return ResourceLoadResult::FILE_READ_ERROR;
        }

        GetEngine()->LoadResourceSync(m_Result.m_Atlas);

        bool loaded = false;
        do {
            GetEngine()->AddSyncCallback([&]() {
                loaded = GetEngine()->IsAssetLoaded(m_Result.m_Atlas);
            });
        } while (!loaded);

        m_pSuccess = true;
        return ResourceLoadResult::SUCCESS;
    }

    void TilesetResource::finish() {
        if (!m_pSuccess)
            return;

        GetResources()->m_Tilesets[m_pID] = make_rc<Tileset>(m_Result);
        m_pSuccess = true;
    }
}
