//
// Created by Matty on 2022-01-23.
//

#include "../include/util/Math.h"
#include "ensure/Ensure.h"

namespace MV {
    float Math::Map(float value, float start1, float stop1, float start2, float stop2) {
        return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
    }

    float Math::FRand(float min, float max) {
        return min + ((double) rand() / (double) RAND_MAX) * (max - min);
    }

    float BezierCurve::GetValue(float t) const {
        ENSURE_TRUE(m_pData.size() > 2, 1.0f);

        uint32_t pointCount = GetDataSize();

        if (pointCount < 2)
            return 0;
        if (t < 0) return m_pData[0].m_Point.y;

        int left = 0;
        while (left < pointCount && m_pData[left].m_Point.x < t) left++;
        if (left) left--;

        if (left == pointCount - 1)
            return m_pData[pointCount - 1].m_Point.y;

        const float d = (t - m_pData[left].m_Point.x) / (m_pData[left + 1].m_Point.x - m_pData[left].m_Point.x);

        return Math::BezierDerivative(d, m_pData[left].m_Point.y, m_pData[left].m_Right.y, m_pData[left + 1].m_Left.y,
                                      m_pData[left + 1].m_Point.y);
    }

    BezierCurve::BezierCurve() {
        m_pData.resize(3);
        m_pData[0].m_Point = {0.0f, 0.0f};
        m_pData[0].m_Right = {0.25f, 0.25f};
        m_pData[1].m_Point = {1.0f, 1.0f};
        m_pData[1].m_Left = {0.75f, 0.75f};
    }
}
