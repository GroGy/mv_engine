//
// Created by Matty on 07.12.2022.
//

#include "util/Script.h"

namespace MV {
    string ScriptUtil::GetMonoTypeToString(MonoTypeEnum type) {
        switch (type) {
            case MONO_TYPE_END:
                return "EndOfList";
            case MONO_TYPE_VOID:
                return "Void";
            case MONO_TYPE_BOOLEAN:
                return "Bool";
            case MONO_TYPE_CHAR:
                return "Char";
            case MONO_TYPE_I1:
                return "Int8";
            case MONO_TYPE_U1:
                return "UInt8";
            case MONO_TYPE_I2:
                return "Int16";
            case MONO_TYPE_U2:
                return "UInt16";
            case MONO_TYPE_I4:
                return "Int32";
            case MONO_TYPE_U4:
                return "UInt32";
            case MONO_TYPE_I8:
                return "Int64";
            case MONO_TYPE_U8:
                return "UInt64";
            case MONO_TYPE_R4:
                return "Float";
            case MONO_TYPE_R8:
                return "Double";
            case MONO_TYPE_STRING:
                return "String";
            case MONO_TYPE_PTR:
                return "Ptr";
            case MONO_TYPE_BYREF:
                return "ByRef";
            case MONO_TYPE_VALUETYPE:
                return "ValueType";
            case MONO_TYPE_CLASS:
                return "Class";
            case MONO_TYPE_VAR:
                return "Var";
            case MONO_TYPE_ARRAY:
                return "Array";
            case MONO_TYPE_GENERICINST:
                return "GenericInstance";
            case MONO_TYPE_TYPEDBYREF:
                return "TypedByRef";
            case MONO_TYPE_I:
                return "GenericI";
            case MONO_TYPE_U:
                return "GenericU";
            case MONO_TYPE_FNPTR:
                return "Fnptr";
            case MONO_TYPE_OBJECT:
                return "Object";
            case MONO_TYPE_SZARRAY:
                return "SZArray";
            case MONO_TYPE_MVAR:
                return "MVar";
            case MONO_TYPE_CMOD_REQD:
                return "CMod_reqd";
            case MONO_TYPE_CMOD_OPT:
                return "CMod_opt";
            case MONO_TYPE_INTERNAL:
                return "Internal";
            case MONO_TYPE_MODIFIER:
                return "Modifier";
            case MONO_TYPE_SENTINEL:
                return "Sentinel";
            case MONO_TYPE_PINNED:
                return "Pinned";
            case MONO_TYPE_ENUM:
                return "Enum";
        }
        return "Unknown";
    }
}