//
// Created by Matty on 2022-03-08.
//

#include "../include/allocator/AllocatorProvider.h"
#include "../include/allocator/DefaultAllocator.h"

namespace MV {
    AllocatorProvider::AllocatorProvider() {

    }

    AllocatorProvider::AllocatorProvider(const eastl::allocator &x) : allocator(x) {

    }

    void *AllocatorProvider::allocate(size_t n, int flags) {
        return DefaultAllocator::GetInstance().allocate(n, flags);
    }

    void *AllocatorProvider::allocate(size_t n, size_t alignment, size_t offset, int flags) {
        return DefaultAllocator::GetInstance().allocate(n, alignment, offset, flags);
    }

    void AllocatorProvider::deallocate(void *p, size_t n) {
        DefaultAllocator::GetInstance().deallocate(p, n);
    }

    const char *AllocatorProvider::get_name() const {
        return "Provider";
    }

    void AllocatorProvider::set_name(const char *pName) {
        allocator::set_name("Provider");
    }

    void *AllocatorProvider::allocate(size_t n) {
        return DefaultAllocator::GetInstance().allocate(n,0);
    }

    void *AllocatorProvider::allocate(size_t n, size_t alignment, size_t offset) {
        return DefaultAllocator::GetInstance().allocate(n,alignment, offset, 0);
    }

    void *AllocatorProvider::reallocate(void *ptr, size_t n, int flags) {
        return DefaultAllocator::GetInstance().reallocate(ptr,n, flags);
    }
}