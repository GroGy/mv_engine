//
// Created by Matty on 2022-02-06.
//

#include "../include/scripting/ScriptManager.h"
#include "../include/common/EngineCommon.h"
#include "../include/common/ExceptionCommon.h"
#include "../include/scripting/module/ScriptingWorldObjectModule.h"
#include "../include/profiler/Profiler.h"
#include "mono/metadata/exception.h"
#include "scripting/module/ScriptingCollidersModule.h"
#include "scripting/module/ScriptingSpriteModule.h"
#include "util/Script.h"
#include "scripting/module/ScriptingInputModule.h"
#include <mono/metadata/assembly.h>
#include <list>
#include <filesystem>

namespace MV {
    ScriptManager *ScriptManager::s_GlobalInstance = nullptr;


    std::list<MonoClass *> GetAssemblyClassList(MonoImage *image) {
        std::list<MonoClass *> class_list;

        const MonoTableInfo *table_info = mono_image_get_table_info(image, MONO_TABLE_TYPEDEF);

        int rows = mono_table_info_get_rows(table_info);

        /* For each row, get some of its values */
        for (int i = 0; i < rows; i++) {
            MonoClass *_class = nullptr;
            uint32_t cols[MONO_TYPEDEF_SIZE];
            mono_metadata_decode_row(table_info, i, cols, MONO_TYPEDEF_SIZE);
            const char *name = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAME]);
            const char *name_space = mono_metadata_string_heap(image, cols[MONO_TYPEDEF_NAMESPACE]);
            _class = mono_class_from_name(image, name_space, name);
            class_list.push_back(_class);
        }
        return class_list;
    }

    template<typename T>
    static void InitModule(MonoImage *engineAssembly) {
        T module{};
        module.InitFunctionCallbacks();
        module.CheckClassesExistence(engineAssembly);
        module.CheckClassFunctionsExistence(engineAssembly);
    };

    static void exceptionHandler(MonoObject *exc, void *user_data) {
        MV::GetLogger()->Script_LogError("Script failed with unhandled exception: {}",
                                         mono_class_get_name(mono_object_get_class(exc)));
        MV::GetEngine()->ForceStopGame();
    }

    void ScriptManager::Init(ScriptManagerInit initData) {
        s_GlobalInstance = this;
        GetLogger()->Script_LogInfo("Initializing Mono....");

        m_pInitData = MV::move(initData);

        mono_install_unhandled_exception_hook(exceptionHandler, nullptr);

        mono_set_assemblies_path("mono_libs");

        initDomain();
        initAppDomain();

        GetLogger()->Script_LogInfo("Initialized Mono.");
    }

    void ScriptManager::initEngineAssembly() {
        GetLogger()->Script_LogInfo("Initializing engine assembly....");
        m_pEngineAssembly = mono_domain_assembly_open(m_pMonoDomain, "Engine.dll");
        if (!m_pEngineAssembly) {
            constexpr auto err = "Failed to initialize engine assembly!";
            GetLogger()->Script_LogError(err);
            throw RuntimeException(err);
        }

        GetLogger()->Script_LogDebug("Initializing engine assembly image....");
        m_pEngineAssemblyImage = mono_assembly_get_image(m_pEngineAssembly);
        GetLogger()->Script_LogDebug("Initialized engine assembly image.");

        GetLogger()->Script_LogInfo("Initialized engine assembly.");
    }

    static void *monoAlloc(size_t size) {
        return MV::DefaultAllocator::GetInstance().allocate(size, AllocationType::SCRIPT);
    }

    static void *monoRealloc(void *mem, size_t size) {
        return MV::DefaultAllocator::GetInstance().reallocate(mem, size, AllocationType::SCRIPT);
    }

    static void monoFree(void *mem) {
        MV::DefaultAllocator::GetInstance().deallocate(mem, 0, AllocationType::SCRIPT);
    }

    static void *monoCalloc(size_t count, size_t size) {
        return MV::DefaultAllocator::GetInstance().allocate(count * size, AllocationType::SCRIPT);
    }

    void ScriptManager::initDomain() {
        GetLogger()->Script_LogDebug("Initializing mono domain....");

        static MonoAllocatorVTable allocs{
                MONO_ALLOCATOR_VTABLE_VERSION,
                monoAlloc,
                monoRealloc,
                monoFree,
                monoCalloc,
        };

        const auto res = mono_set_allocator_vtable(&allocs);

        if (res != 1) {
            GetLogger()->Script_LogError("Failed to set allocators");
        }

        m_pMonoDomain = mono_jit_init("MV Engine");
        if (!m_pMonoDomain) {
            constexpr auto err = "Failed to initialize mono domain!";
            GetLogger()->Script_LogError(err);
            throw RuntimeException(err);
        }
        GetLogger()->Script_LogDebug("Initialized mono domain.");
    }

    void ScriptManager::Start() {

    }

    void ScriptManager::End() {

    }

    void ScriptManager::Cleanup() {
        GetLogger()->Script_LogInfo("Cleaning up Mono....");
        //unloadAppDomain();
        mono_jit_cleanup(m_pMonoDomain);
        m_pMonoDomain = nullptr;
        GetLogger()->Script_LogInfo("Cleaned Mono.");
    }

    void ScriptManager::Update() {
        MV_PROFILE_FUNCTION("ScriptManager::Update");
    }

    ScriptManager::ScriptManager() {
    }

    ScriptClass ScriptManager::GetClass(MV::string_view name) {

        Assert(m_pUserAssemblyImage != nullptr, "Missing user assembly!");

        MV::string className{name};

        auto foundRes = m_pClassCache.find(className.data());

        if (className.rfind(m_pInitData.m_Name, 0) == 0 &&
            className.length() > m_pInitData.m_Name.length() + 1) { // pos=0 limits the search to the prefix
            className = className.substr(m_pInitData.m_Name.length() + 1,
                                         className.length() - m_pInitData.m_Name.length() + 1);
        }

        if (foundRes == m_pClassCache.end()) {
            MonoClass *klass = nullptr;

            klass = mono_class_from_name(m_pEngineAssemblyImage, "MV", className.c_str());
            if (!klass) {
                GetLogger()->Script_LogDebug(
                        "Class not found in engine assembly, looking to user assembly: Class: {} Namespace: {}",
                        className.c_str(), m_pInitData.m_Name);
                klass = mono_class_from_name(m_pUserAssemblyImage, m_pInitData.m_Name.c_str(), className.c_str());
            }
            Assert(klass != nullptr, "Class not found! : ", className.c_str());

            ScriptClass &res = cacheClass(className, klass);

            res.initFieldOfClassType();

            return res;
        } else {
            return foundRes->second;
        }
    }

    static bool wasLoaded = false;

    void ScriptManager::initCore() {
        initEngineAssembly();
        initCoreFunctions();
        InitModule<ScriptingWorldObjectModule>(m_pEngineAssemblyImage);
        InitModule<ScriptingCollidersModule>(m_pEngineAssemblyImage);
        InitModule<ScriptingSpriteModule>(m_pEngineAssemblyImage);
        InitModule<ScriptingInputModule>(m_pEngineAssemblyImage);
        wasLoaded = true;
    }

    void ScriptManager::Reload() {
        Assert(!GetEngine()->IsGameRunning(), "You cant recompile while game is running!");

        GetLogger()->Script_LogInfo("Reloading scripts...");
        m_pDomainVersion++;

        for (auto &&handle: m_pGCHandles)
            mono_gchandle_free(handle);

        m_pGCHandles.clear();

        unloadAppDomain();

        //TODO: support engine assembly reloading

        auto assemblyPath = std::filesystem::path(m_pInitData.m_AssemblyPath.c_str());
        auto resFolder = assemblyPath.parent_path();

        const auto assemblyName = assemblyPath.stem().string();
        resFolder /= assemblyName.substr(0, assemblyName.length() - 3) + ".dll"; //Cut _MV from end of name.
        std::error_code error;
        bool res = std::filesystem::remove(assemblyPath, error);
        if (!res) {
            MV::GetLogger()->Script_LogError("Failed to remove old project assembly.");
        }
        std::filesystem::copy(resFolder, assemblyPath, error);
        if (error) {
            MV::GetLogger()->Script_LogError("Failed to copy new project assembly.");
        }

        initAppDomain();
        if (!wasLoaded) {
            initCore();
        }
        initUserAssembly();
        m_pOnScriptsReload = true;
        GetLogger()->Script_LogInfo("Reloaded scripts.");
    }

    void ScriptManager::Native_ScriptLog(MonoString *message, int32_t line, MonoString *file) {
        MV_PROFILE_FUNCTION("ScriptManager::Native_ScriptLog");
        const auto strMsg = mono_string_to_utf8(message);
        const auto fileMsg = mono_string_to_utf8(file);
        if (m_pLogCallback)
            m_pLogCallback(strMsg, line, fileMsg);
    }

    static void logFN(MonoString *str, int32_t line, MonoString *file) {
        ScriptManager::s_GlobalInstance->Native_ScriptLog(str, line, file);
    };

    static MonoArray *engine_GetWOsByTag(MonoString *tag) {
        return ScriptManager::s_GlobalInstance->Native_GetWOByTag(tag);
    };

    static MonoObject *engine_SpawnWorldObject(MonoReflectionType *type, const glm::vec2 &position) {

        return ScriptManager::s_GlobalInstance->Native_SpawnWorldObject(
                mono_type_get_name(mono_reflection_type_get_type(type)), position);
    };

    MonoObject *ScriptManager::Native_SpawnWorldObject(const char *name, const glm::vec2 &position) {
        auto *engine = GetEngine();
        auto &world = engine->GetWorld();

        ENSURE_TRUE(world.IsValid(), nullptr);

        WorldObjectSpawnParams params;

        params.m_Name = name;

        //TODO: Cut namespace from name

        auto wo = world.SpawnWorldObject(params);

        return wo->GetScript().GetObjectInstance().m_pObject;
    }

    static MonoObject *engine_CastRay(glm::vec2 start, glm::vec2 end, float len) {
        auto *engine = GetEngine();
        auto &world = engine->GetWorld();

        const auto hitRes = world.CastRay(start, end, len);

        return hitRes.IsHit() ? hitRes.m_WorldObject->GetScript().GetObjectInstance().GetMonoObject() : nullptr;
    };

    void ScriptManager::initCoreFunctions() {
        GetLogger()->Script_LogDebug("Initializing core engine functions....");

        mono_add_internal_call("MV.Debug::Message_Native", (const void *) logFN);
        mono_add_internal_call("MV.World::GetWorldObjectsByTag_Native", (const void *) engine_GetWOsByTag);
        mono_add_internal_call("MV.WorldObjectSpawnerInternal::Spawn_Native", (const void *) engine_SpawnWorldObject);
        mono_add_internal_call("MV.World::CastRay_Native", (const void *) engine_CastRay);

        GetLogger()->Script_LogDebug("Initialized core engine functions.");
    }

    void ScriptManager::initUserAssembly() {
        GetLogger()->Script_LogDebug("Initializing user assembly....");
        m_pUserAssembly = mono_domain_assembly_open(m_pMonoAppDomain, m_pInitData.m_AssemblyPath.c_str());
        if (!m_pUserAssembly) {
            constexpr auto err = "Failed to initialize user assembly!";
            GetLogger()->Script_LogError(err);
            throw RuntimeException(err);
        }
        GetLogger()->Script_LogDebug("Loaded user assembly from {}", m_pInitData.m_AssemblyPath);


        GetLogger()->Script_LogDebug("Initializing user assembly image....");
        m_pUserAssemblyImage = mono_assembly_get_image(m_pUserAssembly);
        GetLogger()->Script_LogDebug("Initialized user assembly image.");

        auto classes2 = GetAssemblyClassList(m_pEngineAssemblyImage);
        auto classes = GetAssemblyClassList(m_pUserAssemblyImage);

        for (auto &&c: classes2) {
            const auto name = MV::string(mono_class_get_name(c));
            if (name == "<Module>") continue;
            cacheClass(name, c);
        }

        for (auto &&c: classes) {
            const auto name = MV::string(mono_class_get_name(c));
            if (name == "<Module>") continue;
            cacheClass(name, c);
        }

        MV::PropertyManager manager{};
        manager.Init();
        manager.Cleanup();

        RegisterValidClassToken("UVector2");
        RegisterValidClassToken("IVector2");
        RegisterValidClassToken("Vector2");

        for (auto &&v: m_pValidProperties) {
            RegisterValidClassToken(v);
        }

        //Second pass, all base classes must be initialized
        for (auto &&c: classes) {
            const auto name = MV::string(mono_class_get_name(c));
            if (name == "<Module>") continue;
            m_pClassCache[name].initFieldOfClassType();
        }

        GetLogger()->Script_LogDebug("Initialized user assembly.");
    }

    void ScriptManager::SetUserAssembly(MV::string_view path, bool init) {
        m_pInitData.m_AssemblyPath = path;
        if (init)
            initUserAssembly();
    }

    void ScriptManager::SetUserNamespace(MV::string_view name) {
        m_pInitData.m_Name = name;
    }

    MonoArray *ScriptManager::Native_GetWOByTag(MonoString *tag) {
        MV_PROFILE_FUNCTION("ScriptManager::Native_GetWOByTag");
        const auto engine = GetEngine();
        auto &world = engine->GetWorld();
        if (!world.IsValid()) {
            GetLogger()->Script_LogError("Tried to call Native_GetWOByTag on invalid world!");
            return nullptr;
        }

        auto objects = world.GetWorldObjectsByTag(mono_string_to_utf8(tag));

        auto res = mono_array_new(m_pMonoDomain, GetClass("WorldObject").m_pClass, objects.size());

        for (size_t i = 0; i < objects.size(); ++i) {
            mono_array_set(res, MonoObject*, i, objects[i]->GetScript().m_pInstance.m_pObject);
        }

        return res;
    }

    void ScriptManager::initAppDomain() {
        GetLogger()->Script_LogDebug("Initializing app domain....");
        MV::string str = m_pInitData.m_Name;
        m_pMonoAppDomain = mono_domain_create_appdomain((char *) m_pInitData.m_Name.c_str(), NULL);
        if (!mono_domain_set(m_pMonoAppDomain, false)) {
            GetLogger()->Script_LogError("Failed to set app domain");
        }
        GetLogger()->Script_LogDebug("Initialized app domain..");
    }

    void ScriptManager::unloadAppDomain() {
        if (m_pMonoAppDomain && m_pMonoAppDomain != mono_get_root_domain()) {
            mono_domain_set(mono_get_root_domain(), false);
            mono_domain_unload(m_pMonoAppDomain);
            m_pMonoAppDomain = nullptr;
            for (auto &&c: m_pUserClasses)
                m_pClassCache.erase(c);
            m_pUserClasses.clear();
        }
    }

    void ScriptManager::Load() {
        if (!wasLoaded) {
            initCore();
        }
        initUserAssembly();
    }

    void ScriptManager::SetLogCallback(const LogCallback &callback) {
        m_pLogCallback = callback;
    }

    uint32_t ScriptManager::GetClassToken(MV::string_view className) {
        const auto findRes = m_pClassCache.find(className.data());

        if (findRes != m_pClassCache.end()) {
            return mono_class_get_type_token(findRes->second.m_pClass);
        }

        return 0;
    }

    void ScriptManager::RegisterValidClassToken(uint32_t token) {
        m_pValidClassTokens.emplace(token);
    }

    void ScriptManager::RegisterValidClassToken(MV::string_view className) {
        RegisterValidClassToken(GetClassToken(className));
    }

    bool ScriptManager::IsClassTokenValid(uint32_t token) const {
        const auto &found = m_pValidClassTokens.find(token);

        return found != m_pValidClassTokens.end();
    }

    void ScriptManager::registerGCHandle(uint32_t handle) {
        m_pGCHandles.emplace(handle);
    }

    ScriptClass &ScriptManager::cacheClass(MV::string_view name, MonoClass *klass) {
        ScriptClass res{};
        res.m_pClass = klass;
        Assert(res.m_pClass != nullptr, "Class not found! : ", name.data());
        res.init();

        auto realName = mono_class_get_name(res.m_pClass);

        Assert(name == MV::string(realName), "Class name has to match found class name");

        m_pUserClasses.emplace(realName);
        return m_pClassCache.try_emplace(realName, res).first->second;
    }

    bool ScriptManager::OnScriptsReload() {
        bool res = m_pOnScriptsReload;
        m_pOnScriptsReload = false;
        return res;
    }

    AssemblyInfo ScriptManager::GetAssemblyInfo() {
        AssemblyInfo result{};

        auto classes = GetAssemblyClassList(m_pEngineAssemblyImage);
        auto userClasses = GetAssemblyClassList(m_pUserAssemblyImage);

        for (auto &&c: classes) {
            const auto name = MV::string(mono_class_get_name(c));
            if (name == "<Module>") continue;

            if (mono_class_is_enum(c)) {
                addEnumToAssemblyInfo(c, result);
                continue;
            }

            addClassToAssemblyInfo(c, result);
        }

        for (auto &&c: userClasses) {
            const auto name = MV::string(mono_class_get_name(c));
            if (name == "<Module>") continue;

            if (mono_class_is_enum(c)) {
                addEnumToAssemblyInfo(c, result);
                continue;
            }

            addClassToAssemblyInfo(c, result);
        }


        return result;
    }

    void ScriptManager::addEnumToAssemblyInfo(MonoClass *klass, AssemblyInfo &info) {
        AssemblyEnumEntry v;
        const auto name = MV::string(mono_class_get_name(klass));

        v.m_Name = name;

        info.m_Enums.push_back(MV::move(v));
    }

    void ScriptManager::addClassToAssemblyInfo(MonoClass *klass, AssemblyInfo &info) {
        AssemblyClassEntry v;
        const auto name = MV::string(mono_class_get_name(klass));

        v.m_Name = name;
        v.m_Token = mono_class_get_type_token(klass);

        MonoClass *parent = mono_class_get_parent(klass);
        MV::string parentName = mono_class_get_name(parent);
        if (parentName != "Object" && parentName != "ValueType") {
            v.m_Parent.m_Name = parentName;
            v.m_Parent.m_Valid = true;
            v.m_Parent.m_Token = mono_class_get_type_token(parent);
        }
        {
            void *iter = nullptr;
            MonoClassField *field = nullptr;
            while ((field = mono_class_get_fields(klass, &iter)) != nullptr) {
                AssemblyFieldEntry f;

                MV::string fieldName = mono_field_get_name(field);

                f.m_Name = fieldName;

                const auto monoType = mono_field_get_type(field);

                auto typeID = mono_type_get_type(monoType);

                if (typeID == MONO_TYPE_CLASS || typeID == MONO_TYPE_VALUETYPE) {
                    auto fieldClass = mono_type_get_class(monoType);
                    if (!fieldClass) {
                        f.m_Type.m_Name = fmt::format("Class Unknown ({})", mono_type_get_name(monoType)).c_str();
                    } else {
                        f.m_Type.m_Valid = true;
                        f.m_Type.m_Name = mono_class_get_name(fieldClass);
                        f.m_Type.m_Token = mono_class_get_type_token(fieldClass);
                    }
                } else {

                    if (typeID == MONO_TYPE_SZARRAY) {
                        f.m_Type.m_Name = mono_type_get_name(monoType);
                    } else {
                        f.m_Type.m_Name = ScriptUtil::GetMonoTypeToString((MonoTypeEnum) typeID);
                    }
                }

                auto attrs = mono_custom_attrs_from_field(klass, field);

                v.m_Fields.push_back(MV::move(f));
            }
        }

        {
            void *iter = nullptr;
            MonoMethod *method = nullptr;
            while ((method = mono_class_get_methods(klass, &iter)) != nullptr) {
                AssemblyFunctionEntry f;

                MV::string fnName = mono_method_get_name(method);
                f.m_Name = fnName;

                auto signature = mono_method_signature(method);
                auto returnType = mono_signature_get_return_type(signature);

                {
                    auto typeID = mono_type_get_type(returnType);

                    if (typeID == MONO_TYPE_CLASS || typeID == MONO_TYPE_VALUETYPE) {
                        auto fieldClass = mono_type_get_class(returnType);
                        if (!fieldClass) {
                            f.m_ReturnType.m_Name = fmt::format("Class Unknown ({})",
                                                                mono_type_get_name(returnType)).c_str();
                        } else {
                            f.m_ReturnType.m_Valid = true;
                            f.m_ReturnType.m_Name = mono_class_get_name(fieldClass);
                            f.m_ReturnType.m_Token = mono_class_get_type_token(fieldClass);
                        }
                    } else {
                        if (typeID == MONO_TYPE_SZARRAY) {
                            f.m_ReturnType.m_Name = mono_type_get_name(returnType);
                        } else {
                            f.m_ReturnType.m_Name = ScriptUtil::GetMonoTypeToString((MonoTypeEnum) typeID);
                        }
                    }
                }

                MV::vector<const char *> paramNames;

                {
                    uint32_t count = mono_signature_get_param_count(signature);
                    paramNames.resize(count);
                }

                mono_method_get_param_names(method, paramNames.data());


                void *parIter = nullptr;
                MonoType *param = nullptr;
                int32_t counter = 0;
                while ((param = mono_signature_get_params(signature, &parIter)) != nullptr) {
                    AssemblyFunctionParamEntry p;
                    auto typeID = mono_type_get_type(param);

                    if (typeID == MONO_TYPE_CLASS || typeID == MONO_TYPE_VALUETYPE) {
                        auto fieldClass = mono_type_get_class(param);
                        if (!fieldClass) {
                            p.m_Type.m_Name = fmt::format("Class Unknown ({})", mono_type_get_name(param)).c_str();
                        } else {
                            p.m_Type.m_Valid = true;
                            p.m_Type.m_Name = mono_class_get_name(fieldClass);
                            p.m_Type.m_Token = mono_class_get_type_token(fieldClass);
                        }
                    } else {
                        if (typeID == MONO_TYPE_SZARRAY) {
                            p.m_Type.m_Name = mono_type_get_name(param);
                        } else {
                            p.m_Type.m_Name = ScriptUtil::GetMonoTypeToString((MonoTypeEnum) typeID);
                        }
                    }

                    p.m_Name = paramNames[counter];

                    f.m_Params.push_back(MV::move(p));
                    counter++;
                }

                v.m_Functions.push_back(MV::move(f));
            }
        }

        info.m_Classes.push_back(MV::move(v));
    }

    MV::vector<ScriptClass> ScriptManager::GetClassesThatAreChildOfClass(const ScriptClass &Klass) {
        MV::vector<ScriptClass> result;

        for(auto && c : m_pClassCache) {
            if(c.second.IsChildOfClass(Klass)) {
                result.emplace_back(c.second);
            }
        }

        return result;
    }

    MonoString* ScriptManager::CreateMonoString(MV::string_view data) {
        return mono_string_new(m_pMonoAppDomain, data.data());
    }
}