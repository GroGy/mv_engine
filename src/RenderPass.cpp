//
// Created by Matty on 2021-10-14.
//

#include "../include/render/common/RenderPass.h"

namespace MV {
    RenderPass::RenderPass(const glm::vec4 &clearColor) : m_pClearColor(clearColor) {
        static uint32_t counter = 0;
        m_pID = ++counter;
    }

    void RenderPass::SetClearColor(const glm::vec4 &clearColor) {
        m_pClearColor = clearColor;
    }
}
