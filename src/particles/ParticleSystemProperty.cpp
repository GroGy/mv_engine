//
// Created by Matty on 2022-09-24.
//

#include <utility>

#include "common/Property.h"
#include "particles/cpu/ParticleSystemProperty.h"
#include "common/EngineCommon.h"
#include "profiler/Profiler.h"
#include "util/Math.h"
#include "ext/matrix_transform.hpp"

namespace MV {
    void ParticleSystemProperty::Init() {
        Property::Init();

        m_pParticles = make_rc<MV::vector<CPUParticles::Particle>>();

        m_pParticles->resize(m_GeneralSettings.m_MaxParticles);
        m_pRenderedParticles.resize(m_GeneralSettings.m_MaxParticles);
    }

    void ParticleSystemProperty::Update(float deltaTime) {
        Property::Update(deltaTime);
        m_pUpdateDelta += deltaTime;

        m_pToSpawnTimeAccumulator += deltaTime;

        const float timePerParticle = (1.0f / (float) m_GeneralSettings.m_SpawnRate);

        while (m_pToSpawnTimeAccumulator > timePerParticle) {
            m_pToSpawn++;
            m_pToSpawnTimeAccumulator -= timePerParticle;
        };

        //m_pToSpawn += m_pToSpawnTimeAccumulator / timePerParticle;

        //m_pToSpawnTimeAccumulator = fmod(m_pToSpawnTimeAccumulator,timePerParticle);

        if (m_pPromise) {
            ENSURE_TRUE(m_pPromise->IsDone());

            if (m_pRenderedParticles.size() == m_pParticles->size()) {
                m_pRenderedParticles = *m_pParticles;
            } else {
                *m_pParticles = m_pRenderedParticles;
            }

            m_pActiveParticles = m_pPromise->GetResult()->m_ActiveParticles;

            m_pPromise.reset();
        }

        m_pPromise = GetEngine()->m_MainThreadPool->DispatchSingleFrameTask<ParticleUpdateTask>(m_pUpdateDelta,
                                                                                                MV::GetEngine()->GetTime(),
                                                                                                m_pToSpawn,
                                                                                                m_pParticles, 0,
                                                                                                m_pParticles->size(),
                                                                                                m_GeneralSettings,
                                                                                                m_RenderingSettings,
                                                                                                m_MovementSettings);
        m_pUpdateDelta = 0.0f;
        m_pToSpawn = 0;
    }

    void ParticleSystemProperty::Cleanup() {

    }

    void ParticleSystemProperty::Serialize(nlohmann::json &woData) {
        Property::Serialize(woData);
    }

    void ParticleSystemProperty::Deserialize(const nlohmann::json &woData) {
        Property::Deserialize(woData);
    }

    void ParticleSystemProperty::Sync(Script &owner, const ScriptClassField &field) {
        bool smaller = m_pRenderedParticles.size() < m_GeneralSettings.m_MaxParticles;
        if (m_pRenderedParticles.size() != m_GeneralSettings.m_MaxParticles) {
            m_pRenderedParticles.resize(m_GeneralSettings.m_MaxParticles);
            if (smaller)
                m_pRenderedParticles.shrink_to_fit();
        }

    }

    void ParticleSystemProperty::Destroy() {
        Property::Destroy();
    }

    const std::initializer_list<MV::ParticleVertex> VERTICES = {
            {{-0.5f, 0.5f},  {0.0f, 1.0f}},
            {{0.5f,  -0.5f}, {1.0f, 0.0f}},
            {{0.5f,  0.5f},  {1.0f, 1.0f}},
            {{-0.5f, -0.5f}, {0.0f, 0.0f}}
    };

    void ParticleSystemProperty::Render(FrameContext &context) {
        MV_PROFILE_FUNCTION("ParticleSystemProperty::Render");
        MV_PROFILE_TAG("Particle count", m_pRenderedParticles.size());
        auto &drawRequests = context.m_DrawQueue[ParticlePipeline::s_ParticlePipelineReservedID];

        MaterialInput input{};

        input.m_UniformTextureName = "uTexture";
        input.m_UniformTextures.emplace_back(MissingTexture::sc_MissingTextureReservedID);

        auto &drawData = drawRequests.m_Commands[input];

        if (drawData.m_VertexBuffers[0].empty()) {
            const auto dataSize = VERTICES.size() * sizeof(MV::ParticleVertex);
            drawData.m_VertexBuffers[0].resize(dataSize);
            memcpy(drawData.m_VertexBuffers[0].data(), data(VERTICES), dataSize);
        }

        if (!m_pRenderedParticles.empty()) {
            auto sizeBefore = drawData.m_VertexBuffers[1].size();
            const auto dataSize = m_pRenderedParticles.size() * sizeof(CPUParticles::Particle);
            drawData.m_VertexBuffers[1].resize(drawData.m_VertexBuffers[1].size() + dataSize);
            memcpy(drawData.m_VertexBuffers[1].data() + sizeBefore, m_pRenderedParticles.data(), dataSize);
        }

        drawData.m_Instanced = true;
        drawData.m_InstanceCount += m_pRenderedParticles.size();

        drawData.m_Indices.insert(drawData.m_Indices.end(), {
                0, 1, 2,
                3, 1, 0,
        });

        const auto woPos = m_WorldObject->GetPosition();
        const auto woRot = m_WorldObject->GetRotation();
        const auto woScale = m_WorldObject->GetScale();

        glm::mat4 mat = glm::mat4(1.0f);
        mat = glm::translate(mat, {woPos.x, woPos.y, 0});
        mat = glm::rotate(mat, glm::radians(woRot), {0, 0, 1});
        mat = glm::scale(mat, {woScale.x, woScale.y, 1});

        drawData.m_ModelMatrix = mat;
    }

    void ParticleSystemProperty::SyncPropertyData(const ScriptClassField &field, Script &script, const json &data) {

    }

    void ParticleUpdateTask::Work() {
        ENSURE_VALID(m_pParticles);

        MV::vector<CPUParticles::Particle> &particles = *m_pParticles;

        for (uint32_t i = 0; i < m_pCount; i++) {
            uint32_t index = m_pStartIndex + i;
            CPUParticles::Particle &particle = particles[index];

            updateParticle(particle);
        }
    }

    const char *ParticleUpdateTask::GetName() {
        return "Particle update task";
    }

    void ParticleUpdateTask::updateParticle(CPUParticles::Particle &particle) {
        if (m_pToSpawn > 0 && !particle.m_Alive) {
            m_pToSpawn--;
            particle.m_Alive = true;
            particle.m_SpawnTime = m_pTime;
            particle.m_Position = m_pGeneralSettings.m_SpawnPosition;
            particle.m_Rotation = m_pGeneralSettings.m_SpawnRotation;
            particle.m_Scale = m_pGeneralSettings.m_SpawnScale;

            if (m_pMovementSettings.m_pUseSpawnVelocityRanges) {
                particle.m_Velocity.x = Math::FRand(m_pMovementSettings.m_SpawnVelocityXRange.x,
                                                    m_pMovementSettings.m_SpawnVelocityXRange.y);
                particle.m_Velocity.y = Math::FRand(m_pMovementSettings.m_SpawnVelocityYRange.x,
                                                    m_pMovementSettings.m_SpawnVelocityYRange.y);
            } else {
                particle.m_Velocity.x = m_pMovementSettings.m_SpawnVelocityXRange.x;
                particle.m_Velocity.y = m_pMovementSettings.m_SpawnVelocityYRange.x;
            }

            particle.m_Scale.x = Math::FRand(m_pMovementSettings.m_SpawnScaleXRange.x,
                                             m_pMovementSettings.m_SpawnScaleXRange.y);
            particle.m_Scale.y = Math::FRand(m_pMovementSettings.m_SpawnScaleYRange.x,
                                             m_pMovementSettings.m_SpawnScaleYRange.y);
        }

        if (particle.m_SpawnTime + particle.m_Lifetime < m_pTime) {
            // On death
            particle.m_Alive = false;
        }

        if (particle.m_Alive)
            m_ActiveParticles++;

        if (!particle.m_Alive)
            return;

        // Update particle, code below happens only when particle is alive
        const float lifetime = (m_pTime - particle.m_SpawnTime) / particle.m_Lifetime;

        particle.m_Position += particle.m_Velocity * m_pDeltaTime;
        particle.m_Velocity += m_pMovementSettings.m_WorldForce;

        particle.m_Velocity.x =
                particle.m_Velocity.x * Math::Map(m_pDeltaTime, 0.0f, 1.0f, 1.0f, m_pMovementSettings.m_Dampening.x);
        particle.m_Velocity.y =
                particle.m_Velocity.y * Math::Map(m_pDeltaTime, 0.0f, 1.0f, 1.0f, m_pMovementSettings.m_Dampening.y);

        particle.m_Scale = glm::vec2{m_pRenderingSettings.m_SizeOverLifetime.GetValue(lifetime)};
    }

    ParticleUpdateTask::ParticleUpdateTask(float deltaTime, double time, uint32_t toSpawn,
                                           const MV::rc<MV::vector<CPUParticles::Particle>> &particles,
                                           uint32_t startIndex,
                                           uint32_t count, ParticleSystemGeneralSettings generalSettings,
                                           ParticleRenderingSettings renderingSettings,
                                           ParticleSystemMovementSettings movementSettings) :
            m_pParticles(particles),
            m_pCount(count),
            m_pStartIndex(startIndex),
            m_pGeneralSettings(generalSettings),
            m_pMovementSettings(movementSettings),
            m_pRenderingSettings(MV::move(renderingSettings)),
            m_pToSpawn(toSpawn),
            m_pTime(time),
            m_pDeltaTime(deltaTime) {

    }
}