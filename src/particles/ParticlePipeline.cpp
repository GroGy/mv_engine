//
// Created by Matty on 2022-09-26.
//

#include "particles/cpu/ParticlePipeline.h"
#include "render/vulkan/VulkanShader.h"

namespace MV {
    PipelineBlueprint ParticlePipeline::GetBlueprint() {
        PipelineBlueprint blueprint{};

        blueprint.SetVertexInputDescription(ParticleVertex::GetBindingDescription(),
                                            ParticleVertex::GetAttributeDescriptions());

        blueprint.EnableAlphaBlending(false);

        m_MVPmatrixDescription = blueprint.AddPushConstant("MVP",VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4));

        blueprint.AddShader(
                MV::make_rc<VulkanShader>(".shadercache/particles_vertex_0.shadercache", ShaderType::VERTEX));
        blueprint.AddShader(
                MV::make_rc<VulkanShader>(".shadercache/particles_fragment_0.shadercache", ShaderType::FRAGMENT));

        UniformBlueprint textureUniformBp{ShaderStage::FRAGMENT, 0, 1};
        blueprint.AddUniform("uTexture",textureUniformBp);

        return blueprint;
    }
}