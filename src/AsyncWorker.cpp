//
// Created by Matty on 2022-09-25.
//

#include "async/AsyncWorker.h"
#include "ensure/Ensure.h"
#include "profiler/Profiler.h"

namespace MV {
    void AsyncWorker::Init() {
        m_pThread = std::thread([this](){
            threadMain();
        });
    }

    void AsyncWorker::Cleanup() {
        m_pStop = true;

        while(!m_pFinished) {
            m_pSleepingCV.notify_all();
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }
        m_pThread.join();
    }

    void AsyncWorker::threadMain() {
        m_pThreadID = std::this_thread::get_id();

        MV_PROFILE_THREAD("Async worker");

        std::mutex mutex;
        std::unique_lock<std::mutex> lock{mutex};
        while(!m_pStop) {
            m_pSleepingCV.wait(lock);
            if(m_pStop) break;

            if(m_pCurrentTask) {
                processTask();
            }
        }
        m_pFinished = true;
    }

    void AsyncWorker::processTask() {
        MV_PROFILE_FUNCTION("AsyncTask::Work");
        MV_PROFILE_TAG("Task", m_pCurrentTask->GetName());

        m_pCurrentTask->Work();

        m_pCurrentTask->m_pDone = true;

        m_pProcessedTask.fetch_add(1);
    }

    bool AsyncWorker::CanDispatch() const {
        return m_pProcessedTask == m_pRequestedTask;
    }

    void AsyncWorker::DispatchTask(const rc <AsyncTask> &task, bool frameCritical) {
        ENSURE_TRUE(m_pProcessedTask == m_pRequestedTask);
        m_pRequestedTask.fetch_add(1);
        m_pCurrentTask = task;
        m_pCritical = frameCritical;
        m_pSleepingCV.notify_all();
    }

    void AsyncWorker::Await() {
        while(!CanDispatch()) {
            std::this_thread::yield();
        }
    }

    AsyncWorker::AsyncWorker() {}
}
