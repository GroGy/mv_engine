//
// Created by Matty on 2022-09-19.
//

#include <ensure/EnsureLog.h>
#include "common/EngineCommon.h"

namespace MV {
    void LogEnsure(string_view expression, string_view file, uint32_t line) {
        GetLogger()->LogError("ENSURE FAILED: {};{}:{}", expression, file, line);
    }
}
