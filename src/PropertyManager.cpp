//
// Created by Matty on 2022-03-10.
//

#include "../include/render/common/PropertyManager.h"
#include "../include/common/EngineCommon.h"
#include "precompiler/PropertyRegistrator.h"

namespace MV {

    PropertyManager::~PropertyManager() {
        Cleanup();
    }

    void PropertyManager::Init(const eastl::function<void(uint32_t token, size_t storageIndex)> & clb) {
        auto & allProperties = MV::RegisteredProperties::GetInstance().m_Properties;
        uint32_t maxId = 0;

        // Find highest ID
        for(auto && prop : allProperties) {
            if(prop.m_ID > maxId) maxId = prop.m_ID;
        }

        if(m_pStorages.size() < maxId + 1)
            m_pStorages.resize(maxId + 1);

        for(auto && prop : allProperties) {
            if(!prop.m_HasRegisteredScriptToken) {
                registerPropertyScriptToken(prop.m_ClassName);
                prop.m_HasRegisteredScriptToken = true;
            }
            auto id = prop.m_ID;
            m_pStorages[id] = static_cast<IPropertyStorage*>(prop.m_Ctor());
            initPropertyToken(prop.m_ClassName,id, clb);
        }
    }

    void PropertyManager::Cleanup() {
        for (auto val: m_pStorages) {
            delete val;
        }
        m_pStorages.clear();
    }

    void PropertyManager::ForEachStorage(const eastl::function<void(IPropertyStorage *)> &callback) {
        for(size_t i = 0; i < m_pStorages.size(); i++) {
            callback(m_pStorages[i]);
        }
    }

    void PropertyManager::Clear() {
        for (auto & val: m_pStorages) {
            val->clear();
        }
    }

    void PropertyManager::registerPropertyScriptToken(MV::string_view className) {
        GetEngine()->m_ScriptManager.RegisterValidClassToken(className);
    }

    void PropertyManager::initPropertyToken(MV::string_view className, PropertyIDType id,const eastl::function<void(uint32_t token, size_t storageIndex)> & clb) {
        const auto classToken = GetEngine()->m_ScriptManager.GetClassToken(className);
        m_pTokenProperties[classToken] = id;
        if(clb)
            clb(classToken, id);
    }

    size_t PropertyManager::CreatePropertyFromToken(uint32_t token, const MV::rc<WorldObject> & wo) {
        Assert(m_pTokenProperties.find(token) != m_pTokenProperties.end() && "Token must be present in token properties for this to work");
        const auto index = m_pStorages[m_pTokenProperties[token]]->create();
        m_pStorages[m_pTokenProperties[token]]->get_property(index)->m_WorldObject = wo;
        return index;
    }

    IPropertyStorage *PropertyManager::GetIStorageFromToken(uint32_t token) const {
        return m_pStorages[m_pTokenProperties.at(token)];
    }

    PropertyManager::PropertyManager(PropertyManager &&other) noexcept {
        m_pStorages = MV::move(other.m_pStorages);
        //other.m_pStorages.clear();

        m_pTokenProperties = MV::move(other.m_pTokenProperties);
        //other.m_pTokenProperties.clear();

    }

    PropertyManager &PropertyManager::operator=(PropertyManager &&other) noexcept {
        m_pStorages = MV::move(other.m_pStorages);
        other.m_pStorages.clear();

        m_pTokenProperties = MV::move(other.m_pTokenProperties);
        other.m_pTokenProperties.clear();

        return *this;
    }
}