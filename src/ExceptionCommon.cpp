//
// Created by Matty on 2021-10-01.
//

#include "../include/common/ExceptionCommon.h"

MV::RuntimeException::RuntimeException(MV::string what) : m_pData(MV::move(what)){

}

const char *MV::RuntimeException::what() const noexcept {
    return m_pData.c_str();
}
