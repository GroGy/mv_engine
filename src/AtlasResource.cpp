//
// Created by Matty on 2021-10-31.
//

#include "../include/resources/AtlasResource.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    ResourceLoadResult AtlasResource::process() {
        m_pPath = PathProjectRelativeToAbsolute(m_pPath);
        std::ifstream input{m_pPath.c_str(),std::ios::binary};
        if(input.is_open()) {
            auto header = ReadAssetHeader(input);

            assert(header.m_Type == AssetType::ATLAS);

            input.read((char*)&m_Result.m_Texture, sizeof(uint64_t));

            uint32_t uvCount = 0;

            input.read((char*)&uvCount, sizeof(uint32_t));

            m_Result.m_UVs.resize(uvCount);
            m_Result.m_UVOffsets.resize(uvCount);
            m_Result.m_FrameNames.resize(uvCount);
            m_Result.m_Timings.resize(uvCount);

            input.read((char*)m_Result.m_UVs.data(), sizeof(glm::vec4) * uvCount);
            input.read((char*)m_Result.m_UVOffsets.data(), sizeof(glm::vec4) * uvCount);
            input.read((char*)m_Result.m_Timings.data(), sizeof(float) * uvCount);

            for(uint32_t i = 0 ; i < uvCount; i++) {
                uint32_t length = 0;
                input.read((char*)&length, sizeof(uint32_t));
                if(length > 65536) {
                    input.close();
                    return ResourceLoadResult::FILE_READ_ERROR;
                }

                m_Result.m_FrameNames[i].resize(length);
                input.read(m_Result.m_FrameNames[i].data(), sizeof(char) * length);
            }

            input.close();
        } else {
            return ResourceLoadResult::FILE_READ_ERROR;
        }

        m_pSuccess = true;
        return ResourceLoadResult::SUCCESS;
    }

    void AtlasResource::finish() {
        if (!m_pSuccess)
            return;

        GetResources()->m_Atlases[m_pID] = make_rc<Atlas>(m_Result);
        rc<RenderingResource> rr;
        if(!GetRenderer()->GetRenderingResource(m_Result.m_Texture,rr)) {
            GetEngine()->LoadResource(m_Result.m_Texture);
        }
    }

    AtlasResource::AtlasResource(MV::string path, uint64_t id) : Resource(MV::move(path)), m_pID(id) {

    }
}