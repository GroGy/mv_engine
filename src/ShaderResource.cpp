//
// Created by Matty on 2022-03-13.
//

#include "../include/resources/ShaderResource.h"
#include "../include/common/EngineCommon.h"
#include <filesystem>

namespace MV {
    ShaderResource::ShaderResource(MV::string path, uint64_t id) : Resource(MV::move(path)), m_pID(id) {

    }

    ResourceLoadResult ShaderResource::process() {
        m_pPath = PathProjectRelativeToAbsolute(m_pPath);

        std::ifstream i(m_pPath.c_str());
        if (i.is_open()) {

            try {
                i >> m_Result.m_RawData;
                const auto &json = m_Result.m_RawData;
                m_Result.m_Vertex = json["vertex"].get<MV::string>();
                m_Result.m_Fragment = json["fragment"].get<MV::string>();

                try {
                    m_Result.m_EnableAlphaBlending = json.at("alpha_blending").get<bool>();
                } catch (const nlohmann::json::exception &e) {
                    m_Result.m_EnableAlphaBlending = false;
                }

                const auto &inputBuffersJson = json[Config::sc_ShaderJsonInputBuffersField.data()];
                m_Result.m_DataBuffers.resize(inputBuffersJson.size());

                for (size_t j = 0; j < inputBuffersJson.size(); j++) {
                    const auto &bufJson = inputBuffersJson[j];
                    auto &buffer = m_Result.m_DataBuffers[j];

                    buffer.m_Stride = bufJson[Config::sc_ShaderJsonStrideField.data()].get<uint32_t>();
                    buffer.m_Binding = bufJson[Config::sc_ShaderJsonBindingsField.data()].get<uint32_t>();
                    buffer.m_Rate = static_cast<uint8_t>(bufJson[Config::sc_ShaderJsonRateField.data()].get<uint32_t>());

                    const auto &bufAttrsJson = bufJson[Config::sc_ShaderJsonVertexLayoutField.data()];
                    buffer.m_Attributes.resize(bufAttrsJson.size());

                    for (size_t k = 0; k < bufAttrsJson.size(); k++) {
                        auto &attr = buffer.m_Attributes[k];
                        const auto &attrJson = bufAttrsJson[k];

                        attr.m_Name = attrJson[Config::sc_ShaderJsonLayoutNameField.data()].get<MV::string>();
                        attr.m_Type = attrJson[Config::sc_ShaderJsonLayoutTypeField.data()].get<uint32_t>();;
                        attr.m_Location = attrJson[Config::sc_ShaderJsonLayoutLocationField.data()].get<uint32_t>();;
                        attr.m_Offset = attrJson[Config::sc_ShaderJsonLayoutOffsetField.data()].get<uint32_t>();;
                    }
                }

                const auto &uniformsJson = json[MV::Config::sc_ShaderJsonUniformsField.data()];
                m_Result.m_Uniforms.resize(uniformsJson.size());

                for (size_t j = 0; j < uniformsJson.size(); j++) {
                    auto &v = m_Result.m_Uniforms[j];
                    const auto &uJson = uniformsJson[j];

                    v.m_ShaderStage = uJson[MV::Config::sc_ShaderJsonUniformShaderStageField.data()].get<uint32_t>();
                    v.m_Binding = uJson[MV::Config::sc_ShaderJsonUniformBindingField.data()].get<uint32_t>();
                    v.m_DescriptorCount = uJson[MV::Config::sc_ShaderJsonUniformArraySizeField.data()].get<uint32_t>();
                    v.m_Offset = uJson[MV::Config::sc_ShaderJsonUniformOffsetField.data()].get<uint32_t>();
                    v.m_Size = uJson[MV::Config::sc_ShaderJsonUniformSizeField.data()].get<uint32_t>();
                    v.m_Type = uJson[MV::Config::sc_ShaderJsonUniformTypeField.data()].get<uint32_t>();
                    v.m_Name = uJson[MV::Config::sc_ShaderJsonUniformNameField.data()].get<MV::string>();
                    v.m_Nullable = uJson.contains(MV::Config::sc_ShaderJsonUniformNullableField.data())
                                   ? uJson[MV::Config::sc_ShaderJsonUniformNullableField.data()].get<bool>() : false;
                }

                const auto &pushConstantsJson = json[MV::Config::sc_ShaderJsonPushConstantsField.data()];
                m_Result.m_PushConstants.resize(pushConstantsJson.size());

                for (size_t j = 0; j < pushConstantsJson.size(); j++) {
                    auto &v = m_Result.m_PushConstants[j];
                    const auto &pcJson = pushConstantsJson[j];

                    v.m_ShaderStage = pcJson[MV::Config::sc_ShaderJsonPushConstantShaderStageField.data()].get<uint32_t>();
                    v.m_Offset = pcJson[MV::Config::sc_ShaderJsonPushConstantOffsetField.data()].get<uint32_t>();
                    v.m_Size = pcJson[MV::Config::sc_ShaderJsonPushConstantSizeField.data()].get<uint32_t>();
                    v.m_Name = pcJson[MV::Config::sc_ShaderJsonPushConstantNameField.data()].get<MV::string>();
                }

            } catch (std::exception &e) {
                i.close();
                MV::GetLogger()->LogError("Failed to load shader, error: {}", e.what());
                m_pSuccess = false;
                return ResourceLoadResult::FAILED;
            }
            i.close();
            auto &shaderCompiler = GetEngine()->m_ShaderCompiler;

            const auto res = shaderCompiler.CompileShader(m_pID, m_Result.m_RawData);

            if (!res.m_Error.empty()) {
                MV::GetLogger()->LogError("Failed to compile shader, error: {}", res.m_Error);
                m_pSuccess = false;
                return ResourceLoadResult::FAILED;
            }

            m_Result.m_FragmentPath = MV::ShaderCompiler::GetShaderPath(ShaderType::FRAGMENT, m_pID, 0);
            m_Result.m_VertexPath = MV::ShaderCompiler::GetShaderPath(ShaderType::VERTEX, m_pID, 0);

            m_pSuccess = true;
            return ResourceLoadResult::SUCCESS;
        }

        return ResourceLoadResult::FILE_READ_ERROR;
    }

    void ShaderResource::finish() {
        if (!m_pSuccess)
            return;

        auto renderer = GetRenderer();

        PipelineBlueprint blueprint{};

        blueprint.AddShader(renderer->CreateShader(m_Result.m_FragmentPath, ShaderType::FRAGMENT));
        blueprint.AddShader(renderer->CreateShader(m_Result.m_VertexPath, ShaderType::VERTEX));

        size_t totalAttributes = 0;

        for (auto &&buf: m_Result.m_DataBuffers) {
            totalAttributes += buf.m_Attributes.size();
        }

        MV::vector<VertexBindingDescriptor> bindingDescription(m_Result.m_DataBuffers.size());

        for (size_t i = 0; i < bindingDescription.size(); i++) {
            const auto &buf = m_Result.m_DataBuffers[i];
            bindingDescription[i].binding = buf.m_Binding;
            bindingDescription[i].stride = buf.m_Stride;
            bindingDescription[i].inputRate = static_cast<VertexInputRate>(buf.m_Rate);
        }

        MV::vector<VertexAttributeDescriptor> attributeDescriptions(totalAttributes);
        size_t counter = 0;
        for (auto &&buf: m_Result.m_DataBuffers) {
            for (auto &&att: buf.m_Attributes) {
                attributeDescriptions[counter].format = static_cast<MV::AttributeType>(att.m_Type);
                attributeDescriptions[counter].binding = buf.m_Binding;
                attributeDescriptions[counter].offset = att.m_Offset;
                attributeDescriptions[counter].location = att.m_Location;
                counter++;
            }
        }

        for (auto &&uni: m_Result.m_Uniforms) {
            if (uni.m_Type == static_cast<uint32_t>(MV::UniformDescriptorType::BUFFER)) {
                UniformBlueprint bp{
                        uni.m_ShaderStage,
                        uni.m_Binding,
                        uni.m_DescriptorCount,
                        uni.m_Offset,
                        uni.m_Size
                };
                blueprint.AddUniform(uni.m_Name, bp);
            } else {
                UniformBlueprint bp{
                        uni.m_ShaderStage,
                        uni.m_Binding,
                        uni.m_DescriptorCount
                };
                blueprint.AddUniform(uni.m_Name, bp);
            }
        }

        for (auto &&pc: m_Result.m_PushConstants) {
            blueprint.AddPushConstant(pc.m_Name, pc.m_ShaderStage, pc.m_Offset, pc.m_Size);
        }

        blueprint.SetVertexInputDescription(bindingDescription, attributeDescriptions);

        blueprint.EnableAlphaBlending(m_Result.m_EnableAlphaBlending);

        auto pipeline = renderer->CreateShaderPipeline(MV::move(blueprint));

        renderer->RegisterRenderingResource(m_pID, pipeline);

        m_pSuccess = true;
    }
}