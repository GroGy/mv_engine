//
// Created by Matty on 2021-10-05.
//

#include "../include/render/common/ShaderPipeline.h"

namespace MV {
    void ShaderPipeline::Refresh() {
        if (m_pLoaded) Cleanup();
        Init();
    }

    void ShaderPipeline::AddShader(const rc <Shader> &shader) {
        m_pBlueprint.m_pShaders.emplace_back(shader);
    }

    ShaderPipeline::ShaderPipeline(PipelineBlueprint && bp) : m_pBlueprint(MV::move(bp)) {
        static uint32_t idCounter = 0;
        m_pID = idCounter++;
    }

    const MV::vector<MV::rc<Shader>> &ShaderPipeline::getShaders() const {
        return m_pBlueprint.m_pShaders;
    }

    const MV::vector<VertexBindingDescriptor> &ShaderPipeline::getVertexBindingDescriptors() const {
        return m_pBlueprint.m_pVertexInputDescription;
    }

    const MV::vector<VertexAttributeDescriptor> &ShaderPipeline::getVertexAttributeDescriptions() const {
        return m_pBlueprint.m_pVertexAttributeDescription;
    }

    bool ShaderPipeline::isAlphaBlendingEnabled() const {
        return m_pBlueprint.m_pEnableAlphaBlending;
    }

    uint32_t ShaderPipeline::getCacheSize() const {
        return m_pBlueprint.m_pCacheSize;
    }

    const MV::vector_map<MV::string,PushConstantInfo> &ShaderPipeline::getPushConstants() const {
        return m_pBlueprint.m_pPushConstants;
    }

    const MV::vector_map<MV::string,UniformBlueprint> &ShaderPipeline::getUniformBlueprints() const {
        return m_pBlueprint.m_pUniformDescriptorBlueprints;
    }

    UniformDescription ShaderPipeline::CreateUniform(MV::string_view name) {
        const auto & uniformDescriptor = m_pBlueprint.m_pUniformDescriptorBlueprints[name.data()];

        UniformDescription res{uniformDescriptor.shaderStage,
                               uniformDescriptor.binding,
                               uniformDescriptor.descriptorCount,
                               uniformDescriptor.offset,
                               uniformDescriptor.size,
                               uniformDescriptor.type,
                               name};

        res.pipelineID = m_pID;

        return res;
    }

    UniformDescription ShaderPipeline::CreateUniform(size_t index) {
        const auto & uniformDescriptor = m_pBlueprint.m_pUniformDescriptorBlueprints.at(index).second;

        UniformDescription res{uniformDescriptor.shaderStage,
                               uniformDescriptor.binding,
                               uniformDescriptor.descriptorCount,
                               uniformDescriptor.offset,
                               uniformDescriptor.size,
                               uniformDescriptor.type,
                               "UH"};

        res.pipelineID = m_pID;

        return res;
    }

    PushConstantDescription ShaderPipeline::CreatePushConstant(MV::string_view name) {
        const auto & pcDescriptor = m_pBlueprint.m_pPushConstants[name.data()];
        return {pcDescriptor.shaderStage, pcDescriptor.offset, pcDescriptor.size};
    }

    PushConstantDescription ShaderPipeline::CreatePushConstant(size_t index) {
        const auto & pcDescriptor = m_pBlueprint.m_pPushConstants.at(index).second;
        return {pcDescriptor.shaderStage, pcDescriptor.offset, pcDescriptor.size};
    }

    void PipelineBlueprint::moveFromOther(PipelineBlueprint &&o) {
        m_pAssignedStages = MV::move(o.m_pAssignedStages);
        m_pShaders = MV::move(o.m_pShaders);
        m_pPushConstants = MV::move(o.m_pPushConstants);
        //m_pDepthStencilState = o.m_pDepthStencilState;
        //m_pHasDepthStencilState = o.m_pHasDepthStencilState;
        m_pUniformDescriptorBlueprints = MV::move(o.m_pUniformDescriptorBlueprints);
        m_pCacheSize = MV::move(o.m_pCacheSize);
        m_pVertexInputDescription = MV::move(o.m_pVertexInputDescription);
        m_pVertexAttributeDescription = MV::move(o.m_pVertexAttributeDescription);
        m_pEnableAlphaBlending = o.m_pEnableAlphaBlending;
    }

    PipelineBlueprint::PipelineBlueprint(PipelineBlueprint &&o) noexcept {
        moveFromOther(MV::move(o));
    }

    PipelineBlueprint &PipelineBlueprint::operator=(PipelineBlueprint &&o) noexcept {
        moveFromOther(MV::move(o));
        return *this;
    }

    void PipelineBlueprint::EnableAlphaBlending(bool value) {
        m_pEnableAlphaBlending = value;
    }

    void
    PipelineBlueprint::SetVertexInputDescription(
            const vector<VertexBindingDescriptor> &inputBindingDescription,
            const vector<VertexAttributeDescriptor> &attributeDescription) {
        m_pVertexInputDescription = inputBindingDescription;
        m_pVertexAttributeDescription = attributeDescription;
    }

    void PipelineBlueprint::AddShader(const MV::rc<Shader> &shader) {
        m_pShaders.emplace_back(shader);
    }

    UniformDescription PipelineBlueprint::AddUniform(MV::string_view name, const UniformBlueprint &uniformDescriptor) {
        m_pUniformDescriptorBlueprints[name.data()] = (uniformDescriptor);

        UniformDescription res{uniformDescriptor.shaderStage,
                               uniformDescriptor.binding,
                               uniformDescriptor.descriptorCount,
                               uniformDescriptor.offset,
                               uniformDescriptor.size,
                               uniformDescriptor.type,
                               name};

        return res;
    }

    PushConstantDescription
    PipelineBlueprint::AddPushConstant(MV::string_view name, ShaderStage_ shaderStage, uint32_t offset, uint32_t size) {
        m_pPushConstants[name.data()] = (PushConstantInfo{shaderStage, offset, size});
        return {shaderStage, offset, size};
    }

    const vector <VertexBindingDescriptor> &PipelineBlueprint::GetVertexInputDescription() const {
        return m_pVertexInputDescription;
    }
}