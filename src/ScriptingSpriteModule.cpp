//
// Created by Matty on 2022-09-30.
//

#include "scripting/module/ScriptingSpriteModule.h"
#include "2d/SpriteProperty.h"

namespace MV {
    void ScriptingSpriteModule::InitFunctionCallbacks() {

    }

    static void sprite_SetSpriteIndex(MonoObject * worldObject, MonoObject * spriteProperty, uint32_t index) {
        ENSURE_VALID(worldObject)

        auto * property = ScriptingModule::GetProperty<MV::SpriteProperty>(worldObject, spriteProperty);

        property->SetSpriteIndex_Native(index);
    }

    static void sprite_SetVisible(MonoObject * worldObject, MonoObject * spriteProperty, bool visible) {
        ENSURE_VALID(worldObject)

        auto * property = ScriptingModule::GetProperty<MV::SpriteProperty>(worldObject, spriteProperty);

        property->m_Visible = visible;
    }

    void ScriptingSpriteModule::CheckClassesExistence(MonoImage *engineAssembly) {
        mono_add_internal_call ("MV.Sprite::SetSpriteIndex_Native", (const void *) sprite_SetSpriteIndex);
        mono_add_internal_call ("MV.Sprite::SetVisible_Native", (const void *) sprite_SetVisible);
        //mono_add_internal_call ("MV.Sprite::SetRotation_Native", (const void *) collider_SetRotation);
    }

    void ScriptingSpriteModule::CheckClassFunctionsExistence(MonoImage *engineAssembly) {

    }
}