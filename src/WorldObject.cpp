//
// Created by Matty on 2022-02-06.
//

#include "../include/common/WorldObject.h"
#include "../include/common/EngineCommon.h"
#include "../include/profiler/Profiler.h"

namespace MV {
    WorldObject::WorldObject(MV::string_view scriptName, ScriptType type) : m_pScript(scriptName, type) {

    }

    void WorldObject::SetRotation(float val) {
        m_pRotation = val;
        syncRotation();
        syncPhysicsState();
    }

    void WorldObject::SetScale(const glm::vec2 &val) {
        m_pScale = val;
        syncScale();
        syncPhysicsState();
    }

    void WorldObject::SetPosition(const glm::vec2 &val) {
        m_pPosition = val;
        syncPosition();
        syncPhysicsState();
    }

    void WorldObject::OnStart() {
        MV_PROFILE_FUNCTION("WorldObject::OnStart");
        m_pScript.SetField("_id", m_WorldID);

        syncPosition();
        syncRotation();
        syncScale();

        m_pScript.CallFunction_Void(SCRIPT_ON_START_NAME);
    }

    void WorldObject::OnEnd() {
        MV_PROFILE_FUNCTION("WorldObject::OnEnd");
        if (m_pScript.IsInstanceValid())
            m_pScript.CallFunction_Void(SCRIPT_ON_END_NAME);
    }

    void WorldObject::Update(float delta) {
        MV_PROFILE_FUNCTION("WorldObject::Update");

        if(m_PhysicsBody) {
            const auto pos = m_PhysicsBody->GetPosition();
            SetPosition({pos.x, pos.y});
            const auto angle = m_PhysicsBody->GetAngle();
            SetRotation(angle);
        }

        if (m_pScript.IsInstanceValid())
            m_pScript.CallFunction_Void_Float(SCRIPT_UPDATE_NAME, delta);
    }

    void WorldObject::FixedUpdate(float delta) {
        MV_PROFILE_FUNCTION("WorldObject::FixedUpdate");
        if (m_pScript.IsInstanceValid())
            m_pScript.CallFunction_Void_Float(SCRIPT_FIXED_UPDATE_NAME, delta);
    }

    void WorldObject::ForceSetRotation(float val) {
        m_pRotation = val;
        syncPhysicsState();
    }

    void WorldObject::ForceSetScale(const glm::vec2 &val) {
        m_pScale = val;
        syncPhysicsState();
    }

    void WorldObject::ForceSetPosition(const glm::vec2 &val) {
        m_pPosition = val;
        syncPhysicsState();
    }

    void WorldObject::syncPosition() {
        if (m_pScript.IsInstanceValid())
            m_pScript.SetField("position", m_pPosition);
    }

    void WorldObject::syncRotation() {
        if (m_pScript.IsInstanceValid())
            m_pScript.SetField("rotation", m_pRotation);
    }

    void WorldObject::syncScale() {
        if (m_pScript.IsInstanceValid())
            m_pScript.SetField("scale", m_pScale);
    }

    void WorldObject::Init() {
        m_pScript.CreateInstance();
    }

    void WorldObject::Cleanup() {
        m_pScript.Destroy();
        if(m_PhysicsBody)
            m_PhysicsBody = nullptr;
    }

    void WorldObject::SyncFromData(const nlohmann::json &data) {
        try {
            MV_PROFILE_FUNCTION("WorldObject::SyncFromData");
            const auto &fields = m_pScript.GetFields();
            for (auto &&field: fields) {
                const auto foundRes = data.find(field.first.c_str());

                if (foundRes != data.end()) {
                    auto type = field.second.m_Type;
                    const auto val = foundRes.value();
                    if (type == ScriptClassField::Type::CLASS) {

                        continue;
                    }

                    if (type == ScriptClassField::Type::VEC2) {
                        glm::vec2 fData = {val[0].get<float>(), val[1].get<float>()};
                        m_pScript.SetField(field.first, fData);
                        continue;
                    }

                    if (type == ScriptClassField::Type::F32) {
                        m_pScript.SetField(field.first, val.get<float>());
                        continue;
                    }
                }
            }
        } catch(std::exception &e) {
            MV::GetLogger()->LogError("Failed to sync world object: ", std::to_string(m_WorldID));
        }
    }

    void WorldObject::OnEditorRender() {
        MV_PROFILE_FUNCTION("WorldObject::OnEditorRender");
        if (m_pScript.IsInstanceValid())
            m_pScript.CallFunction_Void("OnGizmoDraw");
    }

    void WorldObject::syncPhysicsState() {
        if(m_PhysicsBody) {
            m_PhysicsBody->SetTransform({m_pPosition.x, m_pPosition.y}, m_pRotation);
            m_PhysicsBody->SetAwake(true);
        }
    }
}

