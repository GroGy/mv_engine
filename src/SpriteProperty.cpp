//
// Created by Matty on 2022-02-24.
//

#include "../include/2d/SpriteProperty.h"
#include "../include/common/EngineCommon.h"
#include "ext/matrix_transform.hpp"

namespace MV {
    struct SpriteVertex {
        glm::vec2 position;
        glm::vec2 uv;
    };

    static constexpr std::initializer_list<SpriteVertex> sc_VerticesBase = {
            {{-0.5f, 0.5f},  {0.0f, 1.0f}},
            {{0.5f,  -0.5f}, {1.0f, 0.0f}},
            {{0.5f,  0.5f},  {1.0f, 1.0f}},
            {{-0.5f, -0.5f}, {0.0f, 0.0f}}
    };

    void SpriteProperty::Render(FrameContext &context) {
        if (!m_Visible) return;
        if (m_Shader != 0 && m_Texture != 0) {
            rc <RenderingResource> rr;
            bool exists = GetRenderer()->GetRenderingResource(m_Shader, rr);

            if (!exists) {
                GetEngine()->LoadResource(m_Shader);
                return;
            }

            if (!rr->IsDataLoaded()) {
                return;
            }

            if (m_IsAtlasSprite) {
                if (!MV::GetEngine()->IsAssetLoaded(m_Texture)) {
                    MV::GetEngine()->LoadResource(m_Texture);
                    return;
                }
            }

            auto &cmd = context.m_DrawQueue[m_Shader];

            MaterialInput matInput{};
            matInput.m_UniformTextureName = "uTexture";

            uint64_t textureId = 0;

            if (m_IsAtlasSprite) {
                const auto atlas = MV::GetResources()->m_Atlases.find(m_Texture);

                if (atlas == MV::GetResources()->m_Atlases.end()) {
                    return;
                }

                if(!m_WasUVLoaded) {
                    m_WasUVLoaded = true;
                    m_UV = atlas->second->GetUVs(m_SpriteIndex);
                    m_Offsets = atlas->second->GetUVOffsets(m_SpriteIndex);
                }

                if (auto tex = atlas->second->GetTexture()) {
                    textureId = tex->GetID();
                } else {
                    return;
                }
            } else {
                textureId = m_Texture;
            }

            rc <RenderingResource> texRR;
            bool texExists = GetRenderer()->GetRenderingResource(textureId, texRR);

            if (!texExists) {
                GetEngine()->LoadResource(textureId);
                return;
            }

            if (!texRR->IsDataLoaded()) {
                return;
            }
            matInput.m_UniformTextures.emplace_back(textureId);
            matInput.m_UniformBufferName = "ubo";
            matInput.m_UniformBuffer.resize(16);
            *((glm::vec4 *) matInput.m_UniformBuffer.data()) = glm::vec4(1, 1, 1, 1);

            auto &subCmd = cmd.m_Commands[matInput];

            auto &vb = subCmd.m_VertexBuffers[0];

            const auto offset = static_cast<uint32_t>(vb.size() / sizeof(SpriteVertex));

            MV::vector<SpriteVertex> vertices{sc_VerticesBase};

            const auto woPos = m_WorldObject->GetPosition();
            const auto woRot = m_WorldObject->GetRotation();
            const auto woScale = m_WorldObject->GetScale();

            glm::mat4 mat = glm::mat4(1.0f);
            mat = glm::translate(mat, {woPos.x, woPos.y, 0});
            mat = glm::translate(mat, {m_Offset.x, m_Offset.y, 0});
            mat = glm::rotate(mat, glm::radians(m_Rotation + woRot), {0, 0, 1});
            mat = glm::scale(mat, {m_Scale.x * woScale.x, m_Scale.y * woScale.y, 1});

            vertices[0].position += glm::vec2{m_Offsets.x,-m_Offsets.y};
            vertices[1].position += glm::vec2{-1.0f + m_Offsets.z,1.0 - m_Offsets.w};
            vertices[2].position += glm::vec2{-1.0f + m_Offsets.z,-m_Offsets.y};
            vertices[3].position += glm::vec2{m_Offsets.x,1.0 - m_Offsets.w};

            for (auto &v: vertices) {
                glm::vec4 vTemp = {v.position.x, v.position.y, 0.0f, 1.0f};
                vTemp = mat * vTemp;
                v.position.x = vTemp.x;
                v.position.y = vTemp.y;
            }

            vertices[0].uv = {m_UV.z, m_UV.w};
            vertices[1].uv = {m_UV.x, m_UV.y};
            vertices[2].uv = {m_UV.x, m_UV.w};
            vertices[3].uv = {m_UV.z, m_UV.y};

            const auto verticesRawSize = vertices.size() * sizeof(SpriteVertex);
            const auto prevSize = vb.size();
            vb.resize(vb.size() + verticesRawSize);
            std::memcpy(vb.data() + prevSize, vertices.data(), verticesRawSize);

            subCmd.m_Indices.insert(subCmd.m_Indices.end(), {
                    offset + 0, offset + 1, offset + 2,
                    offset + 3, offset + 1, offset + 0,
            });
        }
    }

    void SpriteProperty::Cleanup() {

    }

    void SpriteProperty::Update(float deltaTime) {
    }

    void SpriteProperty::Serialize(json &propertyField) {
        propertyField["shader"] = m_Shader;
        propertyField["visible"] = m_Visible;
        propertyField["offset"] = m_Offset;
        propertyField["scale"] = m_Scale;
        propertyField["rotation"] = m_Rotation;
        propertyField["texture"] = m_Texture;
        propertyField["sprite_index"] = m_SpriteIndex;
        propertyField["is_atlas"] = m_IsAtlasSprite;
    }

    void SpriteProperty::Deserialize(const nlohmann::json &propertyField) {
        m_Shader = ReadField<uint64_t>("shader",propertyField);
        m_Visible = ReadField<bool>("visible", propertyField);
        m_Offset = ReadField<glm::vec2>("offset",propertyField);
        m_Scale = ReadField<glm::vec2>("scale",propertyField);
        m_Rotation = ReadField<float>("rotation",propertyField);
        m_Texture = ReadField<uint64_t>("texture",propertyField);
        m_SpriteIndex = ReadField<uint32_t>("sprite_index",propertyField);
        m_IsAtlasSprite = ReadField<bool>("is_atlas",propertyField);
    }

    void SpriteProperty::Sync(Script &owner, const ScriptClassField &field) {
        Script propertyField = owner.GetFieldScript(field);

        propertyField.SetField("_visible", m_Visible);
        propertyField.SetField("_spriteIndex", (uint32_t)m_SpriteIndex);

        auto foundRes = propertyField.GetFields().find("_atlas");

        ENSURE_TRUE(foundRes != propertyField.GetFields().end());

        Script atlasScript = propertyField.GetFieldScript(foundRes->second);

        if (m_IsAtlasSprite && !MV::GetEngine()->IsAssetLoaded(m_Texture)) {
            MV::GetEngine()->LoadResource(m_Texture);
            return;
        }

        const auto atlas = MV::GetResources()->m_Atlases.find(m_Texture);
        if (atlas == MV::GetResources()->m_Atlases.end()) {
            return;
        }

        if(m_IsAtlasSprite) {
            if(!atlasScript.IsInstanceValid()) {
                Script newAtlas{"Atlas"};
                newAtlas.CreateInstance();
                propertyField.SetField(foundRes->second, newAtlas);
                atlasScript = MV::move(newAtlas);
            }

            atlasScript.SetField("_spriteCount",atlas->second->GetUVBuffer().size());

        } else {
            if(atlasScript.IsInstanceValid()) {
                propertyField.SetField(foundRes->second, nullptr);
            }
        }

        //ScriptClass::SetFieldValue(spriteObj, "visible", false);
    }

    void SpriteProperty::SyncPropertyData(const ScriptClassField &field, Script &script, const json &data) {
        Script propertyField = script.GetFieldScript(field);

        const bool isAtlasSprite = ReadField<bool>("is_atlas",data);
        const auto tex = ReadField<uint64_t>("texture",data);

        if (isAtlasSprite && !MV::GetEngine()->IsAssetLoaded(tex)) {
            MV::GetEngine()->LoadResource(tex);
            return;
        }

        const auto atlas = MV::GetResources()->m_Atlases.find(tex);
        if (atlas == MV::GetResources()->m_Atlases.end()) {
            return;
        }

        propertyField.SetField("_visible", ReadField<bool>("visible", data));
        propertyField.SetField("_spriteIndex", ReadField<uint32_t>("sprite_index",data));

        auto foundRes = propertyField.GetFields().find("_atlas");
        ENSURE_TRUE(foundRes != propertyField.GetFields().end());

        Script atlasScript = propertyField.GetFieldScript(foundRes->second);

        if(isAtlasSprite) {
            if(!atlasScript.IsInstanceValid()) {
                Script newAtlas{"Atlas"};
                newAtlas.CreateInstance();
                propertyField.SetField(foundRes->second, newAtlas);
                atlasScript = MV::move(newAtlas);
            }


            atlasScript.SetField("_spriteCount",atlas->second->GetUVBuffer().size());
        } else {
            if(atlasScript.IsInstanceValid()) {
                propertyField.SetField(foundRes->second, nullptr);
            }
        }
        //m_Shader = ReadField<uint64_t>("shader",propertyField);
        //m_Offset = ReadField<glm::vec2>("offset",propertyField);
        //m_Scale = ReadField<glm::vec2>("scale",propertyField);
        //m_Rotation = ReadField<float>("rotation",propertyField);
    }

    template<typename T>
    T SpriteProperty::ReadField(MV::string_view name,const nlohmann::json &field) {
        try {
            return field.at(name.data()).get<T>();
        } catch (const nlohmann::json::exception &e) {
            MV::GetLogger()->LogWarning("Failed to load field {}", name);
        }
        return T{};
    }

    void SpriteProperty::SetSpriteIndex_Native(uint32_t spriteIndex) {
        m_SpriteIndex = spriteIndex;
        m_WasUVLoaded = false;
    }
}