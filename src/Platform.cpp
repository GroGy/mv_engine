//
// Created by Matty on 2022-03-06.
//

#include "../include/platform/Platform.h"

#include <thread>

namespace MV {
    uint64_t Platform::GetThreadCount() {
        return std::thread::hardware_concurrency();
    }
}