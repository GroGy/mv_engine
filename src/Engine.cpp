//
// Created by Matty on 2021-10-01.
//

#include <GLFW/glfw3.h>
#include "../include/Engine.h"

#include "../include/common/EngineCommon.h"
#include "../include/common/ExceptionCommon.h"

#ifdef VK_BACKEND_SUPPORT

#include "../include/render/vulkan/VulkanRenderer.h"
#include "../include/resources/ProjectResource.h"
#include "../include/resources/TextureResource.h"
#include "../include/resources/TilesetResource.h"
#include "../include/resources/TilemapResource.h"
#include "../include/resources/AtlasResource.h"
#include "../include/resources/WorldResource.h"
#include "../include/profiler/Profiler.h"
#include "../include/util/Container.h"
#include "../include/resources/ShaderResource.h"

#endif

namespace MV {
    void Engine::Run() {
        EngineCommon::SetEngineGlobal(this);
        init();
        mainLoop();
        cleanup();
    }

    void Engine::init() {
        MV_PROFILE_FUNCTION("Engine::init");
        /////// We have to init logger before we log anything
        initLogger();
        ///////

        GetLogger()->LogInfo("Initializing MV engine...");


        GetLogger()->LogInfo("Initializing modules...");

        for (auto &&module: m_pModules) {
            bool res = module->OnInit();

            if (!res) GetLogger()->LogError("Failed to initialize module ", module->GetModuleName());
        }

        GetLogger()->LogInfo("Finished initialization of modules");
        compileEngineShaders();
        initRenderer();
        initInput();
        initResourceLoaders();
        initScriptManager();
        initShaderCompiler();
        createEnginePipelines();
        initThreadPool();
        GetLogger()->LogInfo("Finished initialization of MV engine internals");


        GetLogger()->LogInfo("PostInitializing modules...");

        for (auto &&module: m_pModules) {
            bool res = module->OnPostInit();

            if (!res) GetLogger()->LogError("Failed to post-initialize module ", module->GetModuleName());
        }

        GetLogger()->LogInfo("Finished post-initialization of modules");
    }

    void Engine::mainLoop() {
        m_pLastFrame = glfwGetTime();

        while (m_pRun) {
            MV_PROFILE_FRAME_MT();
            m_pStartFrameTime = glfwGetTime();
            m_Input.pollInputEvents();
            update();
            render();
            finishResources();
            processSyncCallbacks();
            limitFrames();
            if (m_Input.ShouldExit()) m_pRun = false;
        }
    }

    void Engine::cleanup() {
        GetLogger()->LogInfo("Cleaning up MV engine resources...");
        cleanupWorld();
        cleanupScriptManager();
        cleanupResourceLoaders();

        m_Renderer->Cleanup([&]() {
            for (auto &&module: m_pModules) {
                bool res = module->OnRenderCleanup();
                if (!res) {
                    GetLogger()->LogError("Failed to render cleanup module ", module->GetModuleName());
                    return res;
                }
            }

            return true;
        });
        m_Window->cleanup();
        m_Window.reset();

        for (auto &&module: m_pModules) {
            bool res = module->OnCleanup();
            if (!res) GetLogger()->LogError("Failed to cleanup module ", module->GetModuleName());
        }

        GetLogger()->LogInfo("Finished cleaning of MV engine resources, exiting...");
    }

    void Engine::RegisterModule(const MV::rc<MV::Module> &module) {
        m_pModules.emplace_back(module);
    }

    Engine::Engine(MV::EngineConfig config) : m_pConfig(MV::move(config)) {
        m_pMainThreadID = std::this_thread::get_id();
        m_pModules.reserve(64);
        if (m_pConfig.m_Editor)
            m_pGameRunning = false;
    }

    void Engine::initRenderer() {
        GetLogger()->Render_LogInfo("Initializing renderer...");

        initWindow();

        RendererConfigCommon commonConfig{m_Window};

        commonConfig.m_Editor = m_pConfig.m_Editor;
        commonConfig.m_ResizeCallback = [&]() {
            for (auto &&module: m_pModules) {
                module->OnWindowResize();
            }
        };

        switch (m_pConfig.m_RenderBackend) {
            case RendererBackend::VULKAN: {
#ifdef VK_BACKEND_SUPPORT
                VulkanRendererConfig vkConfig{};
                m_Renderer = MV::make_rc<VulkanRenderer>(commonConfig, vkConfig);
                m_Renderer->Init();
#else
                throw RuntimeException("Vulkan Backend not implemented");
#endif //VK_BACKEND_SUPPORT
                break;
            }
            case RendererBackend::OPENGL:
                throw RuntimeException("OpenGL Backend not implemented");
            case RendererBackend::DX11:
                throw RuntimeException("DX11 Backend not implemented");
            case RendererBackend::DX12:
                throw RuntimeException("DX12 Backend not implemented");
        }

        GetLogger()->Render_LogInfo("Initialized renderer");
    }

    void Engine::initLogger() {
        EngineCommon::SetLoggerGlobal(&m_Logger);
        m_Logger.init();
        m_Logger.m_pRendererDebuggingEnabled = m_pConfig.m_RendererDebugMessages;
    }

    void Engine::initWindow() {
        MV::rc<WindowBuilder> builder = MV::make_rc<WindowBuilder>(m_pConfig.m_DefaultWindowWidth,
                                                                   m_pConfig.m_DefaultWindowHeight);

        MV::string windowName = m_pConfig.m_ProjectName + " | ";

        switch (m_pConfig.m_RenderBackend) {
            case RendererBackend::VULKAN: {
                windowName += "Vulkan";
                break;
            };
            case RendererBackend::OPENGL: {
                windowName += "OpenGL";
                break;
            };
            case RendererBackend::DX11: {
                windowName += "DX11";
                break;
            };
            case RendererBackend::DX12: {
                windowName += "DX12";
                break;
            };
        }

        builder
                ->setBackgroundColor({1.0f, 1.0f, 1.0f, 1.0f})
                ->setWindowName(windowName)
                ->setMaximized(m_pConfig.m_WindowMaximized)
                ->setResizable(true);

        m_Window = std::unique_ptr<Window>(builder->finish());
    }

    void Engine::initInput() {
        m_Input.init(m_Window->GetContext());
    }

    void Engine::render() {
        MV_PROFILE_FUNCTION("Engine::render")
        auto context = m_Renderer->PrepareRender();
        if (m_pWorld.IsValid()) {
            if (m_pWorld.m_Camera) {
                context.m_ProjMatrix = m_pWorld.m_Camera->CalcProj();
                context.m_ViewMatrix = m_pWorld.m_Camera->GetView();
            }

            m_pWorld.Render(context);
        }

        {
            MV_PROFILE_FUNCTION("Engine::render modules")
            for (auto &&module: m_pModules) {
                MV_PROFILE_FUNCTION("Engine::render module")
                MV_PROFILE_TAG("module name", module->GetModuleName().c_str());
                module->OnRender(context);
            }
        }

        m_Renderer->Render(context);

        if (m_pConfig.m_Editor) {
            {
                MV_PROFILE_FUNCTION("Engine::render modules onEditorDraw")
                m_Renderer->BindMainRenderPassEditor(context);
                for (auto &&module: m_pModules) {
                    MV_PROFILE_FUNCTION("Engine::render module onEditorDraw")
                    MV_PROFILE_TAG("module name", module->GetModuleName().c_str());
                    module->OnEditorRender(context);
                }
            }
        }

        m_Renderer->Present(context);
    }

    double Engine::GetTime() {
        return glfwGetTime();
    }

    static const int RESOURCE_LOADER_THREADS = 4;

    void Engine::initResourceLoaders() {
        GetLogger()->LogInfo("Initializing resource loaders...");
        m_pResourceQueue = MV::make_rc<MV::thread_safe_queue<MV::rc<MV::Resource>>>();
        m_pFinishedResourceQueue = MV::make_rc<MV::thread_safe_queue<MV::rc<MV::Resource>>>();

        m_pResourceLoaders.reserve(RESOURCE_LOADER_THREADS);
        for (uint32_t i = 0; i < RESOURCE_LOADER_THREADS; i++) {
            auto &worker = m_pResourceLoaders.emplace_back(m_pResourceQueue, m_pFinishedResourceQueue);
            worker.Init();
        }
        GetLogger()->LogInfo("Successfully initialized resource loaders.");
    }

    void Engine::cleanupResourceLoaders() {
        GetLogger()->LogInfo("Destroying resource loaders...");
        for (uint32_t i = 0; i < RESOURCE_LOADER_THREADS; i++) {
            m_pResourceLoaders[i].Cleanup();
        }
        GetLogger()->LogInfo("Successfully destroyed resource loaders.");
    }

    void Engine::finishResources() {
        MV_PROFILE_FUNCTION("Engine::finishResources")
        try {
            auto toFinish = MV::move(m_pFinishedResourceQueue->pop(false));
            toFinish->finish();
        } catch (std::exception &e) {

        }
    }

    void Engine::LoadResource(MV::rc<Resource> resource) {
        m_pResourceQueue->push(MV::move(resource));
    }

    void Engine::Stop() {
        m_pRun = false;
    }

    uint32_t Engine::GetVersion() const {
        return ENGINE_VERSION;
    }

    void Engine::LoadProject(MV::string path) {
        m_pResourceQueue->push(MV::make_rc<ProjectResource>(MV::move(path)));
    }

    void Engine::update() {
        MV_PROFILE_FUNCTION("Engine::update");
        double currentTime = glfwGetTime();

        double deltaD = currentTime - m_pLastFrame;
        auto deltaTime = static_cast<float>(deltaD);

        if (deltaTime < 0.0f) { deltaTime = 0.0f; }
        if (deltaTime > MAX_DELTA) { deltaTime = MAX_DELTA; }

        if (m_pWorld.IsValid())
            m_pWorld.Update(deltaTime);

        {
            MV_PROFILE_FUNCTION("Engine::update modules");
            for (auto &&module: m_pModules) {
                MV_PROFILE_FUNCTION("Engine::update module");
                MV_PROFILE_TAG("Module name", module->GetModuleName().c_str());
                module->OnUpdate(deltaTime);
            }
        }
        m_ScriptManager.Update();
        if (m_ScriptManager.OnScriptsReload()) {
            onScriptsReload();
        }

        fixedUpdate(deltaTime);
        m_MainThreadPool->Update();
        m_MainThreadPool->AwaitCriticalTasks();
        m_pLastFrame = currentTime;
    }

    void Engine::fixedUpdate(float delta) {
        static uint32_t gcCounter = 0;
        static float accumulator = 0.0f;

        accumulator += delta;

        float fixedDelta = 1.0f / TICK_RATE;

        while (accumulator > fixedDelta) {
            MV_PROFILE_FUNCTION("Engine::fixedUpdate");
            gcCounter++;
            for (auto &&module: m_pModules)
                module->OnFixedUpdate(fixedDelta);

            if (m_pWorld.IsValid())
                m_pWorld.FixedUpdate(delta);

            if (gcCounter >= GC_TICK_FREQ) {
                gcCounter = 0;
                m_Renderer->CollectGarbage();
            }

            accumulator -= fixedDelta;
        }
    }

    void Engine::limitFrames() {
        /*
        double currentTime = glfwGetTime();
        double frameDuration = currentTime - m_pStartFrameTime;
        double minFrameTime = 1.0 / m_Settings.m_FPSLimit;

        if(frameDuration > minFrameTime) return;

        auto sleepDuration = minFrameTime - frameDuration;
        auto milliseconds = static_cast<uint32_t>(sleepDuration * 1000.0);

        std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));*/
    }

    MV::rc<Resource> Engine::LoadResource(uint64_t id) {
        Assert(GetEngine()->OnMainThread(), "Has to be called from main thread");

        if (!m_pProject.m_Valid)
            return {};

        if (!MV::Contains(m_pProject.m_Assets, id)) {
            m_Logger.LogError("Tried to load invalid asset with id:", std::to_string(id));
            return {};
        }

        try {
            const auto &asset = m_pProject.m_Assets.at(id);

            // We might need to load shader or texture or something like that again for additional data for editor
            if (!m_pConfig.m_Editor) {
                rc <RenderingResource> tmp{};
                if (m_Renderer->GetRenderingResource(id, tmp)) {
                    return {};
                }
            }

            switch (asset.first) {
                case AssetType::SHADER: {
                    auto res = make_rc<ShaderResource>(asset.second.c_str(), id);
                    m_Renderer->RegisterRenderingResource(id, make_rc<RenderingResource>());
                    LoadResource(res);
                    return res;
                }
                case AssetType::TEXTURE: {
                    auto res = make_rc<TextureResource>(asset.second.c_str(), id);
                    m_Renderer->RegisterRenderingResource(id, make_rc<RenderingResource>());
                    LoadResource(res);
                    return res;
                }
                case AssetType::MODEL:
                    m_Logger.LogError("Model assets are not implemented! -> ", asset.second);
                    return {};
                case AssetType::PROJECT:
                    return {};
                case AssetType::ATLAS: {
                    auto res = make_rc<AtlasResource>(asset.second.c_str(), id);
                    LoadResource(res);
                    return res;
                }
                case AssetType::WORLD: {
                    auto res = make_rc<WorldResource>(asset.second.c_str(), id);
                    LoadResource(res);
                    return res;
                }
                case AssetType::TILESET: {
                    auto res = make_rc<TilesetResource>(asset.second.c_str(), id);
                    LoadResource(res);
                    return res;
                }
                case AssetType::TILEMAP: {
                    auto res = make_rc<TilemapResource>(asset.second.c_str(), id);
                    LoadResource(res);
                    return res;
                }
            }
        } catch (std::exception &e) {
            return {};
        }
        return {};
    }

    void Engine::UnloadAsset(uint64_t id) {
        if (MV::Contains(m_LoadedResources.m_Tilesets, id)) {
            m_LoadedResources.m_Tilesets.erase(id);
        }

        if (MV::Contains(m_LoadedResources.m_Atlases, id)) {
            m_LoadedResources.m_Atlases.erase(id);
        }
    }

    void Engine::compileEngineShaders() {
        try {
            m_ShaderCompiler.CompileBuildInShaders();
        } catch (std::exception &e) {
            MV::GetLogger()->LogError("Failed to load engine shaders.\n\t{}\nExiting...", e.what());
            exit(-1);
        }
    }

    void Engine::cleanupWorld() {
        m_pWorld.Cleanup();
    }

    bool Engine::IsAssetLoaded(uint64_t id) const {
        if (MV::Contains(m_LoadedResources.m_Tilesets, id)) {
            return true;
        }

        if (MV::Contains(m_LoadedResources.m_Atlases, id)) {
            return true;
        }

        if (MV::Contains(m_LoadedResources.m_Tilemaps, id)) {
            return true;
        }

        if (m_pWorld.m_ID == id)
            return true;

        MV::rc<MV::RenderingResource> tmp{};
        auto rrPresent = MV::GetRenderer()->GetRenderingResource(id, tmp);

        if (!rrPresent) return false;

        return tmp->IsDataLoaded();
    }

    bool Engine::OnMainThread() const {
        return std::this_thread::get_id() == m_pMainThreadID;
    }

    MV::rc<Resource> Engine::LoadResourceSync(uint64_t id) {
        Assert(!OnMainThread(), "This is slow, dont call on main thread");

        MV::rc<Resource> res;
        bool done = false;

        m_pSyncCallbacks.push([&]() {
            res = LoadResource(id);
            done = true;
        });

        while (!done) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        return res;
    }

    void Engine::processSyncCallbacks() {
        MV_PROFILE_FUNCTION("Engine::processSyncCallbacks")
        try {
            auto clb = MV::move(m_pSyncCallbacks.pop(false));
            clb();
        } catch (std::exception &e) {

        }
    }

    void Engine::AddSyncCallback(const std::function<void()> &callback) {
        Assert(!OnMainThread(), "This is slow, dont call on main thread");

        bool done = false;

        m_pSyncCallbacks.push([&]() {
            callback();
            done = true;
        });

        while (!done) {
            std::this_thread::sleep_for(std::chrono::milliseconds(5));
        }
    }

    void Engine::initScriptManager() {
        ScriptManagerInit config{};

        config.m_Name = m_pConfig.m_ProjectName;
        config.m_AssemblyPath = m_pConfig.m_ProjectName + ".dll";

        m_ScriptManager.Init(config);
    }

    void Engine::cleanupScriptManager() {
        m_ScriptManager.Cleanup();
    }

    bool Engine::IsGameRunning() const {
        return m_pGameRunning;
    }

    void Engine::StartGame() {
        //TODO: Prep for game
        m_pGameRunning = true;
        if (m_pWorld.IsValid())
            m_pWorld.OnStart();

        if(m_pGameInstance.GetCustomClass() != m_pProject.m_GameInstanceClass)
            m_pGameInstance.SetClass(m_pProject.m_GameInstanceClass);

        m_pGameInstance.Init();
        m_pGameInstance.OnStart();
    }

    void Engine::StopGame() {
        //TODO: Reset to state before game
        if (m_pWorld.IsValid())
            m_pWorld.OnEnd();
        m_pGameRunning = false;

        m_pGameInstance.OnEnd();
        m_pGameInstance.Cleanup();
    }

    Engine::~Engine() {
        for (auto &&worker: m_pResourceLoaders) {
            worker.Cleanup();
        }
        if (m_pResourceQueue)
            m_pResourceQueue->wake_cv();
        if (m_pFinishedResourceQueue)
            m_pFinishedResourceQueue->wake_cv();

        m_MainThreadPool->Cleanup();
    }

    bool Engine::IsEditor() const {
        return m_pConfig.m_Editor;
    }

    void Engine::initShaderCompiler() {
        GetLogger()->Render_LogInfo("Initializing shader compiler...");
        m_ShaderCompiler.SetRendererBackend(m_pConfig.m_RenderBackend);
        GetLogger()->Render_LogInfo("Initialized shader compiler.");
    }

    void Engine::onScriptsReload() {
        m_pWorld.Reload();
    }

    void Engine::createEnginePipelines() {
        m_Buildin.m_TilemapPipeline.m_Pipeline = m_Renderer->CreateShaderPipeline(
                MV::move(m_Buildin.m_TilemapPipeline.GetBlueprint()));
        m_Buildin.m_PhysicsDebugPipeline.m_Pipeline = m_Renderer->CreateShaderPipeline(
                MV::move(m_Buildin.m_PhysicsDebugPipeline.GetBlueprint()));
        m_Buildin.m_ParticlePipeline.m_Pipeline = m_Renderer->CreateShaderPipeline(
                MV::move(m_Buildin.m_ParticlePipeline.GetBlueprint()));

        m_Renderer->RegisterRenderingResource(ParticlePipeline::s_ParticlePipelineReservedID,
                                              m_Buildin.m_ParticlePipeline.m_Pipeline);

        MV::rc<TextureResource> res = MV::make_rc<TextureResource>(
                MV::vector<uint8_t>(MV::MissingTexture::sc_MissingTextureData),
                MV::MissingTexture::sc_MissingTextureReservedID);
        m_Renderer->RegisterRenderingResource(MV::MissingTexture::sc_MissingTextureReservedID,
                                              make_rc<RenderingResource>());

        res->m_SaveResult = &m_Buildin.m_MissingTexture.m_Texture;

        LoadResource(res);
    }

    void Engine::SetShowCollision(bool showCollision) {
        if (m_pConfig.m_Editor)
            m_pShowCollision = showCollision;
        else
            m_pShowCollision = false;
    }

    bool Engine::ShowCollisions() const {
        return m_pShowCollision;
    }

    void Engine::ForceStopGame() {
        m_pGameRunning = false;
        m_pForceStopped = true;
    }

    bool Engine::IsForceStopped() const {
        return m_pForceStopped;
    }

    void Engine::ClearForceStop() {
        m_pForceStopped = true;
    }

    void Engine::initThreadPool() {
        GetLogger()->LogInfo("Initializing thread pool...");
        m_MainThreadPool = make_rc<ThreadPool>();
        m_MainThreadPool->Init();
        GetLogger()->LogInfo("Initialized thread pool.");
    }

    void Engine::ReloadShaders() {
        GetLogger()->LogInfo("Reloading engine shaders...");
        m_ShaderCompiler.RecompileBuildInShaders();
        m_Renderer->ReloadShaders();
        GetLogger()->LogInfo("Reloaded engine shaders.");
    }
}