//
// Created by Matty on 08.12.2022.
//

#include "resources/TilemapResource.h"
#include "common/EngineCommon.h"

namespace MV {
    TilemapResource::TilemapResource(MV::string path, uint64_t id) : Resource(MV::move(path)), m_pID(id) {

    }

    static void Read(std::ifstream & input, void * res, size_t size) {
        input.read((char*)res, size);
    }

    template <typename T>
    static void Read(std::ifstream & input, T & res) {
        Read(input, &res, sizeof(T));
    }

    ResourceLoadResult TilemapResource::process() {
        m_pPath = PathProjectRelativeToAbsolute(m_pPath);
        std::ifstream input{m_pPath.c_str()};
        if (input.is_open()) {
            auto header = ReadAssetHeader(input);

            if(header.m_Type != AssetType::TILEMAP)
                return ResourceLoadResult::FILE_READ_ERROR;


            Read(input, m_Result.m_Tileset);
            Read(input, m_Result.m_Width);
            Read(input, m_Result.m_Height);
            Read(input, m_Result.m_HasCollisionData);

            uint32_t tileCount;
            Read(input, tileCount);

            m_Result.m_Data.resize(tileCount);

            for(auto && t : m_Result.m_Data) {
                Read(input, t.m_AtlasIndex);
            }

            if(m_Result.m_HasCollisionData) {
                uint32_t collisionVertCount;
                Read(input, collisionVertCount);

                m_Result.m_Collision.m_Vertices.resize(collisionVertCount);

                for(auto && v : m_Result.m_Collision.m_Vertices) {
                    Read(input, v);
                }
            }

            input.close();
        } else {
            return ResourceLoadResult::FILE_READ_ERROR;
        }

        m_pSuccess = true;
        return ResourceLoadResult::SUCCESS;
    }

    void TilemapResource::finish() {
        if (!m_pSuccess)
            return;

        GetResources()->m_Tilemaps[m_pID] = m_Result;
        m_pSuccess = true;
    }
}