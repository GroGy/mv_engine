//
// Created by Matty on 2021-10-23.
//

#include <json.hpp>
#include <filesystem>
#include "../include/resources/ProjectResource.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    ResourceLoadResult ProjectResource::process() {
        nlohmann::json input;

        std::ifstream i(m_pPath.c_str());
        if (i.is_open()) {
            i >> input;

            if (!input.contains(JSON_NAME))
                return ResourceLoadResult::FILE_READ_ERROR;
            if (!input.contains(JSON_VERSION))
                return ResourceLoadResult::FILE_READ_ERROR;
            if (!input.contains(JSON_ASSETS))
                return ResourceLoadResult::FILE_READ_ERROR;
            if (!input.contains(JSON_DEFAULT_WORLD))
                return ResourceLoadResult::FILE_READ_ERROR;


            m_Result.m_Version = input.at(JSON_VERSION).get<uint32_t>();
            m_Result.m_DefaultWorld = input.at(JSON_DEFAULT_WORLD).get<uint64_t>();
            m_Result.m_Name = input.at(JSON_NAME).get<MV::string>().c_str();

            for (auto&[key, val]: input.at(JSON_ASSETS).items()) {
                std::pair<AssetType, eastl::string> res{};

                res.first = static_cast<AssetType>(val[JSON_ASSET_TYPE].get<uint32_t>());
                res.second = val[JSON_ASSET_PATH].get<MV::string>().c_str();

                m_Result.m_Assets[std::stoull(key)] = MV::move(res);
            }

            std::filesystem::path fsPath{m_pPath.c_str()};

            m_Result.m_Path = fsPath.parent_path().string().c_str();
            auto cachePath = fsPath.parent_path();
            cachePath /= Config::sc_ProjectCacheDirectory.data();
            create_directories(cachePath);
            m_Result.m_CachePath = cachePath.string().c_str();


            m_pSuccess = true;
            return ResourceLoadResult::SUCCESS;
        }

        return ResourceLoadResult::FILE_READ_ERROR;
    }

    void ProjectResource::finish() {
        if (!m_pSuccess)
            return;

        m_Result.m_Valid = true;
        GetEngine()->GetProject() = m_Result;
        GetEngine()->m_ScriptManager.SetUserNamespace(m_Result.m_Name.c_str());

        if(GetEngine()->IsEditor()) {
            GetEngine()->m_ScriptManager.SetUserAssembly(fmt::format("{}\\{}_MV.dll", m_Result.m_CachePath, m_Result.m_Name).c_str(),false);
        } else {
            GetEngine()->m_ScriptManager.SetUserAssembly(fmt::format("{}.dll",m_Result.m_Name).c_str(), true);
        }


        /*
        if (m_Result.m_DefaultWorld != 0) {
            GetEngine()->LoadResource(m_Result.m_DefaultWorld);
        }*/
    }

    ProjectResource::ProjectResource(MV::string path) : Resource(MV::move(path)) {

    }
}
