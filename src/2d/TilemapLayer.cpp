//
// Created by Matty on 2021-10-15.
//

#include "../../include/2d/TilemapLayer.h"

#include <utility>
#include "../../include/common/EngineCommon.h"

namespace MV {
    static const int INDICES_PER_TILE   = 6;
    static const int VERTICES_PER_TILE  = 4;

    void TilemapLayer::Build(MV::rc<VertexBuffer> & vertBuffer, MV::rc<IndexBuffer> & indexBuffer) {
        auto renderer = GetRenderer();

        MV::vector<TileVertex> vertices{};
        vertices.reserve(m_pData.m_Height * m_pData.m_Width * VERTICES_PER_TILE);
        MV::vector<uint32_t> indices{};
        indices.reserve(m_pData.m_Height * m_pData.m_Width * INDICES_PER_TILE);

        uint32_t offset = 0;
        auto tileset = GetResources()->m_Tilesets[m_pData.m_Tileset];

        for(uint32_t x = 0; x < m_pData.m_Width; x++) {
            for(uint32_t y = 0; y < m_pData.m_Height; y++) {
                auto id = m_pData.m_Data[y * m_pData.m_Width + x].m_AtlasIndex;
                if(id == 0) continue;
                tileset->GetTile(id).Build({x,y},vertices, indices,offset);
            }
        }

        size_t vertexStride = sizeof(TileVertex);
        size_t bufferSize = vertices.size() * vertexStride;
        size_t indexBufferSize = indices.size() * sizeof(uint32_t);

        if(bufferSize == 0 || indexBufferSize == 0)
            return;

        if (!vertBuffer) vertBuffer = renderer->CreateVertexBuffer(bufferSize, vertexStride);
        if (!indexBuffer) indexBuffer = renderer->CreateIndexBuffer(indexBufferSize);

        auto vertStaging = renderer->CreateStagingBuffer(bufferSize);
        vertStaging->Upload(vertices.data(), bufferSize);
        vertBuffer->Upload(vertStaging);
        vertStaging->Cleanup();

        auto indexStaging = renderer->CreateStagingBuffer(indexBufferSize);
        indexStaging->Upload(indices.data(), indexBufferSize);
        indexBuffer->Upload(indexStaging);
        indexStaging->Cleanup();
    }

    TilemapLayer::TilemapLayer(MV::TilemapData data) : m_pData(MV::move(data)) {

    }

    void TilemapLayer::BuildCollision(b2World * world) {
        // TODO: Use cooked collision in data
        //auto tileset = GetResources()->m_Tilesets[m_pData.m_Tileset];
        //for(uint32_t x = 0; x < m_pData.m_Width; x++) {
        //    for(uint32_t y = 0; y < m_pData.m_Height; y++) {
        //        auto id = m_pData.m_Data[y * m_pData.m_Width + x].m_AtlasIndex;
        //        if(id == 0) continue;
        //        tileset->GetTile(id).BuildCollision({x,y},world);
        //    }
        //}
    }

    void TilemapLayer::Load(const TileLayerData & data) {

    }

    rc<Texture> TilemapLayer::GetTilesetTexture() const {
        const auto & found = GetResources()->m_Tilesets.find(m_pData.m_Tileset);

        if(found == GetResources()->m_Tilesets.end())
            return {};

        return found->second->GetAtlasTexture();
    }
}
