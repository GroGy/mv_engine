//
// Created by Matty on 2021-10-15.
//

#include "../../include/2d/Tile.h"
#include "box2d/box2d.h"

namespace MV {
    const std::initializer_list<MV::TileVertex> VERTICES = {
            {{0.0f, 1.0f},{0.0f, 1.0f}},
            {{1.0f, 0.0f},{1.0f, 0.0f}},
            {{1.0f, 1.0f},{1.0f, 1.0f}},
            {{0.0f, 0.0f},{0.0f, 0.0f}}
    };
    
    Tile::Tile(const glm::vec4 &color, bool  collision, const::glm::vec4 & uv) : m_pColor(color), m_pHasCollision(collision), m_pUV(uv) {

    }

    void Tile::Build(const glm::uvec2 & pos, MV::vector<TileVertex> &vertexBuffer, MV::vector<uint32_t> & indices,uint32_t & offset) const {
        if(m_pColor.a < 0.001f) return;

        MV::vector<TileVertex> baseVertices = VERTICES;
        for(auto && vert : baseVertices) {
            vert.m_Pos.x += static_cast<float>(pos.x);
            vert.m_Pos.y += static_cast<float>(pos.y);
        }

        baseVertices[0].m_UV = {m_pUV.x, m_pUV.w }; //TL
        baseVertices[1].m_UV = {m_pUV.z, m_pUV.y}; //BR
        baseVertices[2].m_UV = {m_pUV.z, m_pUV.w}; //TR
        baseVertices[3].m_UV = {m_pUV.x, m_pUV.y}; //BL

        indices.insert(indices.end(),{
                               offset + 0 , offset + 1, offset + 2,
                               offset + 3 , offset + 1, offset + 0,
                        });

        offset += VERTICES.size();

        vertexBuffer.insert(vertexBuffer.end(),baseVertices.begin(),baseVertices.end());
    }

    void Tile::BuildCollision(const glm::uvec2 & pos,b2World * world) const {
        if(!m_pHasCollision) return;
        b2BodyDef def;
        def.position.Set(static_cast<float>(pos.x*2), static_cast<float>(pos.y*2));

        auto * body = world->CreateBody(&def);

        b2PolygonShape groundBox;
        groundBox.SetAsBox(1.0f, 1.0f);

        body->CreateFixture(&groundBox, 0.0f);
    }
}
