//
// Created by Matty on 2021-10-23.
//

#include "../../include/2d/TileSet.h"
#include "../../include/common/EngineCommon.h"

namespace MV {
    const Tile &Tileset::GetTile(uint32_t id) const {
        return m_pRegistry.at(id - 1);
    }

    Tileset::Tileset(const TilesetData &data) {
        m_Atlas = data.m_Atlas;
        auto atlas = GetResources()->m_Atlases[m_Atlas];

        m_pRegistry.resize(data.m_Tiles.size());
        for(uint32_t i = 0; i < data.m_Tiles.size(); i++) {
            m_pRegistry[i] = Tile({1,1,1,1},!data.m_Tiles[i].m_Collision.empty(), atlas->GetUVs(data.m_Tiles[i].m_SpriteIndex));
        }
    }

    void Tileset::SetTileCollision(uint32_t id, bool collision) {
        m_pRegistry.at(id).SetCollision(collision);
    }

    rc<Texture> Tileset::GetAtlasTexture() const {
        const auto & atlases = GetResources()->m_Atlases;

        const auto & found = atlases.find(m_Atlas);

        if(found == atlases.end())
            return {};

        return found->second->GetTexture();
    }
}
