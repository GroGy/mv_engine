//
// Created by Matty on 2021-10-15.
//

#include "../../include/2d/TilemapPipeline.h"
#include "../../include/2d/TileVertex.h"

namespace MV {

    PipelineBlueprint TilemapPipeline::GetBlueprint() {
        PipelineBlueprint blueprint{};

        blueprint.SetVertexInputDescription(TileVertex::GetBindingDescription(),
                                            TileVertex::GetAttributeDescriptions());

        blueprint.EnableAlphaBlending(true);

        m_MVPmatrixDescription = blueprint.AddPushConstant("MVP",VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::mat4));

        UniformBlueprint atlasUniformBp{ShaderStage::FRAGMENT, 0, 1};
        UniformBlueprint colorUniformBp{ShaderStage::FRAGMENT, 1, 1, 0, sizeof(glm::vec4)};

        m_AtlasUniform = blueprint.AddUniform("uTexture",MV::move(atlasUniformBp));
        m_ColorUniform = blueprint.AddUniform("color",MV::move(colorUniformBp));

        blueprint.AddShader(
                MV::make_rc<VulkanShader>(".shadercache/tile_vertex_0.shadercache", ShaderType::VERTEX));
        blueprint.AddShader(
                MV::make_rc<VulkanShader>(".shadercache/tile_fragment_0.shadercache", ShaderType::FRAGMENT));

        return blueprint;
    }
}
