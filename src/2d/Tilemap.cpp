//
// Created by Matty on 2021-10-15.
//

#include "../../include/2d/Tilemap.h"
#include "../../include/common/EngineCommon.h"

namespace MV {
    void Tilemap::Render(FrameContext &context) {
        WorldObject::Render(context);

        if (m_pIsLoaded) {
            auto &tilemapPipeline = GetBuildins()->m_TilemapPipeline;

            for (auto &&layer: m_pStaticLayers) {
                const auto texture = layer.m_Layer->GetTilesetTexture();
                if (!texture) {
                    continue;
                }
                tilemapPipeline.m_Pipeline->PreBindUniform(context, tilemapPipeline.m_AtlasUniform, texture, true);
                layer.m_VertexBuffer->Bind(context);
                layer.m_IndexBuffer->Bind(context);
                vkCmdDrawIndexed(context.m_FrameCommandBuffer, layer.m_IndexBuffer->GetIndexCount(), 1, 0, 0, 0);
            }
        }
    }

    Tilemap::Tilemap(size_t width, size_t height) : WorldObject("Tilemap", ScriptType::ENGINE),m_pHeight(height), m_pWidth(width) {
        m_pStaticLayers.reserve(1);
    }

    void Tilemap::Build(b2World *physicsWorld) {
        m_pIsLoaded = false;

        bool canLoad = true;

        for (auto &&layer: m_pStaticLayers) {
            const auto & tilesets = GetResources()->m_Tilesets;

            const auto & found = tilesets.find(layer.m_Layer->GetTileset());

            if (found == tilesets.end()) {
                canLoad = false;
                GetEngine()->LoadResource(layer.m_Layer->GetTileset());
            }
        }

        if (!canLoad) return;

        for (auto &&layer: m_pStaticLayers) {
            layer.m_Layer->Build(layer.m_VertexBuffer, layer.m_IndexBuffer);
            layer.m_Layer->BuildCollision(physicsWorld);
        }
        m_pIsLoaded = true;
    }

    void Tilemap::AddStaticLayer(const rc<TilemapLayer> &layer, float brightness) {
        m_pStaticLayers.emplace_back(StaticLayerData{.m_Layer = layer, .m_Brightness = brightness});
    }

    void Tilemap::Update(float deltaTime) {
        WorldObject::Update(deltaTime);
    }

    void Tilemap::Init() {
    }

    void Tilemap::Cleanup() {
    }

    bool Tilemap::ShouldRender() const {
        if (!m_pIsLoaded)
            return false;

        for (auto &&layer: m_pStaticLayers) {
            if (!layer.m_IndexBuffer || !layer.m_VertexBuffer || layer.m_IndexBuffer->GetSize() == 0 || layer.m_VertexBuffer->GetSize() == 0)
                return false;
        }

        return true;
    }
}