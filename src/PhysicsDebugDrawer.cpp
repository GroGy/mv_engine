//
// Created by Matty on 2021-10-24.
//

#include "../include/2d/PhysicsDebugDrawer.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    static const float ALPHA_DIVIDER = 3.0f;

    void PhysicsDebugDrawer::DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color) {
        for(uint32_t i = 0; i < vertexCount; i++) {
            DrawSegment(vertices[i % vertexCount], vertices[ (i + 1) % vertexCount], color);
        }

        //for (uint32_t i = 1; i < vertexCount - 1; i++) {
        //    m_pVertices.emplace_back(glm::vec2(processPosition(vertices[0].x), processPosition(vertices[0].y)),
        //                             glm::vec4(color.r, color.g, color.b, color.a / ALPHA_DIVIDER));
        //    m_pVertices.emplace_back(glm::vec2(processPosition(vertices[i].x), processPosition(vertices[i].y)),
        //                             glm::vec4(color.r, color.g, color.b, color.a / ALPHA_DIVIDER));
        //    m_pVertices.emplace_back(glm::vec2(processPosition(vertices[i+1].x), processPosition(vertices[i+1].y)),
        //                             glm::vec4(color.r, color.g, color.b, color.a / ALPHA_DIVIDER));
        //}
    }

    void PhysicsDebugDrawer::DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color) {

        //for(uint32_t i = 0; i < vertexCount; i++) {
        //    DrawSegment(vertices[i % vertexCount], vertices[ (i + 1) % vertexCount], color);
        //}

        for (uint32_t i = 1; i < vertexCount - 1; i++) {
            m_pVertices.emplace_back(glm::vec2(processPosition(vertices[0].x), processPosition(vertices[0].y)),
                                     glm::vec4(color.r, color.g, color.b, color.a / ALPHA_DIVIDER));
            m_pVertices.emplace_back(glm::vec2(processPosition(vertices[i].x), processPosition(vertices[i].y)),
                                     glm::vec4(color.r, color.g, color.b, color.a / ALPHA_DIVIDER));
            m_pVertices.emplace_back(glm::vec2(processPosition(vertices[i+1].x), processPosition(vertices[i+1].y)),
                                     glm::vec4(color.r, color.g, color.b, color.a / ALPHA_DIVIDER));
        }
    }

    void PhysicsDebugDrawer::DrawCircle(const b2Vec2 &center, float radius, const b2Color &color) {

    }

    void PhysicsDebugDrawer::DrawSolidCircle(const b2Vec2 &center, float radius, const b2Vec2 &axis,
                                             const b2Color &color) {

    }

    void PhysicsDebugDrawer::DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color) {
        auto dir = glm::normalize(glm::vec2(p2.x,p2.y) - glm::vec2(p1.x,p1.y));
        auto sideDir = glm::vec2(-dir.y,dir.x);
        float segmentWidth = 0.01f;
        glm::vec4 lineCol{1.0,0.24,0.21,1.0};

        glm::vec2 p1glm{processPosition(p1.x),processPosition(p1.y)};
        glm::vec2 p2glm{processPosition(p2.x),processPosition(p2.y)};

        glm::vec2 minP1 = p1glm - sideDir * segmentWidth;
        glm::vec2 maxP1 = p1glm + sideDir * segmentWidth;

        glm::vec2 minP2 = p2glm - sideDir * segmentWidth;
        glm::vec2 maxP2 = p2glm + sideDir * segmentWidth;

        m_pVertices.emplace_back(minP1,lineCol);
        m_pVertices.emplace_back(maxP1,lineCol);
        m_pVertices.emplace_back(maxP2,lineCol);

        m_pVertices.emplace_back(minP1,lineCol);
        m_pVertices.emplace_back(minP2,lineCol);
        m_pVertices.emplace_back(maxP2,lineCol);
    }

    void PhysicsDebugDrawer::DrawTransform(const b2Transform &xf) {

    }

    void PhysicsDebugDrawer::DrawPoint(const b2Vec2 &p, float size, const b2Color &color) {

    }

    void PhysicsDebugDrawer::Render(FrameContext &context) {
        //TODO: Move to command queue, to eventually move it to multiple render threads
        auto vertBufSize = m_pVertices.size() * sizeof(PhysicsDebugVertex);
        if (vertBufSize == 0) return;
        if (vertBufSize > m_pVBO->GetSize()) {
            m_pVBO->Cleanup();
            m_pVBO = GetRenderer()->CreateVertexBuffer(vertBufSize * 2, sizeof(PhysicsDebugVertex));
        }

        if(!m_pStagingBuffer || m_pStagingBuffer->GetSize() < vertBufSize) {
            m_pStagingBuffer = GetRenderer()->CreateStagingBuffer(vertBufSize * 2);
        }
        m_pStagingBuffer->Upload(m_pVertices.data(), vertBufSize);
        m_pVBO->Upload(m_pStagingBuffer);

        m_pVBO->Bind(context);
        vkCmdDraw(context.m_FrameCommandBuffer, m_pVertices.size(), 1, 0, 0);
        m_pVertices.clear();
    }

    void PhysicsDebugDrawer::Init() {
        m_pVBO = GetRenderer()->CreateVertexBuffer(4096 * sizeof(PhysicsDebugVertex), sizeof(PhysicsDebugVertex));
        m_pVertices.reserve(4096);
        SetFlags(b2Draw::e_shapeBit | b2Draw::e_aabbBit);
    }

    void PhysicsDebugDrawer::Cleanup() {
        if(m_pVBO) m_pVBO->Cleanup();
    }

    float PhysicsDebugDrawer::processPosition(float in) {
        return (in);
    }

    b2Vec2 PhysicsDebugDrawer::processPosition(b2Vec2 in) {
        return {processPosition(in.x), processPosition(in.y)};
    }
}