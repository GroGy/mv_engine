//
// Created by Matty on 2022-02-19.
//

#include "../include/resources/ScriptConfigResource.h"

namespace MV {

    ScriptConfigResource::ScriptConfigResource(MV::string path) : Resource(path) {

    }

    ResourceLoadResult ScriptConfigResource::process() {
        return ResourceLoadResult::FAILED;
    }

    void ScriptConfigResource::finish() {

    }
}