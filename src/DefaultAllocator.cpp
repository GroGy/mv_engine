//
// Created by Matty on 2022-03-07.
//

#include "../include/allocator/DefaultAllocator.h"
#include "../include/util/Container.h"

#include <iostream>

namespace MV {
    DefaultAllocator &DefaultAllocator::GetInstance() {
        static DefaultAllocator instance;
        return instance;
    }

    DefaultAllocator::DefaultAllocator(const DefaultAllocator &x) : allocator(x) {
        m_pAllocatedSinceLastFrame = x.m_pAllocatedSinceLastFrame;
        m_pTotalAllocated = x.m_pTotalAllocated;
        m_pScriptsAllocated = x.m_pScriptsAllocated;
    }

    void *DefaultAllocator::allocate(size_t n, int flags) {
        const auto res = allocator::allocate(n, flags);
        m_pTotalAllocated += n;
        m_pAllocatedSinceLastFrame += n;
        if (flags == AllocationType::SCRIPT)
            m_pScriptsAllocated += n;
        return res;
    }

    void *DefaultAllocator::allocate(size_t n, size_t alignment, size_t offset, int flags) {
        m_pTotalAllocated += n;
        m_pAllocatedSinceLastFrame += n;
        if (flags == AllocationType::SCRIPT)
            m_pScriptsAllocated += n;
        const auto res = allocator::allocate(n, alignment, offset, flags);
        return res;
    }

    void DefaultAllocator::deallocate(void *p, size_t n) {
        return deallocate(p, n, 0);
    }

    const char *DefaultAllocator::get_name() const {
        return allocator::get_name();
    }

    void DefaultAllocator::set_name(const char *pName) {
        allocator::set_name(pName);
    }

    DefaultAllocator::DefaultAllocator() : eastl::allocator("Default MV Allocator") {

    }

    size_t DefaultAllocator::get_total_allocated() const {
        return m_pTotalAllocated;
    }

    size_t DefaultAllocator::get_allocated_this_frame() {
        const auto res = m_pAllocatedSinceLastFrame;
        m_pAllocatedSinceLastFrame = 0;
        return res;
    }

    void *DefaultAllocator::reallocate(void *ptr, size_t n, int flags) {
        const auto res = realloc(ptr, n);
        m_pTotalAllocated += n;
        return res;
    }

    void DefaultAllocator::deallocate(void *p, size_t n, int flags) {
        auto size = n;
        m_pTotalAllocated -= size;
        if (flags == AllocationType::SCRIPT)
            m_pScriptsAllocated -= size;
        return allocator::deallocate(p, size);
    }

    size_t DefaultAllocator::get_script_allocated() const {
        return m_pScriptsAllocated;
    }

}

void *operator new[](size_t size, const char *pName, int flags, unsigned int debugFlags, const char *file, int line) {
    return malloc(size);
}

void *operator new[](size_t size, size_t alignment, size_t alignmentOffset, const char *pName, int flags,
                     unsigned int debugFlags, const char *file, int line) {
    return malloc(size);
}
