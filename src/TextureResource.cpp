//
// Created by Matty on 2021-10-15.
//

#include "../include/resources/TextureResource.h"
#include "../include/common/EngineCommon.h"
#include <cassert>

namespace MV {
    void TextureResource::finish() {
        if (!m_pSuccess)
            return;

        auto renderer = GetRenderer();

        MV::TextureBlueprint bp{};

        bp.m_Height = m_Result.m_Height;
        bp.m_Width = m_Result.m_Width;
        bp.m_TextureChannels = m_Result.m_TextureChannels;
        bp.m_MaxAnisotropy = renderer->GetLimits().m_MaxAnisotropy;
        bp.m_Filter = MV::TextureFilter::NEAREST; //TODO: Add to texture data

        auto texture = renderer->CreateTexture(MV::move(bp));

        auto staging = renderer->CreateStagingBuffer(m_Result.m_DataSize);

        staging->Upload(m_Result.m_Data.data(), m_Result.m_DataSize);

        texture->Upload(staging);

        if (m_SaveResult != nullptr) {
            auto* resPtr = (MV::rc<Texture>*)m_SaveResult;
            *resPtr = texture;
        }

        renderer->RegisterRenderingResource(m_pID, texture);

        m_pSuccess = true;
    }

    ResourceLoadResult TextureResource::process() {
        m_pPath = PathProjectRelativeToAbsolute(m_pPath);

        if (!m_pPath.empty()) {
            std::ifstream input{m_pPath.c_str(), std::ios::binary};
            if (input.is_open()) {
                auto header = ReadAssetHeader(input);

                assert(header.m_Type == AssetType::TEXTURE);

                input.read((char *) &m_Result.m_Width, sizeof(uint32_t));
                input.read((char *) &m_Result.m_Height, sizeof(uint32_t));
                input.read((char *) &m_Result.m_TextureChannels, sizeof(uint32_t));

                input.read((char *) &m_Result.m_DataSize, sizeof(size_t));

                m_Result.m_Data.resize(m_Result.m_DataSize);

                input.read((char *) m_Result.m_Data.data(), m_Result.m_DataSize * sizeof(uint8_t));

                input.close();
            } else {
                m_pError = "Failed to open texture file.";
                return ResourceLoadResult::FILE_READ_ERROR;
            }
        } else {
            std::string data((const char *)m_pRawData.data(), m_pRawData.size());
            std::istringstream input(data);

            MV::ResourceHeader header{};

            input.read((char*)&header.m_Type, sizeof(uint32_t));
            input.read((char*)&header.m_Version, sizeof(uint32_t));

            MV::Assert(header.m_Type == AssetType::TEXTURE, "Asset must be texture");

            input.read((char *) &m_Result.m_Width, sizeof(uint32_t));
            input.read((char *) &m_Result.m_Height, sizeof(uint32_t));
            input.read((char *) &m_Result.m_TextureChannels, sizeof(uint32_t));
            input.read((char *) &m_Result.m_DataSize, sizeof(size_t));
            m_Result.m_Data.resize(m_Result.m_DataSize);
            input.read((char *) m_Result.m_Data.data(), m_Result.m_DataSize * sizeof(uint8_t));
        }

        m_pSuccess = true;
        return ResourceLoadResult::SUCCESS;
    }

    TextureResource::TextureResource(MV::string path, uint64_t id) : Resource(MV::move(path)), m_pID(id) {

    }

    TextureResource::TextureResource(MV::vector<uint8_t> rawData, uint64_t id) : Resource(MV::move(rawData)),
                                                                                 m_pID(id) {

    }
}