//
// Created by Matty on 2022-02-06.
//

#include "../include/scripting/ScriptObjectInstance.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    void ScriptObjectInstance::Destroy() {
        if(m_pObject) {
            auto & sm = MV::GetEngine()->m_ScriptManager;
            if(!m_pMonoCreatedObject) {
                mono_gchandle_free(m_pGCHandle);
                sm.m_pGCHandles.erase(m_pGCHandle);
                m_pGCHandle = 0;

                for(auto && v : m_pPropertyGCHandles) {
                    mono_gchandle_free(v);
                    sm.m_pGCHandles.erase(v);
                }

                m_pPropertyGCHandles.clear();
            }

            m_pObject = nullptr;
        }
    }

    MV::string ScriptObjectInstance::GetClassName() {
        ENSURE_VALID(m_pObject, "");

        MonoClass * klass = mono_object_get_class(m_pObject);

        return {mono_class_get_name(klass)};
    }

    ScriptObjectInstance ScriptObjectInstance::CreateFromMonoObject(MonoObject *object) {
        ScriptObjectInstance res{};

        res.m_pObject = object;
        res.m_pMonoCreatedObject = true;

        return res;
    }

    MonoObject *ScriptObjectInstance::GetMonoObject() const {
        return m_pObject;
    }

    MV::string ScriptObjectInstance::GetClassNamePotentionallyEmpty() {
        if(!m_pObject) return "";

        MonoClass * klass = mono_object_get_class(m_pObject);

        if(!klass) return "";

        return {mono_class_get_name(klass)};
    }
}