//
// Created by Matty on 2021-11-01.
//

#include <cassert>
#include "../include/resources/WorldResource.h"
#include "../include/common/EngineCommon.h"
#include "../include/profiler/Profiler.h"

namespace MV {
    WorldResource::WorldResource(MV::string path, uint64_t id) : Resource(MV::move(path)), m_pID(id) {

    }

    static void readWorldObjects(WorldData & worldData , const nlohmann::json & woJson) {
        for(auto && wo : woJson.items()) {
            const auto & val = wo.value();

            const auto woIDKey = wo.key();

            WorldObjectData d{};

            d.m_WorldID = val[Config::sc_JsonWorldIDField.data()].get<uint64_t>();

            try {
                d.m_Static = val.at(Config::sc_JsonStaticField.data()).get<bool>();
            } catch (const std::exception & e) {

            }
            const auto & posJson = val[Config::sc_JsonPositionField.data()];
            d.m_Position = {posJson[0].get<float>(), posJson[1].get<float>()};

            const auto & scaleJson = val[Config::sc_JsonScaleField.data()];
            d.m_Scale = {scaleJson[0].get<float>(), scaleJson[1].get<float>()};

            d.m_Rotation = val[Config::sc_JsonRotationField.data()].get<float>();
            d.m_Name = val[Config::sc_JsonWONameField.data()].get<MV::string>();

            if(val.contains(Config::sc_JsonConfigField.data())) {
               d.m_IsScriptConfig = true;
               d.m_ConfigAsset = val[Config::sc_JsonConfigField.data()].get<uint64_t>();
            } else
                d.m_IsScriptConfig = false;


            d.m_Data = val[Config::sc_JsonDataField.data()];
            d.m_Script = val[Config::sc_JsonScriptField.data()].get<MV::string>();

            worldData.m_WorldObjects[d.m_WorldID] = MV::move(d);
        }
    }

    ResourceLoadResult WorldResource::process() {
        MV_PROFILE_FUNCTION("WorldResource::process");
        m_pPath = PathProjectRelativeToAbsolute(m_pPath);
        std::ifstream input{m_pPath.c_str()};
        if(input.is_open()) {
            try {
                nlohmann::json inputData = nlohmann::json::parse(input);

                const auto jsonSizeArray = inputData[Config::sc_JsonSizeField.data()];
                m_Result.m_Width = jsonSizeArray[0].get<uint32_t>();
                m_Result.m_Height = jsonSizeArray[1].get<uint32_t>();

                m_Result.m_Gravity = inputData[Config::sc_JsonGravityField.data()].get<float>();
                m_Result.m_Name = inputData[Config::sc_JsonNameField.data()].get<MV::string>();

                const auto worldObjectsJson = inputData[Config::sc_JsonWorldObjectsField.data()];

                readWorldObjects(m_Result, worldObjectsJson);

                input.close();
            } catch(std::exception & e) {
                input.close();
                return ResourceLoadResult::FILE_READ_ERROR;
            }

        } else {
            return ResourceLoadResult::FILE_READ_ERROR;
        }

        m_pSuccess = true;
        return ResourceLoadResult::SUCCESS;
    }

    void WorldResource::finish() {
        if (!m_pSuccess)
            return;
        MV_PROFILE_FUNCTION("WorldResource::finish");
        GetEngine()->GetWorld() = MV::move(World(MV::move(m_Result)));
        GetEngine()->GetWorld().Init();
        GetEngine()->GetWorld().m_ID = m_pID;
    }
}