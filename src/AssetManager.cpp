//
// Created by Matty on 2022-03-07.
//

#include "../include/common/AssetManager.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    void AssetManager::Init() {
        const auto & project = GetEngine()->GetProject();


    }

    void AssetManager::Cleanup() {
        for (auto &item : m_pAssetsFromTypes){
            item.set_capacity(0); //Like clear but better
        }
    }

    size_t AssetManager::GetAssetCount(AssetType type) const {
        return m_pAssetsFromTypes[static_cast<size_t>(type)].size();
    }
}