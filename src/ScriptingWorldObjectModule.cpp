//
// Created by Matty on 2022-02-11.
//

#include "../include/scripting/module/ScriptingWorldObjectModule.h"
#include "../include/scripting/ScriptManager.h"
#include "../include/common/EngineCommon.h"
#include "../include/common/ExceptionCommon.h"

namespace MV {
    static void wo_SetPositionFN(uint64_t id, glm::vec2 pos) {
        const auto engine = GetEngine();
        const auto & world = engine->GetWorld();
        if(!world.IsValid()) {
            GetLogger()->Script_LogError("Tried to call Native_WOSetPosition on invalid world!");
            return;
        }
        try {
            world.m_WorldObjects.at(id)->ForceSetPosition(pos);
        } catch (std::exception & e) {
            GetLogger()->Script_LogError("World object not found! ID:", std::to_string(id));
        }
    };
    static void wo_SetScaleFN(uint64_t id, glm::vec2 scale) {
        const auto engine = GetEngine();
        const auto & world = engine->GetWorld();
        if(!world.IsValid()) {
            GetLogger()->Script_LogError("Tried to call Native_WOSetScale on invalid world!");
            return;
        }
        try {
            world.m_WorldObjects.at(id)->ForceSetScale(scale);
        } catch (std::exception & e) {
            GetLogger()->Script_LogError("World object not found! ID:", std::to_string(id));
        }
    };
    static void wo_SetRotationFN(uint64_t id, float rotation) {
        const auto engine = GetEngine();
        const auto & world = engine->GetWorld();
        if(!world.IsValid()) {
            GetLogger()->Script_LogError("Tried to call Native_WOSetRotation on invalid world!");
            return;
        }
        try {
            world.m_WorldObjects.at(id)->ForceSetRotation(rotation);
        } catch (std::exception & e) {
            GetLogger()->Script_LogError("World object not found! ID:", std::to_string(id));
        }
    };

    void ScriptingWorldObjectModule::InitFunctionCallbacks() {
        mono_add_internal_call ("MV.WorldObject::SetPosition_Native",(const void*)wo_SetPositionFN);
        mono_add_internal_call ("MV.WorldObject::SetRotation_Native",(const void*)wo_SetRotationFN);
        mono_add_internal_call ("MV.WorldObject::SetScale_Native",(const void*)wo_SetScaleFN);
    }

    void ScriptingWorldObjectModule::CheckClassesExistence(MonoImage *engineAssembly) {
        auto val = mono_class_from_name(engineAssembly, "MV", "WorldObject");

        if(!val) throw RuntimeException("Missing WorldObject class in engine assembly!");
    }

    void ScriptingWorldObjectModule::CheckClassFunctionsExistence(MonoImage *engineAssembly) {

    }
}