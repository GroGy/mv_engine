//
// Created by Matty on 2022-06-19.
//

#include "../include/2d/BoxColliderProperty.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    void BoxColliderProperty::Cleanup() {
    }

    void BoxColliderProperty::Update(float deltaTime) {
        Property::Update(deltaTime);

        if(m_pFixture) {
            auto * body = m_pFixture->GetBody();
            auto position = body->GetPosition();
            m_WorldObject->SetPosition({position.x, position.y});
            m_WorldObject->SetRotation(body->GetAngle());

        }
    }

    void BoxColliderProperty::Serialize(json &propertyField) {
        propertyField["offset"] = m_Offset;
        propertyField["scale"] = m_Scale;
        propertyField["rotation"] = m_Rotation;
    }

    void BoxColliderProperty::Deserialize(const json &propertyField) {
        m_Offset = propertyField["offset"].get<glm::vec2>();
        m_Scale = propertyField["scale"].get<glm::vec2>();
        m_Rotation = propertyField["rotation"].get<float>();
    }

    void BoxColliderProperty::Sync(Script &owner,const ScriptClassField &field) {
        Script propertyField = owner.GetFieldScript(field);

        propertyField.SetField("_scale", m_Scale);
        propertyField.SetField("_offset", m_Offset);
        propertyField.SetField("_rotation", m_Rotation);
    }

    void BoxColliderProperty::SyncPropertyData(const ScriptClassField &field, Script &script, const json &data) {
        Script propertyField = script.GetFieldScript(field);

        propertyField.SetField("_scale", data["scale"].get<glm::vec2>());
        propertyField.SetField("_offset",data["offset"].get<glm::vec2>());
        propertyField.SetField("_rotation", data["rotation"].get<float>());
    }

    void BoxColliderProperty::Init() {
        auto & world = MV::GetEngine()->GetWorld();

        assert(world.IsValid());

        auto physicsWorld = world.GetPhysicsWorld();

        const auto woPos = m_WorldObject->GetPosition();
        const auto woScale = m_WorldObject->GetScale();

        auto & physBody = m_WorldObject->GetPhysicsBody();
        if(!physBody) {
            b2BodyDef bodyDef;
            bodyDef.position.Set(woPos.x, woPos.y);
            bodyDef.angle = glm::radians(m_WorldObject->GetRotation());
            bodyDef.type = m_WorldObject->m_IsStatic ?  b2_staticBody : b2_dynamicBody;

            physBody = physicsWorld->CreateBody(&bodyDef);
        }

        m_pShape.SetAsBox((woScale.x * m_Scale.x) / 2.0f, (woScale.y * m_Scale.y)/ 2.0f, {m_Offset.x, m_Offset.y},m_Rotation);


        b2FixtureDef def;
        def.shape = &m_pShape;
        def.density = m_Density;
        def.userData.pointer = (uintptr_t)m_WorldObject.get();

        m_pFixture = physBody->CreateFixture(&def);
    }

    void BoxColliderProperty::OnOwnerUpdate() {
        const auto woScale = m_WorldObject->GetScale();
        m_pShape.SetAsBox(woScale.x * m_Scale.x, woScale.y * m_Scale.y, {m_Offset.x, m_Offset.y},m_Rotation);
    }

    void BoxColliderProperty::OnDataChanged() {

    }
}