//
// Created by Matty on 2021-10-23.
//

#include "../include/util/thread_safe_queue.h"

namespace MV {
    MV::thread_safe_queue_no_pop_exception::~thread_safe_queue_no_pop_exception() MV_TXN_SAFE_DYN MV_NOTHROW {

    }

    const char *MV::thread_safe_queue_no_pop_exception::what() const MV_TXN_SAFE_DYN MV_NOTHROW {
        return exception::what();
    }
}