//
// Created by Matty on 2022-01-23.
//

#include "../include/common/Atlas.h"
#include "../include/common/EngineCommon.h"

namespace MV {
    Atlas::Atlas(const AtlasData &data) {
        m_pTexture = data.m_Texture;
        m_pUVs = data.m_UVs;
        m_pFrameNames = data.m_FrameNames;
        m_pOffsets = data.m_UVOffsets;
    }

    const glm::vec4 &Atlas::GetUVs(uint32_t index) const {
        ENSURE_TRUE(index >= 0 && index < m_pUVs.size(), {})
        return m_pUVs[index];
    }

    const glm::vec4 &Atlas::GetUVOffsets(uint32_t index) const {
        ENSURE_TRUE(index >= 0 && index < m_pUVs.size(), {})
        return m_pOffsets[index];
    }

    rc<Texture> Atlas::GetTexture() const {
        rc<RenderingResource> rr;
        const auto success = GetRenderer()->GetRenderingResource(m_pTexture, rr);

        if(!success) return {};

        if(auto texture = MV::dynamic_pointer_cast<Texture>(rr)) {
            return texture;
        }

        return {};
    }
}