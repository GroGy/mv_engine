//
// Created by Matty on 2022-03-06.
//

#include <async/ThreadPool.h>
#include "profiler/Profiler.h"

namespace MV {
    ThreadPool::ThreadPool(size_t workerCount) {
        m_pWorkers.resize(workerCount);
    }

    void ThreadPool::notifyWorkers(MV::queue<MV::rc<AsyncTask>> & queue) {
        for(uint32_t i = 0; i < m_pWorkers.size(); i++) {
            if(queue.empty()) break;
            uint32_t index = (m_pWorkerRotationIndex + i) % m_pWorkers.size();

            auto & worker = m_pWorkers[index];
            if(!worker->CanDispatch()) {
                continue;
            }

            worker->DispatchTask(queue.back(), true);
            queue.pop();
        }
        m_pWorkerRotationIndex++;
    }

    void ThreadPool::AwaitCriticalTasks() {
        MV_PROFILE_FUNCTION("Async critical await");
        for(uint32_t i = 0; i < m_pWorkers.size(); i++) {
            auto & worker = m_pWorkers[i];
            if(worker->DoingCriticalTask()) {
                MV_PROFILE_FUNCTION("Waiting for critical");
                worker->Await();
            }
        }
    }

    void ThreadPool::Update() {
        notifyWorkers(m_pCriticalTasks);
        notifyWorkers(m_pTasks);
    }

    void ThreadPool::Init() {
        for(auto & worker : m_pWorkers) {
            worker = MV::make_rc<AsyncWorker>();
            worker->Init();
        }
    }

    void ThreadPool::Cleanup() {
        for(auto & worker : m_pWorkers) {
            worker->Cleanup();
        }
    }

    MV::rc<AsyncTaskPromise<AsyncTask>> ThreadPool::makePromise(const rc <AsyncTask> &task) {
        return MV::rc<AsyncTaskPromise<AsyncTask>>(new AsyncTaskPromise<AsyncTask>(task));
    }
}
