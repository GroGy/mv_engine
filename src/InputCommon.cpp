//
// Created by Matty on 2021-10-01.
//

#include "../include/input/InputCommon.h"
#include "../include/profiler/Profiler.h"
#include "common/EngineCommon.h"

#include <GLFW/glfw3.h>

namespace MV {
    void glfwKeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
        MV::GetEngine()->m_Input.OnKeyUpdate(static_cast<MV::Key>(key), static_cast<MV::KeyStateUpdate>(action),
                                             scancode);
    }

    void glfwJoystickCallback(int gamepadId, int event) {
        if (event == GLFW_CONNECTED) {
            MV::string name;
            if (!glfwJoystickIsGamepad(gamepadId)) {
                MV_LOG("Gamepad {} is joystick!", gamepadId);
                name = glfwGetJoystickName(gamepadId);
            } else {
                name = glfwGetGamepadName(gamepadId);
            }

            MV_LOG("New gamepad connected with id {}. ({})", gamepadId, name);
        } else {
            MV_LOG("Gamepad {} disconnected.", gamepadId);
        }


        MV::GetEngine()->m_Input.OnJoystickUpdate(gamepadId, event == GLFW_CONNECTED);
    }

    void InputCommon::init(void *inputContext) {
        m_pContext = inputContext;
        m_pInputObject = this;

        GLFWwindow *window = static_cast<GLFWwindow *>(m_pContext);

        glfwSetKeyCallback(window, glfwKeyCallback);
        glfwSetJoystickCallback(glfwJoystickCallback);

        for (int32_t i = 0; i < Config::sc_MaximumSupportedGamepadCount; i++) {
            bool connected = glfwJoystickPresent(i);

            m_pControllers[i].m_State = connected ? GamepadData::GamepadConnectedState::Connected
                                                  : GamepadData::GamepadConnectedState::Disconnected;

            if (!connected) continue;

            m_pControllers[i].m_IsJoystick = !glfwJoystickIsGamepad(i);
            m_pControllers[i].m_Name = m_pControllers[i].m_IsJoystick ? glfwGetJoystickName(i)
                                                                      : glfwGetGamepadName(i);

        }
    }

    bool InputCommon::ShouldExit() const {
        return glfwWindowShouldClose((GLFWwindow *) m_pContext);
    }

    void InputCommon::pollInputEvents() {
        MV_PROFILE_FUNCTION("InputCommon::Update");

        for (auto &&k: m_pKeyUpdates)
            k = KeyUpdate::Stale;

        glfwPollEvents();
        pollGamepads();
    }

    bool InputCommon::IsKeyDown(Key key) const {
        return glfwGetKey((GLFWwindow *) m_pContext, static_cast<int32_t>(key)) == GLFW_PRESS;
    }

    bool InputCommon::IsKeyUp(Key key) const {
        return glfwGetKey((GLFWwindow *) m_pContext, static_cast<int32_t>(key)) == GLFW_RELEASE;
    }

    bool InputCommon::IsKeyPressed(Key key) const {
        return m_pKeyUpdates[static_cast<uint32_t>(key)] == KeyUpdate::Pressed;
    }

    bool InputCommon::IsKeyReleased(Key key) const {
        return m_pKeyUpdates[static_cast<uint32_t>(key)] == KeyUpdate::Released;
    }

    bool InputCommon::IsInputDown(MV::string_view input) const {
        return false;
    }

    bool InputCommon::IsInputUp(MV::string_view input) const {
        return false;
    }

    bool InputCommon::IsInputPressed(MV::string_view input) const {
        return false;
    }

    bool InputCommon::IsInputReleased(MV::string_view input) const {
        return false;
    }

    void InputCommon::pollGamepads() {
        for(int32_t index = 0; index < Config::sc_MaximumSupportedGamepadCount; index++) {
            auto & c = m_pControllers[index];
            if (c.m_State != GamepadData::GamepadConnectedState::Connected)
                continue;

            for (auto &&k: c.m_pKeyUpdates)
                k = KeyUpdate::Stale;

            GLFWgamepadstate state;
            if (!glfwGetGamepadState(index, &state))
                continue;

            for (uint32_t i = 0; i < static_cast<uint32_t>(GamepadKey::COUNT); i++) {
                const bool pressed = state.buttons[i] == GLFW_PRESS;

                if (c.m_pKeyStates[i] && !pressed) {
                    c.m_pKeyUpdates[i] = KeyUpdate::Released;
                }

                if (!c.m_pKeyStates[i] && pressed) {
                    c.m_pKeyUpdates[i] = KeyUpdate::Pressed;
                }

                c.m_pKeyStates[i] = pressed;
            }

            for (uint32_t i = 0; i < Config::sc_GamepadAxisCount; i++) {
                c.m_pAxisValues[i] = state.axes[i];
            }
        }
    }

    bool InputCommon::IsGamepadKeyDown(uint32_t controllerIndex, GamepadKey key) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, false);

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return false;

        return m_pControllers[controllerIndex].m_pKeyStates[static_cast<uint32_t>(key)];
    }

    bool InputCommon::IsGamepadKeyUp(uint32_t controllerIndex, GamepadKey key) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, false)

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return false;

        return !m_pControllers[controllerIndex].m_pKeyStates[static_cast<uint32_t>(key)];
    }

    bool InputCommon::IsGamepadKeyPressed(uint32_t controllerIndex, GamepadKey key) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, false);

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return false;

        return m_pControllers[controllerIndex].m_pKeyUpdates[static_cast<uint32_t>(key)] == KeyUpdate::Pressed;
    }

    bool InputCommon::IsGamepadKeyReleased(uint32_t controllerIndex, GamepadKey key) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, false);

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return false;

        return m_pControllers[controllerIndex].m_pKeyUpdates[static_cast<uint32_t>(key)] == KeyUpdate::Released;
    }

    float InputCommon::GetGamepadTrigger(uint32_t controllerIndex, GamepadTrigger trigger) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, 0.0f);

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return 0.0f;

        auto & axis = m_pControllers[controllerIndex].m_pAxisValues;

        return axis[static_cast<uint32_t>(trigger)];
    }

    glm::vec2 InputCommon::GetGamepadJoystick(uint32_t controllerIndex, GamepadJoystick joystick) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, {0,0} );

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return {0,0};

        const auto axisIndex = static_cast<uint32_t>(joystick);

        auto & axis = m_pControllers[controllerIndex].m_pAxisValues;

        return {axis[axisIndex], axis[axisIndex + 1]};
    }

    bool InputCommon::IsGamepadConnected(uint32_t controllerIndex) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, false);

        return m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Connected;
    }

    bool InputCommon::IsGamepadJoystick(uint32_t controllerIndex) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, false);

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return false;

        return m_pControllers[controllerIndex].m_IsJoystick;
    }

    MV::string InputCommon::GetGamepadName(uint32_t controllerIndex) const {
        ENSURE_TRUE(controllerIndex < Config::sc_MaximumSupportedGamepadCount, "");

        if(m_pControllers[controllerIndex].m_State == GamepadData::GamepadConnectedState::Disconnected)
            return "";

        return m_pControllers[controllerIndex].m_Name;
    }
}
