//
// Created by Matty on 2021-10-01.
//

#include "../include/common/EngineCommon.h"

namespace MV {
    static Engine *s_EngineGlobal = nullptr;
    static Logger *s_LoggerGlobal = nullptr;

    void EngineCommon::SetEngineGlobal(Engine *engine) {
        s_EngineGlobal = engine;
    }

    void EngineCommon::SetLoggerGlobal(Logger *logger) {
        s_LoggerGlobal = logger;
    }

    Engine *GetEngine() {
        return s_EngineGlobal;
    }

    Logger *GetLogger() {
        return s_LoggerGlobal;
    }

    rc<Renderer> GetRenderer() {
        return s_EngineGlobal->m_Renderer;
    }

    Buildin *GetBuildins() {
        return &s_EngineGlobal->m_Buildin;
    }

    LoadedResources *GetResources() {
        return &s_EngineGlobal->m_LoadedResources;
    }
}