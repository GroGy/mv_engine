//
// Created by Matty on 2022-03-06.
//

#ifndef MV_EDITOR_PROFILER_H
#define MV_EDITOR_PROFILER_H

#if !defined(MV_ENGINE_ENABLE_PROFILING)

#include <optick.h>
#define MV_PROFILE_FRAME_MT() OPTICK_FRAME("Main Thread")
#define MV_PROFILE_THREAD(name) OPTICK_THREAD(name)
#define MV_PROFILE_FUNCTION(name) OPTICK_EVENT(name)
#define MV_PROFILE_CATEGORY(name, category) OPTICK_CATEGORY(name,category)
#define MV_PROFILE_TAG(name, value) OPTICK_TAG(name, value)

#else
#define MV_PROFILE_FRAME_MT() {}
#define MV_PROFILE_THREAD(name) {}
#define MV_PROFILE_FUNCTION(name) {}
#define MV_PROFILE_CATEGORY(name, category) {}
#define MV_PROFILE_TAG(name, value) {}

#endif

#endif //MV_EDITOR_PROFILER_H
