//
// Created by Matty on 2022-03-13.
//

#include "UtilShader.h"

#include <fstream>
#include <sstream>

namespace MV {
    string UtilShader::ReadShaderFromFile(string_view path) {
        std::ifstream ifs{path.data(), std::ios::binary};

        if(!ifs.is_open())
            return "";

        std::stringstream ss;
        ss << ifs.rdbuf();
        ifs.close();

        return ss.str().c_str();
    }

    string UtilShader::ReadShaderSourceFromFile(string_view path) {
        std::ifstream ifs{path.data()};

        if(!ifs.is_open())
            return "";

        std::stringstream ss;
        ss << ifs.rdbuf();
        ifs.close();

        return ss.str().c_str();
    }
}