//
// Created by Matty on 2022-03-13.
//

#ifndef MV_EDITOR_UTILSHADER_H
#define MV_EDITOR_UTILSHADER_H

#include <EngineMinimal.h>

namespace MV {
    class UtilShader {
    public:
        static string ReadShaderFromFile(string_view path);
        static string ReadShaderSourceFromFile(string_view path);
    };
}


#endif //MV_EDITOR_UTILSHADER_H
