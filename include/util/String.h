//
// Created by Matty on 2022-03-07.
//

#ifndef MV_EDITOR_STRING_H
#define MV_EDITOR_STRING_H

#include <string>
#include <codecvt>

namespace MV {
    template <typename T>
    inline bool StartsWith(const std::basic_string<T> & string, const T & character) {
        return string.length() == 0 ? false : string[0] == character;
    }

    template <typename T>
    inline bool StartsWith(const eastl::basic_string<T> & string, const T & character) {
        return string.length() == 0 ? false : string[0] == character;
    }

    // Converts WString to string TODO: Remove copy by reimplementing string conversion with eastl
    inline MV::string WStringToString(const MV::wstring & wstring) {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
        std::string res = conv.to_bytes(wstring.data());
        return {res.data()};
    }


    inline MV::wstring StringToWString(const MV::string & string) {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
        return {converter.from_bytes(string.c_str()).c_str()};
    }

}

#endif //MV_EDITOR_STRING_H
