//
// Created by Matty on 2021-10-23.
//

#ifndef MV_ENGINE_THREAD_SAFE_QUEUE_H
#define MV_ENGINE_THREAD_SAFE_QUEUE_H

#include <mutex>
#include <condition_variable>
#include <queue>
#include <EngineMinimal.h>

namespace MV {
class thread_safe_queue_no_pop_exception : public std::exception {
public:
    ~thread_safe_queue_no_pop_exception() MV_TXN_SAFE_DYN MV_NOTHROW override;

    const char *what() const MV_TXN_SAFE_DYN MV_NOTHROW override;
};

    template<typename T>
    class thread_safe_queue {
    private:
        std::queue<T> m_pQueue{};
        mutable std::mutex m_pMutex{};
        mutable std::mutex m_pWaitMutex{};
        std::condition_variable m_pCV{};
        bool m_pWakeCV = false;
    public:
        void push(T &&val);

        T pop(bool wait = true);

        bool empty() const;

        void wake_cv();

        void clear_wake_cv();
    };

    template<typename T>
    void thread_safe_queue<T>::push(T &&val) {
        std::lock_guard<std::mutex> guard{m_pMutex};
        m_pQueue.push(MV::move(val));
        m_pCV.notify_one();
    }

    template<typename T>
    T thread_safe_queue<T>::pop(bool wait) {
        m_pMutex.lock();
        std::unique_lock<decltype(m_pWaitMutex)> lk{m_pWaitMutex};

        if(!wait && m_pQueue.empty()) {
            m_pMutex.unlock();
            throw thread_safe_queue_no_pop_exception{};
        }

        while (m_pQueue.empty()) {
            m_pMutex.unlock();
            m_pCV.wait(lk);
            if (m_pWakeCV) throw thread_safe_queue_no_pop_exception{};
            m_pMutex.lock();
        }

        auto res = MV::move(m_pQueue.front());
        m_pQueue.pop();
        m_pMutex.unlock();
        return res;
    }

    template<typename T>
    bool thread_safe_queue<T>::empty() const {
        std::lock_guard<std::mutex> guard{m_pMutex};
        return m_pQueue.empty();
    }

    template<typename T>
    void thread_safe_queue<T>::wake_cv() {
        m_pWakeCV = true;
        m_pCV.notify_all();
    }

    template<typename T>
    void thread_safe_queue<T>::clear_wake_cv() {
        m_pWakeCV = false;
    }

}


#endif //MV_ENGINE_THREAD_SAFE_QUEUE_H
