//
// Created by Matty on 2022-01-23.
//

#ifndef MV_ENGINE_MATH_H
#define MV_ENGINE_MATH_H

#include <EngineMinimal.h>
#include "geometric.hpp"

namespace MV {
    class Math {
    public:
        static float Map(float value,
                               float start1, float stop1,
                               float start2, float stop2);

        static float FRand(float min, float max);

        template <typename T>
        static inline T Cube(T f)
        {
            return f * f * f;
        }

        template <typename T>
        static inline T Square(T f)
        {
            return f * f;
        }
        
        template <typename T>
        static inline T Bezier(float t, T p0, T p1, T p2, T p3)
        {
            return Cube(1 - t) * p0 + 3 * Square(1 - t) * t * p1 + 3 * (1 - t) * Square(t) * p2 + Cube(t) * p3;
        }

        static inline glm::vec2 Bezier(float t, const glm::vec2& p0, const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3)
        {
            return {Bezier(t, p0.x, p1.x, p2.x, p3.x), Bezier(t, p0.y, p1.y, p2.y, p3.y)};
        }

        template <typename T>
        static inline T BezierDerivative(float t, T p0, T p1, T p2, T p3)
        {
            return 3 * Square(1 - t) * (p1 - p0) + 6 * (1 - t) * t * (p2 - p1) + 3 * Square(t) * (p3 - p2);
        }

        // Tangent
        static inline glm::vec2 BezierDerivative(float t, const glm::vec2& p0, const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3)
        {
            return {BezierDerivative(t, p0.x, p1.x, p2.x, p3.x), BezierDerivative(t, p0.y, p1.y, p2.y, p3.y)};
        }

        // Normal
        static inline glm::vec2 BezierNormal(float t, const glm::vec2& p0, const glm::vec2& p1, const glm::vec2& p2, const glm::vec2& p3)
        {
            auto deriv = BezierDerivative(t, p0, p1, p2, p3);
            return glm::normalize(glm::vec2(-deriv.y, deriv.x));
        }
    };

    class BezierCurve {
    public:

        struct BezierPoint {
            glm::vec2 m_Left;
            glm::vec2 m_Point;
            glm::vec2 m_Right;
        };

        BezierCurve();

        float GetValue(float t) const;

        uint32_t GetDataSize() const { return m_pData.size() - 1; }

        MV::vector<BezierPoint> & GetData() { return m_pData; };
        const MV::vector<BezierPoint> & GetData() const { return m_pData; };
    private:
        MV::vector<BezierPoint> m_pData;
    };
}


#endif //MV_ENGINE_MATH_H
