//
// Created by Matty on 2021-10-05.
//

#ifndef MV_ENGINE_FILEUTIL_H
#define MV_ENGINE_FILEUTIL_H

#include <string>
#include <fstream>
#include <sys/stat.h>
#include <EngineMinimal.h>

namespace MV {
    class FileUtil {
    public:
        /// Checks efficiently if file exists
        /// \param path Path to file that should be checked
        /// \return If file exists
        static bool FileExists(const MV::string &path) {
            struct stat buffer;
            return (stat(path.c_str(), &buffer) == 0);
        }
    };
}


#endif //MV_ENGINE_FILEUTIL_H
