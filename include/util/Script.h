//
// Created by Matty on 07.12.2022.
//

#ifndef MV_EDITOR_SCRIPT_UTIL_H
#define MV_EDITOR_SCRIPT_UTIL_H

#include "EngineMinimal.h"
#include "mono/metadata/blob.h"

namespace MV {
    class ScriptUtil {
    public:
        static MV::string GetMonoTypeToString(MonoTypeEnum type);
    };
}


#endif //MV_EDITOR_SCRIPT_UTIL_H
