//
// Created by Matty on 2022-03-07.
//

#ifndef MV_EDITOR_CONTAINER_H
#define MV_EDITOR_CONTAINER_H

#include <EngineMinimal.h>

namespace MV {
    template <typename T>
    inline bool Contains(const MV::vector<T> & container, const T & val) {
        return container.find(val) != container.end();
    }

    template <typename T>
    inline bool Contains(const MV::unordered_set<T> & container, const T & val) {
        return container.find(val) != container.end();
    }

    template <typename T, typename V>
    inline bool Contains(const MV::unordered_map<T,V> & container, const T & val) {
        return container.find(val) != container.end();
    }

    template <typename T, typename V>
    inline bool Contains(const eastl::vector_map<T,V> & container, const T & val) {
        return container.find(val) != container.end();
    }

    template <typename T, typename V>
    inline bool Contains(const eastl::hash_map<T,V> & container, const T & val) {
        return container.find(val) != container.end();
    }
}


#endif //MV_EDITOR_CONTAINER_H
