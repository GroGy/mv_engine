//
// Created by Matty on 2022-11-12.
//

#ifndef MV_EDITOR_PROPERTYREGISTRATOR_H
#define MV_EDITOR_PROPERTYREGISTRATOR_H

#include <iostream>
#include <EngineMinimal.h>
#include "render/common/IRenderedProperty.h"
#include "render/common/PropertyStorage.h"

#undef GetClassName

namespace MV {
    class RegisteredProperties {
    public:
        static RegisteredProperties& GetInstance();

        static PropertyIDType s_NextComponentTypeID;

        struct PropertyData {
            MV::string  m_ClassName;
            uint32_t    m_ID;
            bool        m_HasRegisteredScriptToken;
            eastl::function<void*()> m_Ctor;
        };

        MV::vector<PropertyData> m_Properties;

        void RegisterProperty(MV::string_view name, uint32_t id, eastl::function<void*()> && ctor);

        template <typename T>
        uint32_t CreatePropertyToken() {
            if (!MV::PropertyID<T>::Registered()) {
                MV::PropertyID<T>::s_ID = s_NextComponentTypeID++;
            }

            return MV::PropertyID<T>::s_ID;
        }
    };

    class IPropertyRegistrator {
    protected:
        IPropertyRegistrator() = default;
    };

    template<typename T>
    class PropertyRegistrator : public IPropertyRegistrator {
    public:
        PropertyRegistrator() {
            auto id = RegisteredProperties::GetInstance().CreatePropertyToken<T>();

            RegisteredProperties::GetInstance().RegisterProperty(T::GetClassName(), id, []() {
                return new PropertyStorage<T>();
            });
        };
    };
}


#endif //MV_EDITOR_PROPERTYREGISTRATOR_H
