//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_INPUTCOMMON_H
#define MV_ENGINE_INPUTCOMMON_H

#include <EngineMinimal.h>
#include "Key.h"
#include "IInputCallbackProvider.h"

namespace MV {
    namespace Config {
        static constexpr uint32_t sc_MaximumSupportedGamepadCount = 16;
        static constexpr uint32_t sc_GamepadAxisCount = 6;
    }

    class InputCommon : public IInputCallbackProvider{
    public: // Friend classes

        friend class Engine;

        friend class IInputCallbackProvider;

    public: // General getters

        // Was window close requested? Cross pressed.
        [[nodiscard]]
        bool ShouldExit() const;

    public: // Key input functions

        [[nodiscard]]
        bool IsKeyDown(Key key) const;

        [[nodiscard]]
        bool IsKeyUp(Key key) const;

        [[nodiscard]]
        bool IsKeyPressed(Key key) const;

        [[nodiscard]]
        bool IsKeyReleased(Key key) const;

    public: // Mapping input functions (Using input names)

        [[nodiscard]]
        bool IsInputDown(MV::string_view input) const;

        [[nodiscard]]
        bool IsInputUp(MV::string_view input) const;

        [[nodiscard]]
        bool IsInputPressed(MV::string_view input) const;

        [[nodiscard]]
        bool IsInputReleased(MV::string_view input) const;

    public: // Gamepad general

        [[nodiscard]]
        bool IsGamepadConnected(uint32_t controllerIndex) const;

        [[nodiscard]]
        bool IsGamepadJoystick(uint32_t controllerIndex) const;

        [[nodiscard]]
        MV::string GetGamepadName(uint32_t controllerIndex) const;

    public: // Gamepad keys

        [[nodiscard]]
        bool IsGamepadKeyDown(uint32_t controllerIndex, GamepadKey key) const;

        [[nodiscard]]
        bool IsGamepadKeyUp(uint32_t controllerIndex,GamepadKey key) const;

        [[nodiscard]]
        bool IsGamepadKeyPressed(uint32_t controllerIndex,GamepadKey key) const;

        [[nodiscard]]
        bool IsGamepadKeyReleased(uint32_t controllerIndex,GamepadKey key) const;

    public: // Gamepad triggers

        [[nodiscard]]
        float GetGamepadTrigger(uint32_t controllerIndex, GamepadTrigger trigger) const;

        [[nodiscard]]
        glm::vec2 GetGamepadJoystick(uint32_t controllerIndex, GamepadJoystick joystick) const;

    private: // Initialization functions

        void init(void* inputContext);

    private: // Update functions

        void pollInputEvents();

        void pollGamepads();

    private: // Internal fields

        void* m_pContext;

    private: // Keyboard fields

        enum class KeyUpdate {
            Stale = 0,
            Released,
            Pressed
        };

        // Contains new key states for all keys, range 512 should cover all keys on keyboard
        MV::array<KeyUpdate,512> m_pKeyUpdates;

    private: // Controller fields

        struct GamepadData {
            enum class GamepadConnectedState {
                Disconnected = 0,
                Connected
            };

            GamepadConnectedState m_State = GamepadConnectedState::Disconnected;
            bool m_IsJoystick = false;
            MV::string m_Name;

            MV::array<KeyUpdate, static_cast<uint32_t>(GamepadKey::COUNT)> m_pKeyUpdates;
            MV::array<bool, static_cast<uint32_t>(GamepadKey::COUNT)> m_pKeyStates;

            MV::array<float, Config::sc_GamepadAxisCount> m_pAxisValues;
        };



        MV::array<GamepadData, Config::sc_MaximumSupportedGamepadCount> m_pControllers;
    };
}


#endif //MV_ENGINE_INPUTCOMMON_H
