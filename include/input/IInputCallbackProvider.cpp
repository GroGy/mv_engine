//
// Created by Matty on 10.12.2022.
//

#include "EngineMinimal.h"
#include "IInputCallbackProvider.h"
#include "InputCommon.h"
#include "GLFW/glfw3.h"

namespace MV {
    void IInputCallbackProvider::OnKeyUpdate(Key key, KeyStateUpdate state, int32_t scancode) {
        ENSURE_VALID(m_pInputObject);

        auto index = static_cast<uint32_t>(key);
        switch (state) {
            case KeyStateUpdate::Released:
                m_pInputObject->m_pKeyUpdates[index] = InputCommon::KeyUpdate::Released;
                break;
            case KeyStateUpdate::Pressed:
                m_pInputObject->m_pKeyUpdates[index] = InputCommon::KeyUpdate::Pressed;
                break;
            case KeyStateUpdate::Repeat:
                break;
        }
    }

    void IInputCallbackProvider::OnJoystickUpdate(int32_t id, bool connected) {
        ENSURE_VALID(m_pInputObject);

        m_pInputObject->m_pControllers[id].m_State = connected
                                                     ? InputCommon::GamepadData::GamepadConnectedState::Connected
                                                     : InputCommon::GamepadData::GamepadConnectedState::Disconnected;

        if(!connected) return;

        auto & name = m_pInputObject->m_pControllers[id].m_Name;
        auto & isJoystick = m_pInputObject->m_pControllers[id].m_IsJoystick;

        isJoystick = !glfwJoystickIsGamepad(id);

        if (isJoystick) {
            name = glfwGetJoystickName(id);
        } else {
            name = glfwGetGamepadName(id);
        }
    }
}
