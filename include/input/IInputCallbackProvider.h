//
// Created by Matty on 10.12.2022.
//

#ifndef MV_EDITOR_IINPUTCALLBACKPROVIDER_H
#define MV_EDITOR_IINPUTCALLBACKPROVIDER_H

#include "Key.h"

namespace MV {
    class InputCommon;

    // Class that extends InputCommon with callbacks, its used mainly for separation of callbacks, to keep input class clean
    class IInputCallbackProvider {
    protected: // Main input common field

        InputCommon * m_pInputObject = nullptr;

    public:

        void OnKeyUpdate(Key key, KeyStateUpdate state, int32_t scancode);

        void OnJoystickUpdate(int32_t id, bool connected);

    };
}


#endif //MV_EDITOR_IINPUTCALLBACKPROVIDER_H
