//
// Created by Matty on 2022-09-08.
//

#ifndef MV_EDITOR_ENGINEMINIMAL_H
#define MV_EDITOR_ENGINEMINIMAL_H

namespace MV {
    // Predeclares here
}

#include <ensure/Ensure.h>
#include <exception>
#include <cstdint>
#include <common/Types.h>
#include <common/Format.h>
#include <vec4.hpp>
#include <vec2.hpp>
#include <vec3.hpp>
#include <json.hpp>

#if MV_ENGINE_DLL
#define MV_API __declspec(dllexport)
#else
#define MV_API
#endif

#if __GNUC__

#define MV_TXN_SAFE_DYN _GLIBCXX_TXN_SAFE_DYN
#define MV_NOTHROW _GLIBCXX_NOTHROW

#else

#define MV_TXN_SAFE_DYN
#define MV_NOTHROW noexcept

#endif

#endif //MV_EDITOR_ENGINEMINIMAL_H
