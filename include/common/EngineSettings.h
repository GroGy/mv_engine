//
// Created by Matty on 2021-10-24.
//

#ifndef MV_ENGINE_ENGINESETTINGS_H
#define MV_ENGINE_ENGINESETTINGS_H

namespace MV {
    struct EngineSettings {
        uint32_t m_FPSLimit = 144;
    };
}

#endif //MV_ENGINE_ENGINESETTINGS_H
