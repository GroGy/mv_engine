//
// Created by Matty on 2022-09-25.
//

#ifndef MV_EDITOR_GRADIENT_H
#define MV_EDITOR_GRADIENT_H

#include "vec4.hpp"
#include "Types.h"

namespace MV {
    enum class GradientType {
        Linear
    };

    class Gradient {
    public:
        explicit Gradient(const glm::vec4 & color);
        explicit Gradient(const glm::vec4 & startColor, const glm::vec4 & endColor);
        void AddColor(float value,const glm::vec4 & color);

        [[nodiscard]]
        const MV::vector<MV::pair<float, glm::vec4>> & GetValues() const {return m_pValues;};
        MV::vector<MV::pair<float, glm::vec4>> & GetValues() {return m_pValues;};

        [[nodiscard]]
        glm::vec4 GetColor(float value) const;
    private:
        GradientType m_pType;
        MV::vector<MV::pair<float, glm::vec4>> m_pValues;
    };
}


#endif //MV_EDITOR_GRADIENT_H
