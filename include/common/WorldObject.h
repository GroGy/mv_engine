//
// Created by Matty on 2022-02-06.
//

#ifndef MV_ENGINE_WORLDOBJECT_H
#define MV_ENGINE_WORLDOBJECT_H

#include <glm.hpp>
#include "../render/common/FrameContext.h"
#include "../scripting/Script.h"
#include <json.hpp>
#include "box2d/b2_body.h"

namespace MV {
    static const MV::string_view SCRIPT_ON_START_NAME = "OnStart";
    static const MV::string_view SCRIPT_ON_END_NAME = "OnEnd";
    static const MV::string_view SCRIPT_UPDATE_NAME = "Update";
    static const MV::string_view SCRIPT_FIXED_UPDATE_NAME = "FixedUpdate";

    class WorldObject {
    public:
        uint64_t m_WorldID;
        MV::string m_Name{};
        MV::string m_Tag{};

        virtual void OnStart();
        virtual void OnEnd();

        virtual void Update(float delta);
        virtual void FixedUpdate(float delta);

        virtual void OnEditorRender();
        virtual void Render(FrameContext & context) {};

        [[nodiscard]]
        Script & GetScript() {return m_pScript;}

        WorldObject(MV::string_view scriptName,ScriptType type = ScriptType::USER);
        [[nodiscard]]
        const glm::vec2 & GetPosition() const {return m_pPosition;};
        [[nodiscard]]
        const glm::vec2 & GetScale() const{return m_pScale;};
        float GetRotation() const {return m_pRotation;};

        void SetRotation(float val);
        void SetScale(const glm::vec2 & val);
        void SetPosition(const glm::vec2 & val);

        // Does not set script fields
        void ForceSetRotation(float val);
        void ForceSetScale(const glm::vec2 & val);
        void ForceSetPosition(const glm::vec2 & val);

        void Init();
        void Cleanup();

        void SyncFromData(const nlohmann::json & data);

        b2Body *& GetPhysicsBody() { return m_PhysicsBody; };

        bool m_IsStatic = false;

        void syncPhysicsState();
    protected:
        Script m_pScript;
        glm::vec2 m_pPosition{0, 0};
        float m_pRotation = 0.0f;
        glm::vec2 m_pScale{1, 1};

        b2Body * m_PhysicsBody = nullptr;
        uint16_t m_pCollisionChannels = 0;
    private:
        void syncPosition();
        void syncRotation();
        void syncScale();

    };
}


#endif //MV_ENGINE_WORLDOBJECT_H
