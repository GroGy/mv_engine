//
// Created by Matty on 2021-11-03.
//

#ifndef MV_ENGINE_CAMERA_H
#define MV_ENGINE_CAMERA_H

#include <glm.hpp>
#include "../../render/common/CameraData.h"

namespace MV {
    static const float VIEW_WIDTH = 16.0f;

    class Camera {
    public:
        CameraData & GetCameraData() { return m_pCameraData;};
        void SetZoom(float zoom);
        void SetPosition(glm::vec2 pos);

        [[nodiscard]]
        float GetZoom() const { return m_pZoom;};

        [[nodiscard]]
        glm::vec2 GetPosition() const { return m_pCameraData.m_Position;};

        void ForcePosition();

        void Update(float deltaTime);

        [[nodiscard]] glm::mat4 GetView() const;
        static glm::mat4 GetView(const glm::vec2 & pos);

        [[nodiscard]] glm::mat4 CalcProj();
        [[nodiscard]] glm::mat4 CalcYInverseProj();

        void SetViewWidth(float val);
        [[nodiscard]] float GetViewWidth() const{ return m_pViewWidth;};

        // Height / Width
        void SetRatio(float ratio);
        [[nodiscard]] float GetRatio() const {return m_pRatio;};
    private:
        CameraData m_pCameraData;
        float m_pZoom = 1.0f;
        bool m_pProjDirty = true;
        float m_pViewWidth = VIEW_WIDTH;
        float m_pRatio =  9.0f / 16.0f; //Default ratio
    };
}


#endif //MV_ENGINE_CAMERA_H
