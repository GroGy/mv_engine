//
// Created by Matty on 2022-03-07.
//

#ifndef MV_EDITOR_FORMAT_H
#define MV_EDITOR_FORMAT_H

#include <fmt/format.h>
#include "EASTL/string.h"
#include "Types.h"
#include "json.hpp"
#include <vec2.hpp>

template<typename Char>
struct fmt::formatter<MV::string, Char> : formatter<std::string, Char> {
    template<typename FormatContext>
    auto format(MV::string const &val, FormatContext &ctx) const
    -> decltype(ctx.out()) {
        return fmt::formatter<std::string, Char>::format(val.c_str(), ctx);
    }
};

template<typename Char>
struct fmt::formatter<MV::string_view, Char> : formatter<std::string_view, Char> {
    template<typename FormatContext>
    auto format(MV::string_view const &val, FormatContext &ctx) const
    -> decltype(ctx.out()) {
        return fmt::formatter<std::string_view, Char>::format(val.data(), ctx);
    }
};

namespace MV {
    using json = nlohmann::json;

    void to_json(json& j, const string& p);
    void from_json(const json& j, string& p);
}

namespace glm {
    using json = nlohmann::json;

    void to_json(json& j, const vec2& p);
    void from_json(const json& j, vec2& p);
}


extern int Vsnprintf8 (char*  pDestination, size_t n, const char*  pFormat, va_list arguments);
extern int Vsnprintf16(char16_t* pDestination, size_t n, const char16_t* pFormat, va_list arguments);
extern int Vsnprintf32(char32_t* pDestination, size_t n, const char32_t* pFormat, va_list arguments);
#if defined(EA_WCHAR_UNIQUE) && EA_WCHAR_UNIQUE
extern int VsnprintfW(wchar_t* pDestination, size_t n, const wchar_t* pFormat, va_list arguments);
#endif

#endif //MV_EDITOR_FORMAT_H
