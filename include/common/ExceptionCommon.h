//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_EXCEPTIONCOMMON_H
#define MV_ENGINE_EXCEPTIONCOMMON_H

#include <EngineMinimal.h>

namespace MV {
    class RuntimeException : public std::exception {
    private:
        MV::string m_pData;
    public:
        explicit RuntimeException(MV::string what);
        [[nodiscard]] const char *what() const noexcept override;
    };
}


#endif //MV_ENGINE_EXCEPTIONCOMMON_H
