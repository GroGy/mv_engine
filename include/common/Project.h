//
// Created by Matty on 2021-10-23.
//

#ifndef MV_ENGINE_PROJECT_H
#define MV_ENGINE_PROJECT_H

#include "resources/common/Asset.h"

static const char *const JSON_NAME = "name";
static const char *const JSON_VERSION = "version";
static const char *const JSON_ASSETS = "assets";
static const char *const JSON_ASSET_TYPE = "type";
static const char *const JSON_ASSET_PATH = "path";
static const char *const JSON_DEFAULT_WORLD = "default_world";

namespace MV {
    struct Project {
        bool m_Valid = false;
        MV::string m_Name{};
        uint32_t m_Version = 0;
        MV::unordered_map<uint64_t, std::pair<AssetType, MV::string>> m_Assets{};
        uint64_t m_DefaultWorld = 0;

        MV::string m_Path{}; //Dont serialize, used at runtime for faster lookup, all assets are located relative to this
        MV::string m_CachePath{}; //Dont serialize, used at runtime for faster lookup, all cached data are there

        MV::string m_GameInstanceClass;
    };
}

#endif //MV_ENGINE_PROJECT_H
