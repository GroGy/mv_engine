//
// Created by Matty on 2021-11-03.
//

#ifndef MV_ENGINE_SCRIPTABLE_H
#define MV_ENGINE_SCRIPTABLE_H

#include "WorldObject.h"

namespace MV {
    struct ScriptableInitData {
        MV::string m_pScriptName;
    };

    class Scriptable : public WorldObject {
    private:
        bool m_pEnabled = true;
    public:
        [[nodiscard]]
        bool Enabled() const { return m_pEnabled; };

        explicit Scriptable(ScriptableInitData && init);

        void Render(FrameContext & context) override;

        friend class World;
    private:
        ScriptableInitData m_pInitData;

        void OnStart() override;

        void OnEnd() override;

        void Update(float delta) override;

        void FixedUpdate(float delta) override;
    };
}


#endif //MV_ENGINE_SCRIPTABLE_H
