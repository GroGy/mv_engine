//
// Created by Matty on 2021-10-28.
//

#ifndef MV_ENGINE_REF_H
#define MV_ENGINE_REF_H

#include "../allocator/AllocatorProvider.h"
#include <EASTL/array.h>
#include <EASTL/string.h>
#include <EASTL/shared_ptr.h>
#include <EASTL/atomic.h>
#include <EASTL/vector.h>
#include <EASTL/queue.h>
#include <EASTL/deque.h>
#include <EASTL/unordered_map.h>
#include <EASTL/map.h>
#include <EASTL/vector_map.h>
#include <EASTL/unordered_set.h>

namespace MV {
    template<typename T>
    using rc = eastl::shared_ptr<T>;

/*
    template<typename T>
    using arc = eastl::shared_ptr<T>;
*/
    template <typename T>
    constexpr typename eastl::remove_reference<T>::type&& move(T&& val){
        return eastl::move(val);
    };

    template<typename T, typename... Args>
    inline rc<T> make_rc(Args&&... args) { return eastl::allocate_shared<T>(MV::AllocatorProvider(), eastl::forward<Args>(args)...); };

    template <typename T, typename U>
    inline rc<T> static_pointer_cast(const rc<U>& sharedPtr) noexcept { return eastl::static_pointer_cast<T>(sharedPtr); }

    template <typename T, typename U>
    inline rc<T> dynamic_pointer_cast(const rc<U>& sharedPtr) noexcept { return eastl::dynamic_pointer_cast<T>(sharedPtr); }

    template<typename T>
    using vector = eastl::vector<T, MV::AllocatorProvider>;

    template<typename K, typename V>
    using map = eastl::map<K, V, eastl::less<K>, MV::AllocatorProvider>;

    template<typename T, size_t N>
    using array = eastl::array<T,N>;

    template<typename T>
    using deque = eastl::deque<T, MV::AllocatorProvider>;

    template<typename T>
    using queue = eastl::queue<T,deque<T>>;

    template<typename K, typename V>
    using unordered_map = eastl::unordered_map<K, V ,eastl::hash<K>, eastl::equal_to<K>, MV::AllocatorProvider>;

    template<typename K,typename V>
    using vector_map = eastl::vector_map<K, V, eastl::less<K>, MV::AllocatorProvider>;

    template<typename K>
    using unordered_set = eastl::unordered_set<K ,eastl::hash<K>, eastl::equal_to<K>, MV::AllocatorProvider>;

    using string = eastl::basic_string<char     ,MV::AllocatorProvider>;
    using wstring = eastl::basic_string<wchar_t ,MV::AllocatorProvider>;

    using string_view = eastl::basic_string_view<char>;
    using wstring_view = eastl::basic_string_view<wchar_t>;

    template <typename T, typename V>
    using pair = eastl::pair<T,V>;

    inline string to_string(int value)
    { return string(string::CtorSprintf(), "%d", value); }
    inline string to_string(long value)
    { return string(string::CtorSprintf(), "%ld", value); }
    inline string to_string(long long value)
    { return string(string::CtorSprintf(), "%lld", value); }
    inline string to_string(unsigned value)
    { return string(string::CtorSprintf(), "%u", value); }
    inline string to_string(uint64_t value)
    { return string(string::CtorSprintf(), "%lu", value); }
    //No support of unsigned long long
    inline string to_string(float value)
    { return string(string::CtorSprintf(), "%f", value); }
    inline string to_string(double value)
    { return string(string::CtorSprintf(), "%f", value); }
    inline string to_string(long double value)
    { return string(string::CtorSprintf(), "%Lf", value); }
}

namespace eastl {
    template<> struct hash<MV::string>
    {
        typedef MV::string                                         string_type;
        typedef typename MV::string::value_type                    value_type;
        typedef typename eastl::add_unsigned<value_type>::type unsigned_value_type;

        size_t operator()(const string_type& s) const
        {
            const unsigned_value_type* p = (const unsigned_value_type*)s.c_str();
            uint32_t c, result = 2166136261U;   // Intentionally uint32_t instead of size_t, so the behavior is the same regardless of size.
            while((c = *p++) != 0)
                result = (result * 16777619) ^ c;
            return (size_t)result;
        }
    };
}
#endif //MV_ENGINE_REF_H
