//
// Created by Matty on 2022-09-20.
//

#ifndef MV_EDITOR_WORLDOBJECTSPAWNPARAMS_H
#define MV_EDITOR_WORLDOBJECTSPAWNPARAMS_H

#include <vec3.hpp>

namespace MV {
    enum class WorldObjectSpawnType {
        Script,
        Prefab,

        Count
    };

    struct WorldObjectSpawnParams {
        MV::string              m_Name;
        glm::vec2               m_SpawnLocation     = {0.0f,0.0f};
        float                   m_SpawnRotation     = 0.0f;
        glm::vec2               m_SpawnScale        = {1.0f,1.0f};
        WorldObjectSpawnType    m_SpawnType         = WorldObjectSpawnType::Script;
        MV::string              m_ScriptName                                        ;
    };
}

#endif //MV_EDITOR_WORLDOBJECTSPAWNPARAMS_H
