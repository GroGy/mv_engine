//
// Created by Matty on 2022-01-23.
//

#ifndef MV_ENGINE_ATLAS_H
#define MV_ENGINE_ATLAS_H

#include "../resources/AtlasData.h"
#include "../render/common/FrameContext.h"
#include "../render/common/Texture.h"

namespace MV {
    class Atlas {
    public:
        explicit Atlas(const AtlasData & data);
        [[nodiscard]] rc<Texture> GetTexture() const;

        [[nodiscard]] const glm::vec4 & GetUVs(uint32_t id) const;
        [[nodiscard]] const glm::vec4 & GetUVOffsets(uint32_t id) const;
        [[nodiscard]] const MV::vector<glm::vec4> & GetUVBuffer() const { return m_pUVs; };
        [[nodiscard]] const MV::vector<MV::string> & GetFrameNames() const { return m_pFrameNames; };

        [[nodiscard]] uint64_t GetTextureAsset() const {return m_pTexture;}
    private:
        uint64_t m_pTexture;
        MV::vector<glm::vec4> m_pUVs;
        MV::vector<glm::vec4> m_pOffsets;
        MV::vector<MV::string> m_pFrameNames;
    };
}


#endif //MV_ENGINE_ATLAS_H
