//
// Created by Matty on 2022-03-07.
//

#ifndef MV_EDITOR_ASSETMANAGER_H
#define MV_EDITOR_ASSETMANAGER_H

#include "../resources/common/Asset.h"

namespace MV {
    class AssetManager {
    public:
        void Init();
        void Cleanup();

        [[nodiscard]]
        size_t GetAssetCount(AssetType type) const;
    private:
        eastl::map<uint64_t, std::pair<AssetType, eastl::string>> m_pAllAssets{};

        eastl::array<eastl::vector<eastl::string>,static_cast<size_t>(AssetType::COUNT)> m_pAssetsFromTypes{};
    };
}


#endif //MV_EDITOR_ASSETMANAGER_H
