//
// Created by Matty on 2022-02-24.
//

#ifndef MV_EDITOR_PROPERTY_H
#define MV_EDITOR_PROPERTY_H

#include "mono/metadata/class.h"
#include "../scripting/Script.h"
#include "WorldObject.h"

namespace MV {
    class Property {
    public:
        virtual ~Property() = default;
        virtual void Init() {};
        virtual void Update(float deltaTime) {};
        virtual void Cleanup() = 0;
        virtual void Serialize(nlohmann::json & woData) {};
        virtual void Deserialize(const nlohmann::json & woData) {};
        virtual void Sync(Script& owner,const ScriptClassField &field) = 0;
        virtual void OnOwnerUpdate() {};

        [[nodiscard]]
        bool IsValid() const { return m_pIsValid; }

        virtual void Destroy() {
            Cleanup();
            m_pIsValid = false;
        }

        MV::rc<WorldObject> m_WorldObject;
    private:
        bool m_pIsValid = true;
    };
}


#endif //MV_EDITOR_PROPERTY_H
