//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_ENGINECONFIG_H
#define MV_ENGINE_ENGINECONFIG_H

#include "../render/common/RendererBackend.h"

namespace MV {
    class EngineConfig {
    public:
        RendererBackend m_RenderBackend = RendererBackend::VULKAN;
        MV::string m_ProjectName = "Empty project";
        uint32_t m_DefaultWindowWidth = 1280;
        uint32_t m_DefaultWindowHeight = 720;
        bool m_WindowMaximized = false;
        bool m_RendererDebugMessages = false;
        bool m_Editor = false;
    };
}

#endif //MV_ENGINE_ENGINECONFIG_H
