//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_ENGINECOMMON_H
#define MV_ENGINE_ENGINECOMMON_H

#include "../Engine.h"
#include "../log/Logger.h"
#include "Types.h"

#define MV_LOG(fmt, ...) MV::GetLogger()->LogDebug(fmt, __VA_ARGS__);

namespace MV {
    class Engine;

    extern MV::Engine * GetEngine();
    extern MV::rc<Renderer> GetRenderer();
    extern MV::Logger * GetLogger();
    extern MV::Buildin * GetBuildins();
    extern MV::LoadedResources * GetResources();

    class EngineCommon {
    public:
        static void SetEngineGlobal(MV::Engine * engine);
        static void SetLoggerGlobal(MV::Logger * logger);
    };
}


#endif //MV_ENGINE_ENGINECOMMON_H
