//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_WORLD_H
#define MV_ENGINE_WORLD_H

#include "camera/Camera.h"
#include "../resources/WorldData.h"
#include "../2d/Tilemap.h"
#include "WorldObject.h"
#include "Scriptable.h"
#include "../render/common/PropertyManager.h"
#include "WorldObjectSpawnParams.h"
#include "2d/RayHitResult.h"

namespace MV {
    class World {
    public:
        World() = default;
        explicit World(WorldData &&resData);

        uint64_t m_ID = 0;
        rc<Camera> m_Camera;
        glm::vec4 m_BackgroundColor = {0.270, 0.278, 0.450,1.0};

        // Contains all world objects that should be added to world object map after tick
        MV::queue<MV::rc<WorldObject>> m_PendingWorldObjects;

        MV::unordered_map<uint64_t, rc<WorldObject>> m_WorldObjects{};
        MV::unordered_map<uint64_t, MV::vector<eastl::pair<MonoClassField*, size_t>>> m_WorldObjectsFields{};

        // Cache for faster lookup of object field, when object is not present here, its bruteforced to find its field, then cached
        MV::unordered_map<MonoObject*, ScriptClassField> m_ObjectFieldCache;

        uint64_t m_WoIDCounter = 0;

        WorldData m_WorldData;
        PropertyManager m_PropertyManager{};

        void OnStart();
        void OnEnd();
        void Update(float delta);
        void FixedUpdate(float delta);

        void Init();
        void Cleanup();
        void Render(FrameContext & context);

        void Reload();

        b2World * GetPhysicsWorld();

        [[nodiscard]] bool IsValid() const {return m_pValid; }

        bool m_ShowColliders = false;
        [[nodiscard]] const glm::uvec2 & GetSize( ) const { return m_pSize;}

        MV::vector<rc<WorldObject>> GetWorldObjectsByTag(MV::string_view tag);

        void MarkWOForSync(uint64_t id);
        void SyncDirtyWorldObjects();
        void SyncAllWorldObjects();

        rc<WorldObject> SpawnWorldObject(const WorldObjectSpawnParams& spawnParams);

        RayHitResult CastRay(const glm::vec2 & start, const glm::vec2 & dir, float len);
    private:
        void loadFromData(const WorldData & data);
        void loadFromData();

        glm::uvec2 m_pSize;
        bool m_pValid = false;
        void renderPhysicsOverlay(FrameContext & context);
        PhysicsDebugDrawer * GetDebugDraw() { return &m_pDebugDrawer; };
        PhysicsDebugDrawer m_pDebugDrawer{};
        b2World * m_pPhysicsWorld = nullptr;

        MV::unordered_set<uint64_t> m_pWorldObjectsToSync{};

        void syncWorldObjectProperties(const nlohmann::json &data,const rc<WorldObject> & wo);
    };
}


#endif //MV_ENGINE_WORLD_H
