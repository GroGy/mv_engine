//
// Created by Matty on 2021-11-03.
//

#ifndef MV_ENGINE_BUILDIN_H
#define MV_ENGINE_BUILDIN_H

#include <render/MissingTexture.h>
#include "../2d/TilemapPipeline.h"
#include "../2d/PhysicsDebugPipeline.h"
#include "particles/cpu/ParticlePipeline.h"

namespace MV {
    class Buildin {
    public:
        MV::TilemapPipeline m_TilemapPipeline;
        MV::PhysicsDebugPipeline m_PhysicsDebugPipeline;
        MV::ParticlePipeline m_ParticlePipeline;
        MV::MissingTexture m_MissingTexture;
    };
}


#endif //MV_ENGINE_BUILDIN_H
