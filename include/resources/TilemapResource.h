//
// Created by Matty on 08.12.2022.
//

#ifndef MV_EDITOR_TILEMAPRESOURCE_H
#define MV_EDITOR_TILEMAPRESOURCE_H

#include "common/Resource.h"
#include "TilemapData.h"

namespace MV {
    class TilemapResource : public Resource {
    public:
        explicit TilemapResource(MV::string path, uint64_t id);

        TilemapData m_Result{};

    private: // Helper functions

    private:

        uint64_t m_pID = 0;

        ResourceLoadResult process() override;

        void finish() override;

    };
}


#endif //MV_EDITOR_TILEMAPRESOURCE_H
