//
// Created by Matty on 2022-03-13.
//

#ifndef MV_EDITOR_SHADERDATA_H
#define MV_EDITOR_SHADERDATA_H

#include <json.hpp>
#include "../common/Types.h"

namespace MV {
    namespace Config {
        static constexpr MV::string_view sc_ShaderJsonInputBuffersField = "input_buffers";

        static constexpr MV::string_view sc_ShaderJsonBindingsField = "binding";
        static constexpr MV::string_view sc_ShaderJsonRateField = "rate";
        static constexpr MV::string_view sc_ShaderJsonStrideField = "stride";


        static constexpr MV::string_view sc_ShaderJsonVertexLayoutField = "layout";

        static constexpr MV::string_view sc_ShaderJsonLayoutLocationField = "location";
        static constexpr MV::string_view sc_ShaderJsonLayoutOffsetField = "offset";
        static constexpr MV::string_view sc_ShaderJsonLayoutTypeField = "type";
        static constexpr MV::string_view sc_ShaderJsonLayoutNameField = "name";


        static constexpr MV::string_view sc_ShaderJsonUniformsField = "uniforms";

        static constexpr MV::string_view sc_ShaderJsonUniformShaderStageField = "shader_stage";
        static constexpr MV::string_view sc_ShaderJsonUniformBindingField = "binding";
        static constexpr MV::string_view sc_ShaderJsonUniformArraySizeField = "array_size";
        static constexpr MV::string_view sc_ShaderJsonUniformOffsetField = "offset";
        static constexpr MV::string_view sc_ShaderJsonUniformSizeField = "size";
        static constexpr MV::string_view sc_ShaderJsonUniformTypeField = "type";
        static constexpr MV::string_view sc_ShaderJsonUniformNameField = "name";
        static constexpr MV::string_view sc_ShaderJsonUniformNullableField = "nullable";


        static constexpr MV::string_view sc_ShaderJsonPushConstantsField = "push_constants";

        static constexpr MV::string_view sc_ShaderJsonPushConstantNameField = "name";
        static constexpr MV::string_view sc_ShaderJsonPushConstantOffsetField = "offset";
        static constexpr MV::string_view sc_ShaderJsonPushConstantSizeField = "size";
        static constexpr MV::string_view sc_ShaderJsonPushConstantShaderStageField = "shader_stage";
    }

    struct ShaderDataUniform {
        uint32_t m_ShaderStage = 0;
        uint32_t m_Binding = 0;
        uint32_t m_DescriptorCount = 1;
        uint32_t m_Offset = 0;
        uint32_t m_Size = 0;
        uint32_t m_Type = 0;
        MV::string m_Name;
        bool     m_Nullable = false;
    };

    struct ShaderDataPushConstant {
        uint32_t m_ShaderStage = 0;
        uint32_t m_Offset = 0;
        uint32_t m_Size = 0;
        MV::string m_Name;
    };

    struct ShaderDataBufferLayoutAttribute {
        uint32_t m_Type = 0;
        uint32_t m_Location = 0;
        uint32_t m_Offset = 0;
        MV::string m_Name;
    };

    struct ShaderDataBuffer {
        uint32_t m_Binding = 0;
        uint8_t m_Rate = 0; //Per vertex / Per instance
        uint32_t m_Stride = 0;

        MV::vector<ShaderDataBufferLayoutAttribute> m_Attributes{};
    };

    class ShaderData {
    public:
        nlohmann::json m_RawData;

        // glsl code
        MV::string m_Vertex;
        MV::string m_Fragment;

        MV::string m_VertexPath;
        MV::string m_FragmentPath;

        MV::vector<ShaderDataBuffer> m_DataBuffers{};
        MV::vector<ShaderDataUniform> m_Uniforms{};
        MV::vector<ShaderDataPushConstant> m_PushConstants{};

        bool m_EnableAlphaBlending;
    };
}

#endif //MV_EDITOR_SHADERDATA_H
