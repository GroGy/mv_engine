//
// Created by Matty on 2021-11-03.
//

#ifndef MV_ENGINE_TILESETRESOURCE_H
#define MV_ENGINE_TILESETRESOURCE_H

#include "common/Resource.h"
#include "TilesetData.h"

namespace MV {
    class TilesetResource : public Resource {
    public:
        explicit TilesetResource(MV::string path, uint64_t id);

        TilesetData m_Result{};

    private: // Helper functions



    private:
        uint64_t m_pID = 0;
        ResourceLoadResult process() override;

        void finish() override;
    };
}


#endif //MV_ENGINE_TILESETRESOURCE_H
