//
// Created by Matty on 2021-10-23.
//

#ifndef MV_ENGINE_PROJECTRESOURCE_H
#define MV_ENGINE_PROJECTRESOURCE_H

#include "common/Resource.h"
#include "../common/Project.h"

namespace MV {
    namespace Config {
        static constexpr MV::string_view sc_ProjectCacheDirectory = ".mv_cache";
    }

    class ProjectResource : public Resource {
    public:
        ProjectResource(MV::string path);
    private:
        ResourceLoadResult process() override;

        void finish() override;

        Project m_Result;
    private:

    };
}


#endif //MV_ENGINE_PROJECTRESOURCE_H
