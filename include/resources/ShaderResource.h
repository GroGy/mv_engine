//
// Created by Matty on 2022-03-13.
//

#ifndef MV_EDITOR_SHADERRESOURCE_H
#define MV_EDITOR_SHADERRESOURCE_H

#include "common/Resource.h"
#include "ShaderData.h"

namespace MV {
    class ShaderResource : public Resource {
    public:
        explicit ShaderResource(MV::string path, uint64_t id);
        ShaderData m_Result;
    private:
        uint64_t m_pID = 0;
        ResourceLoadResult process() override;

        void finish() override;
    };
}


#endif //MV_EDITOR_SHADERRESOURCE_H
