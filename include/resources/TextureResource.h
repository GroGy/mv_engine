//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_TEXTURERESOURCE_H
#define MV_ENGINE_TEXTURERESOURCE_H

#include "common/Resource.h"
#include "TextureData.h"

namespace MV {
    class TextureResource : public Resource {
    public:
        explicit TextureResource(MV::string path, uint64_t id);
        explicit TextureResource(MV::vector<uint8_t> rawData, uint64_t id);

        TextureData m_Result{};
        // Can be set to instance of object where result will be saved on finish
        void* m_SaveResult = nullptr;
    private:
        uint64_t m_pID = 0;
        ResourceLoadResult process() override;

        void finish() override;
    };
}


#endif //MV_ENGINE_TEXTURERESOURCE_H
