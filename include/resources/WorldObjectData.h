//
// Created by Matty on 2022-02-08.
//

#ifndef TEST_GAME_WORLDOBJECTDATA_H
#define TEST_GAME_WORLDOBJECTDATA_H

#include <EngineMinimal.h>
#include <glm.hpp>
#include <json.hpp>

namespace MV {
    namespace Config {
        static constexpr MV::string_view sc_JsonWorldIDField = "id";
        // Json array of 2 values, x,y
        static constexpr MV::string_view sc_JsonPositionField = "position";
        // Json array of 2 values, x,y
        static constexpr MV::string_view sc_JsonScaleField = "scale";
        static constexpr MV::string_view sc_JsonRotationField = "rotation";
        static constexpr MV::string_view sc_JsonWONameField = "name";
        static constexpr MV::string_view sc_JsonConfigField = "config";
        static constexpr MV::string_view sc_JsonStaticField = "static";

        static constexpr MV::string_view sc_JsonDataField = "data";
        static constexpr MV::string_view sc_JsonScriptField = "script";
    }

    struct WorldObjectData {
        uint64_t m_WorldID;
        glm::vec2 m_Position;
        float m_Rotation;
        glm::vec2 m_Scale;
        MV::string m_Name;
        bool m_Static;

        /// If true, this is whole config else its script config with override values
        bool m_IsScriptConfig;
        uint64_t m_ConfigAsset;

        /// Data of world object, meaning values assigned to its fields
        nlohmann::json m_Data = nlohmann::json::object();
        // Name of script this data is for
        MV::string m_Script;
    };
}

#endif //TEST_GAME_WORLDOBJECTDATA_H
