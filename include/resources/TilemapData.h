//
// Created by Matty on 08.12.2022.
//

#ifndef MV_EDITOR_TILEMAPDATA_H
#define MV_EDITOR_TILEMAPDATA_H

#include <EngineMinimal.h>

namespace MV {
    struct WorldTile {
        uint16_t m_AtlasIndex;
    };

    struct TilesetCollision {
        MV::vector<glm::vec2> m_Vertices;
    };

    struct TilemapData {
        uint64_t m_Tileset = 0;
        MV::vector<WorldTile> m_Data{};
        uint32_t m_Width, m_Height;
        bool m_HasCollisionData;
        TilesetCollision m_Collision;
    };
}


#endif //MV_EDITOR_TILEMAPDATA_H
