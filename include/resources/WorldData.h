//
// Created by Matty on 2021-11-03.
//

#ifndef MV_ENGINE_WORLDDATA_H
#define MV_ENGINE_WORLDDATA_H

#include "WorldObjectData.h"
#include <unordered_map>

namespace MV {
    namespace Config {
        // Name of size field in json, size is stored as 1D array of two values, size on X and Y
        static constexpr MV::string_view sc_JsonSizeField = "size";
        static constexpr MV::string_view sc_JsonGravityField = "gravity";
        static constexpr MV::string_view sc_JsonNameField = "name";
        static constexpr MV::string_view sc_JsonWorldObjectsField = "world_objects";
    }

    struct TileLayerData {
        uint64_t m_Tileset;
        float m_Brightness = 1.0f;
        MV::vector<uint32_t> m_Data;
    };

    // World data before world is created from them
    struct WorldData {
        MV::string m_Name;
        uint32_t m_Width = 0;
        uint32_t m_Height = 0;
        float m_Gravity = -9.8f;
        MV::unordered_map<uint64_t,WorldObjectData> m_WorldObjects{};
    };
}


#endif //MV_ENGINE_WORLDDATA_H
