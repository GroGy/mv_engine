//
// Created by Matty on 2021-10-31.
//

#ifndef MV_ENGINE_ATLASRESOURCE_H
#define MV_ENGINE_ATLASRESOURCE_H

#include "common/Resource.h"
#include "AtlasData.h"

namespace MV {
    class AtlasResource : public Resource {
    public:
        explicit AtlasResource(MV::string path, uint64_t id);

        AtlasData m_Result{};
    private:
        uint64_t m_pID = 0;
        ResourceLoadResult process() override;

        void finish() override;
    };
}


#endif //MV_ENGINE_ATLASRESOURCE_H
