//
// Created by Matty on 2021-11-03.
//

#ifndef MV_ENGINE_LOADEDRESOURCES_H
#define MV_ENGINE_LOADEDRESOURCES_H

#include "../common/Atlas.h"
#include "../2d/TileSet.h"
#include "TilemapData.h"

namespace MV {
    class LoadedResources {
    public:
        MV::unordered_map<uint64_t, rc<Tileset>> m_Tilesets{};
        MV::unordered_map<uint64_t, rc<Atlas>> m_Atlases{};
        MV::unordered_map<uint64_t, MV::TilemapData> m_Tilemaps{};
    };
}


#endif //MV_ENGINE_LOADEDRESOURCES_H
