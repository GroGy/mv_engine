//
// Created by Matty on 2021-11-01.
//

#ifndef MV_ENGINE_WORLDRESOURCE_H
#define MV_ENGINE_WORLDRESOURCE_H

#include "common/Resource.h"
#include "WorldData.h"

namespace MV {
    class WorldResource : public Resource {
    public:
        explicit WorldResource(MV::string path, uint64_t id);

        WorldData m_Result{};
    private:
        uint64_t m_pID = 0;
        ResourceLoadResult process() override;

        void finish() override;
    };
}

#endif //MV_ENGINE_WORLDRESOURCE_H
