//
// Created by Matty on 2021-11-03.
//

#ifndef MV_ENGINE_TILESETDATA_H
#define MV_ENGINE_TILESETDATA_H

#include <EngineMinimal.h>

namespace MV {
    namespace Config {
        static constexpr MV::string_view sc_AtlasJsonFieldName = "atlas";
        static constexpr MV::string_view sc_TilesJsonFieldName = "tiles";
        static constexpr MV::string_view sc_TileCollisionJsonFieldName = "collision";
        static constexpr MV::string_view sc_SpriteJsonFieldName = "sprite";
    }

    struct TileData {
        uint16_t m_SpriteIndex;
        // Max amount of vertices per tile is 255 (one byte)
        MV::vector<glm::vec2> m_Collision;
    };

    struct TilesetData {
        nlohmann::json m_RawData;
        uint64_t m_Atlas = 0;
        MV::vector<TileData> m_Tiles{};
    };
}


#endif //MV_ENGINE_TILESETDATA_H
