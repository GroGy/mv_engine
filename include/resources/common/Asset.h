//
// Created by Matty on 2021-10-19.
//

#ifndef MV_ENGINE_ASSET_H
#define MV_ENGINE_ASSET_H

#include <EngineMinimal.h>

namespace MV {
    namespace Config {
        static constexpr MV::string_view sc_AssetExtension = ".mvasset";
        static constexpr MV::string_view sc_ScriptExtension = ".cs";
    }

    enum class AssetType : uint32_t {
        SHADER = 0,
        TEXTURE = 1,
        MODEL = 2,
        PROJECT = 3,
        ATLAS = 4,
        WORLD = 5,
        TILESET = 6,
        SCRIPT = 7,
        SCRIPT_CONFIG = 8,
        SCRIPT_CONFIG_ADDITIONAL_DATA = 9,
        BINARY_DATA = 10,
        ANIMATION = 11,
        TILEMAP = 12,

        COUNT
    };

    template<AssetType>
    struct AssetName {
        static constexpr MV::string_view ToString() {
            return "<MISSING>";
        }
    };


    template<>
    struct AssetName<AssetType::SHADER> {
        static constexpr MV::string_view ToString() {
            return "Shader";
        }
    };

    static constexpr MV::string_view AssetTypeToString(AssetType type) {
        switch (type) {
            case AssetType::SHADER:
                return "Shader";
            case AssetType::TEXTURE:
                return "Texture";
            case AssetType::MODEL:
                return "Model";
            case AssetType::PROJECT:
                return "Project";
            case AssetType::ATLAS:
                return "Atlas";
            case AssetType::WORLD:
                return "World";
            case AssetType::TILESET:
                return "Tileset";
            case AssetType::SCRIPT:
                return "Script";
            case AssetType::SCRIPT_CONFIG:
                return "Script Config";
            case AssetType::SCRIPT_CONFIG_ADDITIONAL_DATA:
                return "Script Config Extra data";
            case AssetType::BINARY_DATA:
                return "BLOB";
            case AssetType::COUNT:
                return "<missing>";
            case AssetType::ANIMATION:
                return "Animation";
            case AssetType::TILEMAP:
                return "Tilemap";
        }
        return "<missing>";
    }

    class Asset {
    private:

    public:

    };
}


#endif //MV_ENGINE_ASSET_H
