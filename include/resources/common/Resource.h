//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_RESOURCE_H
#define MV_ENGINE_RESOURCE_H

#include <fstream>
#include "Asset.h"

namespace MV {
    enum class ResourceLoadResult {
        SUCCESS,
        FILE_NOT_FOUND,
        FILE_READ_ERROR,
        FAILED
    };

    struct ResourceHeader {
        uint32_t m_Version;
        AssetType m_Type;
    };

    class Resource {
    public:
        [[nodiscard]]
        bool IsFinished() const { return m_pFinished;};

        [[nodiscard]]
        bool Success() const { return m_pSuccess;};

        [[nodiscard]]
        MV::string_view Error() const { return m_pError;};

        ResourceLoadResult Process();

        friend class Engine;
        friend class ResourceLoader;
        static MV::string PathProjectRelativeToAbsolute(MV::string_view pathRel);
    protected:
        explicit Resource(MV::string path) : m_pPath(MV::move(path)) {};
        explicit Resource(MV::vector<uint8_t> data) : m_pRawData(MV::move(data)) {};

        MV::string m_pPath;

        MV::vector<uint8_t> m_pRawData;

        ResourceHeader ReadAssetHeader(std::ifstream & input);

        bool m_pSuccess = false;

        MV::string m_pError;
    private:
        bool m_pFinished = false;

        virtual ResourceLoadResult process() = 0;

        virtual void finish() = 0;
    };
}


#endif //MV_ENGINE_RESOURCE_H
