//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_RESOURCELOADER_H
#define MV_ENGINE_RESOURCELOADER_H

#include <thread>
#include "../../util/thread_safe_queue.h"
#include "Resource.h"
#include "../../common/Types.h"

typedef MV::rc<MV::thread_safe_queue<MV::rc<MV::Resource>>> ResourceQueue;

namespace MV {
    class ResourceLoader {
    private:
        ResourceQueue m_pResourceQueue{};
        void processLoop();
        ResourceQueue m_pFinishedResourcesQueue{};
        std::thread m_pWorkerThread;
        bool m_pValid = false;
        bool m_pRun = true;
    public:
        ResourceLoader() = default;
        ResourceLoader(ResourceQueue resourceQueue, ResourceQueue finishedQueue);
        void Init();
        void Cleanup();
    };
}


#endif //MV_ENGINE_RESOURCELOADER_H
