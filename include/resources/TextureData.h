//
// Created by Matty on 2021-10-19.
//

#ifndef MV_ENGINE_TEXTUREDATA_H
#define MV_ENGINE_TEXTUREDATA_H

namespace MV {
    struct TextureData {
        uint32_t m_Width = 0;
        uint32_t m_Height = 0;
        uint32_t m_TextureChannels = 0;
        size_t m_DataSize = 0;
        MV::vector<uint8_t> m_Data{};
    };
}

#endif //MV_ENGINE_TEXTUREDATA_H
