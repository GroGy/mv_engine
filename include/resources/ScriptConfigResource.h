//
// Created by Matty on 2022-02-19.
//

#ifndef MV_EDITOR_SCRIPTCONFIGRESOURCE_H
#define MV_EDITOR_SCRIPTCONFIGRESOURCE_H

#include "common/Resource.h"
#include "../common/ScriptableConfig.h"

namespace MV {
    class ScriptConfigResource : public Resource {
    public:
        ScriptConfigResource(MV::string path);
    private:
        ResourceLoadResult process() override;

        void finish() override;

        ScriptableConfig m_Result;
    private:

    };
}


#endif //MV_EDITOR_SCRIPTCONFIGRESOURCE_H
