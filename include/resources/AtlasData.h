//
// Created by Matty on 2021-10-31.
//

#ifndef MV_ENGINE_ATLASDATA_H
#define MV_ENGINE_ATLASDATA_H

#include <EngineMinimal.h>

namespace MV {
    struct AtlasData {
        uint64_t m_Texture;
        MV::vector<glm::vec4> m_UVs;
        MV::vector<MV::string> m_FrameNames;
        MV::vector<glm::vec4> m_UVOffsets;
        MV::vector<float> m_Timings;
    };
}

#endif //MV_ENGINE_ATLASDATA_H
