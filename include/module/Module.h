//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_MODULE_H
#define MV_ENGINE_MODULE_H

#include "../render/common/FrameContext.h"

namespace MV {
    class Module {
    public:
        /// Called before renderer is initialized
        virtual bool OnInit() = 0;

        /// Called after renderer is initialized
        virtual bool OnPostInit() = 0;

        /// Called on every update tick
        virtual void OnUpdate(float deltaTime) { };

        /// Called on every update tick
        virtual void OnFixedUpdate(float deltaTime) {};

        /// Called on every render frame
        virtual void OnRender(FrameContext & context) = 0;

        virtual void OnEditorRender(FrameContext & context) {};

        /// Called on exit
        virtual bool OnCleanup() = 0;

        /// Called when renderer is cleaning up
        virtual bool OnRenderCleanup() = 0;

        virtual MV::string GetModuleName() const = 0;

        virtual void OnWindowResize() { };
    };
}


#endif //MV_ENGINE_MODULE_H
