//
// Created by Matty on 2022-09-25.
//

#ifndef MV_ENGINE_ASYNCWORKER_H
#define MV_ENGINE_ASYNCWORKER_H

#include "AsyncTask.h"

namespace MV {
    class AsyncWorker {
    public:
        AsyncWorker();

        void Init();

        void Cleanup();

        [[nodiscard]]
        bool DoingCriticalTask() const { return m_pCritical; };

    public: // Tasks

        [[nodiscard]]
        bool CanDispatch() const;

        void DispatchTask(const MV::rc<AsyncTask> & task, bool frameCritical = false);

        void Await();

    private: // Internal functions for async task processing

        void threadMain();

        void processTask();

    private:
        bool m_pCritical = false;

        std::thread::id m_pThreadID;

        std::thread m_pThread;

        MV::rc<AsyncTask> m_pCurrentTask;

        volatile std::atomic_uint32_t m_pProcessedTask = 0;

        volatile std::atomic_uint32_t m_pRequestedTask = 0;

        std::condition_variable m_pSleepingCV;

        volatile bool m_pStop = false;

        bool m_pFinished = false;
    };
}


#endif //MV_ENGINE_ASYNCWORKER_H
