//
// Created by Matty on 2022-03-06.
//

#ifndef MV_EDITOR_THREADPOOL_H
#define MV_EDITOR_THREADPOOL_H

#include "../platform/Platform.h"
#include "AsyncWorker.h"
#include "common/Types.h"

#include <async/AsyncTask.h>

namespace MV {
    class ThreadPool {
    public:
        ThreadPool(size_t workerCount = 4);

        void Init();

        void Cleanup();

        template <typename T, typename... Args>
        inline MV::rc<AsyncTaskPromise<T>> DispatchTask(Args&&... args);

        template <typename T, typename... Args>
        inline MV::rc<AsyncTaskPromise<T>> DispatchSingleFrameTask(Args&&... args);

        void AwaitCriticalTasks();

        void Update();

    private: // Helper functions

        void notifyWorkers(MV::queue<MV::rc<AsyncTask>> & queue);

        MV::rc<AsyncTaskPromise<AsyncTask>> makePromise(const MV::rc<AsyncTask> & task);

    private:
        MV::vector<MV::rc<AsyncWorker>> m_pWorkers;

        uint32_t m_pWorkerRotationIndex = 0;

        MV::queue<MV::rc<AsyncTask>> m_pTasks;

        MV::queue<MV::rc<AsyncTask>> m_pCriticalTasks;
    };

    template<typename T, typename... Args>
    MV::rc<AsyncTaskPromise<T>> ThreadPool::DispatchTask(Args&&... args) {
        static_assert(std::is_base_of_v<MV::AsyncTask, T>);

        auto res = MV::make_rc<T>(eastl::forward<Args>(args)...);

        auto promise = makePromise(res);

        m_pTasks.emplace(res);

        return eastl::reinterpret_pointer_cast<AsyncTaskPromise<T>>(promise);
    }


    template<typename T, typename... Args>
    MV::rc<AsyncTaskPromise<T>> ThreadPool::DispatchSingleFrameTask(Args &&... args) {
        static_assert(std::is_base_of_v<MV::AsyncTask, T>);

        auto res = MV::make_rc<T>(eastl::forward<Args>(args)...);

        auto promise = makePromise(res);

        m_pCriticalTasks.emplace(res);

        notifyWorkers(m_pCriticalTasks);

        return eastl::reinterpret_pointer_cast<AsyncTaskPromise<T>>(promise);
    }
}


#endif //MV_EDITOR_THREADPOOL_H
