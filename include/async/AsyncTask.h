//
// Created by Matty on 2022-09-25.
//

#ifndef MV_ENGINE_ASYNCTASK_H
#define MV_ENGINE_ASYNCTASK_H

#include <EngineMinimal.h>

namespace MV {
    class AsyncTask {
    public:
        friend class AsyncWorker;

        virtual void Work() = 0;

        virtual const char * GetName() = 0;

        [[nodiscard]]
        bool IsDone() const { return m_pDone;}
    private:

        bool m_pDone = false;

    };

    template <typename T>
    class AsyncTaskPromise {
    public:
        friend class ThreadPool;

        [[nodiscard]]
        bool IsDone() const {
            return m_pResult->IsDone();
        }

        const MV::rc<T> & GetResult() {
            ENSURE_TRUE(IsDone(), m_pResult);

            return m_pResult;
        }

    private:
        AsyncTaskPromise(const MV::rc<T> & result) {
            static_assert(std::is_base_of_v<AsyncTask, T>);
            m_pResult = result;
        }

        MV::rc<T> m_pResult;
    };
}


#endif //MV_ENGINE_ASYNCTASK_H
