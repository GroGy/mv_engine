//
// Created by Matty on 07.12.2022.
//

#ifndef MV_EDITOR_ASSEMBLYENTRY_H
#define MV_EDITOR_ASSEMBLYENTRY_H

#include "EngineMinimal.h"

namespace MV {
    enum class AssemblyEntryVisibility {
        Public,
        Private,
        Protected
    };

    struct AssemblyClassRef {
        MV::string m_Name;
        uint32_t m_Token;
        bool m_Valid = false;
    };

    struct AssemblyFieldEntry {
        AssemblyEntryVisibility m_Visibility;
        MV::string m_Name;
        AssemblyClassRef m_Type;
    };

    struct AssemblyFunctionParamEntry {
        MV::string m_Name;
        AssemblyClassRef m_Type;

        bool m_Ref;
    };

    struct AssemblyFunctionEntry {
        bool m_IsStatic;
        MV::string m_Name;
        AssemblyEntryVisibility m_Visibility;
        AssemblyClassRef m_ReturnType;
        MV::vector<AssemblyFunctionParamEntry> m_Params;
    };

    struct AssemblyEnumEntry {
        MV::string m_Name;
        MV::vector<MV::string> m_Values;
    };

    struct AssemblyClassEntry {
        MV::string m_Name;
        bool m_Internal;
        AssemblyClassRef m_Parent;
        uint32_t m_Token;

        MV::vector<AssemblyFunctionEntry> m_Functions;
        MV::vector<AssemblyFieldEntry> m_Fields;
    };

    struct AssemblyInfo {
        MV::vector<AssemblyClassEntry> m_Classes;
        MV::vector<AssemblyEnumEntry> m_Enums;
    };
}

#endif //MV_EDITOR_ASSEMBLYENTRY_H
