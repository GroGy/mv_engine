//
// Created by Matty on 2022-02-06.
//

#ifndef MV_ENGINE_SCRIPTMANAGER_H
#define MV_ENGINE_SCRIPTMANAGER_H

#include <EngineMinimal.h>
#include <mono/jit/jit.h>
#include <queue>
#include "Script.h"
#include "../common/Types.h"
#include "ScriptClass.h"
#include "AssemblyEntry.h"
#include <functional>

namespace MV {
    using LogCallback = eastl::function<void(MV::string_view, int32_t, MV::string_view)>;

    struct ScriptManagerInit {
        MV::string m_Name;
        MV::string m_AssemblyPath;
    };

    class ScriptManager {
    public: // General init functions and global instance

        static ScriptManager *s_GlobalInstance;

        ScriptManager();

        void Init(ScriptManagerInit initData);

        void Start();

        void End();

        void Cleanup();

    public: // Setters and getters

        void SetUserNamespace(MV::string_view name);

        void SetUserAssembly(MV::string_view path, bool init = false);

        void SetLogCallback(const LogCallback &callback);

        inline uint32_t GetDomainVersion() const { return m_pDomainVersion; };

    public: // Loading / Reloading of assemblies

        void Reload();

        void Load();

    public: // Update loop callback, for GC

        void Update();

    public: // Class functions

        ScriptClass GetClass(MV::string_view name);

        uint32_t GetClassToken(MV::string_view className);

        // Registers class as valid class token, valid class token means, that class has implemented custom property editing in editor
        void RegisterValidClassToken(uint32_t token);

        void RegisterValidClassToken(MV::string_view className);

        bool IsClassTokenValid(uint32_t token) const;

        MonoString* CreateMonoString(MV::string_view data);

    public: // Callback functions for engine updates, returns true ONCE

        bool OnScriptsReload();

    public: // Callbacks called from scripting engine

        void Native_ScriptLog(MonoString *message, int32_t line, MonoString *file);

        MonoArray *Native_GetWOByTag(MonoString *tag);

        MonoObject *Native_SpawnWorldObject(const char *name, const glm::vec2 &position);

    public: // Friend classes

        friend class ScriptClass;

        friend class Script;

        friend class ScriptObjectInstance;

    public: // Assembly inspection functions

        // Gets info about assembly, this is expensive and should be used only when assembly changes
        MV::AssemblyInfo GetAssemblyInfo();

        MV::vector<ScriptClass> GetClassesThatAreChildOfClass(const ScriptClass & Klass);

    private: // Internal init functions

        void initDomain();

        void initAppDomain();

        void initEngineAssembly();

        void initUserAssembly();

        void initCoreFunctions();

        void initCore();

    private: // Assembly info helper functions

        void addEnumToAssemblyInfo(MonoClass* klass,AssemblyInfo &info);

        void addClassToAssemblyInfo(MonoClass* klass,AssemblyInfo &info);

    private: // Helper functions

        void unloadAppDomain();

        void registerGCHandle(uint32_t handle);

        ScriptClass &cacheClass(MV::string_view name, MonoClass *klass);

    private: // Scripting backend fields

        MonoDomain *m_pMonoDomain = nullptr;

        MonoDomain *m_pMonoAppDomain = nullptr;

        MonoAssembly *m_pEngineAssembly = nullptr;

        MonoImage *m_pEngineAssemblyImage = nullptr;

        MonoAssembly *m_pUserAssembly = nullptr;

        MonoImage *m_pUserAssemblyImage = nullptr;

    private: // Internal fields

        ScriptManagerInit m_pInitData;

        LogCallback m_pLogCallback;

        uint32_t m_pDomainVersion = 1;

        // Cache for all classes in script engine, used for fast lookup
        MV::unordered_map<MV::string, ScriptClass> m_pClassCache{};

        // Contains all classes in user assembly, useful for checking, if class is from user assembly
        MV::unordered_set<MV::string> m_pUserClasses{};

        MV::unordered_set<uint32_t> m_pValidClassTokens{};

        // Garbage collection handles, if dropped, all objects are dropped
        MV::unordered_set<uint32_t> m_pGCHandles{};

        // Valid property names, invalid names are shown as not supported
        MV::vector<MV::string> m_pValidProperties{};

        bool m_pOnScriptsReload = false;
    };
}


#endif //MV_ENGINE_SCRIPTMANAGER_H
