//
// Created by Matty on 2022-02-06.
//

#ifndef MV_ENGINE_SCRIPT_H
#define MV_ENGINE_SCRIPT_H

#include "ScriptClass.h"
#include "ensure/Ensure.h"
#include <glm.hpp>
#include <string>
#include <string_view>

namespace MV {
    enum class ScriptType {
        USER,
        ENGINE
    };

    //Some methods cant be const because they update class description on script reload, in theory at runtime this cant happen, be since we allow hotreloading in engine itself, we have to keep it nonconst

    class Script {
    public:
        [[nodiscard]]
        bool AckDestroyed() const { return m_pDestroyedAck; }

        [[nodiscard]]
        bool Destroyed() const { return m_pDestroyed; }

        void Destroy();

        void AckDestruction() { m_pDestroyedAck = true; };

        [[nodiscard]]
        MV::string GetName() const { return m_pName; }

        void Init();

        void CreateInstance();

        [[nodiscard]]
        bool IsInstanceValid() const;

        explicit Script(MV::string_view className, ScriptType type = ScriptType::USER);

        void CallFunction_Void(MV::string_view name);
        void CallFunction_Void_Float(MV::string_view name, float val);

        template <typename T>
        inline void SetField(MV::string_view field, const T & val);

        template <typename T>
        inline void SetField(const ScriptClassField & field, const T & val);

        void SetFieldRaw(MV::string_view fieldName, void * val);

        friend class ScriptManager;

        const MV::unordered_map<MV::string,ScriptClassField> & GetFields() const;

        template <typename T>
        inline void GetFieldValue(T* value, const ScriptClassField & field);

        Script GetFieldScript(const ScriptClassField & field);

        const ScriptObjectInstance & GetObjectInstance() const;

        void SetInstance(ScriptObjectInstance &&instance);

    private:
        bool m_pDestroyed = false;
        bool m_pDestroyedAck = false;
        MV::string m_pName;
        ScriptClass m_pClass;
        uint32_t m_pClassVersion = 0;
        ScriptObjectInstance m_pInstance;
        ScriptType m_pType;
    };


    template<typename T>
    inline void Script::GetFieldValue(T *value, const ScriptClassField &field) {
        ENSURE_TRUE(m_pInstance.IsValid(),);
        m_pClass.GetFieldValue(value,m_pInstance, field);
    }

    template <typename T>
    inline void Script::SetField(MV::string_view field, const T & val) {
        ENSURE_TRUE(m_pInstance.IsValid(),);
        m_pClass.SetFieldValue(m_pInstance, field, val);
    }

    template <>
    inline void Script::SetField<MV::Script>(MV::string_view field, const MV::Script & val) {
        ENSURE_TRUE(m_pInstance.IsValid(),);
        m_pClass.SetFieldValue(m_pInstance, field, val.GetObjectInstance());
    }

    template <typename T>
    inline void Script::SetField(const ScriptClassField & field, const T & val) {
        SetField(field.m_FieldName, val);
    }
}


#endif //MV_ENGINE_SCRIPT_H
