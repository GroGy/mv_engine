//
// Created by Matty on 2022-02-11.
//

#ifndef TEST_GAME_SCRIPTINGMODULE_H
#define TEST_GAME_SCRIPTINGMODULE_H

#include <mono/metadata/object.h>
#include "ScriptManager.h"
#include "common/World.h"
#include "common/EngineCommon.h"

namespace MV {
    class ScriptingModule {
    public:
        static size_t GetPropertyIndex(MonoObject *worldObject, MonoObject *boxCollider, World &world, ScriptManager &scriptManager);

        virtual void InitFunctionCallbacks() = 0;
        virtual void CheckClassesExistence(MonoImage * engineAssembly) = 0;
        virtual void CheckClassFunctionsExistence(MonoImage * engineAssembly) = 0;

        template <typename T>
        inline static T* GetProperty(MonoObject *worldObject, MonoObject *boxCollider);

        static MV::Property* GetPropertyRaw(MonoObject *worldObject, MonoObject *boxCollider);

        static void FindObjectField(MonoObject *worldObject, MonoObject *boxCollider, World &world,
                        ScriptManager &scriptManager,
                        uint64_t &woID, ScriptClassField &monoField);
    };

    template<typename T>
    T *ScriptingModule::GetProperty(MonoObject *worldObject, MonoObject *boxCollider) {
        const auto engine = GetEngine();
        auto & world = engine->GetWorld();
        auto & sm = engine->m_ScriptManager;
        ENSURE_TRUE(world.IsValid(), nullptr);

        size_t index = ScriptingModule::GetPropertyIndex(worldObject, boxCollider, world, sm);

        PropertyStorage<T> & storage = world.m_PropertyManager.GetStorage<T>();
        auto * res = storage.get_property(index);
        return res ? static_cast<T*>(res) : nullptr;
    }
}


#endif //TEST_GAME_SCRIPTINGMODULE_H
