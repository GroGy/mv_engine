//
// Created by Matty on 2022-02-06.
//

#ifndef MV_ENGINE_SCRIPTCLASS_H
#define MV_ENGINE_SCRIPTCLASS_H

#include <mono/metadata/object.h>
#include "ScriptObjectInstance.h"

#undef GetClassName

namespace MV {
#undef BOOL

    struct ScriptClassField {
        enum class Type {
            I8 = 0,
            I16,
            I32,
            I64,
            U8,
            U16,
            U32,
            U64,
            STRING,
            F32,
            F64,
            ARRAY,
            PROPERTY,
            WORLD_OBJECT,
            BOOL,
            CHAR,
            IVEC2,
            IVEC3,
            UVEC2,
            UVEC3,
            VEC2,
            VEC3,

            CLASS, // Used as tmp container, if we resolve it we change it to some accoarding type, if not, we change it to unknown
            UNKNOWN,
            COUNT
        };

        MonoClassField* m_Field;
        MV::string m_FieldName;
        bool m_ShowInEditor;
        Type m_Type;
        uint32_t m_Class;
    };

    class ScriptClass {
    public:
        ScriptObjectInstance CreateObject();

        friend class ScriptManager;

        void CallFunction_Void(const ScriptObjectInstance & instance,MV::string_view  name);
        void CallFunction_Void_Float(const ScriptObjectInstance & instance,MV::string_view  name, float val);

        template<typename T>
        inline void SetFieldValue(const ScriptObjectInstance &instance,MV::string_view fieldName, const T & val);
        template<typename T>
        inline static void SetFieldValue(MonoObject* instance,MV::string_view fieldName, const T & val);

        void SetFieldValue(const ScriptObjectInstance &instance,MV::string_view fieldName, void * val);

        const MV::unordered_map<MV::string,ScriptClassField> & GetFields() const;

        ScriptObjectInstance GetFieldClassObject(const ScriptObjectInstance &instance, const ScriptClassField & field);

        template<typename T>
        inline void GetFieldValue(T* result, const ScriptObjectInstance &instance, const ScriptClassField & field);

        const ScriptClassField * FindField(MV::string_view name) const;

        inline MV::string_view GetClassName() const {return m_pClassName;}

        bool IsChildOfClass(const ScriptClass & other);
    private:
        uint32_t m_pClassVersion = 0;
        MonoClass * m_pClass = nullptr;
        string m_pClassName;

        void init();

        void initFieldOfClassType();

        void initClassFunctions(MonoClass * klass);
        void initClassFields(MonoClass * klass);

        void initFunctions();
        void initFields();

        void checkVersion();

        MonoString * convertToMonoString(MV::string_view string);

        MV::unordered_map<MV::string,MonoMethod*> m_pFunctions;
        MV::unordered_map<MV::string,ScriptClassField> m_pFields;
    };

    template<typename T>
    inline void ScriptClass::SetFieldValue(const ScriptObjectInstance &instance, MV::string_view fieldName, const T &val) {
        mono_field_set_value(instance.m_pObject,m_pFields[MV::string(fieldName)].m_Field,(void *)&val);
    }

    template<>
    inline void ScriptClass::SetFieldValue<MV::string>(const ScriptObjectInstance &instance, MV::string_view fieldName, const MV::string &val) {
        MonoString * str = convertToMonoString(val);
        mono_field_set_value(instance.m_pObject,m_pFields[MV::string(fieldName)].m_Field,(void *)str);
        mono_free(str);
    }

    template<>
    inline void ScriptClass::SetFieldValue<MV::ScriptObjectInstance>(const ScriptObjectInstance &instance, MV::string_view fieldName, const MV::ScriptObjectInstance &val) {
        mono_field_set_value(instance.m_pObject,m_pFields[MV::string(fieldName)].m_Field,(void *)val.m_pObject);
    }

    template<>
    inline void ScriptClass::SetFieldValue<MV::string_view>(const ScriptObjectInstance &instance, MV::string_view fieldName, const MV::string_view &val) {
        MonoString * str = convertToMonoString(val);
        mono_field_set_value(instance.m_pObject,m_pFields[MV::string(fieldName)].m_Field,(void *)str);
        mono_free(str);
    }

    template<typename T>
    inline void ScriptClass::SetFieldValue(MonoObject * instance, MV::string_view fieldName, const T &val) {
        mono_field_set_value(instance, mono_class_get_field_from_name(mono_object_get_class(instance),fieldName.data()),(void *)&val);
    }

    template<typename T>
    inline void ScriptClass::GetFieldValue(T *result, const ScriptObjectInstance &instance, const ScriptClassField &field) {
        return mono_field_get_value(instance.m_pObject,field.m_Field , (void*)result);
    }
}

#endif //MV_ENGINE_SCRIPTCLASS_H
