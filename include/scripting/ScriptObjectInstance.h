//
// Created by Matty on 2022-02-06.
//

#ifndef MV_ENGINE_SCRIPTOBJECTINSTANCE_H
#define MV_ENGINE_SCRIPTOBJECTINSTANCE_H

#include <EngineMinimal.h>
#include <mono/metadata/object.h>

#undef GetClassName

namespace MV {
    class ScriptObjectInstance {
    public:
        friend class ScriptClass;
        friend class ScriptManager;
        bool IsValid() const { return m_pObject != nullptr && (m_pMonoCreatedObject || m_pGCHandle != 0); }
        void Destroy();

        MV::string GetClassName();
        MV::string GetClassNamePotentionallyEmpty();

        MonoObject * GetMonoObject() const;

        static ScriptObjectInstance CreateFromMonoObject(MonoObject * object);
    private:
        MonoObject *m_pObject = nullptr;
        uint32_t m_pGCHandle = 0;
        bool    m_pMonoCreatedObject = false;

        MV::vector<uint32_t> m_pPropertyGCHandles{};
    };
}


#endif //MV_ENGINE_SCRIPTOBJECTINSTANCE_H
