//
// Created by Matty on 07.12.2022.
//

#ifndef MV_EDITOR_GAMEINSTANCE_H
#define MV_EDITOR_GAMEINSTANCE_H

#include "EngineMinimal.h"
#include "scripting/Script.h"

namespace MV {
    /// Main game instance data storing class
    /// stores class that is used as game instance and instance itself
    class GameInstance {
    public:
        GameInstance();

        void Init();

        void Cleanup();

        void SetClass(MV::string_view className);

        inline MV::string_view GetCustomClass() const { return m_pCustomClassName;};

    public: // Callbacks that are propagated to scripting

        void OnStart();

        void OnEnd();
    private:
        MV::string m_pCustomClassName;

        MV::Script m_pGameInstance;
    };
}

#endif //MV_EDITOR_GAMEINSTANCE_H
