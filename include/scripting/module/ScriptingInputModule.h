//
// Created by Matty on 10.12.2022.
//

#ifndef MV_EDITOR_SCRIPTINGINPUTMODULE_H
#define MV_EDITOR_SCRIPTINGINPUTMODULE_H

#include "scripting/ScriptingModule.h"

namespace MV {
    class ScriptingInputModule : public ScriptingModule {
    public:
        void InitFunctionCallbacks() override;

        void CheckClassesExistence(MonoImage *engineAssembly) override;

        void CheckClassFunctionsExistence(MonoImage *engineAssembly) override;
    };
};


#endif //MV_EDITOR_SCRIPTINGINPUTMODULE_H
