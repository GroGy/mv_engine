//
// Created by Matty on 2022-02-11.
//

#ifndef TEST_GAME_SCRIPTINGWORLDOBJECTMODULE_H
#define TEST_GAME_SCRIPTINGWORLDOBJECTMODULE_H

#include "../ScriptingModule.h"

namespace MV {
    class ScriptingWorldObjectModule : public ScriptingModule {
    public:
        void InitFunctionCallbacks() override;

        void CheckClassesExistence(MonoImage *engineAssembly) override;

        void CheckClassFunctionsExistence(MonoImage *engineAssembly) override;
    };
}


#endif //TEST_GAME_SCRIPTINGWORLDOBJECTMODULE_H
