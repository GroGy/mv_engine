//
// Created by Matty on 2022-09-30.
//

#ifndef MV_EDITOR_SCRIPTINGSPRITEMODULE_H
#define MV_EDITOR_SCRIPTINGSPRITEMODULE_H

#include "scripting/ScriptingModule.h"

namespace MV {
    class ScriptingSpriteModule : public ScriptingModule {
    public:
        void InitFunctionCallbacks() override;

        void CheckClassesExistence(MonoImage *engineAssembly) override;

        void CheckClassFunctionsExistence(MonoImage *engineAssembly) override;
    };
};


#endif //MV_EDITOR_SCRIPTINGSPRITEMODULE_H
