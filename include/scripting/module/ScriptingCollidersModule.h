//
// Created by Matty on 2022-09-22.
//

#ifndef MV_EDITOR_SCRIPTINGCOLLIDERSMODULE_H
#define MV_EDITOR_SCRIPTINGCOLLIDERSMODULE_H

#include "scripting/ScriptingModule.h"

namespace MV {
    class ScriptingCollidersModule : public ScriptingModule {
    public:
        void InitFunctionCallbacks() override;

        void CheckClassesExistence(MonoImage *engineAssembly) override;

        void CheckClassFunctionsExistence(MonoImage *engineAssembly) override;
    };
};


#endif //MV_EDITOR_SCRIPTINGCOLLIDERSMODULE_H
