//
// Created by Matty on 2021-10-24.
//

#ifndef MV_ENGINE_PHYSICSDEBUGDRAWER_H
#define MV_ENGINE_PHYSICSDEBUGDRAWER_H

#include <box2d/box2d.h>
#include "../render/common/FrameContext.h"
#include "../render/common/VertexBuffer.h"
#include "PhysicsDebugVertex.h"

namespace MV {
    class PhysicsDebugDrawer : public b2Draw {
    private:
        MV::rc<VertexBuffer> m_pVBO;
        MV::rc<StagingBuffer> m_pStagingBuffer;
        MV::vector<PhysicsDebugVertex> m_pVertices;
        inline b2Vec2 processPosition(b2Vec2 in);
        inline float processPosition(float in);
    public:
        void DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color) override;

        void DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color) override;

        void DrawCircle(const b2Vec2 &center, float radius, const b2Color &color) override;

        void DrawSolidCircle(const b2Vec2 &center, float radius, const b2Vec2 &axis, const b2Color &color) override;

        void DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color) override;

        void DrawTransform(const b2Transform &xf) override;

        void DrawPoint(const b2Vec2 &p, float size, const b2Color &color) override;

        void Render(FrameContext & context);

        void Init();

        void Cleanup();
    };
}


#endif //MV_ENGINE_PHYSICSDEBUGDRAWER_H
