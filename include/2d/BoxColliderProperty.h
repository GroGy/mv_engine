//
// Created by Matty on 2022-06-19.
//

#ifndef MV_EDITOR_BOXCOLLIDERPROPERTY_H
#define MV_EDITOR_BOXCOLLIDERPROPERTY_H

#include "../common/Format.h"
#include "ColliderProperty.h"

#undef GetClassName

namespace MV {
    namespace Config {
        static constexpr MV::string_view sc_BoxColliderPropertyName = "BoxCollider";
    }

    class BoxColliderProperty : public ColliderProperty {
    public:
        static MV::string_view GetClassName() { return Config::sc_BoxColliderPropertyName; }

        void Init() override;

        void Cleanup() override;

        void Update(float deltaTime) override;

        void Serialize(json &propertyField) override;

        void Deserialize(const json &propertyField) override;

        void Sync(Script &owner,const ScriptClassField &field) override;

        void OnOwnerUpdate() override;

        void OnDataChanged();

        glm::vec2 m_Offset{0, 0};
        glm::vec2 m_Scale{1, 1};
        float m_Rotation = 0;

        static void SyncPropertyData(const ScriptClassField &field, Script &script, const json &data);
    };
}


#endif //MV_EDITOR_BOXCOLLIDERPROPERTY_H
