//
// Created by Matty on 2021-10-23.
//

#ifndef MV_ENGINE_TILESET_H
#define MV_ENGINE_TILESET_H

#include "Tile.h"
#include "../resources/TilesetData.h"

namespace MV {
    class Tileset {
    public:
        [[nodiscard]] const Tile & GetTile(uint32_t id) const;
        [[nodiscard]] const MV::vector<Tile> & GetTiles() const {return m_pRegistry;};
        explicit Tileset(const TilesetData & data);

        void SetTileCollision(uint32_t id, bool collision);

        rc<Texture> GetAtlasTexture() const;

        inline uint64_t GetAtlasAsset() const {return m_Atlas;};
    private:
        MV::vector<Tile> m_pRegistry;
        uint64_t m_Atlas = 0;
    };
}


#endif //MV_ENGINE_TILESET_H
