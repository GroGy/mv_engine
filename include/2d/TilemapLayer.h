//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_TILEMAPLAYER_H
#define MV_ENGINE_TILEMAPLAYER_H

#include "../render/common/VertexBuffer.h"
#include "Tile.h"
#include "../render/common/IndexBuffer.h"
#include "../resources/WorldData.h"
#include "resources/TilemapData.h"

namespace MV {
    class TilemapLayer {
    private:
        MV::TilemapData m_pData;
    public:
        TilemapLayer(MV::TilemapData data);
        void Build(MV::rc<VertexBuffer> & vertBuffer, MV::rc<IndexBuffer> & indexBuffer);
        void BuildCollision(b2World * world);
        void Load(const TileLayerData & data);
        uint64_t GetTileset() const { return m_pData.m_Tileset;}
        rc<Texture> GetTilesetTexture() const;
    };
}


#endif //MV_ENGINE_TILEMAPLAYER_H
