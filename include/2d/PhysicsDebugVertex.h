//
// Created by Matty on 2021-10-24.
//

#ifndef MV_ENGINE_PHYSICSDEBUGVERTEX_H
#define MV_ENGINE_PHYSICSDEBUGVERTEX_H

#include "../render/common/Vertex.h"

namespace MV {
    class PhysicsDebugVertex : public VertexBase<PhysicsDebugVertex> {
    private:
        glm::vec2 m_pPos;
        glm::vec4 m_pColor;
    public:
        PhysicsDebugVertex(const glm::vec2 &pos, const glm::vec4 &color) noexcept : m_pPos(pos), m_pColor(color) {}
        static MV::vector<VertexBindingDescriptor> GetBindingDescription() {
            MV::vector<VertexBindingDescriptor> bindingDescription(1);
            bindingDescription[0].binding = 0;
            bindingDescription[0].stride = sizeof(PhysicsDebugVertex);
            bindingDescription[0].inputRate = VertexInputRate::PER_VERTEX;
            return bindingDescription;
        }

        static MV::vector<VertexAttributeDescriptor> GetAttributeDescriptions() {
            MV::vector<VertexAttributeDescriptor> attributeDescriptions(2);

            attributeDescriptions[0].binding = 0;
            attributeDescriptions[0].location = 0;
            attributeDescriptions[0].format = AttributeFormat<decltype(m_pPos)>::GetAttributeType();
            attributeDescriptions[0].offset = offsetof(PhysicsDebugVertex, m_pPos);

            attributeDescriptions[1].binding = 0;
            attributeDescriptions[1].location = 1;
            attributeDescriptions[1].format = AttributeFormat<decltype(m_pColor)>::GetAttributeType();
            attributeDescriptions[1].offset = offsetof(PhysicsDebugVertex, m_pColor);

            return attributeDescriptions;
        }
    };
}


#endif //MV_ENGINE_PHYSICSDEBUGVERTEX_H
