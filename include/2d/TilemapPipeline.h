//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_TILEMAPPIPELINE_H
#define MV_ENGINE_TILEMAPPIPELINE_H


#include "../render/vulkan/VulkanPipeline.h"

namespace MV {
    namespace Config {
        static constexpr uint32_t sc_TilemapColorUniformIndex = 0;
        static constexpr uint32_t sc_TilemapAtlasUniformIndex = 1;
        static constexpr uint32_t sc_TilemapMVPPushConstantIndex = 0;
    }

    class TilemapPipeline {
    public:
        rc<ShaderPipeline> m_Pipeline;
        PushConstantDescription m_MVPmatrixDescription;
        UniformDescription m_ColorUniform;
        UniformDescription m_AtlasUniform;
        PipelineBlueprint GetBlueprint();
    };
}


#endif //MV_ENGINE_TILEMAPPIPELINE_H
