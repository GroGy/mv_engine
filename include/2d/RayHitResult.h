//
// Created by Matty on 11.12.2022.
//

#ifndef MV_EDITOR_RAYHITRESULT_H
#define MV_EDITOR_RAYHITRESULT_H

#include "EngineMinimal.h"
#include "box2d/b2_fixture.h"

namespace MV {
    class RayHitResult {
    public:
        [[nodiscard]] bool IsHit() const { return m_Hit != nullptr;};

        WorldObject* m_WorldObject;
        b2Fixture * m_Hit = nullptr;
        glm::vec2 m_Position;
        glm::vec2 m_Normal;
    };
}

#endif //MV_EDITOR_RAYHITRESULT_H
