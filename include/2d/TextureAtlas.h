//
// Created by Matty on 2021-10-24.
//

#ifndef MV_ENGINE_TEXTUREATLAS_H
#define MV_ENGINE_TEXTUREATLAS_H

#include "../render/common/Texture.h"

namespace MV {
    class TextureAtlas {
    private:
        uint32_t m_pTextureCount = 0;
        MV::rc<Texture> m_pAtlasTexture{};
        MV::vector<glm::vec4> m_pUVs{};
    public:
        [[nodiscard]]
        uint32_t GetTextureCount() const { return m_pTextureCount; }

        [[nodiscard]]
        glm::vec4 GetUV(uint32_t id) const { return m_pUVs.at(id); };
    };
}


#endif //MV_ENGINE_TEXTUREATLAS_H
