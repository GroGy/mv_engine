//
// Created by Matty on 2021-10-24.
//

#ifndef MV_ENGINE_PHYSICSDEBUGPIPELINE_H
#define MV_ENGINE_PHYSICSDEBUGPIPELINE_H


#include "../render/vulkan/VulkanPipeline.h"

namespace MV {
    namespace Config {
        static constexpr uint32_t sc_PhysicsDebugMVPPushConstantIndex = 0;
    }

    class PhysicsDebugPipeline {
    public:
        rc<ShaderPipeline> m_Pipeline;
        PushConstantDescription m_MVPmatrixDescription;

        PipelineBlueprint GetBlueprint();
    };
}

#endif //MV_ENGINE_PHYSICSDEBUGPIPELINE_H
