//
// Created by Matty on 2022-09-23.
//

#ifndef MV_EDITOR_COLLIDERPROPERTY_H
#define MV_EDITOR_COLLIDERPROPERTY_H

#include "common/Property.h"
#include "box2d/b2_polygon_shape.h"
#include "box2d/b2_fixture.h"

namespace MV {
    // Base class for all colliders, supplies functions applying velocity etc
    class ColliderProperty : public MV::Property {
    public:
        static constexpr float sc_ForceMultiplier = 100.0f;

        void ApplyForceToCenter(const glm::vec2 & force);

        void ApplyForce(const glm::vec2 & force, const glm::vec2 & point);

        void AllowSleep(bool value);

    public:

        float m_Density = 1.0f;

    protected:
        b2Fixture * m_pFixture = nullptr;
        b2PolygonShape m_pShape{};
    };
}


#endif //MV_EDITOR_COLLIDERPROPERTY_H
