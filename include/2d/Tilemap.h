//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_TILEMAP_H
#define MV_ENGINE_TILEMAP_H


#include <box2d/box2d.h>
#include "../render/common/VertexBuffer.h"
#include "TilemapLayer.h"
#include "../render/common/IndexBuffer.h"
#include "PhysicsDebugDrawer.h"
#include "../common/WorldObject.h"

namespace MV {
    static const int VELOCITY_ITERATIONS = 8;
    static const int POSITION_ITERATIONS = 3;

    struct StaticLayerData {
        MV::rc<VertexBuffer> m_VertexBuffer{};
        MV::rc<IndexBuffer> m_IndexBuffer{};
        MV::rc<TilemapLayer> m_Layer{};
        bool m_HasCollision = true;
        float m_Brightness = 1.0f;
    };

class Tilemap : public WorldObject {
    private:
        size_t m_pWidth, m_pHeight;
        bool m_pIsLoaded = false;
        bool m_pBoundTextures = false;

        MV::vector<StaticLayerData> m_pStaticLayers{};
    public:
        [[nodiscard]] glm::uvec2 GetSize( ) const { return {m_pWidth, m_pHeight};}

        [[nodiscard]] bool IsLoaded() const { return m_pIsLoaded; }
        [[nodiscard]] bool ShouldRender() const;

        void Build(b2World *physicsWorld);

        Tilemap(size_t width, size_t height);

        void AddStaticLayer(const MV::rc<TilemapLayer> &layer, float brightness = 1.0f);

        void Render(FrameContext &context) override;

        void Update(float deltaTime) override;

        void Init();

        void Cleanup();
    };
}


#endif //MV_ENGINE_TILEMAP_H
