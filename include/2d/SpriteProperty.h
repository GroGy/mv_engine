//
// Created by Matty on 2022-02-24.
//

#ifndef MV_EDITOR_SPRITEPROPERTY_H
#define MV_EDITOR_SPRITEPROPERTY_H

#include "../common/Property.h"
#include "../render/common/IRenderedProperty.h"

#undef GetClassName

namespace MV {
    namespace Config {
        static constexpr MV::string_view sc_SpritePropertyName = "Sprite";
    }

    class [[mv_register_property()]] SpriteProperty : public Property, public IRenderedProperty {
    public:
        static MV::string_view GetClassName() {return Config::sc_SpritePropertyName; }

        void Render(FrameContext &context) override;

        void Cleanup() override;

        void Update(float deltaTime) override;

        void Serialize(nlohmann::json &propertyField) override;

        void Deserialize(const nlohmann::json &propertyField) override;

        void Sync(Script& owner,const ScriptClassField &field) override;

        [[mv_serialize()]]
        bool m_Visible = false;

        [[mv_serialize()]]
        glm::vec2 m_Offset{0,0};

        [[mv_serialize()]]
        glm::vec2 m_Scale{1,1};

        [[mv_serialize()]]
        float m_Rotation = 0;

        [[mv_serialize()]]
        uint64_t m_Shader = 0;

        [[mv_serialize()]]
        uint64_t m_Texture = 0;

        [[mv_serialize()]]
        uint16_t m_SpriteIndex = 0;
        bool m_IsAtlasSprite = false;

        bool m_WasUVLoaded = false;
        glm::vec4 m_UV = {0,0,1,1};
        glm::vec4 m_Offsets = {0,0,1.0f,1.0f};

        static void SyncPropertyData(const ScriptClassField & field,Script &script, const nlohmann::json &data);

        template<typename T>
        inline static T ReadField(MV::string_view name,const nlohmann::json &field);

    public: // Script functions

        void SetSpriteIndex_Native(uint32_t spriteIndex);

    private:

    };
}


#endif //MV_EDITOR_SPRITEPROPERTY_H
