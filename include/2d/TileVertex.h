//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_TILEVERTEX_H
#define MV_ENGINE_TILEVERTEX_H

#include "../render/common/Vertex.h"

namespace MV {
    class TileVertex : VertexBase<TileVertex> {
    public:
        glm::vec2 m_Pos;
        glm::vec2 m_UV;

        TileVertex(const glm::vec2 &pos, const glm::vec2 &uv) noexcept : m_Pos(pos), m_UV(uv) {}

        static MV::vector<VertexBindingDescriptor> GetBindingDescription() {
            MV::vector<VertexBindingDescriptor> bindingDescription(1);
            bindingDescription[0].binding = 0;
            bindingDescription[0].stride = sizeof(TileVertex);
            bindingDescription[0].inputRate = VertexInputRate::PER_VERTEX;
            return bindingDescription;
        }

        static MV::vector<VertexAttributeDescriptor> GetAttributeDescriptions() {
            MV::vector<VertexAttributeDescriptor> attributeDescriptions(2);

            attributeDescriptions[0].binding = 0;
            attributeDescriptions[0].location = 0;
            attributeDescriptions[0].format = AttributeFormat<decltype(m_Pos)>::GetAttributeType();
            attributeDescriptions[0].offset = offsetof(TileVertex, m_Pos);

            attributeDescriptions[1].binding = 0;
            attributeDescriptions[1].location = 1;
            attributeDescriptions[1].format = AttributeFormat<decltype(m_UV)>::GetAttributeType();
            attributeDescriptions[1].offset = offsetof(TileVertex, m_UV);

            return attributeDescriptions;
        }
    };
}


#endif //MV_ENGINE_TILEVERTEX_H
