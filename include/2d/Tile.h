//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_TILE_H
#define MV_ENGINE_TILE_H

#include <glm.hpp>
#include "TileVertex.h"
#include "box2d/box2d.h"

namespace MV {
    class Tile {
    private:
        glm::vec4 m_pColor;
        bool m_pHasCollision;
        glm::vec4 m_pUV;
    public:
        void SetCollision(bool collision) {m_pHasCollision = collision;}
        Tile() = default;
        explicit Tile(const glm::vec4 & color, bool collision, const::glm::vec4 & uv);

        void Build(const glm::uvec2 & pos,MV::vector<TileVertex> & vertexBuffer, MV::vector<uint32_t> & indices,uint32_t & offset) const;
        void BuildCollision(const glm::uvec2 & pos,b2World * world) const;

        inline const glm::vec4 & GetUV() const { return m_pUV; }
    };
}


#endif //MV_ENGINE_TILE_H
