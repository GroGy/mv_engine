//
// Created by Matty on 2022-03-07.
//

#ifndef MV_EDITOR_DEFAULTALLOCATOR_H
#define MV_EDITOR_DEFAULTALLOCATOR_H

#include <EngineMinimal.h>
#include <EASTL/allocator.h>

namespace MV {
    namespace AllocationType {
        enum Flags {
          ENGINE = 0,
          SCRIPT = 1
        };
    }

    class DefaultAllocator : eastl::allocator {
    public:
        DefaultAllocator();
        DefaultAllocator(const DefaultAllocator &x);

        void *allocate(size_t n, int flags);

        void *reallocate(void * ptr, size_t n, int flags);

        void *allocate(size_t n, size_t alignment, size_t offset, int flags);

        void deallocate(void *p, size_t n);

        void deallocate(void *p, size_t n, int flags);

        const char *get_name() const;

        void set_name(const char *pName);

        static DefaultAllocator & GetInstance();

        size_t get_total_allocated() const;

        size_t get_allocated_this_frame();

        size_t get_script_allocated() const;
    private:
        size_t m_pAllocatedSinceLastFrame = 0;
        size_t m_pTotalAllocated = 0;
        size_t m_pScriptsAllocated = 0;
    };
}

void* operator new[](size_t size, const char* pName, int flags, unsigned debugFlags, const char* file, int line);
void* operator new[](size_t size, size_t alignment, size_t alignmentOffset, const char* pName, int flags, unsigned debugFlags, const char* file, int line);

#endif //MV_EDITOR_DEFAULTALLOCATOR_H
