//
// Created by Matty on 2022-03-08.
//

#ifndef MV_EDITOR_ALLOCATORPROVIDER_H
#define MV_EDITOR_ALLOCATORPROVIDER_H

#include <EASTL/allocator.h>

namespace MV {
    class AllocatorProvider : public eastl::allocator {
    public:
        AllocatorProvider();

        AllocatorProvider(const allocator &x);

        void *allocate(size_t n);

        void *allocate(size_t n, size_t alignment, size_t offset);

        void *allocate(size_t n, int flags);

        void *allocate(size_t n, size_t alignment, size_t offset, int flags);

        void *reallocate(void * ptr, size_t n, int flags);

        void deallocate(void *p, size_t n);

        const char *get_name() const;

        void set_name(const char *pName);
    };
}


#endif //MV_EDITOR_ALLOCATORPROVIDER_H
