//
// Created by Matty on 2022-09-26.
//

#ifndef MV_EDITOR_PARTICLEPIPELINE_H
#define MV_EDITOR_PARTICLEPIPELINE_H

#include "render/common/ShaderPipeline.h"
#include "render/common/Vertex.h"
#include "ParticleSystemProperty.h"

namespace MV {
    class ParticleVertex : VertexBase<ParticleVertex> {
    public:
        glm::vec2 m_Pos;
        glm::vec2 m_UV;

        ParticleVertex(const glm::vec2 &pos, const glm::vec2 &uv) noexcept : m_Pos(pos), m_UV(uv) {}

        static MV::vector<VertexBindingDescriptor> GetBindingDescription() {
            MV::vector<VertexBindingDescriptor> bindingDescription(2);
            bindingDescription[0].binding = 0;
            bindingDescription[0].stride = sizeof(ParticleVertex);
            bindingDescription[0].inputRate = VertexInputRate::PER_VERTEX;

            bindingDescription[1].binding = 1;
            bindingDescription[1].stride = sizeof(MV::CPUParticles::Particle);
            bindingDescription[1].inputRate = VertexInputRate::PER_INSTANCE;

            return bindingDescription;
        }

        static MV::vector<VertexAttributeDescriptor> GetAttributeDescriptions() {
            MV::vector<VertexAttributeDescriptor> attributeDescriptions(7);

            attributeDescriptions[0].binding = 0;
            attributeDescriptions[0].location = 0;
            attributeDescriptions[0].format = AttributeFormat<decltype(m_Pos)>::GetAttributeType();
            attributeDescriptions[0].offset = offsetof(ParticleVertex, m_Pos);

            attributeDescriptions[1].binding = 0;
            attributeDescriptions[1].location = 1;
            attributeDescriptions[1].format = AttributeFormat<decltype(m_UV)>::GetAttributeType();
            attributeDescriptions[1].offset = offsetof(ParticleVertex, m_UV);

            attributeDescriptions[2].binding = 1;
            attributeDescriptions[2].location = 2;
            attributeDescriptions[2].format = AttributeFormat<decltype(MV::CPUParticles::Particle::m_Position)>::GetAttributeType();
            attributeDescriptions[2].offset = offsetof(MV::CPUParticles::Particle, m_Position);

            attributeDescriptions[3].binding = 1;
            attributeDescriptions[3].location = 3;
            attributeDescriptions[3].format = AttributeFormat<decltype(MV::CPUParticles::Particle::m_Scale)>::GetAttributeType();
            attributeDescriptions[3].offset = offsetof(MV::CPUParticles::Particle, m_Scale);

            attributeDescriptions[4].binding = 1;
            attributeDescriptions[4].location = 4;
            attributeDescriptions[4].format = AttributeFormat<decltype(MV::CPUParticles::Particle::m_Rotation)>::GetAttributeType();
            attributeDescriptions[4].offset = offsetof(MV::CPUParticles::Particle, m_Rotation);

            attributeDescriptions[5].binding = 1;
            attributeDescriptions[5].location = 5;
            attributeDescriptions[5].format = AttributeFormat<decltype(MV::CPUParticles::Particle::m_Alive)>::GetAttributeType();
            attributeDescriptions[5].offset = offsetof(MV::CPUParticles::Particle, m_Alive);

            attributeDescriptions[6].binding = 1;
            attributeDescriptions[6].location = 6;
            attributeDescriptions[6].format = AttributeFormat<decltype(MV::CPUParticles::Particle::m_Color)>::GetAttributeType();
            attributeDescriptions[6].offset = offsetof(MV::CPUParticles::Particle, m_Color);

            return attributeDescriptions;
        }
    };

    class ParticlePipeline {
    public:
        static constexpr uint64_t s_ParticlePipelineReservedID = 3;

        rc<ShaderPipeline> m_Pipeline;
        PushConstantDescription m_MVPmatrixDescription;
        PipelineBlueprint GetBlueprint();
    };
}


#endif //MV_EDITOR_PARTICLEPIPELINE_H
