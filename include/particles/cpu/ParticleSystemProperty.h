//
// Created by Matty on 2022-09-24.
//

#ifndef MV_EDITOR_PARTICLESYSTEMPROPERTY_H
#define MV_EDITOR_PARTICLESYSTEMPROPERTY_H

#include "common/Property.h"
#include "common/Gradient.h"
#include "render/common/IRenderedProperty.h"
#include "async/AsyncTask.h"
#include "util/Math.h"

namespace MV {
    enum class ParticleCoordinateSpace {
        Local,
        World
    };

    struct ParticleSystemGeneralSettings {
        uint32_t m_MaxParticles = 0;
        float m_Lifetime;

        // Spawn rate per second
        uint32_t m_SpawnRate = 16;

        ParticleCoordinateSpace m_CoordinateSpace = ParticleCoordinateSpace::Local;
        glm::vec2 m_SpawnPosition = {0.0f, 0.0f};
        glm::vec2 m_SpawnScale = {1.0f, 1.0f};
        float m_SpawnRotation = 0.0f;
    };

    struct ParticleSystemMovementSettings {
        bool m_pUseSpawnVelocityRanges = false;
        glm::vec2 m_SpawnVelocityXRange = {0.0f, 0.0f};
        glm::vec2 m_SpawnVelocityYRange = {0.0f, 0.0f};

        bool m_pUseSpawnScaleRanges = false;
        glm::vec2 m_SpawnScaleXRange = {1.0f, 1.0f};
        glm::vec2 m_SpawnScaleYRange = {1.0f, 1.0f};

        glm::vec2 m_WorldForce = {0.0f, 0.0f};

        glm::vec2 m_Dampening = {0.5f,0.5f};
    };

    enum class ParticleColorType {
        Constant,
        Gradient
    };

    struct ParticleRenderingSettings {
        ParticleColorType m_ColorType;
        glm::vec4 m_Color;
        MV::Gradient m_ColorOverLifetime{glm::vec4{1.0f}};
        MV::BezierCurve m_SizeOverLifetime{};
    };

    namespace CPUParticles {
        struct Particle {
            alignas(16) glm::vec2 m_Position = {0.0f, 0.0f};
            alignas(16) glm::vec2 m_Scale = {1.0f, 1.0f};
            alignas(16) glm::vec2 m_Velocity = {0.0f, 0.0f};
            alignas(16) float m_Rotation = 0.0f;
            alignas(16) float m_Lifetime = 1.0f;
            alignas(16) double m_SpawnTime = 0.0f;
            alignas(16) bool m_Alive = false;
            alignas(16) glm::vec4 m_Color = {1.0f, 1.0f, 1.0f, 1.0f};

        };
    }

    class ParticleUpdateTask : public MV::AsyncTask {
    private:
        uint32_t m_pStartIndex;
        uint32_t m_pCount;
        MV::rc<MV::vector<CPUParticles::Particle>> m_pParticles;
        uint32_t m_pToSpawn;

        float m_pSpawnThisUpdate;
        double m_pTime;
        float m_pDeltaTime;
    public:
        ParticleUpdateTask(float deltaTime,double time,
                           uint32_t toSpawn,
                           const MV::rc<MV::vector<CPUParticles::Particle>> & particles,
                           uint32_t startIndex,
                           uint32_t count,
                           ParticleSystemGeneralSettings generalSettings,
                           ParticleRenderingSettings renderingSettings,
                           ParticleSystemMovementSettings movementSettings
        );

        void Work() override;

        const char *GetName() override;

        uint32_t m_ActiveParticles = 0;

    private:

        void updateParticle(CPUParticles::Particle &particle);

    private: // Settings

        ParticleSystemGeneralSettings m_pGeneralSettings;
        ParticleRenderingSettings m_pRenderingSettings;
        ParticleSystemMovementSettings m_pMovementSettings;
    };


    namespace Config {
        static constexpr MV::string_view sc_ParticleSystemPropertyName = "ParticleSystem";
    }


    class ParticleSystemProperty : public MV::Property, public MV::IRenderedProperty {
    public: // Global property name getter

        static MV::string_view GetClassName() { return Config::sc_ParticleSystemPropertyName; }

        ParticleSystemProperty() = default;

    public: // IRenderedProperty overrides

        void Render(FrameContext &context) override;

    public: // Property overrides
        void Init() override;

        void Update(float deltaTime) override;

        void Cleanup() override;

        void Serialize(nlohmann::json &woData) override;

        void Deserialize(const nlohmann::json &woData) override;

        void Sync(Script &owner, const ScriptClassField &field) override;

        void Destroy() override;

        static void SyncPropertyData(const ScriptClassField &field, Script &script, const json &data);

        [[nodiscard]]
        uint32_t GetActiveParticles() const {return m_pActiveParticles;};

    public: // Public fields

        ParticleSystemGeneralSettings m_GeneralSettings;
        ParticleRenderingSettings m_RenderingSettings;
        ParticleSystemMovementSettings m_MovementSettings;

    private:

        MV::rc<MV::vector<CPUParticles::Particle>> m_pParticles;
        MV::vector<CPUParticles::Particle> m_pRenderedParticles;

        MV::rc<AsyncTaskPromise<ParticleUpdateTask>> m_pPromise;

        uint32_t m_pActiveParticles = 0;

        float m_pToSpawnTimeAccumulator = 0.0f;
        uint32_t m_pToSpawn = 0;
        // Delta since last update, should be mostly 0, but if simulation takes too long, we can skip
        float m_pUpdateDelta = 0.0f;
    };
}


#endif //MV_EDITOR_PARTICLESYSTEMPROPERTY_H
