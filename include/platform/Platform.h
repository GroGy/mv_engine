//
// Created by Matty on 2022-03-06.
//

#ifndef MV_ENGINE_PLATFORM_H
#define MV_ENGINE_PLATFORM_H

#include <cstdint>

namespace MV {
    class Platform {
    public:
        static uint64_t GetThreadCount();
    };
}


#endif //MV_ENGINE_PLATFORM_H
