//
// Created by Matty on 2022-09-19.
//

#ifndef MV_EDITOR_ENSURELOG_H
#define MV_EDITOR_ENSURELOG_H

#include <common/Types.h>

namespace MV {
    extern void LogEnsure(MV::string_view expression, MV::string_view file, uint32_t line);
}


#endif //MV_EDITOR_ENSURELOG_H
