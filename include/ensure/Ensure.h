//
// Created by Matty on 2022-09-19.
//

#ifndef MV_EDITOR_ENSURE_H
#define MV_EDITOR_ENSURE_H

#include <ensure/EnsureLog.h>

#ifdef _MSC_VER
#define MV_DEBUG_BREAK() __debugbreak()
#elif __GNUC__
#include <csignal>
#define MV_DEBUG_BREAK() abort();
#endif

#define ENSURE_DEBUG_BREAK() MV_DEBUG_BREAK()
//TODO: Add check if debugger
#define ENSURE_IMPL(Capture, Always, InExpression, ...) \
	((InExpression) && (([Capture] () -> bool \
	{ \
		static bool __executed = false; \
		if ((!__executed || Always)) \
		{ \
			__executed = true;                                   \
            MV::LogEnsure(#InExpression, __FILE__, __LINE__);     \
			if (false) \
			{ \
				return false; \
			} \
			return true; \
		} \
		return false; \
	})() && ([] () -> bool { ENSURE_DEBUG_BREAK(); return true; } ())))

#define ENSURE(InExpression) ENSURE_IMPL(,false, InExpression)

#define ENSURE_VALID(Object, ...) if(ENSURE_IMPL(,false, (Object == nullptr))) return __VA_ARGS__;

#define ENSURE_TRUE(InExpression, ...) if(ENSURE_IMPL(,false, !(InExpression))) return __VA_ARGS__;

#define ENSURE_NO_ENTRY(...) if(ENSURE_IMPL(,false, true)) return __VA_ARGS__;

#endif //MV_EDITOR_ENSURE_H
