//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_LOGGER_H
#define MV_ENGINE_LOGGER_H

#include <EngineMinimal.h>
#include <iostream>

namespace MV {

    template<typename ...Args>
    static constexpr void Assert(bool val, Args &&...args) {
        if (!val) {
            std::cerr << "Assertion Failed: ";
            (std::cerr<< ... << args) << std::endl;
            MV_DEBUG_BREAK();
        }
    }

    class Logger {
    private:
        void init();

        bool m_pRendererDebuggingEnabled = true;
        bool m_pScriptDebuggingEnabled = true;

        void logInfo(eastl::string_view msg);
        void logDebug(eastl::string_view msg);
        void logWarning(eastl::string_view msg);
        void logError(eastl::string_view msg);

        void render_logInfo(eastl::string_view msg);
        void render_logDebug(eastl::string_view msg);
        void render_logWarning(eastl::string_view msg);
        void render_logError(eastl::string_view msg);

        void script_logInfo(eastl::string_view msg);
        void script_logDebug(eastl::string_view msg);
        void script_logWarning(eastl::string_view msg);
        void script_logError(eastl::string_view msg);
    public:
        friend class Engine;
        template<typename... Args>
        void LogInfo(fmt::format_string<Args...> fmt, Args &&...args) {
            logInfo(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void LogError(fmt::format_string<Args...> fmt, Args &&...args) {
            logError(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void LogWarning(fmt::format_string<Args...> fmt, Args &&...args) {
            logWarning(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void LogDebug(fmt::format_string<Args...> fmt, Args &&...args) {
            logDebug(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void Render_LogInfo(fmt::format_string<Args...> fmt, Args &&...args) {
            render_logInfo(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void Render_LogWarning(fmt::format_string<Args...> fmt, Args &&...args) {
            render_logWarning(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void Render_LogError(fmt::format_string<Args...> fmt, Args &&...args) {
            render_logError(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void Render_LogDebug(fmt::format_string<Args...> fmt, Args &&...args) {
            render_logDebug(fmt::format(fmt, args...).c_str());
        };

        template<typename... Args>
        void Script_LogInfo(fmt::format_string<Args...> fmt, Args &&...args) {
            script_logInfo(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void Script_LogError(fmt::format_string<Args...> fmt, Args &&...args) {
            script_logError(fmt::format(fmt, args...).c_str());
        };
        template<typename... Args>
        void Script_LogWarning(fmt::format_string<Args...> fmt, Args &&...args) {
            script_logWarning(fmt::format(fmt, args...).c_str());
        };

        template<typename... Args>
        void Script_LogDebug(fmt::format_string<Args...> fmt, Args &&...args) {
            script_logDebug(fmt::format(fmt, args...).c_str());
        };
    };
}


#endif //MV_ENGINE_LOGGER_H
