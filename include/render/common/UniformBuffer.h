//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_UNIFORMBUFFER_H
#define MV_ENGINE_UNIFORMBUFFER_H

#include "Buffer.h"
#include "FrameContext.h"

namespace MV {
    class UniformBuffer : public Buffer {
    protected:
        size_t m_pSize;
    public:
        explicit UniformBuffer(size_t size) : m_pSize(size) {};

        virtual void Bind(FrameContext & context) = 0;

        virtual void Upload(const void * data, size_t size) = 0;
    };
}


#endif //MV_ENGINE_UNIFORMBUFFER_H
