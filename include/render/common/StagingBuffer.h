//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_STAGINGBUFFER_H
#define MV_ENGINE_STAGINGBUFFER_H

#include "Buffer.h"
#include "FrameContext.h"
#include "RenderingResource.h"

namespace MV {
    class StagingBuffer : public Buffer, public RenderingResource {
    protected:
        size_t m_pSize;
    public:
        explicit StagingBuffer(size_t size) : m_pSize(size) {};

        virtual void Upload(const void *data,size_t size) = 0;

        [[nodiscard]] size_t GetSize() const { return m_pSize; }
    };
}


#endif //MV_ENGINE_STAGINGBUFFER_H
