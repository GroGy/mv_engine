//
// Created by Matty on 2021-10-14.
//

#ifndef MV_ENGINE_FRAMEBUFFER_H
#define MV_ENGINE_FRAMEBUFFER_H

#include <limits>
#include <EngineMinimal.h>

namespace MV {
    class ColorAttachment {
    protected:
        uint32_t m_pID = (std::numeric_limits<uint32_t>::max)();
    public:
        explicit ColorAttachment(uint32_t id) : m_pID(id){};
    };

    class FrameBuffer {
    private:
        bool m_pMarkedForDeletion = false;
    protected:
        uint32_t m_pWidth = 0, m_pHeight = 0;
        bool m_pValid = false;
    public:
        FrameBuffer() = default;

        FrameBuffer(uint32_t width,uint32_t height);

        void SetSize(uint32_t width, uint32_t height);

        virtual void Init() = 0;

        virtual void Cleanup() = 0;

        void MarkForDeletion();

        [[nodiscard]] bool MarkedForDeletion() const;
    };
}

#endif //MV_ENGINE_FRAMEBUFFER_H
