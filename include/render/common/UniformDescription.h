//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_UNIFORMDESCRIPTION_H
#define MV_ENGINE_UNIFORMDESCRIPTION_H

#ifdef VK_BACKEND_SUPPORT
#include <vulkan/vulkan.h>
#endif

#include "UniformBuffer.h"
#include "Texture.h"

namespace MV {
    enum class UniformDescriptorType {
        BUFFER = 0,
        TEXTURE
    };

    struct UniformDescription {
    public:
        UniformDescription() = default;
        //UniformDescription (const UniformDescription & other) = delete;
        //UniformDescription& operator=(const UniformDescription & other) = delete;

        UniformDescription(ShaderStage_ stage, uint32_t binding, uint32_t descriptorCount,
                           uint32_t offset,
                           uint32_t size,UniformDescriptorType type, MV::string_view name) : shaderStage(
                stage), binding(binding), descriptorCount(descriptorCount), size(size),
                                                                       offset(offset), type(type), name(name) {};
        ShaderStage_ shaderStage{};
        uint32_t binding{};
        uint32_t descriptorCount{};
        uint32_t offset{};
        uint32_t size{};
        UniformDescriptorType type{};
        uint32_t version = 0;
        uint32_t pipelineID = (std::numeric_limits<uint32_t>::max)();
        MV::vector<MV::rc<UniformBuffer>> uniformBuffers{};
        MV::string name;
#ifdef VK_BACKEND_SUPPORT
        MV::vector<VkDescriptorSet> descriptorSets{};
        uint32_t descriptorSetLayoutID = 0;
        VkDescriptorSetLayout layout = VK_NULL_HANDLE;
#endif

    };
}

#endif //MV_ENGINE_UNIFORMDESCRIPTION_H
