//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_TEXTURE_H
#define MV_ENGINE_TEXTURE_H

#include "FrameContext.h"
#include "StagingBuffer.h"
#include "TextureFormat.h"
#include "TextureFilter.h"
#include "TextureAddressingMode.h"

namespace MV {
    enum class TextureSamples : uint32_t {
        SAMPLE_COUNT_1 = 0x00000001,
        SAMPLE_COUNT_2 = 0x00000002,
        SAMPLE_COUNT_4 = 0x00000004,
        SAMPLE_COUNT_8 = 0x00000008,
        SAMPLE_COUNT_16 = 0x00000010,
        SAMPLE_COUNT_32 = 0x00000020,
        SAMPLE_COUNT_64 = 0x00000040,
    };

    enum class TextureUsage : uint32_t {
        TEXTURE,
        ATTACHMENT
    };


    class TextureBlueprint {
    public:
        uint32_t m_Height = 0;
        uint32_t m_TextureChannels = 0;
        uint32_t m_Mipmaps = 1;
        TextureFormat m_Format = TextureFormat::R8G8B8A8_SRGB;
        TextureSamples m_Samples = TextureSamples::SAMPLE_COUNT_1;
        uint32_t m_Flags = 0; //For now, this is vulkan only
        uint32_t m_Width = 0;
        TextureFilter m_Filter = TextureFilter::LINEAR;
        TextureAddressingMode m_AddressingMode = TextureAddressingMode::REPEAT;
        bool m_EnableAnisotropy = false;
        float m_MaxAnisotropy = 0.0f;
        TextureUsage m_Usage = TextureUsage::TEXTURE;
        // Percentage close filtering
            //compareEnable = VK_FALSE;
            //compareOp = VK_COMPARE_OP_ALWAYS;

        // Mipmaps
            //mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
            //mipLodBias = 0.0f;
            //minLod = 0.0f;
            //maxLod = 0.0f;


    };

    class Texture : public RenderingResource {
    public:
        Texture() = default;

        explicit Texture(TextureBlueprint &&blueprint);

        virtual void Upload(const MV::rc<StagingBuffer> &staging) = 0;

        virtual void Init() = 0;

        virtual void Cleanup() = 0;

        [[nodiscard]] bool IsLoaded() const { return m_pLoaded; }

        [[nodiscard]] glm::uvec2 GetSize() const { return glm::uvec2{m_pBlueprint.m_Width, m_pBlueprint.m_Height};};
    private:
        bool m_pValid = false;
    protected:
        TextureBlueprint m_pBlueprint{};
        bool m_pLoaded = false;

        [[nodiscard]] bool isValid() const { return m_pValid; };
    };
}


#endif //MV_ENGINE_TEXTURE_H
