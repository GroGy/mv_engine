//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_TEXTUREADDRESSINGMODE_H
#define MV_ENGINE_TEXTUREADDRESSINGMODE_H

namespace MV {
    enum class TextureAddressingMode : uint32_t {
        REPEAT = 0,
        MIRRORED_REPEAT = 1,
        CLAMP_TO_EDGE = 2,
        CLAMP_TO_BORDER = 3,
        MIRROR_CLAMP_TO_EDGE = 4,
    };
}

#endif //MV_ENGINE_TEXTUREADDRESSINGMODE_H
