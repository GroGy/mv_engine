//
// Created by Matty on 2022-03-10.
//

#ifndef MV_EDITOR_PROPERTYSTORAGE_H
#define MV_EDITOR_PROPERTYSTORAGE_H

#include "IRenderedProperty.h"
#include "../../common/Property.h"

namespace MV {
    class IPropertyStorage {
    public:
        virtual ~IPropertyStorage() = default;
        virtual void for_each(const eastl::function<void(Property*)> & callback) = 0;
        virtual void for_each(const eastl::function<void(IRenderedProperty*)> & callback) = 0;
        virtual void reserve(size_t size) = 0;
        [[nodiscard]] virtual size_t size() const = 0;
        virtual void clear() = 0;
        [[nodiscard]] virtual bool empty() const = 0;
        virtual void erase(size_t index) = 0;
        virtual bool is_rendered_property() const = 0;
        virtual size_t get_last_index() const = 0;

        virtual size_t create() = 0;

        virtual void construct_from_mono(){};

        virtual Property * get_property(size_t index) = 0;

        virtual void sync_property_fields(const MV::ScriptClassField & field, MV::Script & script, const nlohmann::json & data) const = 0;
    protected:

    };

    template <typename T>
    class PropertyStorage : public IPropertyStorage {
    public:
        ~PropertyStorage() override;
        PropertyStorage();

        template <typename ...Args>
        T& emplace_back(Args&& ... args);
        T& push_back(T && val);
        T& at(size_t index);
        const T& at(size_t index) const;
        T& operator[](size_t index) { return m_pData[index]; }
        const T& operator[](size_t index) const { return m_pData[index]; }
        typename MV::vector<T>::iterator begin() { return m_pData.begin(); }
        typename MV::vector<T>::iterator end() { return m_pData.end(); }
        typename MV::vector<T>::const_iterator begin() const { return m_pData.begin(); }
        typename MV::vector<T>::const_iterator end()   const { return m_pData.end(); }

        //template <eastl::enable_if<eastl::is_base_of_v<T, IRenderedProperty>>>
        void for_each(const eastl::function<void(IRenderedProperty *)> &callback) override;
        void for_each(const eastl::function<void(Property *)> &callback) override;
        void for_each(eastl::function<void(T *)> &callback);

        void reserve(size_t size) override;

        [[nodiscard]] size_t size() const override;

        void clear() override;

        bool empty() const override;

        void erase(size_t index) override;

        bool is_rendered_property() const override;

        size_t get_last_index() const override;

        void construct_from_mono() override;

        size_t create() override;

        Property *get_property(size_t index) override;

        void sync_property_fields(const ScriptClassField & field,Script &script, const json &data) const override;

    private:
        MV::vector<T> m_pData{};
        MV::queue<size_t> m_pFreeItems{};
        size_t m_pLastInsertedIndex = 0;
    };

    template<typename T>
    template<typename... Args>
    T &PropertyStorage<T>::emplace_back(Args &&... args) {
        if(m_pFreeItems.empty()) {
            m_pLastInsertedIndex = m_pData.size();
            return m_pData.emplace_back(args...);
        }
        size_t index = m_pFreeItems.front();
        m_pFreeItems.pop();

        m_pData[index] = MV::move(T{args...});

        m_pLastInsertedIndex = index;

        return m_pData[index];
    }

    template<typename T>
    T &PropertyStorage<T>::push_back(T &&val) {
        if(m_pFreeItems.empty()) {
            m_pLastInsertedIndex = m_pData.size();
            return m_pData.push_back(MV::move(val));
        }
        size_t index = m_pFreeItems.front();
        m_pFreeItems.pop();

        m_pData[index] = MV::move(val);

        m_pLastInsertedIndex = index;

        return m_pData[index];
    }

    template<typename T>
    T &PropertyStorage<T>::at(size_t index) {
        return m_pData.at(index);
    }

    template<typename T>
    const T &PropertyStorage<T>::at(size_t index) const {
        return m_pData.at(index);
    }

    template<typename T>
    void PropertyStorage<T>::for_each(const eastl::function<void(Property *)> &callback) {
        for (size_t i = 0; i < m_pData.size(); ++i) {
            callback(&m_pData[i]);
        }
    }

    template<typename T>
    void PropertyStorage<T>::for_each(const eastl::function<void(IRenderedProperty *)> &callback) {
        if(!is_rendered_property())
            return;
        for (size_t i = 0; i < m_pData.size(); ++i) {
            callback((IRenderedProperty*)&m_pData[i]);
        }
    }

    template<typename T>
    void PropertyStorage<T>::reserve(size_t size) {
        m_pData.reserve(size);
    }

    template<typename T>
    size_t PropertyStorage<T>::size() const {
        return m_pData.size();
    }

    template<typename T>
    PropertyStorage<T>::PropertyStorage() {
        static_assert(eastl::is_base_of_v<Property, T> && "T must be base of IProperty");
    }

    template<typename T>
    void PropertyStorage<T>::for_each(eastl::function<void(T *)> &callback) {
        for (auto & val : m_pData) {
            callback(&val);
        }
    }

    template<typename T>
    PropertyStorage<T>::~PropertyStorage() {

    };

    template<typename T>
    void PropertyStorage<T>::clear() {
        m_pData.clear();
        m_pFreeItems = MV::queue<size_t>{};
    }

    template<typename T>
    bool PropertyStorage<T>::empty() const {
        return m_pData.empty();
    }

    template<typename T>
    void PropertyStorage<T>::erase(size_t index) {
        m_pData[index].Destroy();
        m_pFreeItems.push(index);
    }

    template<typename T>
    bool PropertyStorage<T>::is_rendered_property() const {
        return eastl::is_base_of_v<IRenderedProperty, T>;
    }

    template<typename T>
    size_t PropertyStorage<T>::get_last_index() const {
        return m_pLastInsertedIndex;
    }

    template<typename T>
    void PropertyStorage<T>::construct_from_mono() {
        IPropertyStorage::construct_from_mono();
    }

    template<typename T>
    size_t PropertyStorage<T>::create() {
        emplace_back();
        return get_last_index();
    }

    template<typename T>
    Property *PropertyStorage<T>::get_property(size_t index) {
        if(index > m_pData.size() || index < 0)
            return nullptr;
        return &m_pData[index];
    }

    template<typename T>
    void PropertyStorage<T>::sync_property_fields(const ScriptClassField & field,Script &script, const json &data) const {
        T::SyncPropertyData(field, script, data);
    }
}


#endif //MV_EDITOR_PROPERTYSTORAGE_H
