//
// Created by Matty on 2022-03-07.
//

#ifndef MV_EDITOR_IRENDEREDPROPERTY_H
#define MV_EDITOR_IRENDEREDPROPERTY_H

#include "FrameContext.h"
#include "../../log/Logger.h"

namespace MV {
    class IRenderedProperty {
    public:
        virtual void Render(FrameContext & context) = 0;
    };

    using PropertyIDType = uint32_t;

    template <typename T>
    class PropertyID
    {
    public:
        static PropertyIDType ID();
        static bool     Registered();
    private:
        static PropertyIDType s_ID;
        friend class PropertyManager;
        friend class RegisteredProperties;
    };

    template <typename T>
    PropertyIDType PropertyID<T>::s_ID = (std::numeric_limits<PropertyIDType>::max)();

    template<typename T>
    inline PropertyIDType PropertyID<T>::ID()
    {
        Assert(s_ID != (std::numeric_limits<PropertyIDType>::max)(), "Component type was not registered");
        return s_ID;
    }

    template<typename T>
    inline bool PropertyID<T>::Registered()
    {
        return s_ID != (std::numeric_limits<PropertyIDType>::max)();
    }

}


#endif //MV_EDITOR_IRENDEREDPROPERTY_H
