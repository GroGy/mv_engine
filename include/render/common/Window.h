//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_WINDOW_H
#define MV_ENGINE_WINDOW_H

#include <vec4.hpp>
#include "../../common/Types.h"

namespace MV {
    class Window {
    private:
        /// Background color
        glm::vec4 m_pBackgroundColor;
        /// Window name
        MV::string m_pWindowName;
        /// GLFW Window object
        void * m_pWindowImpl;
        /// Window dimensions
        static uint32_t m_pWindowWidth, m_pWindowHeight;
        Window() = default;

        void cleanup();
    public:
        /// Gets window width
        /// \return Width of window
        static uint32_t GetWindowWidth() { return m_pWindowWidth; };
        /// Gets window height
        /// \return Height of window
        static uint32_t GetWindowHeight() { return m_pWindowHeight; };
        friend class WindowBuilder;
        friend class Engine;
        /// Gets window name
        /// \return Window name
        [[nodiscard]] MV::string GetWindowName() const;
        void SetWindowName(const MV::string & name);

        void * GetContext() const;
    };

    class WindowBuilder : public eastl::enable_shared_from_this<WindowBuilder> {
    public:
        WindowBuilder(uint32_t width, uint32_t height);
        MV::rc<WindowBuilder> setWindowName(const MV::string& name);
        MV::rc<WindowBuilder> setBackgroundColor(const glm::vec4& bgColor);
        MV::rc<WindowBuilder> setResizable(bool resizable);
        MV::rc<WindowBuilder> setMaximized(bool maximized);
        MV::Window* finish();
    private:
        bool m_pEnableResize = false;
        bool m_pMaximized = false;
        bool m_pHasColor = false;
        glm::vec4 m_pBackgroundColor;
        bool m_pHasName = false;
        MV::string m_pWindowName;
        bool m_pHasDecorations = false;
        uint32_t m_pWidth, m_pHeight = 0;
    };
}


#endif //MV_ENGINE_WINDOW_H
