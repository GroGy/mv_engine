//
// Created by Matty on 2021-10-31.
//

#ifndef MV_ENGINE_RENDERINGRESOURCE_H
#define MV_ENGINE_RENDERINGRESOURCE_H

namespace MV {
    class RenderingResource {
    protected:
        bool m_pDataLoaded = false;
        uint64_t m_pId = 0;
    public:
        [[nodiscard]] bool IsDataLoaded() const { return m_pDataLoaded; }
        virtual ~RenderingResource() = default;
        uint64_t GetID() const { return m_pId; };

        friend class Renderer;
    };
}

#endif //MV_ENGINE_RENDERINGRESOURCE_H
