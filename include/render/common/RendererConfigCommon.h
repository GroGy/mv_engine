//
// Created by Matty on 2021-10-03.
//

#ifndef MV_ENGINE_RENDERERCONFIGCOMMON_H
#define MV_ENGINE_RENDERERCONFIGCOMMON_H

#include <functional>
#include "Window.h"

namespace MV {
    class RendererConfigCommon {
    public:
        explicit RendererConfigCommon(std::unique_ptr<MV::Window> & window);
        std::unique_ptr<MV::Window> & m_Window;
        MV::string m_ApplicationName = "MV Engine";
        bool m_Editor = false;
        std::function<void()> m_ResizeCallback;
    };
}


#endif //MV_ENGINE_RENDERERCONFIGCOMMON_H
