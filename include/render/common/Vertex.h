//
// Created by Matty on 2021-10-08.
//

#ifndef MV_ENGINE_VERTEX_H
#define MV_ENGINE_VERTEX_H

#include "ShaderPipeline.h"

namespace MV {
    template <typename T>
    class VertexBase {
        static MV::vector<VertexBindingDescriptor> GetBindingDescription() {
            static_assert(std::is_base_of_v<VertexBase,T>());
            return T::GetBindingDescription();
        }

        static MV::vector<VertexAttributeDescriptor> GetAttributeDescriptions() {
            static_assert(std::is_base_of_v<VertexBase,T>());
            return T::GetAttributeDescriptions();
        }
    };

    class Vertex : VertexBase<Vertex>{
    private:
        glm::vec3 m_pPos;
        glm::vec3 m_pColor;
    public:
        static MV::vector<VertexBindingDescriptor> GetBindingDescription() {
                    MV::vector<VertexBindingDescriptor> bindingDescription(1);
                    bindingDescription[0].binding = 0;
                    bindingDescription[0].stride = sizeof(Vertex);
                    bindingDescription[0].inputRate = VertexInputRate::PER_VERTEX;
                    return bindingDescription;
        }

        static MV::vector<VertexAttributeDescriptor> GetAttributeDescriptions() {
            MV::vector<VertexAttributeDescriptor> attributeDescriptions(2);

            attributeDescriptions[0].binding = 0;
            attributeDescriptions[0].location = 0;
            attributeDescriptions[0].format = AttributeFormat<decltype(m_pPos)>::GetAttributeType();
            attributeDescriptions[0].offset = offsetof(Vertex, m_pPos);

            attributeDescriptions[1].binding = 0;
            attributeDescriptions[1].location = 1;
            attributeDescriptions[1].format = AttributeFormat<decltype(m_pColor)>::GetAttributeType();
            attributeDescriptions[1].offset = offsetof(Vertex, m_pColor);

            return attributeDescriptions;
        }
    };


}

#endif //MV_ENGINE_VERTEX_H
