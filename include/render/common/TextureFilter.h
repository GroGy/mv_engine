//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_TEXTUREFILTER_H
#define MV_ENGINE_TEXTUREFILTER_H
namespace MV {
    enum class TextureFilter : uint32_t {
        NEAREST = 0,
        LINEAR = 1,
        CUBIC_IMG = 1000015000
    };
}
#endif //MV_ENGINE_TEXTUREFILTER_H
