//
// Created by Matty on 2021-10-05.
//

#ifndef MV_ENGINE_SHADERPIPELINE_H
#define MV_ENGINE_SHADERPIPELINE_H

#include <utility>

#include "Shader.h"
#include "FrameContext.h"
#include "AttributeFormat.h"
#include "ShaderStage.h"
#include "UniformDescription.h"

namespace MV {
    enum class VertexInputRate {
        PER_VERTEX = 0,
        PER_INSTANCE = 1,
    };

    struct UniformBlueprint {
    public:
        UniformBlueprint() = default;

        ///
        /// \param descriptorCount Amount of elements in array of UBO, if single UBO is used, just set it to 1
        UniformBlueprint(ShaderStage_ stage, uint32_t binding, uint32_t descriptorCount,
                         uint32_t offset,
                         uint32_t size) : shaderStage(
                stage), binding(binding), descriptorCount(descriptorCount), size(size),
                                          offset(offset), type(UniformDescriptorType::BUFFER) {};

        ///
        /// \param descriptorCount Amount of elements in array of UBO, if single UBO is used, just set it to 1
        UniformBlueprint(ShaderStage_ stage, uint32_t binding, uint32_t descriptorCount) : shaderStage(
                stage), binding(binding), descriptorCount(descriptorCount), type(UniformDescriptorType::TEXTURE) {};

        ShaderStage_ shaderStage;
        uint32_t binding;
        uint32_t descriptorCount;
        uint32_t offset{};
        uint32_t size{};
        UniformDescriptorType type;
    };

    struct VertexAttributeDescriptor {
        uint32_t location;
        uint32_t binding;
        AttributeType format;
        uint32_t offset;
    };

    struct VertexBindingDescriptor {
        uint32_t binding;
        uint32_t stride;
        VertexInputRate inputRate;
    };

    struct PushConstantInfo {
        PushConstantInfo() = default;

        PushConstantInfo(ShaderStage_ shaderStage, uint32_t offset, uint32_t size) : shaderStage(shaderStage), offset(offset), size(size) {

        };

        ShaderStage_ shaderStage;
        uint32_t offset;
        uint32_t size;
    };

    struct PushConstantDescription {
        PushConstantDescription() = default;

        PushConstantDescription(ShaderStage_ shaderStage, uint32_t offset, uint32_t size) : m_ShaderStage(shaderStage), m_Offset(offset),m_Size(size){};

        ShaderStage_ m_ShaderStage;
        uint32_t m_Offset;
        uint32_t m_Size;
    };

    class PipelineBlueprint {
    protected:
        uint32_t m_pCacheSize = 128;

        MV::vector<MV::rc<Shader>> m_pShaders;
        MV::vector<ShaderType> m_pAssignedStages;

        MV::vector<VertexBindingDescriptor> m_pVertexInputDescription{};
        MV::vector<VertexAttributeDescriptor> m_pVertexAttributeDescription{};

        //TODO: Allow checking for push constants support and if there is one, use them
        MV::vector_map<MV::string,PushConstantInfo> m_pPushConstants;
        MV::vector_map<MV::string,UniformBlueprint> m_pUniformDescriptorBlueprints;

        //VulkanPipelineBlueprintResult buildPipeline();

        /*TODO: Add depth stencil support
        bool m_pHasDepthStencilState = false;
        VkPipelineDepthStencilStateCreateInfo m_pDepthStencilState{};
        */

        void moveFromOther(PipelineBlueprint &&o);

        bool m_pEnableAlphaBlending = false;
    public:
        friend class ShaderPipeline;
        friend class VulkanPipeline;

        PipelineBlueprint() = default;

        PipelineBlueprint(PipelineBlueprint &&o) noexcept ;

        PipelineBlueprint &operator=(PipelineBlueprint &&o) noexcept;

        void EnableAlphaBlending(bool value);

        UniformDescription AddUniform(MV::string_view name, const UniformBlueprint &uniformDescriptor);

        PushConstantDescription AddPushConstant(MV::string_view name, ShaderStage_ shaderStage, uint32_t offset, uint32_t size);

        void SetVertexInputDescription(const MV::vector<VertexBindingDescriptor> &inputBindingDescription,
                                       const MV::vector<VertexAttributeDescriptor> &attributeDescription);

        void AddShader(const MV::rc<Shader> &shader);

        //void SetDepthStencilState(const VkPipelineDepthStencilStateCreateInfo &info);

        //void SetCachedDescriptorSets(uint32_t cacheSize);

        [[nodiscard]]
        const vector <VertexBindingDescriptor> &GetVertexInputDescription() const;
    };

    class ShaderPipeline : public RenderingResource {
    public:
        explicit ShaderPipeline(PipelineBlueprint && bp);

        void AddShader(const MV::rc<Shader> &shader);

        virtual void Init() = 0;

        virtual void Cleanup() = 0;

        virtual void Bind(FrameContext &context) = 0;

        void Refresh();

        virtual void BindWireframe(FrameContext &context) = 0;

        virtual void PreBindUniform(FrameContext &context, UniformDescription & description, const void* data, size_t size, bool update) = 0;
        virtual void PreBindUniform(FrameContext &context, UniformDescription & description, const MV::rc<Texture> & texture, bool update) = 0;
        virtual void BindPushConstant(FrameContext &context, PushConstantDescription description, const void * data, size_t size) = 0;

        virtual void BindUniforms(FrameContext &context, const MV::vector<eastl::reference_wrapper<UniformDescription>> & uniforms) = 0;

        virtual UniformDescription CreateUniform(MV::string_view name);
        /// Elements are sorted, so care, they WILL NOT be in order they were inserted
        virtual UniformDescription CreateUniform(size_t index);
        virtual PushConstantDescription CreatePushConstant(MV::string_view name);
        /// Elements are sorted, so care, they WILL NOT be in order they were inserted
        virtual PushConstantDescription CreatePushConstant(size_t index);
    public:
        virtual void buildPipeline() = 0;
        PipelineBlueprint m_pBlueprint;
        bool m_pLoaded = false;
        bool m_pValid = false;
        uint32_t m_pVersion = 0;
        uint32_t m_pID = (std::numeric_limits<uint32_t>::max)();

        [[nodiscard]]
        const MV::vector<MV::rc<Shader>> & getShaders() const;
        [[nodiscard]]
        const MV::vector<VertexBindingDescriptor> & getVertexBindingDescriptors() const;
        [[nodiscard]]
        const MV::vector<VertexAttributeDescriptor> & getVertexAttributeDescriptions() const;
        [[nodiscard]]
        const MV::vector_map<MV::string,PushConstantInfo> & getPushConstants() const;
        [[nodiscard]]
        const MV::vector_map<MV::string,UniformBlueprint> & getUniformBlueprints() const;
        [[nodiscard]]
        bool isAlphaBlendingEnabled() const;
        [[nodiscard]]
        uint32_t getCacheSize() const;
    };
}


#endif //MV_ENGINE_SHADERPIPELINE_H
