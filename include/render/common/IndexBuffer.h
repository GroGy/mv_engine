//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_INDEXBUFFER_H
#define MV_ENGINE_INDEXBUFFER_H

#include "Buffer.h"
#include "FrameContext.h"
#include "StagingBuffer.h"

namespace MV {
    class IndexBuffer : public Buffer {
    protected:
        size_t m_pSize;
    public:
        explicit IndexBuffer(size_t size) : m_pSize(size) {};

        virtual void Bind(FrameContext & context) =0;

        [[nodiscard]]
        size_t GetIndexCount() const { return m_pSize / sizeof(uint32_t); };

        virtual void Upload(const MV::rc<StagingBuffer> & staging, size_t dataSize = 0) = 0;

        [[nodiscard]]
        size_t GetSize() const {return m_pSize;};
    };
}


#endif //MV_ENGINE_INDEXBUFFER_H
