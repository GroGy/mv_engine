//
// Created by Matty on 2021-10-05.
//

#ifndef MV_ENGINE_SHADER_H
#define MV_ENGINE_SHADER_H

#include <limits>
#include <EngineMinimal.h>

namespace MV {
    enum class ShaderType : uint8_t {
        VERTEX = 0,
        FRAGMENT = 1,
        GEOMETRY = 2,
        TESSELATION = 3,
        COMPUTE = 4,

        SIZE
    };

    class Shader {
    public:
        Shader() = default;
        Shader(MV::string path, ShaderType type)
                : m_pPath(MV::move(path)), m_pType(type), m_pValid(true), m_pID(getID()){
        }

        [[nodiscard]]
        bool IsLoaded() const { return m_pLoaded; }

        virtual void Init() = 0;
        virtual void Cleanup() = 0;
        void Refresh();

        [[nodiscard]]
        ShaderType GetType() const { return m_pType; };
    protected:
        bool m_pValid = false;
        MV::string m_pPath{};
        ShaderType m_pType = ShaderType::SIZE;
        bool m_pLoaded = false;
    private:
        uint64_t getID();
        uint64_t m_pID = (std::numeric_limits<uint64_t>::max)();
    };
}


#endif //MV_ENGINE_SHADER_H
