//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_RENDERERBACKEND_H
#define MV_ENGINE_RENDERERBACKEND_H

namespace MV {
    enum class RendererBackend {
        VULKAN = 1,
        OPENGL = 2,
        DX11 = 3,
        DX12 = 4
    };
}

#endif //MV_ENGINE_RENDERERBACKEND_H
