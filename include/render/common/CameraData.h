//
// Created by Matty on 2021-10-24.
//

#ifndef MV_ENGINE_CAMERADATA_H
#define MV_ENGINE_CAMERADATA_H

namespace MV {
    struct CameraData {
        glm::mat4 m_ViewMatrix{};
        glm::mat4 m_ProjMatrix{};
        glm::vec2 m_Position{};
        glm::vec2 m_TargetPosition{};
    };
}

#endif //MV_ENGINE_CAMERADATA_H
