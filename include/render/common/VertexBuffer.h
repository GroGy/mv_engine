//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_VERTEXBUFFER_H
#define MV_ENGINE_VERTEXBUFFER_H

#include "Buffer.h"
#include "FrameContext.h"
#include "StagingBuffer.h"
#include "RenderingResource.h"

namespace MV {
    class VertexBuffer : public Buffer, public RenderingResource {
    protected:
        size_t m_pSize;
        size_t m_pSingleVertSize;
    public:
        explicit VertexBuffer(size_t size, size_t singleVertexSize) : m_pSize(size), m_pSingleVertSize(singleVertexSize) {};

        virtual void Bind(FrameContext & context, uint32_t binding = 0) =0;

        [[nodiscard]]
        virtual size_t GetVertexCount() const = 0;

        [[nodiscard]]
        size_t GetSize() const { return m_pSize; }

        [[nodiscard]]
        size_t GetStride() const { return m_pSingleVertSize; }


        virtual void Upload(const MV::rc<StagingBuffer> & staging, size_t dataSize = 0) = 0;
    };
}


#endif //MV_ENGINE_VERTEXBUFFER_H
