//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_BUFFER_H
#define MV_ENGINE_BUFFER_H

namespace MV {
    class Buffer {
    private:

    public:
        virtual void Init() = 0;
        virtual void Cleanup() = 0;
    protected:

    };
}


#endif //MV_ENGINE_BUFFER_H
