//
// Created by Matty on 2021-10-14.
//

#ifndef MV_ENGINE_RENDERPASS_H
#define MV_ENGINE_RENDERPASS_H

#include "FrameContext.h"

namespace MV {
    class RenderPass {
    private:
        uint32_t m_pID = 0;
    protected:
        glm::vec4 m_pClearColor;
    public:
        explicit RenderPass(const glm::vec4 & clearColor);

        void SetClearColor(const glm::vec4 & clearColor);

        virtual void Init() = 0;
        virtual void Cleanup() = 0;
        virtual void Bind(FrameContext & context) = 0;

        [[nodiscard]]
        uint32_t GetID() const { return m_pID;}
    };
}


#endif //MV_ENGINE_RENDERPASS_H
