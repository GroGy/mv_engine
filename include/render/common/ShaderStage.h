//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_SHADERSTAGE_H
#define MV_ENGINE_SHADERSTAGE_H

namespace MV {
    using ShaderStage_ = uint32_t;
    namespace ShaderStage {
        enum Flags : uint32_t {
            VERTEX = 0x00000001,
            TESSELLATION_CONTROL = 0x00000002,
            TESSELLATION_EVALUATION = 0x00000004,
            GEOMETRY = 0x00000008,
            FRAGMENT = 0x00000010,
            COMPUTE = 0x00000020,
            ALL_GRAPHICS = 0x0000001F,
            ALL = 0x7FFFFFFF,
        };
    }
}

#endif //MV_ENGINE_SHADERSTAGE_H
