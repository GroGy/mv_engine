//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_FRAMECONTEXT_H
#define MV_ENGINE_FRAMECONTEXT_H

#include <glm.hpp>

#ifdef VK_BACKEND_SUPPORT

#include <vulkan/vulkan.h>
#include "../vulkan/VulkanSwapChain.h"

#endif


namespace MV {
    struct MaterialInput {
        MV::string m_UniformBufferName;
        MV::vector<uint8_t> m_UniformBuffer;
        MV::string m_UniformTextureName;
        MV::vector<uint64_t> m_UniformTextures;

        inline bool operator==(const MaterialInput & other) const {
            return other.m_UniformBuffer == m_UniformBuffer && other.m_UniformTextures == m_UniformTextures && m_UniformBufferName == other.m_UniformBufferName;
        }
    };

    struct MaterialDrawData {
        MV::array<MV::vector<uint8_t>, 3> m_VertexBuffers{};
        MV::vector<uint32_t> m_Indices{};
        bool m_Instanced = false;
        uint32_t m_InstanceCount = 0;
        glm::mat4 m_ModelMatrix = glm::mat4{1.0f};
    };
}
namespace eastl {
    template<>
    struct hash<MV::vector<uint8_t>> {
        size_t operator()(MV::vector<uint8_t> const &vec) const {
            size_t seed = vec.size();
            for (auto &i: vec) {
                seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
            }
            return seed;
        }
    };

    template<>
    struct hash<MV::vector<uint64_t>> {
        size_t operator()(MV::vector<uint64_t> const &vec) const {
            size_t seed = vec.size();
            for (auto &i: vec) {
                seed ^= i + 0x7d11a67 + (seed << 6) + (seed >> 2);
            }
            return seed;
        }
    };

    template<>
    struct hash<MV::MaterialInput> {
        size_t operator()(const MV::MaterialInput & val) const {
            hash<decltype(val.m_UniformTextures)> H1;
            hash<decltype(val.m_UniformBuffer)> H2;
            hash<decltype(val.m_UniformBufferName)> H3;
            return H1(val.m_UniformTextures) ^ H2(val.m_UniformBuffer) ^ H3(val.m_UniformBufferName);
        }
    };
}


namespace MV {
    struct PipelineMaterials {
        MV::unordered_map<MaterialInput, MaterialDrawData> m_Commands{};
    };

    class FrameContext {
    public:
        bool m_Invalid = false;
        bool m_ForceFinish = false;
        bool m_IsRenderPassActive = false;
        uint32_t m_ActiveRenderPassID = 0;
#ifdef VK_BACKEND_SUPPORT
        VkCommandBuffer m_FrameCommandBuffer = VK_NULL_HANDLE;
        uint32_t m_SwapImageIndex = 0;
        VulkanSwapChain *m_SwapChain = nullptr;
#endif
        glm::mat4 m_ViewMatrix{};
        glm::mat4 m_ProjMatrix{};

        /// Shader ID to requests
        eastl::hash_map<uint64_t, PipelineMaterials> m_DrawQueue{};

    };
}


#endif //MV_ENGINE_FRAMECONTEXT_H
