//
// Created by Matty on 2022-03-10.
//

#ifndef MV_EDITOR_PROPERTYMANAGER_H
#define MV_EDITOR_PROPERTYMANAGER_H

#include "IRenderedProperty.h"
#include "PropertyStorage.h"

#ifdef GetClassName
    #undef GetClassName
#endif

namespace MV{

    class PropertyManager {
    public:
        PropertyManager() = default;

        PropertyManager(const PropertyManager &other) noexcept = delete;
        PropertyManager& operator=( const PropertyManager & ) noexcept = delete;

        PropertyManager(PropertyManager &&other) noexcept;
        PropertyManager& operator=(PropertyManager && other ) noexcept ;

        ~PropertyManager();

        void Init(const eastl::function<void(uint32_t token, size_t storageIndex)> & clb = nullptr);
        void Cleanup();

        void Clear();

        template<typename T>
        PropertyStorage<T> & GetStorage();

        template<typename T>
        const PropertyStorage<T> & GetStorage() const;

        void ForEachStorage(const eastl::function<void(IPropertyStorage*)> & callback);

        size_t CreatePropertyFromToken(uint32_t token, const MV::rc<WorldObject> & wo);

        IPropertyStorage * GetIStorageFromToken(uint32_t token) const;
    private:
        MV::vector<IPropertyStorage*> m_pStorages{};
        MV::unordered_map<uint32_t, PropertyIDType> m_pTokenProperties{};

        static void registerPropertyScriptToken(MV::string_view name);

        void initPropertyToken(MV::string_view className, PropertyIDType id,const eastl::function<void(uint32_t token, size_t storageIndex)> & clb);
    };

    template<typename T>
    PropertyStorage<T> &PropertyManager::GetStorage() {
        Assert(m_pStorages[(size_t)PropertyID<T>::ID()] != nullptr, "Storage is not initialized");
        return *static_cast<PropertyStorage<T>*>(m_pStorages[(size_t)PropertyID<T>::ID()]);
    }

    template<typename T>
    const PropertyStorage<T> &PropertyManager::GetStorage() const {
        Assert(m_pStorages[(size_t)PropertyID<T>::ID()] != nullptr, "Storage is not initialized");
        return *static_cast<PropertyStorage<T>*>(m_pStorages[(size_t)PropertyID<T>::ID()]);
    }
}


#endif //MV_EDITOR_PROPERTYMANAGER_H
