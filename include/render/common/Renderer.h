//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_RENDERER_H
#define MV_ENGINE_RENDERER_H

#include <utility>
#include <functional>

#include "FrameContext.h"
#include "RendererConfigCommon.h"
#include "RenderPass.h"
#include "VertexBuffer.h"
#include "StagingBuffer.h"
#include "IndexBuffer.h"
#include "UniformBuffer.h"
#include "Texture.h"
#include "ShaderPipeline.h"

namespace MV {
    struct RendererLimits {
        bool m_AnisotropyEnabled;
        float m_MaxAnisotropy;
    };

    class Renderer {
    public:
        explicit Renderer(RendererConfigCommon commonConfig) : m_pCommonConfig(MV::move(commonConfig)) {};

        virtual void Init() = 0;

        virtual FrameContext PrepareRender() = 0;

        virtual void Render(FrameContext & context) = 0;

        virtual void Present(FrameContext & context) = 0;

        virtual void Cleanup(const std::function<bool()> & moduleCleanupCallback) = 0;

        virtual MV::rc<RenderPass> GetMainRenderPass() = 0;
        virtual MV::rc<RenderPass> GetEditorRenderPass() = 0;

        virtual MV::rc<VertexBuffer> CreateVertexBuffer(size_t size, size_t stride) = 0;

        virtual MV::rc<StagingBuffer> CreateStagingBuffer(size_t size) = 0;

        virtual MV::rc<IndexBuffer> CreateIndexBuffer(size_t size) = 0;

        virtual MV::rc<UniformBuffer> CreateUniformBuffer(size_t size) = 0;

        virtual MV::rc<Texture> CreateTexture(TextureBlueprint && bp) = 0;

        virtual MV::rc<ShaderPipeline> CreateShaderPipeline(PipelineBlueprint && bp) = 0;

        virtual MV::rc<Shader> CreateShader(MV::string path, ShaderType type) = 0;

        virtual void CollectGarbage() = 0;

        virtual void BindMainRenderPassEditor(FrameContext & context) = 0;

        [[nodiscard]]
        virtual RendererLimits GetLimits() const = 0;

        void RegisterRenderingResource(uint64_t id, rc<RenderingResource> rr) {
            rr->m_pId = id;
            m_pLoadedResources[id] = MV::move(rr);
        }

        bool GetRenderingResource(uint64_t id, rc<RenderingResource> & res) {
            const auto & found = m_pLoadedResources.find(id);
            if(found == m_pLoadedResources.end()) return false;
            res = found->second;
            return true;
        }

        void SetDrawWireframe(bool value) { m_pDrawWireframe = value;}

        [[nodiscard]]
        bool GetDrawsWireframe() const { return m_pDrawWireframe;};

        virtual void ReloadShaders() {};
    protected:
        RendererConfigCommon m_pCommonConfig;

        MV::unordered_map<uint64_t, rc<RenderingResource>> m_pLoadedResources{};

        bool m_pDrawWireframe = false;
    private:

    };
}


#endif //MV_ENGINE_RENDERER_H
