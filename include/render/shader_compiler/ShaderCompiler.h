//
// Created by Matty on 2022-03-13.
//

#ifndef MV_EDITOR_SHADERCOMPILER_H
#define MV_EDITOR_SHADERCOMPILER_H

#include "../common/RendererBackend.h"
#include "../common/Shader.h"
#include <json.hpp>

namespace MV {
    namespace Config {
        static constexpr string_view sc_ShaderCacheFolder = ".shadercache";
        static constexpr string_view sc_EngineShaderSource = "assets/shaders";

        static constexpr string_view sc_FragmentShaderExtension = ".frag";
        static constexpr string_view sc_VertexShaderExtension = ".vert";
    }

    struct ShaderBuildResult {
        MV::string m_Error{};
        MV::string m_VertData{};
        MV::string m_FragData{};
    };

    struct ShaderCompilationResult {
        MV::string m_Error{};
    };

    struct SpirVCompilationResult {
        MV::string m_Error{};
        MV::vector<uint32_t> m_SpriVResult{};
    };

    class ShaderCompiler {
    public:
        void CompileBuildInShaders();

        void RecompileBuildInShaders();

        void SetRendererBackend(MV::RendererBackend backend);
        /// Since we are caching if result is valid, we cant be const here
        bool IsShaderCompiled(string_view path, uint64_t id);

        ShaderCompilationResult CompileShader(uint64_t id,const nlohmann::json & shaderData);

        ShaderCompilationResult CompileEngineShader(string_view name,const nlohmann::json & shaderData);

        static string GetShaderPath(ShaderType type, string_view id, uint32_t variant);
        static string GetShaderPath(ShaderType type, uint64_t id, uint32_t variant);
    private:
        ShaderCompilationResult compileShaderImpl(string_view idName,const nlohmann::json & shaderData);
        MV::unordered_set<uint64_t> m_pCompiledShaders{};
        RendererBackend m_pBackend;
        void preprocessShader(ShaderType type);
        MV::vector<uint32_t> readShader(ShaderType type, string_view idName);
        ShaderBuildResult buildShaderCode(const nlohmann::json & shaderData);
        SpirVCompilationResult compileToSpirV(string_view shaderCode, bool optimize,ShaderType type, string_view id, const nlohmann::json & macroDefinitions);
        SpirVCompilationResult buildAndCompileShaderCode(const nlohmann::json & shaderData, ShaderType type, string_view idName);
        SpirVCompilationResult readOrCompileShader(const nlohmann::json & shaderData, ShaderType type, string_view idName, uint32_t variant);
        void saveShader(string_view name, ShaderType type, const MV::vector<uint32_t> & data,uint32_t variant);
    };
}


#endif //MV_EDITOR_SHADERCOMPILER_H
