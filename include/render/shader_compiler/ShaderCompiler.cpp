//
// Created by Matty on 2022-03-13.
//

#include "ShaderCompiler.h"
#include "../../common/EngineCommon.h"
#include "../../util/UtilShader.h"
#include <filesystem>
#include <shaderc/shaderc.hpp>
#include <fstream>

namespace MV {
    bool ShaderCompiler::IsShaderCompiled(string_view path, uint64_t id) {
        return false;
    }

    void ShaderCompiler::SetRendererBackend(RendererBackend backend) {
        m_pBackend = backend;
    }

    static shaderc_shader_kind shaderTypeToShaderCKind(ShaderType type) {
        switch (type) {
            case ShaderType::VERTEX:
                return shaderc_vertex_shader;
            case ShaderType::FRAGMENT:
                return shaderc_fragment_shader;
            case ShaderType::GEOMETRY:
                return shaderc_geometry_shader;
            case ShaderType::TESSELATION:
                return shaderc_tess_control_shader;
            case ShaderType::COMPUTE:
                return shaderc_compute_shader;
            case ShaderType::SIZE:
                return shaderc_glsl_infer_from_source;
        }
        return shaderc_glsl_infer_from_source;
    }

    static string_view shaderTypeToSuffix(ShaderType type) {
        switch (type) {
            case ShaderType::VERTEX:
                return "vertex";
            case ShaderType::FRAGMENT:
                return "fragment";
            case ShaderType::GEOMETRY:
                return "geometry";
            case ShaderType::TESSELATION:
                return "tesselation";
            case ShaderType::COMPUTE:
                return "compute";
            case ShaderType::SIZE:
                return "WTF";
        }
        return "WTF";
    }

    string ShaderCompiler::GetShaderPath(ShaderType type, string_view id, uint32_t variant) {
        return fmt::format("{}/{}_{}_{}.shadercache", Config::sc_ShaderCacheFolder, id, shaderTypeToSuffix(type),
                           variant).c_str();
    }

    ShaderCompilationResult ShaderCompiler::CompileShader(uint64_t id, const nlohmann::json &shaderData) {
        return compileShaderImpl(fmt::format("{}", id).c_str(), shaderData);
    }


    void ShaderCompiler::preprocessShader(ShaderType type) {
        shaderc::Compiler compiler;
        shaderc::CompileOptions options;

        const auto kind = shaderTypeToShaderCKind(type);
    }

    ShaderBuildResult ShaderCompiler::buildShaderCode(const json &shaderData) {
        if (!shaderData.contains("fragment"))
            return {"Missing fragment field!", {}, {}};
        if (!shaderData.contains("vertex"))
            return {"Missing vertex field!", {}, {}};
        return {"", shaderData["vertex"].get<MV::string>(), shaderData["fragment"].get<MV::string>()};
    }

    MV::vector<uint32_t> ShaderCompiler::readShader(ShaderType type, string_view id) {
        std::ifstream ifs{GetShaderPath(type, id,0).data(), std::ios::binary};
        ifs.unsetf(std::ios::skipws);

        if (!ifs.is_open()) {
            return {};
        }

        ifs.seekg(0, std::ios::end);
        const auto fileSize = ifs.tellg();
        ifs.seekg(0, std::ios::beg);

        std::vector<uint8_t> vec;
        vec.reserve(fileSize);
        vec.insert(vec.begin(),
                   std::istream_iterator<uint8_t>(ifs),
                   std::istream_iterator<uint8_t>());

        MV::vector<uint32_t> vecRes;
        vecRes.resize(fileSize / 4);
        std::memcpy(vecRes.data(), vec.data(), vec.size());
        ifs.close();

        return vecRes;
    }

    SpirVCompilationResult
    ShaderCompiler::compileToSpirV(string_view shaderCode, bool optimize, ShaderType type, string_view id,
                                   const nlohmann::json &macroDefinitions) {
        shaderc::Compiler compiler;
        shaderc::CompileOptions options;

        if (optimize) options.SetOptimizationLevel(shaderc_optimization_level_performance);

        for (auto &&field: macroDefinitions.items()) {
            if (field.value().empty()) {
                options.AddMacroDefinition(field.key());
            } else {
                options.AddMacroDefinition(field.key(), field.value().get<MV::string>().c_str());
            }
        }

        shaderc::SpvCompilationResult result = compiler.CompileGlslToSpv(
                shaderCode.data(), shaderTypeToShaderCKind(type), "shader", options);

        if (result.GetCompilationStatus() != shaderc_compilation_status_success) {
            return {result.GetErrorMessage().c_str(), {}};
        }


        return {"", {result.cbegin(), result.cend()}};
    }

    SpirVCompilationResult
    ShaderCompiler::buildAndCompileShaderCode(const json &shaderData, ShaderType type, string_view id) {
        const auto buildRes = buildShaderCode(shaderData);

        if (!buildRes.m_Error.empty())
            return {buildRes.m_Error, {}};

        SpirVCompilationResult cmpRes;

        nlohmann::json macroDefinitions = shaderData.contains("macroDefinitions") ? shaderData.at("macroDefinitions")
                                                                                  : nlohmann::json{};

        if (type == ShaderType::VERTEX) {
            cmpRes = compileToSpirV(buildRes.m_VertData, !MV::GetEngine()->IsEditor(), ShaderType::VERTEX, id,
                                    macroDefinitions);
        } else {
            cmpRes = compileToSpirV(buildRes.m_FragData, !MV::GetEngine()->IsEditor(), ShaderType::FRAGMENT, id,
                                    macroDefinitions);
        }
        if (!cmpRes.m_Error.empty())
            return {cmpRes.m_Error, {}};


        return cmpRes;
    }

    SpirVCompilationResult
    ShaderCompiler::readOrCompileShader(const json &shaderData, ShaderType type, string_view id, uint32_t variant) {
        const auto path = GetShaderPath(type, id, variant);
        MV::vector<uint32_t> result;

        const auto pth = std::filesystem::path(path.data());

        if (!std::filesystem::exists(pth)) {
            auto cmpRes = buildAndCompileShaderCode(shaderData, type, id);
            if (!cmpRes.m_Error.empty()) {
                return {cmpRes.m_Error, {}};
            }
            result = MV::move(cmpRes.m_SpriVResult);
            saveShader(fmt::format("{}", id).c_str(), type, result, variant);
        } else {
            MV::GetLogger()->LogInfo("Found cached shader: {}", path);
            //result = readShader(type, id);
            /*
            if(result.empty()) {
                auto cmpRes = buildAndCompileShaderCode(shaderData, type, id);
                if(!cmpRes.m_Error.empty()) {
                    return {cmpRes.m_Error, {}};
                }
                result = MV::move(cmpRes.m_SpriVResult);
                saveShader(fmt::format("{}", id).c_str(), type, result);
            }*/
        }

        return {"", result};
    }

    ShaderCompilationResult ShaderCompiler::CompileEngineShader(string_view name, const json &shaderData) {
        return compileShaderImpl(name, shaderData);
    }

    void
    ShaderCompiler::saveShader(string_view name, ShaderType type, const MV::vector<uint32_t> &data, uint32_t variant) {
        const auto pth = std::filesystem::path(
                fmt::format("{}/{}_{}_{}.shadercache", Config::sc_ShaderCacheFolder, name, shaderTypeToSuffix(type),
                            variant));
        std::filesystem::create_directories(pth.parent_path());
        std::ofstream ofs{pth, std::ios::binary};

        if (!ofs.is_open())
            return;

        ofs.write((char *) data.data(), data.size() * 4);

        ofs.close();
    }

    ShaderCompilationResult ShaderCompiler::compileShaderImpl(string_view idName, const json &shaderData) {
        auto vertRes = readOrCompileShader(shaderData, ShaderType::VERTEX, idName, 0);

        if (!vertRes.m_Error.empty())
            return {vertRes.m_Error};

        uint32_t variantCount = 1;
        try{
            const auto &uniformsJson = shaderData.at("uniforms");

            for (auto &&uniform: uniformsJson) {
                if (uniform.at("type").get<uint32_t>() == static_cast<uint32_t>(UniformDescriptorType::TEXTURE)) {
                    try {
                        if (uniform.at("nullable").get<bool>()) {
                            variantCount++;
                        }
                    } catch (std::exception &e) {

                    }
                }
            }

            MV::Assert(variantCount <= 32);
        } catch (nlohmann::json::exception & e) {
            MV::GetLogger()->LogWarning("Missing field from shader json. This is probably bug. err_id:{}\n\t{}", e.id,e.what());
        }

        for (uint32_t i = 0; i < variantCount; i++) {
            auto fragRes = readOrCompileShader(shaderData, ShaderType::FRAGMENT, idName, i);
            if (!fragRes.m_Error.empty())
                return {fragRes.m_Error};
        }


        return {""};
    }

    void ShaderCompiler::CompileBuildInShaders() {
        const auto path = fmt::format("{}", Config::sc_EngineShaderSource);
        const auto stdPath = std::filesystem::path(path);

        // pair -> first is if vertex was found, second is if fragment was found
        MV::unordered_map<string, eastl::pair<bool, bool>> foundShaders;

        for (auto const &entry: std::filesystem::directory_iterator{stdPath}) {
            const auto fPath = entry.path();
            auto fileName = MV::string(entry.path().stem().string().c_str());
            auto extension = MV::string(entry.path().extension().string().c_str());

            if (extension == Config::sc_VertexShaderExtension) {
                auto &v = foundShaders[fileName];
                v.first = true;
            }

            if (extension == Config::sc_FragmentShaderExtension) {
                auto &v = foundShaders[fileName];
                v.second = true;
            }
        }

        for (auto &&v: foundShaders) {
            if (v.second.first && v.second.second) {
                auto fragPath = fmt::format("{}/{}.frag", Config::sc_EngineShaderSource, v.first);
                auto vertPath = fmt::format("{}/{}.vert", Config::sc_EngineShaderSource, v.first);
                auto fragSource = UtilShader::ReadShaderSourceFromFile(fragPath.c_str());
                auto vertSource = UtilShader::ReadShaderSourceFromFile(vertPath.c_str());

                nlohmann::json shaderJson{};

                shaderJson["vertex"] = vertSource;
                shaderJson["fragment"] = fragSource;

                const auto res = CompileEngineShader(v.first, shaderJson);
                if (!res.m_Error.empty())
                    MV::GetLogger()->LogError("Failed to compile engine building shader {} with error:\n\t {}", v.first,
                                              res.m_Error);
            }
        }
    }

    string ShaderCompiler::GetShaderPath(ShaderType type, uint64_t id, uint32_t variant) {
        return GetShaderPath(type, fmt::format("{}", id).c_str(),variant);
    }

    void ShaderCompiler::RecompileBuildInShaders() {
        const auto path = fmt::format("{}", Config::sc_ShaderCacheFolder);
        const auto stdPath = std::filesystem::path(path);

        std::filesystem::remove_all(stdPath);

        CompileBuildInShaders();
    }
}