//
// Created by Matty on 2021-10-14.
//

#ifndef MV_ENGINE_MAINPIPELINE_H
#define MV_ENGINE_MAINPIPELINE_H

#ifdef VK_BACKEND_SUPPORT

#include "../VulkanPipeline.h"
#include "../VulkanSwapChain.h"

namespace MV {
    class MainPipeline {
    public:
        rc<VulkanPipeline> m_Pipeline;
        PipelineBlueprint GetBlueprint();
    };
}

#endif

#endif //MV_ENGINE_MAINPIPELINE_H
