//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_VULKANSTAGINGBUFFER_H
#define MV_ENGINE_VULKANSTAGINGBUFFER_H

#include "../common/StagingBuffer.h"
#include "VulkanBufferImpl.h"

namespace MV {
class VulkanStagingBuffer : public StagingBuffer, public VulkanBufferImpl {
public:
    explicit VulkanStagingBuffer(size_t size);

    void Init() override;

    void Cleanup() override;

    void Upload(const void *data, size_t size) override;

    VkBuffer GetBuffer();
};
}


#endif //MV_ENGINE_VULKANSTAGINGBUFFER_H
