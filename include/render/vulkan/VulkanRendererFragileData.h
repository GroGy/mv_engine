//
// Created by Matty on 2021-10-03.
//

#ifndef MV_ENGINE_VULKANRENDERERFRAGILEDATA_H
#define MV_ENGINE_VULKANRENDERERFRAGILEDATA_H

#ifdef VK_BACKEND_SUPPORT

#include <vk_mem_alloc.h>
#include "VulkanRendererData.h"
#include "../common/Window.h"
#include "VulkanSwapChain.h"
#include "renderpasses/MainRenderPass.h"
#include "pipelines/MainPipeline.h"
#include "VulkanFrameBuffer.h"
#include "renderpasses/EditorRenderPass.h"

namespace MV {
    class VulkanRendererFragileData {
    public:
        void Init(VulkanRendererData & vulkanRendererData, const std::unique_ptr<Window> & window);
        void Rebuild(VulkanRendererData & vulkanRendererData, const std::unique_ptr<Window> & window);
        void Cleanup(VulkanRendererData & vulkanRendererData);

        void ForceUnloadShaders(VulkanRendererData & vulkanRendererData);

        VulkanSwapChain m_SwapChain{};
        MV::vector<VkCommandBuffer> m_CommandBuffers{};

        rc<MainRenderPass> m_MainRenderPass;
        rc<EditorRenderPass> m_EditorRenderPass;

        MV::vector<VkFence> m_ImagesInFlight;

        MV::vector<rc<VulkanPipeline>> m_Pipelines{};
        MV::vector<rc<VulkanRenderPass>> m_RenderPasses{};
    private:
        void initSwapChain(VulkanRendererData & vulkanRendererData, const std::unique_ptr<Window> & window);
        void cleanupSwapChain(VulkanRendererData & vulkanRendererData);

        void initCommandPool(VulkanRendererData & vulkanRendererdata);

        void initRenderPasses(VulkanRendererData & vulkanRendererData);
        void cleanupRenderPasses(VulkanRendererData &vulkanRendererData);

        void initPipelines();
        void cleanupPipelines();

        void initCommandBuffers(VulkanRendererData &vulkanRendererData);
        void cleanupCommandBuffers(VulkanRendererData &vulkanRendererData);

        void initFenceSlots(VulkanRendererData &vulkanRendererData);
    };
}

#endif

#endif //MV_ENGINE_VULKANRENDERERFRAGILEDATA_H
