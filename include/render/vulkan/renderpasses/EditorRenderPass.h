//
// Created by Matty on 2021-10-19.
//

#ifndef MV_ENGINE_EDITORRENDERPASS_H
#define MV_ENGINE_EDITORRENDERPASS_H


static const int EDITOR_VIEWPORT_HEIGHT = 768;
static const int EDITOR_VIEWPORT_WIDTH = 1366;
#ifdef VK_BACKEND_SUPPORT

#include "../VulkanRenderPass.h"
#include "../VulkanSwapChain.h"
#include "../VulkanTexture.h"

namespace MV {
    class EditorRenderPass : public VulkanRenderPass {
    private:
        uint32_t m_pSwapImageCount = 0;
    public:
        MV::vector<MV::rc<VulkanTexture>> m_Textures{};

        EditorRenderPass() : VulkanRenderPass({0.8f,0.8f,0.8f,1.0f}) {};
        explicit EditorRenderPass(VulkanSwapChain & swapChain);

        void OnPreInit() override;

        void OnCleanup() override;
    };
}

#endif

#endif //MV_ENGINE_EDITORRENDERPASS_H
