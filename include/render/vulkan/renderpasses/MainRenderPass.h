//
// Created by Matty on 2021-10-05.
//

#ifndef MV_ENGINE_MAINRENDERPASS_H
#define MV_ENGINE_MAINRENDERPASS_H


#ifdef VK_BACKEND_SUPPORT

#include "../VulkanRenderPass.h"
#include "../VulkanSwapChain.h"

namespace MV {
    class MainRenderPass : public VulkanRenderPass {
    public:
        MainRenderPass() : VulkanRenderPass({0.2f,0.2f,0.2f,1.0f}) {};
        explicit MainRenderPass(VulkanSwapChain & swapChain);
    };
}

#endif

#endif //MV_ENGINE_MAINRENDERPASS_H
