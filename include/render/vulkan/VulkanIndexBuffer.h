//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_VULKANINDEXBUFFER_H
#define MV_ENGINE_VULKANINDEXBUFFER_H

#include "VulkanBufferImpl.h"
#include "../common/IndexBuffer.h"

namespace MV {
    class VulkanIndexBuffer : public IndexBuffer, public VulkanBufferImpl {
    public:
        explicit VulkanIndexBuffer(size_t size);

        void Bind(FrameContext &context) override;

        void Upload(const MV::rc<StagingBuffer> &staging, size_t dataSize) override;

        void Init() override;

        void Cleanup() override;
    };
}


#endif //MV_ENGINE_VULKANINDEXBUFFER_H
