//
// Created by Matty on 2021-10-03.
//

#ifndef MV_ENGINE_VULKANRENDERERCONFIG_H
#define MV_ENGINE_VULKANRENDERERCONFIG_H

#ifdef VK_BACKEND_SUPPORT
namespace MV {
    /// Stores config used to setup vulkan renderer
    class VulkanRendererConfig {
    public:
        bool m_DebugValidationLayers = true;
    };
}
#endif

#endif //MV_ENGINE_VULKANRENDERERCONFIG_H
