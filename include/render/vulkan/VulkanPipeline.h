//
// Created by Matty on 2021-10-05.
//

#ifndef MV_ENGINE_VULKANPIPELINE_H
#define MV_ENGINE_VULKANPIPELINE_H

#ifdef VK_BACKEND_SUPPORT

#include <vk_mem_alloc.h>
#include "VulkanRendererData.h"
#include "../common/ShaderPipeline.h"
#include "VulkanShader.h"

namespace MV {
    class VulkanPipeline : public ShaderPipeline {
    public:
        VulkanPipeline(PipelineBlueprint && blueprint);

        void Init() override;

        void Cleanup() override;

        void Bind(FrameContext &context) override;

        void BindWireframe(FrameContext &context) override;

        void BindPushConstant(FrameContext &context, PushConstantDescription description, const void * data, size_t size) override;

        void PreBindUniform(FrameContext &context, UniformDescription &description, const void *data,
                            size_t size, bool update) override;

        void PreBindUniform(FrameContext &context, UniformDescription &description,
                            const MV::rc<Texture> &texture, bool update) override;

        void BindUniforms(FrameContext &context,
                          const vector<eastl::reference_wrapper<UniformDescription>> &uniforms) override;

        void ResetBatchCounters();

        UniformDescription & GetBatchUniform(MV::string_view name);

        PushConstantDescription GetMVPPushConstant();
    protected:
        void buildPipeline() override;
    private:
        MV::unordered_map<VkDescriptorSetLayout, eastl::pair<uint32_t, MV::vector<VkDescriptorSet>>> m_pDescriptorSets;
        VkPipeline m_pPipeline = VK_NULL_HANDLE;
        VkPipeline m_pWireframePipeline = VK_NULL_HANDLE;
        VkPipelineLayout m_pLayout = VK_NULL_HANDLE;
        MV::unordered_map<MV::string,VkDescriptorSetLayout> m_pUniformDescriptorSetLayouts{};
        VkDescriptorPool m_pDescriptorPool = VK_NULL_HANDLE;
        bool m_pHasUniforms = false;

        MV::unordered_map<MV::string,eastl::pair<uint32_t,MV::vector<UniformDescription>>> m_pBatchUniforms{};

        void buildShaders();

        void updateDescriptor(FrameContext &context, UniformDescription &description);
    };
}

#endif

#endif //MV_ENGINE_VULKANPIPELINE_H
