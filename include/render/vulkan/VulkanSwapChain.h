//
// Created by Matty on 2021-10-04.
//

#ifndef MV_ENGINE_VULKANSWAPCHAIN_H
#define MV_ENGINE_VULKANSWAPCHAIN_H


#ifdef VK_BACKEND_SUPPORT

#include "VulkanRendererData.h"
#include "../common/Window.h"
#include <vk_mem_alloc.h>

namespace MV {
    class VulkanSwapChain {
    private:
        void initImageViews(VulkanRendererData & vulkanRendererData);
        void cleanupImageViews(VulkanRendererData &vulkanRendererData);
        void createImage(VulkanRendererData &vulkanRendererData, size_t i);
    public:
        void Init(VulkanRendererData &rendererData, const std::unique_ptr<Window> & window);
        void Cleanup(VulkanRendererData &rendererData);

        VkSwapchainKHR m_SwapChain = VK_NULL_HANDLE;
        VkFormat m_SwapChainImageFormat;
        VkExtent2D m_SwapChainExtent;
        MV::vector<VkImage> m_SwapChainImages{};
        MV::vector<VkImageView> m_ImageViews{};
    };
}

#endif

#endif //MV_ENGINE_VULKANSWAPCHAIN_H
