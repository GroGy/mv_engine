//
// Created by Matty on 2021-10-18.
//

#ifndef MV_ENGINE_VULKANUNIFORMBUFFER_H
#define MV_ENGINE_VULKANUNIFORMBUFFER_H


#include "../common/UniformBuffer.h"
#include "VulkanBufferImpl.h"

namespace MV {
    class VulkanUniformBuffer : public UniformBuffer, public VulkanBufferImpl {
    public:
        explicit VulkanUniformBuffer(size_t size);

        void Init() override;

        void Cleanup() override;

        void Bind(FrameContext &context) override;

        void Upload(const void *data, size_t size) override;

        void Refresh();

        size_t GetSize() const { return m_pSize; };
    };
}


#endif //MV_ENGINE_VULKANUNIFORMBUFFER_H
