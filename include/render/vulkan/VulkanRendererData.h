//
// Created by Matty on 2021-10-03.
//

#ifndef MV_ENGINE_VULKANRENDERERDATA_H
#define MV_ENGINE_VULKANRENDERERDATA_H

#ifdef VK_BACKEND_SUPPORT

#include <vk_mem_alloc.h>
#include "VulkanSamplerInfo.h"

namespace MV {
    struct VulkanRendererData {
        VkInstance m_Instance = VK_NULL_HANDLE;
        VkPhysicalDevice m_PhysicalDevice = VK_NULL_HANDLE;
        VkDevice m_Device = VK_NULL_HANDLE;
        VmaAllocator m_Allocator = VK_NULL_HANDLE;
        VkDebugUtilsMessengerEXT m_DebugMessenger = VK_NULL_HANDLE;

        VkQueue m_GraphicsQueue = VK_NULL_HANDLE;
        VkQueue m_PresentQueue = VK_NULL_HANDLE;

        VkPresentModeKHR m_SurfacePresentMode{};
        VkSurfaceFormatKHR m_SurfaceFormat{};
        VkExtent2D m_SurfaceExtent{};
        VkSurfaceKHR m_Surface = VK_NULL_HANDLE;

        VkCommandPool m_CommandPool = VK_NULL_HANDLE;
        VkCommandPool m_UploadCommandPool = VK_NULL_HANDLE;

        MV::vector<VkSemaphore> m_ImageAvailableSemaphores;
        MV::vector<VkSemaphore> m_RenderFinishedSemaphores;

        MV::vector<VkFence> m_InFlightFences;

        MV::unordered_map<VulkanSamplerInfo, VkSampler> m_TextureSamplers{};
    };
}

#endif

#endif //MV_ENGINE_VULKANRENDERERDATA_H
