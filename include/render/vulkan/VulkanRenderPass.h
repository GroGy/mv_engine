//
// Created by Matty on 2021-10-05.
//

#ifndef MV_ENGINE_VULKANRENDERPASS_H
#define MV_ENGINE_VULKANRENDERPASS_H

#ifdef VK_BACKEND_SUPPORT

#include <vk_mem_alloc.h>
#include "VulkanRendererData.h"
#include "../common/RenderPass.h"
#include "VulkanFrameBuffer.h"
#include "VulkanTextureImpl.h"

namespace MV {
    struct VulkanAttachmentBlueprint {
        bool m_Valid = false;
        VkFormat m_Format{};
        uint32_t m_SampleCount = 1;
        VkImageLayout m_Layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        VkAttachmentLoadOp m_LoadOP = VK_ATTACHMENT_LOAD_OP_CLEAR;
        VkAttachmentStoreOp m_StoreOP = VK_ATTACHMENT_STORE_OP_STORE;
        VkAttachmentLoadOp m_StencilLoadOP = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        VkAttachmentStoreOp m_StencilStoreOP = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        VkImageLayout m_InitialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        VkImageLayout m_FinalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        explicit VulkanAttachmentBlueprint(VkFormat format);

        VulkanAttachmentBlueprint() = default;
    };

    class VulkanSubPassBlueprint {
    private:
        bool m_pValid = false;
        MV::vector<VulkanAttachmentBlueprint> m_pAttachments;
        VulkanAttachmentBlueprint m_pDepthStencilAttachment;

        VkSubpassDescription buildSubPass();

    public:
        VkPipelineBindPoint m_BindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

        friend class VulkanRenderPassBlueprint;

        VulkanSubPassBlueprint() = default;

        explicit VulkanSubPassBlueprint(VulkanAttachmentBlueprint attachment);

        void SetDepthAttachment(VulkanAttachmentBlueprint depthAttachment);

        void AddAttachment(VulkanAttachmentBlueprint attachment);
    };

    struct RenderPassBlueprintResult {
        VkRenderPass m_RenderPass;
        MV::vector<VulkanFrameBuffer> m_FrameBuffers{};
    };

    class VulkanRenderPassBlueprint {
    public:
        VulkanRenderPassBlueprint() = default;
        VulkanRenderPassBlueprint(uint32_t swapImageCount, uint32_t width, uint32_t height,
                                  MV::vector<VkImageView> renderImageViews);

        static VkSampleCountFlagBits GetSamples(uint32_t sampleCount);

        friend class VulkanRenderPass;
        friend class VulkanSwapChain;
        friend class VulkanRendererFragileData;

        void AddSubPass(VulkanSubPassBlueprint pass);

        void SetResultImageViews(MV::vector<VkImageView> imageViews);

        void AddSubPassDep(VkSubpassDependency deps);

        void SetSize(uint32_t width, uint32_t height) { m_pWidth = width; m_pHeight = height;}
    private:
        uint32_t m_pSwapImageCount = 0;
        uint32_t m_pWidth = 0;
        uint32_t m_pHeight = 0;
        bool m_pValid = false;

        MV::vector<VkSubpassDependency> m_pSubpassDeps{};

        MV::vector<VkImageView> m_pRenderImageViews{};

        void initFrameBuffers(VulkanRendererData &vulkanRendererData, RenderPassBlueprintResult &res);

        MV::vector<VulkanSubPassBlueprint> m_pSubPasses;

        RenderPassBlueprintResult buildRenderPass(VulkanRendererData &rendererData);
    };

    class VulkanRenderPass : public RenderPass {
    public:
        explicit VulkanRenderPass(const glm::vec4 &clearColor) : RenderPass(clearColor) {};

        explicit VulkanRenderPass(const glm::vec4 &clearColor, VulkanRenderPassBlueprint blueprint);

        void SetBlueprint(VulkanRenderPassBlueprint blueprint);

        VkRenderPass m_RenderPass = VK_NULL_HANDLE;

        MV::vector<VulkanTextureImpl> m_Attachments{};

        void Init() override;

        void Cleanup() override;

        void Bind(FrameContext &context) override;

        MV::vector<VulkanFrameBuffer> m_FrameBuffers{};

        friend class VulkanSwapChain;
        friend class VulkanRendererFragileData;

        [[nodiscard]] VkExtent2D GetSize() const { return {m_pBlueprint.m_pWidth, m_pBlueprint.m_pHeight}; }
    private:
        bool m_pValid = false;
    protected:
        VulkanRenderPassBlueprint m_pBlueprint;
        virtual void OnPreInit() {}
        virtual void OnCleanup() {};
    };
}

#endif

#endif //MV_ENGINE_VULKANRENDERPASS_H
