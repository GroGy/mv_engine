//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_VULKANVERTEXBUFFER_H
#define MV_ENGINE_VULKANVERTEXBUFFER_H

#include "../common/VertexBuffer.h"
#include "VulkanBufferImpl.h"
#include "VulkanStagingBuffer.h"

namespace MV {
    class VulkanVertexBuffer : public VertexBuffer, public VulkanBufferImpl {
    private:
        size_t m_pFilled = 0;
    public:
        explicit VulkanVertexBuffer(size_t size,size_t vertexStride);

        void Init() override;

        void Cleanup() override;

        void Bind(FrameContext &context, uint32_t binding = 0) override;

        void Upload(const MV::rc<StagingBuffer> &staging, size_t dataSize) override;

        size_t GetVertexCount() const override;
    };
}


#endif //MV_ENGINE_VULKANVERTEXBUFFER_H
