//
// Created by Matty on 2021-10-03.
//

#ifndef MV_ENGINE_VULKANSWAPCHAINSUPPORTDETAILS_H
#define MV_ENGINE_VULKANSWAPCHAINSUPPORTDETAILS_H


#ifdef VK_BACKEND_SUPPORT

#include <vulkan/vulkan.h>
#include "../common/Window.h"

namespace MV {
    class VulkanSwapChainSupportDetails {
    public:
        VkSurfaceCapabilitiesKHR m_Capabilities;
        MV::vector<VkSurfaceFormatKHR> m_Formats;
        MV::vector<VkPresentModeKHR> m_PresentModes;

        VkSurfaceFormatKHR ChooseSwapSurfaceFormat();
        VkPresentModeKHR ChooseSwapPresentMode();
        VkExtent2D ChooseSwapExtent(const std::unique_ptr<MV::Window> & window);

        static VulkanSwapChainSupportDetails GetSwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface);
    };
}
#endif


#endif //MV_ENGINE_VULKANSWAPCHAINSUPPORTDETAILS_H
