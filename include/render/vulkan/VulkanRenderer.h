//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_VULKANRENDERER_H
#define MV_ENGINE_VULKANRENDERER_H

#ifdef VK_BACKEND_SUPPORT

#include "../common/Renderer.h"

#include "VulkanRendererConfig.h"
#include "VulkanRendererData.h"
#include "VulkanRendererFragileData.h"
#include "VulkanQueueFamilies.h"
#include "VulkanSwapChainSupportDetails.h"
#include "VulkanRendererFragileData.h"
#include "VulkanImGuiInitInfo.h"
#include "VulkanActiveBufferStorage.h"

namespace MV {
    class VulkanRenderer : public MV::Renderer {
        static constexpr uint32_t FRAMES_IN_FLIGHT = 4;
    public:
        VulkanRenderer(RendererConfigCommon commonConfig,VulkanRendererConfig config);

        MV::ImGui_ImplVulkan_InitInfo CreateImguiInfo();

        friend class VulkanShader;
        friend class VulkanPipeline;
        friend class VulkanPipelineBlueprint;
        friend class VulkanFrameBuffer;
        friend class VulkanFrameBufferBlueprint;
        friend class VulkanRenderPass;
        friend class VulkanBufferImpl;
        friend class VulkanTextureImpl;
        friend class VulkanSwapChain;

        void Init() override;

        FrameContext PrepareRender() override;

        void Render(FrameContext & context) override;

        void Present(FrameContext & context) override;

        void Cleanup(const std::function<bool()> & moduleCleanupCallback) override;

        void ExecuteSingleCommand(const std::function<void(VkCommandBuffer)> & fn);

        void ReloadShaders() override;

        MV::rc<VertexBuffer> CreateVertexBuffer(size_t size, size_t vertexStride) override;

        MV::rc<StagingBuffer> CreateStagingBuffer(size_t size) override;

        MV::rc<IndexBuffer> CreateIndexBuffer(size_t size) override;

        MV::rc<UniformBuffer> CreateUniformBuffer(size_t size) override;

        MV::rc<ShaderPipeline> CreateShaderPipeline(PipelineBlueprint &&bp) override;

        MV::rc<Texture> CreateTexture(TextureBlueprint &&bp) override;

        MV::rc<Shader> CreateShader(MV::string path, ShaderType type) override;

        MV::rc<RenderPass> GetMainRenderPass() override;

        MV::rc<RenderPass> GetEditorRenderPass() override;

        void CollectGarbage() override;

        void BindMainRenderPassEditor(FrameContext & context) override;

        [[nodiscard]]
        RendererLimits GetLimits() const override;

        VulkanRendererData m_pData{};
    private:
        RendererLimits m_pLimits{};

        VulkanActiveBufferStorage m_pBufferStorage{};
        MV::vector<MV::rc<StagingBuffer>> m_pReusableStagingBuffers{};

        size_t      m_pCurrentFence = 0;
        uint32_t    m_pCurrentImageIndex = 0;

        VulkanRendererFragileData m_pFragileData{};

        VkDescriptorPool m_pImguiDescriptorPool = VK_NULL_HANDLE;

        const MV::vector<const char*> m_pValidationLayers = {
                "VK_LAYER_KHRONOS_validation"
        };

        const MV::vector<const char*> m_pRequiredExtensions = {
                VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };

        VulkanRendererConfig m_pConfig;

        /// Initializes base vulkan app instance
        void initBaseInstance();

        /// Initializes debug modules only when VulkanRendererData::m_DebugValidationLayers is true
        void initDebuggingModules();
        VkResult initDebugMessenger(const VkDebugUtilsMessengerCreateInfoEXT* createInfo);
        void cleanupDebugMessenger();

        void initSurface();

        void initPhysicalDevice();

        bool isGPUSuitable(VkPhysicalDevice device);
        bool hasGPUExtensionSupport(VkPhysicalDevice device);

        void initLogicalDevice();

        void initAllocator();

        void initSyncElements();

        void rebuildFragileData();

        void initLimits();

        VkSampler getOrCreateSampler(VulkanSamplerInfo info);

        MV::vector<const char *> getRequiredExtensions();

        uint32_t m_pBatchingVertexBuffersUsed = 0;
        MV::vector<MV::rc<VertexBuffer>> m_pBatchingVertexBuffers{};
        MV::vector<MV::rc<VertexBuffer>> m_pFreeBatchingVertexBuffers{};
        uint32_t m_pBatchingIndexBuffersUsed = 0;
        MV::vector<MV::rc<IndexBuffer>> m_pBatchingIndexBuffers{};
        MV::vector<MV::rc<IndexBuffer>> m_pFreeBatchingIndexBuffers{};
        uint32_t m_pBatchUniformBuffersUsed = 0;
        MV::vector<MV::rc<UniformBuffer>> m_pBatchingUniformBuffers{};

        void resetBatchBuffers();

        MV::rc<VertexBuffer> getBatchVertexBuffer(size_t size, size_t vertexStride);
        MV::rc<IndexBuffer> getBatchIndexBuffer(size_t size);
        MV::rc<UniformBuffer> getBatchUniformBuffer(size_t size);

        void renderCommandFromQueue(FrameContext &context, const pair<const uint64_t, PipelineMaterials> &command,
                                 const rc <RenderingResource> &pipelineResource);

        void renderSubcommand(FrameContext &context, eastl::shared_ptr<VulkanPipeline> &pipeline,
                              const pair<const MaterialInput, MaterialDrawData> &subCommand);
    };
}

#endif

#endif //MV_ENGINE_VULKANRENDERER_H
