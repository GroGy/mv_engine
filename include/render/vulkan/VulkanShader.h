//
// Created by Matty on 2021-10-05.
//

#ifndef MV_ENGINE_VULKANSHADER_H
#define MV_ENGINE_VULKANSHADER_H

#ifdef VK_BACKEND_SUPPORT

#include "VulkanRendererData.h"
#include "../common/Shader.h"
#include <vk_mem_alloc.h>

namespace MV {
    class VulkanShader : public Shader {
    public:
        VulkanShader(MV::string path, ShaderType type);
        void Init() override;
        void Cleanup() override;

        VkShaderModule m_Module = VK_NULL_HANDLE;
    };
}

#endif


#endif //MV_ENGINE_VULKANSHADER_H
