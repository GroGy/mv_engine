//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_VULKANSAMPLERINFO_H
#define MV_ENGINE_VULKANSAMPLERINFO_H

#include "../common/TextureFilter.h"
#include "../common/TextureAddressingMode.h"
#include <EngineMinimal.h>

namespace MV {
    struct VulkanSamplerInfo {
        TextureFilter m_MinFilter = TextureFilter::LINEAR;
        TextureFilter m_MagFilter = TextureFilter::LINEAR;
        TextureAddressingMode m_AddressingMode = TextureAddressingMode::REPEAT;
        bool m_EnableAnisotropy = false;
        float m_MaxAnisotropy = 0;
        // Percentage close filtering
        //compareEnable = VK_FALSE;
        //compareOp = VK_COMPARE_OP_ALWAYS;

        // Mipmaps
        //mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        //mipLodBias = 0.0f;
        //minLod = 0.0f;
        //maxLod = 0.0f;
        VulkanSamplerInfo() = default;

        bool operator==(const VulkanSamplerInfo &rhs) const {
            return m_MinFilter == rhs.m_MinFilter &&
                   m_MagFilter == rhs.m_MagFilter &&
                   m_AddressingMode == rhs.m_AddressingMode &&
                   m_EnableAnisotropy == rhs.m_EnableAnisotropy &&
                   m_MaxAnisotropy == rhs.m_MaxAnisotropy;
        }

        bool operator!=(const VulkanSamplerInfo &rhs) const {
            return !(rhs == *this);
        }


    };
}



namespace eastl { // NOLINT(cert-dcl58-cpp)
    template <>
    struct hash<MV::VulkanSamplerInfo>
    {
        std::size_t operator()(const MV::VulkanSamplerInfo& k) const
        {
            using std::hash;

            std::size_t seed = 0x31535439;

            seed ^= (seed << 6) + (seed >> 2) + 0x3449770D + hash<uint32_t>()(static_cast<uint32_t>(k.m_MinFilter));
            seed ^= (seed << 6) + (seed >> 2) + 0x49751560 + hash<uint32_t>()(static_cast<uint32_t>(k.m_MagFilter));
            seed ^= (seed << 6) + (seed >> 2) + 0x3449770D + hash<uint32_t>()(static_cast<uint32_t>(k.m_AddressingMode));
            seed ^= (seed << 6) + (seed >> 2) + 0x15215661 + hash<float>()(k.m_MaxAnisotropy);
            seed ^= (seed << 6) + (seed >> 2) + 0x654860D1 + hash<bool>()(k.m_EnableAnisotropy);

            return seed;
        }
    };
}

#endif //MV_ENGINE_VULKANSAMPLERINFO_H
