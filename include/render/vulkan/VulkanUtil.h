//
// Created by Matty on 2021-10-03.
//

#ifndef MV_ENGINE_VULKANUTIL_H
#define MV_ENGINE_VULKANUTIL_H

#ifdef VK_BACKEND_SUPPORT

#include <EngineMinimal.h>
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

#define VKCheck(fn) VulkanUtil::CheckVkResult(fn);

namespace MV {
    class VulkanUtil {
    public:
        static void CheckVkResult(VkResult res);
        static void LoadDataToAllocation(VmaAllocator allocator, VmaAllocation allocation, VmaAllocationInfo allocInfo, const void* data, size_t dataSize);
    private:
        static MV::string resToString(VkResult res);
    };
}

#endif

#endif //MV_ENGINE_VULKANUTIL_H
