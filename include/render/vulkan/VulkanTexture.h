//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_VULKANTEXTURE_H
#define MV_ENGINE_VULKANTEXTURE_H

#include "../common/Texture.h"
#include "VulkanTextureImpl.h"

namespace MV {
    class VulkanTexture : public Texture, public VulkanTextureImpl {
    public:
        VulkanTexture(TextureBlueprint &&blueprint);

        void Upload(const MV::rc<StagingBuffer> &staging) override;

        void Init() override;

        void Cleanup() override;

        void TransitionToLayout(VkImageLayout layout);
    };
}


#endif //MV_ENGINE_VULKANTEXTURE_H
