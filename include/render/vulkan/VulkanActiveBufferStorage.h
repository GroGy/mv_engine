//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_VULKANACTIVEBUFFERSTORAGE_H
#define MV_ENGINE_VULKANACTIVEBUFFERSTORAGE_H

#include "VulkanBufferImpl.h"
#include "VulkanTextureImpl.h"

namespace MV {
    class VulkanActiveBufferStorage {
    private:
        MV::vector<MV::rc<VulkanBufferImpl>> m_pTrackedBuffers{};
        MV::vector<MV::rc<VulkanTextureImpl>> m_pTrackedTextures{};
        void clearMarkedBuffers();
        void clearMarkedTextures();
    public:
        VulkanActiveBufferStorage();
        void TrackVulkanBuffer(const MV::rc<VulkanBufferImpl> & buffer);
        void TrackVulkanTexture(const MV::rc<VulkanTextureImpl> & texture);

        void ClearMarked();
        void ClearEverything();
    };
}


#endif //MV_ENGINE_VULKANACTIVEBUFFERSTORAGE_H
