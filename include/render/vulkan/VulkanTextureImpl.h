//
// Created by Matty on 2021-10-17.
//

#ifndef MV_ENGINE_VULKANTEXTUREIMPL_H
#define MV_ENGINE_VULKANTEXTUREIMPL_H

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include "VulkanBufferImpl.h"
#include "VulkanSamplerInfo.h"

namespace MV {
    class VulkanTextureImpl {
    public:
        uint32_t m_TextureWidth, m_TextureHeight;
        VkImage m_Texture = VK_NULL_HANDLE;
        VkImageView m_TextureView = VK_NULL_HANDLE;
        VkFormat m_pFormat = VK_FORMAT_UNDEFINED;
        VkImageLayout m_TextureLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        VkSampler GetSampler();
        friend class VulkanActiveBufferStorage;
    private:
        void deallocate();
        bool m_pMarkedForDealloc = false;
        [[nodiscard]]
        bool shouldDealloc() const { return m_pMarkedForDealloc; }
    protected:
        void transitionImageLayout(VkImageLayout oldLayout, VkImageLayout newLayout);
        bool m_pHasData = false;
        VulkanTextureImpl(uint32_t width, uint32_t height);
        void allocate(VkImageCreateInfo imageInfo,VmaAllocationCreateInfo allocInfo,VulkanSamplerInfo samplerInfo);
        void loadData(const void *data, size_t size);
        void copyFrom(VulkanBufferImpl & other);
        VmaAllocation m_pAllocation = VK_NULL_HANDLE;
        VmaAllocationInfo m_pAllocInfo{};
        VulkanSamplerInfo m_pSamplerInfo{};
        void markForDealloc() {m_pMarkedForDealloc = true;};
        size_t m_pDataSize = 0;
    };
}


#endif //MV_ENGINE_VULKANTEXTUREIMPL_H
