//
// Created by Matty on 2021-10-14.
//

#ifndef MV_ENGINE_VULKANFRAMEBUFFER_H
#define MV_ENGINE_VULKANFRAMEBUFFER_H

#ifdef VK_BACKEND_SUPPORT

#include <vulkan/vulkan.h>
#include "../common/FrameBuffer.h"

namespace MV {
    class VulkanColorAttachment : public ColorAttachment {
    public:
        VulkanColorAttachment(uint32_t id, VkImageView imageView);
        VkImageView m_ImageView = VK_NULL_HANDLE;
    };

    struct VulkanFrameBufferBlueprintResult {
        VkFramebuffer m_FrameBuffer;
    };

    class VulkanFrameBufferBlueprint {
    public:
        VulkanFrameBufferBlueprint() = default;

        VulkanFrameBufferBlueprint(const VkRenderPass &renderPass, uint32_t width, uint32_t height,
                             MV::vector<VulkanColorAttachment> attachments, uint32_t layers = 1);

        friend class VulkanFrameBuffer;

        [[nodiscard]]
        bool IsValid() const;
    private:
        bool m_pValid = false;
        uint32_t m_pWidth = 0;
        uint32_t m_pHeight = 0;
        uint32_t m_pLayers = 0;
        MV::vector<VkImageView> m_pAttachments{};
        VkRenderPass m_pRenderPass = VK_NULL_HANDLE;

        VulkanFrameBufferBlueprintResult buildFrameBuffer();
    };

    class VulkanFrameBuffer : public FrameBuffer {
    private:
        VulkanFrameBufferBlueprint m_pBlueprint{};
    public:
        VkFramebuffer m_FrameBuffer = VK_NULL_HANDLE;
        VulkanFrameBuffer() = default;
        explicit VulkanFrameBuffer(VulkanFrameBufferBlueprint && blueprint);

        void Init() override;

        void Cleanup() override;
    };
}

#endif

#endif //MV_ENGINE_VULKANFRAMEBUFFER_H
