//
// Created by Matty on 2021-10-03.
//

#ifndef MV_ENGINE_VULKANQUEUEFAMILIES_H
#define MV_ENGINE_VULKANQUEUEFAMILIES_H

#ifdef VK_BACKEND_SUPPORT

#include <vulkan/vulkan.h>
#include <optional>

namespace MV {
struct QueueFamilies {
public:
    std::optional<uint32_t> m_GraphicsFamily{};
    std::optional<uint32_t> m_PresentFamily{};

    bool IsComplete() {
        return m_GraphicsFamily.has_value() && m_PresentFamily.has_value();
    }

    static QueueFamilies FindSupportedQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR surface);
};
}

#endif

#endif //MV_ENGINE_VULKANQUEUEFAMILIES_H
