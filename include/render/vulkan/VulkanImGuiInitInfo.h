//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_VULKANIMGUIINITINFO_H
#define MV_ENGINE_VULKANIMGUIINITINFO_H

#ifdef VK_BACKEND_SUPPORT

#include <vulkan/vulkan.h>

namespace MV {
    struct ImGui_ImplVulkan_InitInfo {
        VkInstance Instance;
        VkPhysicalDevice PhysicalDevice;
        VkDevice Device;
        uint32_t QueueFamily;
        VkQueue Queue;
        VkPipelineCache PipelineCache;
        VkDescriptorPool DescriptorPool;
        uint32_t Subpass;
        uint32_t MinImageCount;          // >= 2
        uint32_t ImageCount;             // >= MinImageCount
        VkSampleCountFlagBits MSAASamples;            // >= VK_SAMPLE_COUNT_1_BIT
        const VkAllocationCallbacks *Allocator;

        void (*CheckVkResultFn)(VkResult err);
    };
}

#endif

#endif //MV_ENGINE_VULKANIMGUIINITINFO_H
