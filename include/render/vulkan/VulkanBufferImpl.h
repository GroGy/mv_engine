//
// Created by Matty on 2021-10-15.
//

#ifndef MV_ENGINE_VULKANBUFFERIMPL_H
#define MV_ENGINE_VULKANBUFFERIMPL_H

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

namespace MV {
    class VulkanBufferImpl {
    private:
        void deallocate();
        bool m_pMarkedForDealloc = false;
        [[nodiscard]]
        bool shouldDealloc() const { return m_pMarkedForDealloc; }
    protected:
        void allocate(VkBufferCreateInfo bufferInfo, VmaAllocationCreateInfo allocInfo);
        void loadData(const void *data, size_t size);
        void copyTo(VulkanBufferImpl & other);
        void copyFrom(VulkanBufferImpl & other, size_t size = 0);
        VmaAllocation m_pAllocation = VK_NULL_HANDLE;
        VmaAllocationInfo m_pAllocInfo{};
        void markForDealloc() {m_pMarkedForDealloc = true;};
        size_t m_pDataSize = 0;
    public:
        friend class VulkanActiveBufferStorage;
        VkBuffer m_Buffer = VK_NULL_HANDLE;
    };
}


#endif //MV_ENGINE_VULKANBUFFERIMPL_H
