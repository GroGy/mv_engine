//
// Created by Matty on 2021-10-01.
//

#ifndef MV_ENGINE_ENGINE_H
#define MV_ENGINE_ENGINE_H

#include <memory>
#include <vector>
#include "scripting/ScriptManager.h"
#include "resources/LoadedResources.h"
#include "common/Buildin.h"
#include "common/World.h"
#include "common/EngineSettings.h"
#include "module/Module.h"
#include "common/EngineConfig.h"
#include "render/common/Renderer.h"
#include "log/Logger.h"
#include "render/common/Window.h"
#include "input/InputCommon.h"
#include "resources/common/ResourceLoader.h"
#include "common/Project.h"
#include "allocator/DefaultAllocator.h"
#include "render/shader_compiler/ShaderCompiler.h"
#include "async/ThreadPool.h"
#include "scripting/module/game_instance/GameInstance.h"

namespace MV {
    const uint32_t ENGINE_VERSION = 1;
    static const float MAX_DELTA = 0.05f;
    static const int TICK_RATE = 32;
    static const int GC_TICK_FREQ = 512;

    class Engine {
    private:
        EngineConfig m_pConfig;

        double m_pLastFrame = 0.0;

        double m_pStartFrameTime = 0.0;

        bool m_pGameRunning = true;

        bool m_pForceStopped = false;

        void init();
        void mainLoop();
        void render();
        void cleanup();
        void cleanupResourceLoaders();
        void finishResources();
        void cleanupWorld();
        void cleanupScriptManager();

        void processSyncCallbacks();

        void update();
        void fixedUpdate(float delta);

        void limitFrames();

        void initRenderer();
        void initLogger();
        void initWindow();
        void initInput();
        void initResourceLoaders();
        void compileEngineShaders();
        void initScriptManager();
        void initShaderCompiler();
        void createEnginePipelines();
        void initThreadPool();

        void onScriptsReload();

        MV::vector<MV::rc<MV::Module>> m_pModules{};

        ResourceQueue m_pResourceQueue{};
        ResourceQueue m_pFinishedResourceQueue{};
        MV::vector<ResourceLoader> m_pResourceLoaders{};
        bool m_pRun = true;
        bool m_pShowCollision = false;
        Project m_pProject;
        World m_pWorld;
        GameInstance m_pGameInstance;

        std::thread::id m_pMainThreadID;

        MV::thread_safe_queue<std::function<void()>> m_pSyncCallbacks;
    public:
        [[nodiscard]] bool OnMainThread() const;

        [[nodiscard]] bool IsGameRunning() const;
        [[nodiscard]] bool IsForceStopped() const;
        [[nodiscard]] bool IsEditor() const;

        [[nodiscard]] bool ShowCollisions() const;

        Project & GetProject() { return m_pProject;};
        World & GetWorld() { return m_pWorld;};

        explicit Engine(EngineConfig config);
        ~Engine();
        void RegisterModule(const MV::rc<MV::Module> & module);
        void Run();
        void Stop();

        double GetTime();

        [[nodiscard]] uint32_t GetVersion() const;

        void LoadProject(MV::string path);

        [[nodiscard]]
        RendererBackend GetRenderingBackend() const { return m_pConfig.m_RenderBackend; }

        void LoadResource(MV::rc<Resource> resource);

        MV::rc<Resource> LoadResourceSync(uint64_t id);

        MV::rc<Resource> LoadResource(uint64_t id);

        void AddSyncCallback(const std::function<void()> & callback);

        void UnloadAsset(uint64_t id);

        bool IsAssetLoaded(uint64_t id) const;

        void StartGame();

        void StopGame();

        void ForceStopGame();

        void ClearForceStop();

        void SetShowCollision(bool val);

        void ReloadShaders();

        MV::rc<Renderer> m_Renderer;
        std::unique_ptr<Window> m_Window;
        Logger m_Logger{};
        InputCommon m_Input{};
        EngineSettings m_Settings{};
        Buildin m_Buildin{};
        LoadedResources m_LoadedResources{};
        ScriptManager m_ScriptManager{};
        ShaderCompiler m_ShaderCompiler{};
        MV::rc<ThreadPool> m_MainThreadPool;
    };
}

#endif //MV_ENGINE_ENGINE_H
