﻿using System;

namespace MV
{
    public partial class GameInstance
    {
        public virtual void OnBegin() { }
        public virtual void OnEnd() { }

        static GameInstance GetGameInstance() {
            return GetGameInstance_Native();
        }
    }
}
