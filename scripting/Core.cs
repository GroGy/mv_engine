using System;
using System.Runtime.CompilerServices;
using System.Collections;
namespace MV
{
    [System.AttributeUsage(System.AttributeTargets.Field |
                           System.AttributeTargets.Property)
    ]
    public class ShowInEditor : System.Attribute
    {

    }

    public partial class SpawnWorldObject<T> where T : WorldObject {
        public T Spawn(MV.Vector2 Position) {
            return (T)Spawn_Internal(Position);
        }
    }
}