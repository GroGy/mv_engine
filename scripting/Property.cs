using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Property
    {
        public virtual void OnRender() {
            
        }

        /// Update called as much as possible
        public virtual void Update(float deltaTime) {}

        /// Update called 32 times a second
        public virtual void FixedUpdate(float deltaTime) { }

        /// Called on start of lifetime
        public virtual void OnStart() { }

        /// Called on end of lifetime
        public virtual void OnEnd() { }
    }
}