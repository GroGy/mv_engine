using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class WorldObject
    {
        private UInt64 _id;

        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
                SetPosition_Native(_id, position);
            }
        }
        private float rotation;
        public float Rotation
        {
            get { return rotation; }
            set
            {
                rotation = value;
                SetRotation_Native(_id, rotation);
            }
        }
        private Vector2 scale;
        public Vector2 Scale
        {
            get { return scale; }
            set
            {
                scale = value;
                SetScale_Native(_id, scale);
            }
        }

        

        /// Update called as much as possible
        public virtual void Update(float deltaTime) {}

        /// Update called 32 times a second
        public virtual void FixedUpdate(float deltaTime) { }

        /// Called on start of lifetime
        public virtual void OnStart() { }

        /// Called on end of lifetime
        public virtual void OnEnd() { }

        /// This is called only from editor, you can use Gizmo class to draw here
        public virtual void OnGizmoDraw() {}
    }
}