﻿using System;

namespace MV
{
    public partial class ParticleSystem : MV.Property
    {
        public void Burst() {
            Burst_Native(_owner, this);
        }
    }
}
