﻿using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class ParticleSystem : MV.Property
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void Burst_Native(WorldObject Owner, ParticleSystem Self);
    }
}
