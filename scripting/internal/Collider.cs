﻿using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Collider : MV.Property
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void SetScale_Native(WorldObject Owner, Collider Self, MV.Vector2 Scale);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void SetOffset_Native(WorldObject Owner, Collider Self, MV.Vector2 Offset);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void SetRotation_Native(WorldObject Owner, Collider Self, float Rotation);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void ApplyForceToCenter_Native(WorldObject Owner, Collider Collider, Vector2 Force);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void ApplyForce_Native(WorldObject Owner, Collider Collider, Vector2 Force, Vector2 Point);
    }
}
