using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Input
    {
        // Keyboard
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsKeyDown_Native(KeyboardKey key);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsKeyUp_Native(KeyboardKey key);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsKeyPressed_Native(KeyboardKey key);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsKeyReleased_Native(KeyboardKey key);

        // Gamepad
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsGamepadKeyDown_Native(int controllerId, GamepadKey key);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsGamepadKeyUp_Native(int controllerId, GamepadKey key);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsGamepadKeyPressed_Native(int controllerId, GamepadKey key);
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsGamepadKeyReleased_Native(int controllerId, GamepadKey key);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern float GetGamepadTrigger_Native(int controllerId, GamepadTrigger trigger);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern float GetGamepadJoystickAxis_Native(int controllerId, GamepadJoystick joystick, bool yInsteadOfX);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern string GetGamepadName_Native(int controllerId);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsGamepadJoystick_Native(int controllerId);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern bool IsGamepadConnected_Native(int controllerId);
    }
}