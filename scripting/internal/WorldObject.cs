using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class WorldObject
    {
        
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        extern static void SetPosition_Native(UInt64 id,Vector2 pos);
        
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        extern static void SetRotation_Native(UInt64 id,float rotation);
        
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        extern static void SetScale_Native(UInt64 id,Vector2 scale);
    }
}