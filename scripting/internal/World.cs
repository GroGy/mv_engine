using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class World
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern WorldObject[] GetWorldObjectsByTag_Native(string tag);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern WorldObject CastRay_Native(Vector2 start, Vector2 dir, float lenght);        
    }
}