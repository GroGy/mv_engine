using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Gizmo
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern void DrawLine_Native(Vector2 a, Vector2 b, float width);
    }
}