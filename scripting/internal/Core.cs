using System;
using System.Runtime.CompilerServices;
using System.Collections;
using System.Data.SqlTypes;

namespace MV
{
    public partial class SpawnWorldObject<T> where T : WorldObject {
        private static WorldObject Spawn_Internal(MV.Vector2 Position) {
            return WorldObjectSpawnerInternal.Spawn_Native(typeof(T), Position);
        }
    }
    class WorldObjectSpawnerInternal
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        public static extern WorldObject Spawn_Native(System.Type Type, MV.Vector2 Position);
    }
}