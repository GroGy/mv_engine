using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Sprite : MV.Property
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void SetSpriteIndex_Native(WorldObject Owner, Sprite Self, UInt32 Index);

        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern void SetVisible_Native(WorldObject Owner, Sprite Self, bool Visible);        
    }
}