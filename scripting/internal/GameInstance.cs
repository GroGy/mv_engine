﻿using System;
using System.Runtime.CompilerServices;

namespace MV
{
    partial class GameInstance
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private static extern GameInstance GetGameInstance_Native();
    }
}
