using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Debug
    {
        [MethodImplAttribute(MethodImplOptions.InternalCall)]
        private extern static void Message_Native(string message, int line,string file);
    }
}