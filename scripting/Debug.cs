using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Debug
    {
        public static void Message(string message, [CallerLineNumber] int line = 0,[CallerFilePath] string file = null) {
            Message_Native(message, line, file);
        }
        
    }
}