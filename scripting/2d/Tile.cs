using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public struct Tile
    {
        private UInt32 _id;

        public UInt32 ID {
            get { return _id; }
        }
    }
}