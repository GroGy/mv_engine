using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public class Atlas
    {
        private UInt32 _spriteCount;
        public UInt32 SpriteCount
        {
            get { return _spriteCount; }
        }
    }
}