using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Sprite : MV.Property
    {
        private UVector2 _size;
        private UInt64 _shader;

        private bool _visible;
        public bool Visible
        {
            get { return _visible; }
            set
            {
                _visible = value;
                SetVisible_Native(_owner, this, _visible);
            }
        }


        private UInt32 _spriteIndex;
        public UInt32 SpriteIndex
        {
            get { return _spriteIndex; }
            set
            {
                _spriteIndex = value;
                SetSpriteIndex_Native(_owner, this, _spriteIndex);
            }
        }

        private Atlas _atlas;
        public Atlas Atlas {
            get { return _atlas; }
        }

        public bool IsUsingAtlas() {
            return _atlas != null;
        }


    }
}