﻿using System;

namespace MV
{
    public partial class Collider : MV.Property
    {
        private Vector2 _scale;
        public Vector2 Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                SetScale_Native(_owner, this, _scale);
            }
        }

        private Vector2 _offset;
        public Vector2 Offset
        {
            get { return _offset; }
            set
            {
                _offset = value;
                SetOffset_Native(_owner, this, _offset);
            }
        }

        private float _rotation;
        public float Rotation
        {
            get { return _rotation; }
            set
            {
                _rotation = value;
                SetRotation_Native(_owner, this, _rotation);
            }
        }


        private bool _collisionEnabled;
        private UInt16 _collisionMask;
        private UInt16 _collisionGroup;

        private Vector2 _velocity;
        public Vector2 Velocity
        {
            get { return _velocity; }
        }
        public void ApplyForce(Vector2 Velocity, Vector2 Point)
        {
            ApplyForce_Native(_owner, this, Velocity, Point);
        }

        public void ApplyForceToCenter(Vector2 Velocity)
        {
            ApplyForceToCenter_Native(_owner, this, Velocity);
        }
    }
}
