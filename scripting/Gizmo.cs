using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class Gizmo
    {
        public static void DrawLine(Vector2 a, Vector2 b, float width) {
            DrawLine_Native(a,b,width);
        }
    }
}