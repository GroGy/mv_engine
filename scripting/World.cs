using System;
using System.Runtime.CompilerServices;

namespace MV
{
    public partial class World
    {
        public static WorldObject[] GetWorldObjectsByTag(string tag)
        {
            return GetWorldObjectsByTag_Native(tag);
        }

        public static RaycastResult CastRay(Vector2 start, Vector2 dir, float lenght) {
            RaycastResult result = new RaycastResult();

            result.Hit = CastRay_Native(start, dir, lenght);

            return result;
        }
    }
}