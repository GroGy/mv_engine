using System;
using System.Runtime.InteropServices;

namespace MV
{
    [StructLayout(LayoutKind.Explicit)]
    public struct UVector2
    {
        [FieldOffset(0)] public UInt32 X;
        [FieldOffset(4)] public UInt32 Y;
        public UVector2(UInt32 scalar)
        {
            X = Y = scalar;
        }

        public UVector2(UInt32 x, UInt32 y)
        {
            X = x;
            Y = y;
        }
    }
}