using System;
using System.Runtime.InteropServices;

namespace MV
{
    [StructLayout(LayoutKind.Explicit)]
    public struct Vector2
    {
        [FieldOffset(0)] public float X;
        [FieldOffset(4)] public float Y;
        public Vector2(float scalar)
        {
            X = Y = scalar;
        }

        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static Vector2 operator+(Vector2 a, Vector2 b) {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }
        public static Vector2 operator-(Vector2 a, Vector2 b) {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        public static Vector2 operator*(Vector2 a, Vector2 b) {
            return new Vector2(a.X * b.X, a.Y * b.Y);
        }

        public static Vector2 operator*(Vector2 a, float b) {
            return new Vector2(a.X * b, a.Y * b);
        }
    }
}