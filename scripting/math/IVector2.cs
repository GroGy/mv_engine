using System;
using System.Runtime.InteropServices;

namespace MV
{
    [StructLayout(LayoutKind.Explicit)]
    public struct IVector2
    {
        [FieldOffset(0)] public Int32 X;
        [FieldOffset(4)] public Int32 Y;
        public IVector2(Int32 scalar)
        {
            X = Y = scalar;
        }

        public IVector2(Int32 x, Int32 y)
        {
            X = x;
            Y = y;
        }
    }
}