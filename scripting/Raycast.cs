﻿using System;

namespace MV
{
    public struct RaycastResult
    {
        public bool IsHit() { return Hit != null; }

        public WorldObject Hit;
        public Vector2 Position;
        public Vector2 Normal;
    }
}
