cmake_minimum_required(VERSION 3.20)
project(mv_engine)

add_subdirectory(CMakeConfig)
add_subdirectory(CMakePrecompiler)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fuse-ld=lld")
endif()

if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    add_definitions("-DNOMINMAX=1")
endif()

option(VK_BACKEND "Enables vulkan support in engine" YES)
option(BUILD_TESTS "Builds tests for engine" NO)

if (VK_BACKEND)
    add_definitions(-DVK_BACKEND_SUPPORT)
endif (VK_BACKEND)

if (BUILD_TESTS)
    add_subdirectory(tests)
endif ()

add_definitions(-DEASTL_EASTDC_VSNPRINTF=0)
add_definitions(-D_NO_DEBUG_HEAP=1)


set(MV_ENGINE_HEADERS
        ${CMAKE_CURRENT_SOURCE_DIR}/include/Engine.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/module/Module.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/Renderer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/RendererBackend.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/common/EngineConfig.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/common/EngineCommon.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/FrameContext.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanRenderer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/Window.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/input/InputCommon.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/log/Logger.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/common/ExceptionCommon.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanRendererConfig.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/RendererConfigCommon.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanUtil.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanRendererFragileData.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanRendererData.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanQueueFamilies.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanSwapChainSupportDetails.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanSwapChain.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanRenderPass.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/renderpasses/MainRenderPass.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/FrameBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanFrameBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/RenderPass.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanImGuiInitInfo.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/resources/common/Resource.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/resources/common/ResourceLoader.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/resources/TextureResource.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/resources/ModelResource.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/Tile.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/common/World.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/ui/UI.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/Buffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/VertexBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/IndexBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/StagingBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanVertexBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanPipeline.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/Shader.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/ShaderPipeline.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/util/FileUtil.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanShader.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/AttributeFormat.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/Vertex.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/pipelines/MainPipeline.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanTextureImpl.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanSamplerInfo.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/TextureFilter.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/TextureAddressingMode.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/ShaderStage.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/UniformDescription.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/UniformBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanUniformBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/resources/common/Asset.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/resources/TextureData.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/renderpasses/EditorRenderPass.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/TileSet.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/resources/ProjectResource.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/util/thread_safe_queue.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/input/Key.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/TilemapPipeline.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/TileVertex.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanActiveBufferStorage.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/TilemapLayer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanBufferImpl.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanStagingBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/Tilemap.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/Texture.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanIndexBuffer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/TextureFormat.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/vulkan/VulkanTexture.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/common/Project.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/common/EngineSettings.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/PhysicsDebugPipeline.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/PhysicsDebugDrawer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/PhysicsDebugVertex.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/CameraData.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/2d/TextureAtlas.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/common/Types.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/render/common/RenderingResource.h
        ${CMAKE_CURRENT_SOURCE_DIR}/include/internal/backward.hpp
        )

set(MV_ENGINE_SOURCE
        src/util/FuzzyMatch.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Engine.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Module.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/EngineCommon.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/FrameContext.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanRenderer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Window.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/ExceptionCommon.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Logger.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/InputCommon.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanRendererConfig.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/RendererConfigCommon.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanUtil.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanRendererFragileData.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanSwapChainSupportDetails.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanAllocator.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanSwapChain.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanQueueFamilies.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanRenderPass.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/MainRenderPass.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanPipeline.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Shader.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/ShaderPipeline.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanShader.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/FileUtil.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Vertex.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/MainPipeline.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/FrameBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanFrameBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/RenderPass.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Resource.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/ResourceLoader.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/TextureResource.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/ModelResource.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/2d/Tile.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/World.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/UI.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Buffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/VertexBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/IndexBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/StagingBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanVertexBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/2d/Tilemap.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanStagingBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanBufferImpl.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/2d/TilemapLayer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanActiveBufferStorage.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/2d/TileVertex.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/2d/TilemapPipeline.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanIndexBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Texture.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanTexture.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanTextureImpl.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/UniformBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/VulkanUniformBuffer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Asset.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/vk/EditorRenderPass.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/2d/TileSet.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/ProjectResource.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/thread_safe_queue.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/PhysicsDebugPipeline.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/PhysicsDebugDrawer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/PhysicsDebugVertex.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/TextureAtlas.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/backward.cpp

        src/AtlasResource.cpp
        include/resources/AtlasResource.h
        include/resources/AtlasData.h
        src/WorldResource.cpp
        include/resources/WorldResource.h
        src/TilesetResource.cpp
        include/resources/TilesetResource.h
        src/TilesetData.cpp
        include/resources/TilesetData.h
        src/Scriptable.cpp
        include/common/Scriptable.h
        src/WorldData.cpp
        include/resources/WorldData.h
        src/Camera.cpp
        include/common/camera/Camera.h
        src/Buildin.cpp
        include/common/Buildin.h
        src/LoadedResources.cpp
        include/resources/LoadedResources.h
        src/Atlas.cpp include/common/Atlas.h
        src/Math.cpp include/util/Math.h
        src/WorldObject.cpp
        include/common/WorldObject.h
        src/ScriptManager.cpp
        include/scripting/ScriptManager.h
        src/Script.cpp
        include/scripting/Script.h src/ScriptClass.cpp
        include/scripting/ScriptClass.h src/ScriptObjectInstance.cpp
        include/scripting/ScriptObjectInstance.h
        include/resources/WorldObjectData.h
        src/ScriptingWorldObjectModule.cpp
        include/scripting/module/ScriptingWorldObjectModule.h
        src/ScriptingModule.cpp
        include/scripting/ScriptingModule.h
        src/ScriptConfigResource.cpp
        include/resources/ScriptConfigResource.h
        src/ScriptableConfig.cpp
        include/common/ScriptableConfig.h
        src/Property.cpp
        include/common/Property.h
        src/SpriteProperty.cpp
        include/2d/SpriteProperty.h
        include/async/ThreadPool.h
        include/platform/Platform.h
        src/ThreadPool.cpp
        src/Platform.cpp
        include/profiler/Profiler.h
        src/IRenderedProperty.cpp
        include/render/common/IRenderedProperty.h
        src/Container.cpp
        include/util/Container.h
        include/util/String.h
        src/AssetManager.cpp
        include/common/AssetManager.h
        include/common/Format.h
        src/DefaultAllocator.cpp
        src/AllocatorProvider.cpp
        include/allocator/DefaultAllocator.h
        include/allocator/AllocatorProvider.h
        src/Format.cpp include/render/common/PropertyStorage.h
        src/PropertyManager.cpp
        include/render/common/PropertyManager.h
        include/render/shader_compiler/ShaderCompiler.cpp
        include/render/shader_compiler/ShaderCompiler.h
        src/ShaderResource.cpp
        include/resources/ShaderResource.h
        include/resources/ShaderData.h include/util/UtilShader.cpp
        include/util/UtilShader.h src/BoxColliderProperty.cpp
        include/2d/BoxColliderProperty.h
        include/EngineMinimal.h
        include/ensure/Ensure.h
        src/EnsureLog.cpp
        include/ensure/EnsureLog.h
        include/common/WorldObjectSpawnParams.h
        src/ScriptingCollidersModule.cpp
        include/scripting/module/ScriptingCollidersModule.h
        src/ColliderProperty.cpp
        include/2d/ColliderProperty.h
        src/particles/ParticleSystemProperty.cpp
        include/particles/cpu/ParticleSystemProperty.h
        src/Gradient.cpp
        include/common/Gradient.h
        src/AsyncWorker.cpp
        include/async/AsyncWorker.h
        src/AsyncTask.cpp
        include/async/AsyncTask.h
        src/particles/ParticlePipeline.cpp
        include/particles/cpu/ParticlePipeline.h
        src/ScriptingSpriteModule.cpp
        include/scripting/module/ScriptingSpriteModule.h
        src/MissingTexture.cpp
        include/render/MissingTexture.h
        src/PropertyRegistrator.cpp
        include/precompiler/PropertyRegistrator.h
        src/CameraProperty.cpp
        include/2d/CameraProperty.h
        src/game_instance/GameInstance.cpp
        include/scripting/module/game_instance/GameInstance.h
        include/scripting/AssemblyEntry.h
        src/util/Script.cpp
        include/util/Script.h
        include/util/FuzzyMatch.h include/resources/TilemapData.h
        src/TilemapResource.cpp
        include/resources/TilemapResource.h
        src/ScriptingInputModule.cpp
        include/scripting/module/ScriptingInputModule.h include/input/IInputCallbackProvider.cpp include/input/IInputCallbackProvider.h include/2d/RayHitResult.h)


if (CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
    set(MV_ENGINE_INCLUDE_DIR
            ${CMAKE_CURRENT_SOURCE_DIR}/include
            )
else ()
    set(MV_ENGINE_INCLUDE_DIR
            ${CMAKE_CURRENT_SOURCE_DIR}/include PARENT_SCOPE
            )
endif ()

if(BUILD_SHARED_LIBS)
    add_definitions(-DMV_ENGINE_DLL=1)
else()
    add_definitions(-DMV_ENGINE_DLL=0)
endif()

add_library(${PROJECT_NAME} ${MV_ENGINE_HEADERS} ${MV_ENGINE_SOURCE})

set(MV_ENGINE_PRIVATE_VENDOR_INCLUDE_DIRS
        ${CMAKE_CURRENT_SOURCE_DIR}/vendor/private/spdlog/include
        ${CMAKE_CURRENT_SOURCE_DIR}/vendor/private/stb
        PARENT_SCOPE
        )

set(BUILD_SHARED_LIBS OFF CACHE BOOL "enable shared libs" FORCE)
add_subdirectory(vendor/private/glfw)
target_link_libraries(${PROJECT_NAME} PRIVATE glfw)

#set(BUILD_SHARED_LIBS ON CACHE BOOL "enable shared libs" FORCE)
#add_definitions("-DEASTL_DLL=1")
add_subdirectory(vendor/public/EASTL)
target_link_libraries(${PROJECT_NAME} PUBLIC EASTL)

add_subdirectory(vendor/public/fmt)
target_link_libraries(${PROJECT_NAME} PUBLIC fmt)

set(SPDLOG_BUILD_SHARED OFF CACHE BOOL "enable shared libs" FORCE)
add_subdirectory(vendor/private/spdlog)
target_link_libraries(${PROJECT_NAME} PRIVATE spdlog)

set(BUILD_SHARED_LIBS ON CACHE BOOL "enable shared libs" FORCE)
add_subdirectory(vendor/public/box2d)
target_link_libraries(${PROJECT_NAME} PUBLIC box2d)

add_subdirectory(vendor/public/optick)
target_link_libraries(${PROJECT_NAME} PUBLIC OptickCore)


if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    target_link_libraries(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/vendor/public/mono/windows/lib/mono-2.0-sgen.lib)
    target_link_libraries(${PROJECT_NAME} PUBLIC ws2_32 version bcrypt winmm)
else()
    set(BUILD_SHARED_LIBS OFF CACHE BOOL "enable shared libs" FORCE)
    target_link_libraries(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/vendor/public/mono/lib/libmono-2.0.a ${CMAKE_CURRENT_SOURCE_DIR}/vendor/public/mono/lib/libmono-2.0.dll.a)
    target_link_libraries(${PROJECT_NAME} PUBLIC ws2_32 version bcrypt winmm)
    target_link_libraries(${PROJECT_NAME} PUBLIC ssp)
endif()

set(BUILD_SHARED_LIBS OFF CACHE BOOL "enable shared libs" FORCE)
add_subdirectory(vendor/private/SPIRV-Tools)
target_link_libraries(${PROJECT_NAME} PRIVATE SPIRV-Tools)

set(BUILD_SHARED_LIBS OFF CACHE BOOL "enable shared libs" FORCE)
add_subdirectory(vendor/private/glslang)
target_link_libraries(${PROJECT_NAME} PRIVATE glslang)

set(BUILD_SHARED_LIBS OFF CACHE BOOL "enable shared libs" FORCE)
set(SHADERC_SKIP_TESTS ON)
add_subdirectory(vendor/private/shaderc)
target_link_libraries(${PROJECT_NAME} PRIVATE shaderc)

if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    message(STATUS "Attempting to locate Vulkan SDK using manual path......")
    set(VULKAN_SDK "C:/VulkanSDK")
    set(VULKAN_VERSION "1.3.224.1")
    set(VULKAN_PATH "${VULKAN_SDK}/${VULKAN_VERSION}")
    message(STATUS "Using manual specified path: ${VULKAN_PATH}")

    # Check if manual set path exists
    if(NOT EXISTS "${VULKAN_PATH}")
        message("Error: Unable to locate this Vulkan SDK path VULKAN_PATH: ${VULKAN_PATH}, please specify correct path.
		For more information on correct installation process, please refer to subsection 'Getting started with Lunar-G SDK'
		and 'Setting up first project with CMake' in Chapter 3, 'Shaking hands with the device' in this book 'Learning Vulkan', ISBN - 9781786469809.")
        return()
    endif()

    # vulkan-1 library for build Vulkan application.
    set(VULKAN_LIB_LIST "vulkan-1")

    # Include Vulkan header files from Vulkan SDK
    target_include_directories(${PROJECT_NAME} PUBLIC ${VULKAN_PATH}/Include)

    # Link directory for vulkan-1
    link_directories(${VULKAN_PATH}/Bin;${VULKAN_PATH}/Lib;)

    target_link_directories(${PROJECT_NAME} PUBLIC ${VULKAN_PATH}/Bin;${VULKAN_PATH}/Lib;)

    target_link_libraries(${PROJECT_NAME} PRIVATE ${VULKAN_LIB_LIST})
else()
    find_package(Vulkan REQUIRED)
    target_link_libraries(${PROJECT_NAME} PRIVATE ${Vulkan_LIBRARY})
    target_include_directories(${PROJECT_NAME} PUBLIC ${Vulkan_INCLUDE_DIRS})
endif()

add_definitions(-DSHADER_COMPILER)

target_include_directories(${PROJECT_NAME} PUBLIC ${MV_ENGINE_PRIVATE_VENDOR_INCLUDE_DIRS})
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_include_directories(${PROJECT_NAME} PUBLIC ${MV_PUBLIC_VENDOR_INCLUDE_DIRS})

if(MSVC)
    target_compile_options(${PROJECT_NAME}
            PUBLIC
            "/wd4068;" # disable "unknown pragma 'mark'" warnings
            )
endif()

add_custom_command(TARGET ${PROJECT_NAME} PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
        ${CMAKE_CURRENT_SOURCE_DIR}/scripting $<TARGET_FILE_DIR:${PROJECT_NAME}>/scripting)


set(MV_Engine_External_Includes
        ${MV_PUBLIC_VENDOR_INCLUDE_DIRS}
        ${MV_ENGINE_PRIVATE_VENDOR_INCLUDE_DIRS}
        ${VULKAN_PATH}/Include
        PARENT_SCOPE)

set(    MV_Engine_Includes
        ${CMAKE_CURRENT_SOURCE_DIR}/include
        PARENT_SCOPE
)